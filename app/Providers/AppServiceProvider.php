<?php

namespace App\Providers;
use DB; // Illuminate\Support\Facades\DB;
use File; // Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
	// Add in boot function
/*

DB::listen(function($query) {
    File::append(
        storage_path('/logs/query.log'),
        '[' . date('Y-m-d H:i:s') . ']' . PHP_EOL . $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL . PHP_EOL
    );
});

*/

    }
}