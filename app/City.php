<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $primary_key = 'id';

    protected $table = 'cities';

    protected $fillable = [
        'city_name', 'city_state', 'city_state_id'
    ];
}
