<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Recipe extends Model
{
    use HasApiTokens;
    
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'outlet_id', 'name', 'price'
    ];
    
    protected $table = 'recipes';

}
