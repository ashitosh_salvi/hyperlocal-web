<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    //
    protected $primary_key = 'id';

    protected $table = 'cart_items';

    protected $fillable = [
        'item_quantity', 'item_id', 'outlet_id', 'customer_id'
    ];
    
}
