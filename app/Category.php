<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = array();

    public function getData($priority='1,2,3')
    {
        $outlet_id = Auth::guard('outlet')->id();
        // dd( $outlet_id );
        return static::whereIn('priority',explode(",",$priority))->where('outlet_id', $outlet_id)->orderBy('created_at','desc')->get();
    }

    public function setPriority($parent_category)
    {
        $priority=\DB::select('select child_c.id,child_c.category_name as child_name ,(select (select grand_parent.category_name  from categories as grand_parent where grand_parent.id=parent_c.parent_category LIMIT 1 )as grand_parent from categories as parent_c where parent_c.id=child_c.parent_category LIMIT 1) as grand_parent_name,
           (select parent_c.category_name from categories as parent_c where parent_c.id=child_c.parent_category LIMIT 1) as parent_name
           from categories as child_c where child_c.id='.$parent_category.' limit 1');
        return $priority;
    }

    public function storeData($input)
    {
    	return static::create($input);
    }

    public function findData($id)
    {
        return static::find($id);
    }

    public function updateData($id, $input)
    {
        return static::find($id)->update($input);
    }

    public function deleteData($id)
    {
        return static::find($id)->delete();
    }
}
