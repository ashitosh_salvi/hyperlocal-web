<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Outlet extends Authenticatable
{
    use Notifiable,HasApiTokens,Notifiable;
    //
    protected $fillable = [
        'outlet_user_name', 'password', 'outlet_name','merchant_id', 'outlet_type', 'gst', 'fssai_license', 'shop_license', 'img_url', 'outlet_address', 'outlet_geoaddress', 'outlet_latitude', 'outlet_longitude', 'branch', 'flag'
    ];

    // protected $hidden = [
    //     'password',
    // ];

    public function routeNotificationForFcm(){
        return $this->app_firebase_token;
    }
}