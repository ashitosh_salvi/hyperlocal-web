<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class OrderIdMaster extends Model
{
    use HasApiTokens;
    
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id'
    ];
    
    protected $table = 'order_id_master';

}
