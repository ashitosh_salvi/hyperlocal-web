<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'customer_address','customer_id','customer_latitude','customer_longitude'
    ];
    
    protected $table = 'customer_addresses';
}
