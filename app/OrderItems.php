<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id', 'item_id', 'item_name', 'item_code', 'item_qty', 'item_price'
    ];
    
    protected $table = 'order_items';
    
}
