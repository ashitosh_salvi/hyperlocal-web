<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOutletRating extends Model
{
    //
    protected $fillable = [
        'customer_id', 'outlet_id', 'rating', 'rating_desc'
    ];
}
