<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $primary_key = 'id';

    protected $table = 'states';

    protected $fillable = [
        'state_code', 'state_name'
    ];

}
