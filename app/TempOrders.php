<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class TempOrders extends Model
{
    use HasApiTokens;
    
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id', 'promocode', 'amount'
    ];
    
    protected $table = 'temp_orders';

}
