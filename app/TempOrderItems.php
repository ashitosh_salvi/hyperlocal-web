<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOrderItems extends Model
{
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id', 'item_id', 'item_name', 'item_code', 'item_qty', 'item_price'
    ];
    
    protected $table = 'temp_order_items';
    
}
