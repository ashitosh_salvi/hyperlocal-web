<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOutletFavourite extends Model
{
    //
    protected $fillable = [
        'customer_id', 'outlet_id', 'is_favourite', 'favourite_desc'
    ];
}
