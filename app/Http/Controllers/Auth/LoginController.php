<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo() {
       
        // dd(Auth::user());
        // exit;
        // if( Auth::user()->hasRole('ROLE_SUPERADMIN') ) {
            $this->redirectTo = '/admin_dashboard';
            // return $this->redirectTo;
        // } else if ( Auth::user()->hasRole('ROLE_MERCHANT') ) {
            // $this->redirectTo = '/merchant_dashboard';
            return $this->redirectTo;
        // }

    }

    public function OutletLogout()
    {   
        if (Auth::guard('outlet')->id() != null) {
            $getLoginOutlet = \App\Outlet::where('id',Auth::guard('outlet')->id())->first();

            $outlet = \App\Outlet::where('id',Auth::guard('outlet')->id())
                        ->update(["session_id" => null]);
            \Session::getHandler()->destroy($getLoginOutlet->session_id);
            Auth::guard('outlet')->logout();
        }
        
        // $this->redirectTo = '/outlet_login';
        return redirect('/outlet_login');
        
    }

}