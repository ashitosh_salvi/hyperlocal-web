<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rider;
use App\Orders;
use App\Outlet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Validator; 
use Illuminate\Support\Facades\DB;
use Response;

class RiderController extends Controller
{
    //

    public function rider_list(){

        $list = Rider::all();

        $data = Array(
            'list' => $list
        );
        return view('rider.rider_list')->with($data);
    }

    public function get_rider(Request $request){
       
        $emp = Rider::where('id',$request->id)->first();
        
        return Array( 'rider' =>$emp);

    }

    public function create_rider(Request $request){

        $request->validate(['email' => 'required|unique:riders,email' ,'mobile' => 'required|unique:riders,mobile']);
        
        $rider = new Rider();

        $rider -> first_name = $request -> first_name;
        $rider -> last_name = $request -> last_name;
        $rider -> email = $request -> email;
        $rider -> mobile = $request -> mobile;
        $rider -> password = Hash::make( $request -> password );
        $rider -> status = 1;

        $rider -> address = $request -> address;
        $rider -> city = $request -> city;
        $rider -> state = $request -> state;
        $rider -> country = $request -> country;
        $rider -> pin_code = $request -> pin_code;

        $filename = '';
        $path = $request->file('image_url');
        if($request->file('image_url')){
            $img_temp = $request->file('image_url');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "images/".$filename;
            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                $image_path = "public/images/".$filename;
            }
            
            move_uploaded_file($img_temp,$image_path);
        }
        $rider -> image_url = $filename;
        $rider -> flag = 0;
        $addfilename = '';
        
        if($request->file('address_proof')){
            $img_temp = $request->file('address_proof');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $addfilename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            Storage::disk('public')->put($addfilename, File::get($img_temp));
            $rider -> address_proof_image_url = $addfilename;
        }

        if($request->file('photo_id_image_url')){
            $img_temp = $request->file('photo_id_image_url');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $pidfilename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            Storage::disk('public')->put($pidfilename, File::get($img_temp));
            $rider -> photo_id_image_url = $pidfilename;
        }

        if($request->file('other_image_url')){
            $img_temp = $request->file('other_image_url');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $ofilename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            Storage::disk('public')->put($ofilename, File::get($img_temp));
            $rider -> other_image_url = $ofilename;
        }

        $rider -> firebase_token = '';

        $rider->save();
        
        echo 'save';
        return;

    }

    public function edit_rider(Request $request){

        $request->validate(
            [
            'email_edit' => 'required|unique:riders,email,'. $request->rider_id_edit,
            'mobile_edit' => 'required|unique:riders,mobile,'. $request->rider_id_edit
            ]);

        $rider = Rider::find($request->rider_id_edit);

        $rider -> first_name = $request -> first_name_edit;
        $rider -> last_name = $request -> last_name_edit;
        $rider -> email = $request -> email_edit;
        $rider -> mobile = $request -> mobile_edit;

        if ( !empty($request -> password_edit) ) {
        $rider -> password = $request -> password_edit;
        }
        $rider -> address = $request -> address_edit;
        $rider -> city = $request -> city_edit;
        $rider -> state = $request -> state_edit;
        $rider -> country = $request -> country_edit;
        $rider -> pin_code = $request -> pin_code_edit;

        $filename = '';
        $path = $request->file('image_url_edit');
        if($request->file('image_url_edit')){
            $img_temp = $request->file('image_url_edit');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "images/".$filename;
            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                $image_path = "public/images/".$filename;
            }
            move_uploaded_file($img_temp,$image_path);
        }
        if ( !empty($filename) ) {
            $rider -> image_url = $filename;
        }

        $addfilename = '';
        if($request->file('image_address_url_edit')){
            $img_temp = $request->file('image_address_url_edit');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $addfilename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            Storage::disk('public')->put($addfilename, File::get($img_temp));
            $rider -> address_proof_image_url = $addfilename;
        }

        if($request->file('photo_id_image_url_edit')){
            $img_temp = $request->file('photo_id_image_url_edit');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $pidfilename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            Storage::disk('public')->put($pidfilename, File::get($img_temp));
            $rider -> photo_id_image_url = $pidfilename;
        }

        if($request->file('other_image_url_edit')){
            $img_temp = $request->file('other_image_url_edit');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $ofilename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            Storage::disk('public')->put($ofilename, File::get($img_temp));
            $rider -> other_image_url = $ofilename;
        }

        $rider->save();
        
        return;

    }

    public function get_rider_list(Request $request) {
        
        $columns = array( 
            0 =>'first_name',
            1 =>'last_name',
            2 =>'email',
            3 =>'mobile',
            4 =>'state',
            5 =>'city',
            6 =>'status',
            7 =>'action',
        );
        
        $totalData = Rider::count();            
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')))
        {            
            $Rider = Rider::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }else {
            $search = $request->input('search.value'); 

            $Rider =  Rider::where('id','LIKE',"%{$search}%")
                            ->orWhere('first_name', 'LIKE',"%{$search}%")
                            ->orWhere('last_name', 'LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%")
                            ->orWhere('state', 'LIKE',"%{$search}%")
                            ->orWhere('city', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = Rider::where('id','LIKE',"%{$search}%")
                            ->orWhere('first_name', 'LIKE',"%{$search}%")
                            ->orWhere('last_name', 'LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%")
                            ->orWhere('state', 'LIKE',"%{$search}%")
                            ->orWhere('city', 'LIKE',"%{$search}%")
                            ->count();
        }

        $data = array();
            if(!empty($Rider))
            {
                foreach ($Rider as $key=>$rider)
                {
                    
                    $nestedData['first_name'] = $rider->first_name;
                    $nestedData['last_name'] = $rider->last_name;
                    $nestedData['email'] = $rider->email;
                    $nestedData['mobile'] = $rider->mobile;
                    // $nestedData['state'] = $rider->state;
                    // $nestedData['city'] = $rider->city;
                    $nestedData['status']= '<input class="tgl tgl-ios tgl_checkbox" id="cb_'. ( $rider->id + 23 ) .'" name="rider_status" type="checkbox" data-attr="'. $rider->status .'" '. ( ( $rider->status == '1' ) ? "checked" : "" ) .' rider-id="'. $rider->id .'"><label class="tgl-btn" for="cb_'. ( $rider->id + 23 ) .'"></label>';
                    $nestedData['action'] = '<div class="action-group"><a title="View Rider" class="view btn operations" onclick="getRiderView('.$rider->id.')" data-toggle="modal" data-target="#viewRiderModal-1"> <i class="fa fa-eye"></i></a><a title="Edit Rider" class="btn operations" onclick="getRider('.$rider->id.')" data-toggle="modal" data-target="#editRiderModal-1"><i class="fa fa-edit"></i></a><!--<a title="Delete Rider" data-attr="'.$rider->id.'" onclick="delete_rider('.$rider->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a>--><a id="merchant_list'.$rider->id.'" title="Assign Merchant" class="btn operations" data-toggle="modal" onclick="getRiderMerchants('.$rider->id.', \''.$rider->first_name.' '.$rider->last_name.'\')" data-target="#getRiderMerchantsModal-1" data-array=\''.$rider->merchant_id.'\'><i class="fa fa-link"></i></a></div>';
                    $data[] = $nestedData;
                }
    
            }
            $json_data = array(
                "draw"            => intval($request->input('draw')),  
                "recordsTotal"    => intval($totalData),  
                "recordsFiltered" => intval($totalFiltered), 
                "data"            => $data   
            );

            echo json_encode($json_data);

    }

    public function update_rider_status(Request $request) {
        $rider = Rider::find($request->rider_id);
        $rider->status = $request->rider_status;
        $rider->save();
        return;
    }

    public function assign_rider_merchant(Request $request) {
        // dd( $request );
        // exit;
        $rider = Rider::find($request->rider_id);
        if ( isset($request->merchant) ) {
            $merchant_list = json_encode( $request->merchant );
            $rider->merchant_id = $merchant_list;
        } else {
            $rider->merchant_id = NULL;
        }
        
        
        $rider->save();
        return;
    }

    public function admin_rider_report(Request $request)
    {
        // $riders = Rider::where('status', 1)->get();
        return view( 'reports.admin_rider_reports');
    }

    public function get_admin_rider_report(Request $request) {

        $rider = array();
        $columns = array( 
            0 =>'first_name',
            1 =>'mobile'
        );
        $totalData = Rider::all()->count();            
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        
        if(empty($request->input('search.value')))
        {            
            $rider = Rider::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }else{
            $search = $request->input('search.value'); 

            $rider =  Rider::where('first_name','LIKE',"%{$search}%")
                                ->orWhere('last_name', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();

            $totalFiltered = Rider::where('first_name','LIKE',"%{$search}%")
                            ->orWhere('last_name', 'LIKE',"%{$search}%")
                            ->count();
        }

        if(!empty($rider))
        {   
            foreach ($rider as $key=>$single_rider)
            {
                // $orders = Orders::select('outlet_id', DB::raw('count(*) as total'))->where('rider_id',$single_rider->id)->groupBy('outlet_id')->get();
                $orders = Orders::where('rider_id',$single_rider->id)
                                ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                                ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                                ->count();
                $single_rider->order = $orders;
            }
        }
        
        $data = array();
        if(!empty($rider))
        {
            foreach ($rider as $key=>$rider)
            {
                $nestedData['rider_name'] = $rider->first_name.' '.$rider->last_name ;
                $nestedData['rider_number'] = $rider->mobile;
                $nestedData['orders'] = '<a href="javascript:void(0);" onclick="viewRiderDetails('.$rider->id.',\''.$nestedData['rider_name'].'\')">'.$rider->order.'</a>';
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);

    }

    public function get_admin_rider_details_report(Request $request,$rider_id = null)
    {
        $orders = array();
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $totalData = Orders::select(\DB::raw('outlet_id, count(outlet_id) as count,sum(final_amount) as total'))
                            ->where('rider_id',$rider_id)
                            ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                            ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                            ->groupBy('outlet_id')
                            ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        
        if(empty($request->input('search.value')))
        {            
            $orders = Orders::select(\DB::raw('outlet_id, count(outlet_id) as count,sum(final_amount) as total'))
                        ->where('rider_id',$rider_id)
                        ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                        ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                        ->offset($start)
                        ->limit($limit)
                        ->groupBy('outlet_id')
                        ->get();
        }else{

            $search = $request->input('search.value'); 

            $orders = Orders::select(\DB::raw('outlet_id, count(outlet_id) as count,sum(final_amount) as total'))
                        ->where('rider_id',$rider_id)
                        ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                        ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                        ->groupBy('outlet_id')
                        ->offset($start)
                        ->limit($limit)
                        ->get();

            $totalFiltered = Orders::select(\DB::raw('outlet_id, count(outlet_id) as count,sum(final_amount) as total'))
                            ->where('rider_id',$rider_id)
                            ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                            ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                            ->groupBy('outlet_id')
                            ->count();
        }
        if(!empty($orders))
        {   
            foreach ($orders as $key=>$order)
            {
                $outlet = Outlet::find($order->outlet_id);
                $outlet_name = $outlet['outlet_name'];

                $order->outlet_name = $outlet_name;
            }
        }
        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key=>$order)
            {
                $nestedData['outlet_name']      = $order->outlet_name;
                $nestedData['count_of_order']   = $order->count;
                $nestedData['value_of_order']   = $order->total;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
    }

    public function getOutletname($outlet_id){
        $outlets = Outlet::where('id',$outlet_id)->first();
        return $outlets['outlet_name'];
    }

    public function export_get_admin_rider_report($from_date = null, $to_date = null,$search = null)
    {   
        $filename = 'hyperlocal_rider_history_'.$from_date.'_'.$to_date;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename.".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Rider Name','Rider Phone','Orders');
        $callback = function() use ($from_date,$to_date,$search,$columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
        
            if(empty($search))
            {            
                $rider = Rider::all();
            }
            else
            {
                $rider =  Rider::where('first_name','LIKE',"%{$search}%")
                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                        ->get();
            }

            if(!empty($rider))
            {   
                foreach ($rider as $key=>$single_rider)
                {
                    $orders = Orders::where('rider_id',$single_rider->id)
                            ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                            ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                            ->count();
                    $single_rider->order = $orders;
                }
            }
            
            
            if(!empty($rider))
            {
                foreach ($rider as $key=>$rider)
                {   
                    // $data = array();
                    $nestedData = [];
                    $nestedData['rider_name']   = $rider->first_name.' '.$rider->last_name ;
                    $nestedData['rider_number'] = $rider->mobile;
                    $nestedData['orders']       = $rider->order;
                    // $data[] = $nestedData;
                    fputcsv($file,$nestedData);
                }
            }
            fclose($file);
        };

        return Response::stream($callback, 200, $headers);
        
    }

    public function export_get_admin_rider_details_report($from_date = null, $to_date = null,$rider_id = null)
    {   
        $rider = Rider::find($rider_id);
        $filename = $rider->first_name.'_'.$rider->last_name.'_'.$from_date.'_'.$to_date;
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename.".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Outlet Name','Count of Orders','Value of Orders');
        $callback = function() use ($from_date,$to_date,$rider_id,$columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            $orders = Orders::select(\DB::raw('outlet_id, count(outlet_id) as count,sum(final_amount) as total'))
                ->where('rider_id',$rider_id)
                ->whereRaw('date(rider_assign_date) >= ?',[$from_date])
                ->whereRaw('date(rider_assign_date) <= ?',[$to_date])
                ->groupBy('outlet_id')
                ->get();
        
            if(!empty($orders))
            {   
                foreach ($orders as $key=>$order)
                {
                    $outlet = Outlet::find($order->outlet_id);
                    $outlet_name = $outlet['outlet_name'];

                    $order->outlet_name = $outlet_name;
                }
            }
            $data = array();
            if(!empty($orders))
            {
                foreach ($orders as $key=>$order)
                {
                    $nestedData['outlet_name']      = $order->outlet_name;
                    $nestedData['count_of_order']   = $order->count;
                    $nestedData['value_of_order']   = $order->total;
                    fputcsv($file,$nestedData);
                    $data[] = $nestedData;
                }
            }
            fclose($file);
        };
        
        return Response::stream($callback, 200, $headers);
        
    }


}