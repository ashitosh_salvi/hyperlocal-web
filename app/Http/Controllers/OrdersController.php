<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Rider;
use App\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Notify;
use App\Notifications\OrderRecived;
use App\Outlet;
use App\Customer;

class OrdersController extends Controller
{
    // Main Index Function 
    public function index() {
        $orders = Orders::reorder('id', 'ASC')->get();
        // dd( $orders );
        return view('orders.orders_list')->with('orders', $orders);
    }

    // Get Order Details
    public function getOrderDetails(Request $request) {

        $orders = Orders::where('orders.id', $request->id)->first();
        $rider = Rider::find($orders->rider_id);    
        $order_items = Orders::where('orders.id', $request->id)->join('order_items', 'order_items.order_id', '=', 'orders.order_id')->get();

        return Array( 'orders' => $orders, 'order_items' => $order_items ,'rider' => $rider);

    }

    public function get_current_order_list( Request $request ) {
        
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $columns = array( 
            0 =>'order_id',
            1 =>'customer_name',
            2 =>'customer_address',
            3 =>'total_amount',
            4 =>'final_amount',
            5 =>'status',
            5 =>'special_instruction',
            6 =>'table_no',
            7 =>'payment_mode',
            8 =>'created_at',
            9 =>'action',
        );
        $totalData = DB::table('orders')->where('orders.outlet_id', $outlet_id)
        ->where('orders.status','<>','delivered')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value'))) {            

            $Orders = Orders::offset($start)
                        ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
                        ->leftJoin('customer_addresses', 'orders.customer_addr_id', '=', 'customer_addresses.id')
                        ->where('orders.outlet_id', $outlet_id)
                        ->where('orders.status','<>','delivered') 
                        ->limit($limit)
                        ->orderBy('orders.created_at','DESC')
                        //->orderBy($order,$dir)
                        ->get(['orders.id as id','orders.*','customers.first_name','customers.last_name','customer_addresses.your_location']);
            
        } else {

            $search = $request->input('search.value'); 
    
                $Orders =  DB::table('orders')
                                ->Join('customers', 'orders.customer_id', '=', 'customers.id')
                                ->Join('customer_addresses', 'orders.customer_addr_id', '=', 'customer_addresses.id')
                                ->where('orders.outlet_id', $outlet_id)
                                ->where('orders.status','!=','delivered') 
                                ->where('orders.order_id','LIKE',"%{$search}%")
                                ->orWhere('orders.total_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.final_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.created_at', 'LIKE',"%{$search}%")
                                //->orWhere('customer_addresses.your_location', 'LIKE',"%{$search}%")
                                //->orWhere('customers.first_name', 'LIKE',"%{$search}%")
                                //->orWhere('customers.last_name', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy('orders.created_at','DESC')
                                //->orderBy($order,$dir)
                                ->get(['orders.id as id','orders.*','customers.first_name','customers.last_name','customer_addresses.your_location']);
    
                $totalFiltered = DB::table('orders')
                                ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
                                ->where('orders.outlet_id', $outlet_id)
                                ->where('orders.status','<>','delivered') 
                                ->where('orders.order_id','LIKE',"%{$search}%")
                                ->orWhere('orders.total_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.final_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.created_at', 'LIKE',"%{$search}%")
                                // ->orWhere('customer_addresses.your_location', 'LIKE',"%{$search}%")
                                // ->orWhere('customers.first_name', 'LIKE',"%{$search}%")
                                // ->orWhere('customers.last_name', 'LIKE',"%{$search}%")
                                ->count();

        }
        $data = array();

        if(!empty($Orders)) {
            foreach ($Orders as $key=>$order) {
                
                $nestedData['order_id'] = $order->order_id;
                $nestedData['customer_name'] = $order->first_name.' '.$order->last_name;
                $nestedData['offer_discount'] = $order->discount;
                $nestedData['customer_address'] = $order->your_location;
                $nestedData['total_amount'] = $order->total_amount;
                $nestedData['final_amount'] = $order->final_amount;
                $nestedData['status'] = $order->status;
                $nestedData['special_instruction'] = $order->special_instruction;
                $nestedData['table_no'] = $order->table_no;
                $nestedData['payment_mode'] = $order->payment_mode;
                $nestedData['created_at'] = date('d/m/Y h:i A', strtotime($order->created_at)+60*60*5.5);
                $nestedData['action'] = '';
                
                $nestedData['action'] .= '<div class="action-group">
                        <a title="View Order" class="view btn operations" onclick="getOrdersView('. $order->id .')" data-toggle="modal" data-target="#viewOrderModal-1"> <i class="fa fa-eye"></i></a>';
                if($order->out_for_delivery_date == '' && ($order->status != 'rejected' && $order->status !='cancelled')){        
                    $nestedData['action'] .= '<a title="Assign to Rider" class="btn operations" data-toggle="modal" onclick="getRiderOrderModal('. $order->id .', '. $order->merchant_id .', \''. $order->order_id .'\', '. ( ($order->rider_id == "") ? 'null' : $order->rider_id ) .')" data-target="#getRiderOrderModal-1">
                            <i class="fa fa-motorcycle"></i>
                        </a>';
                }
                // $nestedData['action'] .= '<a title="Delete Order" onclick="delete_order('. $order->id .')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a>
                //         </div>';
                if($order->status != 'rejected' && $order->status !='cancelled'){
                    $nestedData['action'] .= '<a title="Update Order" data-status="'.$order->status.'" onclick="update_order('. $order->id .')" 
                                            data-orderid="'. $order->order_id .'"
                                            class="btn operations tblord'. $order->id .'"><i class="fa fa fa-link"></i></a>
                         </div>';
                }

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);

    }
    public function get_past_order_list( Request $request ) {
        
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $columns = array( 
            0 =>'order_id',
            1 =>'customer_name',
            2 =>'customer_address',
            3 =>'total_amount',
            4 =>'final_amount',
            5 =>'status',
            6 =>'table_no',
            7 =>'payment_mode',
            8 =>'created_at',
            9 =>'action',
        );
        $totalData = DB::table('orders')->where('orders.outlet_id', $outlet_id)
        ->where('orders.status','delivered')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value'))) {            
            
            $Orders = DB::table('orders')
                        ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
                        ->where('orders.outlet_id', $outlet_id)
                        ->where('orders.status','delivered') 
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy('orders.created_at','DESC')
                        //->orderBy($order,$dir)
                        ->get(['orders.id as id','orders.*','customers.first_name','customers.last_name']);
        } else {

            $search = $request->input('search.value'); 
    
                $Orders =  DB::table('orders')
                                ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
                                ->where('orders.outlet_id', $outlet_id)
                                ->where('orders.status','delivered') 
                                ->where('orders.order_id','LIKE',"%{$search}%")
                                ->orWhere('orders.total_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.final_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.created_at', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy('orders.created_at','DESC')
                                //->orderBy($order,$dir)
                                ->get(['orders.id as id','orders.*','customers.first_name','customers.last_name']);
    
                $totalFiltered = DB::table('orders')
                                ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
                                ->where('orders.outlet_id', $outlet_id)
                                ->where('orders.status','delivered') 
                                ->where('orders.order_id','LIKE',"%{$search}%")
                                ->orWhere('orders.total_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.final_amount', 'LIKE',"%{$search}%")
                                ->orWhere('orders.created_at', 'LIKE',"%{$search}%")
                                ->count();

        }
        $data = array();
          
        if(!empty($Orders)) {
            foreach ($Orders as $key=>$order) {
                
                $nestedData['order_id'] = $order->order_id;

                $nestedData['customer_name'] = $order->first_name.' '.$order->last_name;
                $nestedData['offer_discount'] = $order->discount;
                // $nestedData['customer_address'] = 'Test 123';
                $nestedData['total_amount'] = $order->total_amount;
                $nestedData['final_amount'] = $order->final_amount;
                $nestedData['status'] = $order->status;
                $nestedData['table_no'] = $order->table_no;
                $nestedData['payment_mode'] = $order->payment_mode;
                $nestedData['created_at'] = date('d/m/Y h:i A', strtotime($order->created_at)+60*60*5.5);
                
                /*$nestedData['action']= '<div class="action-group"><a title="View Order" class="view btn operations" onclick="getOrdersView('. $order->id .')" data-toggle="modal" data-target="#viewOrderModal-1"> <i class="fa fa-eye"></i></a>
                
                <a title="Delete Order" onclick="delete_order('. $order->id .')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';*/

                $nestedData['action']= '<div class="action-group"><a title="View Order" class="view btn operations" onclick="getOrdersView('. $order->id .')" data-toggle="modal" data-target="#viewOrderModal-1"> <i class="fa fa-eye"></i></a>';

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);

    }

    // Edit Order Details
    public function editOrderDetails(Request $request) {

        $orders = Orders::find($request->order_id);

        // dd( $request );
        // exit;
        $orders -> order_id = $request->order_id_edit;
        $orders -> promocode = $request->promocode_edit;
        $orders -> amount = $request->amount_edit;
        $orders -> delivery_charges = $request->delivery_charges_edit;
        $orders -> discount = $request->discount_edit;
        $orders -> total_amount = $request->total_amount_edit;
        $orders -> tax = $request->tax_edit;
        $orders -> final_amount = $request->final_amount_edit;

        $item_count =  $request -> item_count_edit;

        for($i = 0; $i < $item_count; $i++ ){
            
            // if ( $request->item_id_edit[ $i ] != null ) {
                $order_item = OrderItems::find($request->item_id_edit[ $i ]);
            // } else {
                // $order_item = new OrderItems();
            // }

            $order_item -> order_id = $request->order_id_edit;
            $order_item -> item_id = $request->item_id[ $i ];
            $order_item -> item_name = $request->item_name[ $i ];
            $order_item -> item_code = $request->item_code[ $i ];
            $order_item -> item_qty = $request->item_qty[ $i ];
            $order_item -> item_price = $request->item_price[ $i ];
            
            $order_item -> save();

        }

        $orders->save();

    }

    public function get_all_riders(Request $request) {
        $merchant_id = $request->merchant_id;
        $riders = array();
        $get_riders = DB::table('riders')
                            ->where('status', 1)
                            ->where('online', 1)
                            ->get(['id', 'first_name', 'last_name', 'merchant_id']);
        foreach( $get_riders as $rider ) {
            // $live_order = false;
            // $live_order_count = Orders::where('rider_id',$rider->id)
            //                             ->where('status','!=','delivered')
            //                             ->count();
            // if ($live_order_count != 0) {
            //     $live_order = true;
            // }
            
            // if ($live_order == false) {
                if ( $rider->merchant_id != null ) {
                    $merchant_ids = json_decode( $rider->merchant_id );
                    
                    if (is_array($merchant_ids) || is_object($merchant_ids)) {
                        foreach( $merchant_ids as $key => $val ) {
                            
                            if ( $val == $merchant_id ) {
                                array_push($riders, $rider);
                            }
                        }
                    }
                } 
                // else {
                //     array_push($riders, $rider);
                // }
            // }
            
        }

        return $riders;
    }

    public function delete_order(Request $request) {
        $order = Orders::find($request->order_id);
        
        $auth = Outlet::find(25);
        
        
        $auth->order_id = '116';
        $auth->msgbody = '00002';

        //$auth->notify(new OrderRecived);

        $note = $auth->unreadNotifications->sortBy('created_at')->first();
        dd($note);

        //dd($auth->unreadNotifications);

        // delete option is not required        
        //$order->delete();
        //return;
    }

    public function assign_rider_order(Request $request) {
        date_default_timezone_set('Asia/Kolkata');
        if ( isset($request->riders) ) {
            $order = Orders::find($request->rider_order_id);
            $order->rider_id = $request->riders;
            $order->rider_assign_date = date("Y-m-d h:i:s"); 
            // $order->confirm_date = date("Y-m-d h:i:sa"); 
            //$order -> out_for_delivery_date = date("Y-m-d h:i:sa");
            $order->status = 'assigned';
            $order->save();

            $rider = Rider::find($request->riders);
            $rider->msgbody = 'Hi Rider, You have received order - '.$order->order_id;
            $rider->msgtitle = 'Order Assign.';

            $rider->order_id = strval($order->id);

            try{
                $rider->notify(new Notify);
            }
            catch(\Exception $ex){
                return $ex->getMessage();
            }
            

        }

        return;
    }
    
    public function update_order_status(Request $request){
        
        $order = Orders::find($request->order_update_id);        
        if( $order->status == 'cancelled'){
            return;
        }

        $order->status = $request->status_update;
        $customer = Customer::find($order->customer_id);
        $outlet = Outlet::find($order->outlet_id);
        $customer->order_id = strval($order->id);
        $rider = new Rider();
        if($order->rider_id){
            $rider = Rider::find($order->rider_id);
        }

        if($outlet->outlet_type == 'product'){
            $order_type = 'order';
        }else{
            $order_type = 'food';
        }

        if($request -> status_update){
            if($request -> status_update == 'rejected'){
                $customer->msgtitle = 'Order Rejected.';
                if($order->payment_mode == 'cod'){
                    $customer->msgbody = 'Dear '. $customer->first_name .', We are sorry but '. $outlet->outlet_name .' has rejected your order. Please check other which can serve you at the moment.';
                }else{
                    $customer->msgbody = 'Dear '. $customer->first_name .', We are sorry but '. $outlet->outlet_name .' has rejected your order. Please check other Outlet which can serve you at the moment. Your paid order amount ₹'. $order->final_amount .' will get refunded in 8-10 business day.';
                }
                    
                
                if($order->rider_id){
                    
                    $rider->msgbody = 'Hi Rider, You order - '.$order->order_id.' has been rejected';
                    $rider->msgtitle = 'Order Rejected.';
                    $rider->order_id = strval($order->id);

                    try{
                        $rider->notify(new Notify);
                    }
                    catch(\Exception $ex){
                        $exp = $ex->getMessage();
                    }

                } 
            }
            if($request -> status_update == 'accepted'){
                $order->confirm_date = date("Y-m-d h:i:sa");
                $customer->msgtitle = 'Order Confirm.';

                if($order->payment_mode == 'cod'){
                    $customer->msgbody = 'Dear '. $customer->first_name .', Thank you for ordering with '. $outlet->outlet_name .'. Your order is currently being prepared & we will keep you updated as soon as your '.$order_type.' is on its way. Request you to keep cash ₹'. $order->final_amount .' ready.';
                }else{
                    $customer->msgbody = 'Dear '. $customer->first_name .', Thank you for ordering with '. $outlet->outlet_name .'. We have received your payment ₹'. $order->final_amount .' for '.$order->order_id.'. Your order is currently being prepared & we will keep you updated as soon as your '.$order_type.' is on its way.';
                }
                 
                    
            }
            if($request -> status_update == 'pickedup'){
                $order -> out_for_delivery_date = date("Y-m-d h:i:sa");    
                $customer->msgtitle = 'Order Out for Delivery.';

                if($order->payment_mode == 'cod'){  
                    $customer->msgbody = 'Dear '. $customer->first_name .', your food is currently on its way to you by our Rider '.$rider->first_name.'. Request you to keep cash ₹'.$order->final_amount.' ready.';
                }else{
                    $customer->msgbody ='Dear '. $customer->first_name .', your food is currently on its way to you by our Rider '.$rider->first_name.'.';
                }


            }
            else if($request -> status_update == 'reachedlocation'){
                $order -> reached_location_date = date("Y-m-d h:i:sa");
                
            }
            else if($request -> status_update == 'delivered'){
                $order -> delivered_date = date("Y-m-d h:i:sa");   
                $customer->msgtitle = 'Order Delivered';

                if($order->payment_mode == 'cod'){
                    $customer->msgbody = 'Thank you for Using HyperLocal App. We have received the Order amount ₹'.$order->final_amount.'. Hope you enjoy your order.';                
                }else{
                    $customer->msgbody = 'Thank you for Using HyperLocal App - We hope you enjoy your order.';
                }

            }

            if($request->status_update == 'ready')
            {  
                $order->ready_datetime = date("Y-m-d h:i:sa"); 
                $customer->msgtitle = 'Order Ready';
                if($order->payment_mode == 'cod'){
                    $customer->msgbody = 'Dear '. $customer->first_name .', Your order is ready.';                
                }else{
                    $customer->msgbody = 'Dear '. $customer->first_name .', Your order is ready.';   
                }
            }
        }
        
        if($customer->msgtitle){
            try{
                $customer->notify(new Notify);
                // dump($customer);
            }
            catch(\Exception $ex){
                $expc = $ex->getMessage();
                
            }
        }
        

        // if($request->notification_read){
        //     $authO = Outlet::find(Auth::guard('outlet')->id());
        //     $authO->notifications->markAsRead();    
        // }

        $order->save();
        return;
    }


    public function get_recived_order_id(Request $request){

        $auth = Outlet::find(Auth::guard('outlet')->id());
        $note = $auth->unreadNotifications->sortBy('created_at')->first();
        $read = $note;
        if($auth->session_id == null)
        {   
            return 1;
        }
        if($note){
            // check order is pending or not
            $order_id = $note->data['order_id'];
            $order = Orders::where('id',$order_id)->first(); 
            if($order->status != 'pending')
            {
                $read->markAsRead();
                return 0;
            }

            $read->markAsRead();  
            $this->sendNotification($auth->firebase_token,$note->date);
            return $note->data;
        }
        return 0;
    }

    public function sendNotification($token,$order)
    {
        $url ="https://fcm.googleapis.com/fcm/send";
        $order_array = json_decode($order);
        $fields=array(
            "to"=>$token,
            "notification"=>array(
                "body"=>"New Order Received",
                "title"=>"New Order Received",
                "icon"=>"https://www.hyperlocalonline.in/assets/images/app_name.png",
                "click_action"=>"https://www.hyperlocalonline.in/hladmin/orders_list/".$order_array['order_id']
            )
        );

        $headers=array(
            'Authorization: key=AAAAFsT12I8:APA91bEksY6D9J3AK9SWvjUBea9qzNNVm7qJZIjCgqgKKf7l5WG_7APxh6VkBqnoAswhf845UOzKn-QoXj7TQ95T7ELxuN_CSg5JKiGXkcyMvfnf4ydTiZ61kOV5Y-BdGv9_omUxZVWh',
            'Content-Type:application/json'
        );

        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($fields));
        $result=curl_exec($ch);
        curl_close($ch);
        // dd($result);
    }


}