<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;

class TransferController extends Controller
{
    public function __construct(){
        
        //older
        //$this->key_id = 'rzp_test_Hp2TkJvb9rQJx6';
        //$this->key_secret = 'y33JNokTHPMZ4HvdK90ihNWS';

        // $this->key_id = 'rzp_test_ruoishHwaDblnv';
        // $this->key_secret = 'FkwqiyJQZhxn3qThgutDZJ65';

        // live key
+        $this->key_id = 'rzp_live_aIibEHGq9YYfQV';
+        $this->key_secret = 'CqZMGdW12q0SUBMeMmpicJsi';

    }

    public function orderTransfer(){

        $order = Orders::where('status','delivered')
                ->paginate(10);
        //$order = Orders::all();
        //dd($order);
        $data['orders'] = $order;
        return view('superadmin.transfer')->with($data)->withFlag(1);

    }

    public function get_orderTransfer(Orders $id){
        $data['order'] = $id;
        return view('superadmin.transfer')->with($data)->withFlag(0);
    }

    public function transfer_amount(Request $request){

        //dd

        $url = "https://" . $this->key_id . ":" . $this->key_secret . "@api.razorpay.com/v1/transfers";
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('account' => 'acc_FenrV7ZUuMgs9g','amount' => '100','currency' => 'INR'),
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
        )
        ));

        $response =  curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            echo($err);
        }
        curl_close($curl);
        //echo($response);
        return redirect()->route('transfer')->withSuccess(true);
    }


}