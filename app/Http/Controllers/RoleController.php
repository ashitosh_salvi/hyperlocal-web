<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RoleController extends Controller
{
    public function create_role(Request $request){
                
        $role = new Roles(); 

        $role -> name = $request -> name;
        $role -> description = $request -> description;
        $role -> status = 1;
        $role -> flag = 0;
        
        $role->save();

        return ;


    }

    public function get_role_list(Request $request){

        $columns = array( 
            0 =>'id',
            1 =>'name',
            2 =>'description',
            3 =>'status',
            4 =>'action',
        );  
        $totalData = Roles::count();            
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')))
        {            
            $roles = Roles::offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }else {
            $search = $request->input('search.value'); 

            $roles =  Roles::where('name','LIKE',"%{$search}%")
                        ->orWhere('description','LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Roles::where('name','LIKE',"%{$search}%")
                            ->orWhere('description','LIKE',"%{$search}%")
                            ->count();
        }

    
        $data = array();
        if(!empty($roles))
        {
            foreach ($roles as $key=>$role_v)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $role_v->name;
                $nestedData['description'] = $role_v->description;
                $nestedData['status'] = '<input class="tgl tgl-ios tgl_checkbox role_status" id="cb_'. $role_v->id .'" name="role_status" type="checkbox" data-status="'. $role_v->status .'" '. ( ( $role_v->status == '1' ) ? "checked" : "" ) .' data-id="'. $role_v->id .'"><label class="tgl-btn" for="cb_'. $role_v->id .'"></label>';
                $nestedData['action'] = '<div class="action-group"><a title="View Menu Item" class="view btn operations" onclick="getRoleView('.$role_v->id.')" data-toggle="modal" data-target="#viewRoleModal-1"> <i class="fa fa-eye"></i></a><a title="Edit Menu Item" class="btn operations" onclick="getRole('.$role_v->id.')" data-toggle="modal" data-target="#editRoleModal-1"><i class="fa fa-edit"></i></a><a title="Delete Menu Item" data-attr="'.$role_v->id.'" onclick="delete_role('.$role_v->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
 
    }
    public function get_role(Request $request){

        $pd = Roles::where('id',$request->id)->first();

        return Array( 'role' =>$pd);
    }

    public function role_list(){

        return view('merchant.role_list');
    }

    public function edit_role(Request $request){
        
        $role = Roles::find($request -> role_id);
        $role -> name = $request -> name_ed;
        $role -> description = $request -> description_ed;
        $role->save();

        return ;


    }

    public function delete_role(Request $request) {
        $role = Roles::find($request->role_id);
        $role->delete();
        return;
    }

    public function update_role_status(Request $request) {
        $role = Roles::find($request->role_id);
        $role->status = $request->role_status;
        $role->save();
        return;
    }


}
