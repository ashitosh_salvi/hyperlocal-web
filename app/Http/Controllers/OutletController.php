<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Outlet;
use App\Recipe;
use App\Category;
use App\CustomerOutletFavourite;
use App\CustomerOutletRating;
use App\Offers;
use App\Merchant;
use App\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Log;

class OutletController extends Controller
{
    //
    protected $redirectTo = '/outlet_dashboard';
    protected $guard = 'outlet';

    public function showLoginForm(){
       
        // if(view()->exists('merchant.merchant_login')){
            
        // }
        // return view('login');
        if(Auth::guard('outlet')->id() != null)
        {
            // return view('merchant.outlet_dashboard');
            return redirect('/outlet_dashboard');
        }
        return view('merchant.outlet_login');
    }

    public function index() {
        return view('merchant.outlet_dashboard');
    }

    public function generate_qr() {

        $qrData = Outlet::find(Auth::guard('outlet')->id())->qr_data;


        /*check if outlet product exists*/
        $productExists = Recipe::where('outlet_id', Auth::guard('outlet')->id())->exists();

        if($productExists)
        {
            /*generate qr code*/
            if(!empty($qrData))
            {
                $data = Array(
                    'qrData' => $qrData,
                    'message' => ''
                );
            }
            else
            {
                $qrData = Auth::guard('outlet')->id();

                $data = Array(
                    'qrData' => $qrData,
                    'message' => ''
                );
            }
        }
        else
        {
            $data = Array(
                'message' => 'You can not generate QR code without products.'
            );
        }

        return view('merchant.generate_qr')->with($data);
    }

    public function downloadQR($qrData)
    {
        $img_id = uniqid();
        $path = 'images/qr_images/';
        $file = $path.$img_id.".png"; 
        QrCode::size(500)->format('png')->generate("https://www.hyperlocalonline.in/hladmin/restaurant/" . $qrData , public_path($file));
        return response()->download(public_path($file));
    }

    public function restaurant($id) {
        $outlet = Outlet::where('id', $id)->where('flag', 0)->first();

        if(!empty($outlet))
        {
            $list = $this->get_items_by_outlet_id($id, 0, $outlet->food_cat, $outlet->outlet_type);

            $data = Array(
                'outlet' => $outlet,
                'list' => $list
            );
        }
        else
        {
            $data = Array(
                'outlet' => '',
                'list' => ''
            );
        }

        return view('restorent.restorent')->with($data);
    }

    public function get_items_by_outlet_id($outlet_id, $customer_id, $food_category, $outlet_type){
        $get_outlet_type = Outlet::where('id', $outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');
            if ( $get_outlet_type[0] == "product" ) {
                return $this->get_items_by_outlet_id_product($outlet_id, $customer_id, $food_category, $outlet_type);
            }
            //return $get_outlet_type;

        $da = Recipe::where('status','1')
                    ->pluck('category_id');    
            //return $da;

        $category = Category::where('outlet_id',$outlet_id)
                    ->where('priority','1')
                    ->where('status','1')
                    ->whereIn('id',$da)
                    ->where('outlet_type','recipe')
                    ->get(['id','category_name']);

        foreach($category as $cate){

            
                $cate -> sub_category = Category::where('parent_category',$cate->id)
                                        ->where('outlet_type','recipe')
                                        ->where('status','1')
                                        ->get(['id','category_name']);

                $main_category_items = Recipe::where('category_id',$cate->id)
                                        ->where('status','1')
                                        ->where(function($q){
                                            $q->whereNull('sub_category_id')
                                                ->orWhere('sub_category_id','0');
                                        })
                                        ->Where(function($q){
                                            $q->where(function($q){
                                                    $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                                    ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                                })
                                                ->orWhere(function($q){
                                                    $q->whereNull('from_time')
                                                    ->whereNull('to_time');
                                                });        
                                        })
                                        ->get();
                foreach($main_category_items as $item){
                    $get_item_qty = DB::table('cart_items')->where('item_id', $item->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();
                        $item['item_id'] = empty($item->id) ? '' : strval( $item->id );
                        $item['item_name'] = empty($item->name) ? '' : strval( $item->name );
                            
                        $item['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                        $item['image_url'] = URL::asset('/images') . "/" . ( empty($item->image_url) ? 'test.jpg' : $item->image_url );
                        $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                        $item['category'] = '';
                        $item['price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['special_price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['discount'] = "";
                        $item['charges_gst'] = empty($item->charges_gst) ? '' : strval( $item->charges_gst );
                }


                $cate-> main_category_items = $main_category_items->toArray();                        

                foreach($cate -> sub_category as $recipe){

                    $recipe -> items = Recipe::where('sub_category_id',$recipe->id)
                                    ->where('status','1')
                                    ->Where(function($q){
                                        $q->where(function($q){
                                                $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                                ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                            })
                                            ->orWhere(function($q){
                                                $q->whereNull('from_time')
                                                ->whereNull('to_time');
                                            });        
                                    })
                                    ->get();
                    foreach($recipe -> items as $item){
                        $get_item_qty = DB::table('cart_items')->where('item_id', $item->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();
                        $item['item_id'] = empty($item->id) ? '' : strval( $item->id );
                        $item['item_name'] = empty($item->name) ? '' : strval( $item->name );
                            
                        $item['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                        $item['image_url'] = URL::asset('/images') . "/" . ( empty($item->image_url) ? 'test.jpg' : $item->image_url );
                        $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                        $item['category'] = '';
                        $item['price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['special_price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['discount'] = "";
                        $item['charges_gst'] = empty($item->charges_gst) ? '' : strval( $item->charges_gst );
                    }
                }
        }

        //$all_items['offer_list'] = $this->offer_by_outlet($outlet_id);
        $all_items['user_rating'] = $this->get_avg_rating($outlet_id);
        $all_items['category'] = $category->toArray();
        
        return array(
            'message' => 'Items List Retrieved',
            'data' => $all_items
        );
    }

    public function get_items_by_outlet_id_product($outlet_id, $customer_id, $food_category, $outlet_type) {
        if ( isset($request->outlet_id) ) {
            
            $outlet_id = $request->outlet_id;
            $customer_id = $request->customer_id;
            
            $outlet_type = $request->outlet_type;
            
            $food_category = array('1','2','3');
            if($outlet_type == 'recipe'){
                if($request->food_category){
                    $food_category = explode(',',$request->food_category);
                }
            }

            $all_cart_items = array();
            
            //$all_items['offer_list'] = $this->offer_by_outlet($outlet_id);
            $all_cart_items['user_rating'] = $this->get_avg_rating($outlet_id);
            $all_cart_items['category'] = array();

            $get_outlet_type = Outlet::where('id', $outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');

            if ( $get_outlet_type[0] == "product" ) {

                $final_product = array();
                $temp = array();
                // $all_category = DB::select('select * from categories where status = 1 and id in (select child_category_id from products where child_category_id <> "" and outlet_id = ? group by child_category_id)',[$outlet_id]);


                $all_category = DB::select('select * from categories where outlet_id = ? and priority = "1" and status = "1"',[$outlet_id]);
                foreach ($all_category as $category) {
                    
                    if($category->priority == 1){
                        
                        $get_all_products = DB::select('select * from products where outlet_id = ? and category_id = ? and status = "1"',[$outlet_id,$category->id]);
                    
                    
                    }
                    // elseif ($category->priority == 2) {
                    //     $get_all_products = DB::select('select * from products where outlet_id = ? and sub_category_id = ? and child_category_id IS NULL and status = "1"',[$outlet_id,$category->id]);   
                    // }
                    // else{
                    //     $get_all_products = DB::select('select * from products where outlet_id = ? and child_category_id = ? and status = "1"',[$outlet_id,$category->id]);
                    // }
                    // print_r($get_all_products);exit();
                    if(!empty($get_all_products)){

                        foreach ( $get_all_products as $rec ) {
                            $get_order_items = DB::table('products')->where('id', $rec->id)->first();

                            $get_item_qty = DB::table('cart_items')->where('item_id', $rec->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();

                            $final_product['item_id'] = empty($get_order_items->id) ? '' : strval( $get_order_items->id );
                            $final_product['item_name'] = empty($get_order_items->product_name) ? '' : strval( $get_order_items->product_name );
                            $final_product['category'] = $category->category_name;
                            $final_product['price'] = empty($get_order_items->price) ? '' : strval( $get_order_items->price );
                            $final_product['special_price'] = empty($get_order_items->special_price) ? '' : strval( $get_order_items->special_price );
                            $final_product['discount'] = empty($get_order_items->discount) ? '' : strval( $get_order_items->discount );
                            $final_product['image_url'] = URL::asset('/images') . "/" . ( empty($get_order_items->image_url) ? 'test.jpg' : $get_order_items->image_url );
                            $final_product['charges_gst'] = "";
                            $final_product['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                            $final_product['food_category'] = '';
                            array_push($temp, $final_product);
                        }
                        $all_cart_items['category'][$category->category_name] = $temp;
                    }
                    $temp = array();
                }

            } else {

                $final_recipe = array();
                $temp = array();

                // $all_category = DB::select('select * from categories where status = 1 and id in (select sub_category_id from recipes where sub_category_id <> "" and outlet_id = ? group by sub_category_id)',[$outlet_id]);

                $all_category = DB::select('select * from categories where outlet_id = ? and priority = "1" and status = "1"',[$outlet_id]);

                foreach ($all_category as $category) {

                    if($category->priority == 1){
                        
                        // $get_all_recipes = DB::select('select * from recipes where outlet_id = ? and category_id = ? 
                        //         and status = "1" and 
                        //     (("from_time" > ? and "to_time" < ? ) OR ("from_time" is null and "to_time" is null)) '
                        //     ,[$outlet_id,$category->id,strtotime('now'),strtotime('now')]);
                        
                        $get_all_recipes = DB::table('recipes')->where('outlet_id',$outlet_id)
                            ->where('status','1')
                            ->where('category_id',$category->id)
                            ->whereIn('food_cat',$food_category)
                            ->Where(function($q){
                                $q->where(function($q){
                                        $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                        ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                    })
                                    ->orWhere(function($q){
                                        $q->whereNull('from_time')
                                        ->whereNull('to_time');
                                    });        
                            })
                            
                            ->get();
                            
                    
                    }
                    // elseif ($category->priority == 2) {
                    //     $get_all_recipes = DB::select('select * from recipes where outlet_id = ? and sub_category_id = ? and status = "1"',[$outlet_id,$category->id]);
                    // }
                    if(!$get_all_recipes->isEmpty()){

                        foreach ( $get_all_recipes as $rec ) {
                            $get_order_items = DB::table('recipes')->where('id', $rec->id)->first();
                            $get_item_qty = DB::table('cart_items')->where('item_id', $rec->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();

                            $final_recipe['item_id'] = empty($get_order_items->id) ? '' : strval( $get_order_items->id );
                            $final_recipe['item_name'] = empty($get_order_items->name) ? '' : strval( $get_order_items->name );
                            $final_product['category'] = $category->category_name;
                            $final_recipe['price'] = empty($get_order_items->price) ? '' : strval( $get_order_items->price );
                            $final_recipe['special_price'] = empty($get_order_items->price) ? '' : strval( $get_order_items->price );
                            $final_recipe['discount'] = "";
                            $final_recipe['image_url'] = URL::asset('/images') . "/" . ( empty($get_order_items->image_url) ? 'test.jpg' : $get_order_items->image_url );
                            $final_recipe['charges_gst'] = empty($get_order_items->charges_gst) ? '' : strval( $get_order_items->charges_gst );
                            $final_recipe['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                            $final_recipe['food_category'] = empty($get_order_items->food_cat) ? '-1' : strval($get_order_items->food_cat);
                            

                            array_push($temp, $final_recipe);
                        }
                        $all_cart_items['category'][$category->category_name] = $temp;
                    }
                    $temp = array();
                }


            }

            return array(
                'message' => 'Items List Retrieved',
                'data' => $all_cart_items
            );
            
        }
    }

    public function get_avg_rating($outlet_id) {
        $get_avg_rating = CustomerOutletRating::where('outlet_id', $outlet_id)->get(['rating']);

        
        if ( count( $get_avg_rating ) == 0 ) {
            return "0";
        } else {
            $c = 0;
            $rate = 0;
            foreach($get_avg_rating as $rating) {
                $rate += ( 5 * $rating['rating'] );
                $c += 1;
            }
            $count = ( 5 * $c );
            $avg_rate = number_format( ($rate / $count), 1 );

            return $avg_rate;
        }
        exit;
    }

    public function imageCrop(Request $request)
    {   
        
        $data = $request->image;
        $extension = explode('/', mime_content_type($data))[1];

        $image_array_1 = explode(";", $data);

        $image_array_2 = explode(",", $image_array_1[1]);

        $data = base64_decode($image_array_2[1]);

        $image_name =  'outlet_'. time() . '.'.$extension;
        $ext_image_name = 'images/'.$image_name;
        if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
            $ext_image_name = 'public/images/'.$image_name;
        }
        

        file_put_contents($ext_image_name, $data);

        return response()->json([
                'image_name' => $image_name,
            ]);
    }

    public function outlet_list(){

        // dd( Auth::guard('merchant')->id() );

        $merchant_id = Auth::guard('merchant')->id();

        if ( $merchant_id != null ) {

            $list = Outlet::where('merchant_id', $merchant_id)->get();

        } else {

            $list = Outlet::all();
        }

        $data = Array(
            'list' => $list
        );

        return view('merchant.outlet_list')->with($data);
    }

    public function admin_outlet_list(){


        $list = Outlet::all();
    
        $data = Array(
            'list' => $list
        );
        
        return view('merchant.admin_outlet_list')->with($data);
    }

    public function get_admin_outlet_list(Request $request) {

        $Outlet = array();

        $columns = array( 
            0 =>'outlet_name',
            1 =>'outlet_user_name',
            2 =>'outlet_type',
            3 =>'action',
        );
        $totalData = Outlet::all()->count();            
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')))
        {            
            $Outlet = Outlet::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }else {
            $search = $request->input('search.value'); 

            $Outlet =  Outlet::where('id','LIKE',"%{$search}%")
                                ->orWhere('outlet_user_name', 'LIKE',"%{$search}%")
                                ->orWhere('outlet_name', 'LIKE',"%{$search}%")
                                ->orWhere('outlet_type', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();

            $totalFiltered = Outlet::where('id','LIKE',"%{$search}%")
                            ->orWhere('outlet_user_name', 'LIKE',"%{$search}%")
                            ->orWhere('outlet_name', 'LIKE',"%{$search}%")
                            ->orWhere('outlet_type', 'LIKE',"%{$search}%")
                            ->count();
        }
        
        $data = array();
        if(!empty($Outlet))
        {
            foreach ($Outlet as $key=>$outlet)
            {
                
                $nestedData['outlet_name'] = $outlet->outlet_name;
                $nestedData['outlet_user_name'] = $outlet->outlet_user_name;
                $nestedData['outlet_type'] = $outlet->outlet_type;
                $nestedData['action'] = '<input class="tgl tgl-ios tgl_checkbox" id="cb_'. ( $outlet->id ) .'" name="outlet_status" type="checkbox" data-attr="'. $outlet->flag .'" '. ( ( $outlet->flag == '0' ) ? "checked" : "" ) .' outlet-id="'. $outlet->id .'"><label class="tgl-btn" for="cb_'. ( $outlet->id ) .'"></label>';
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);

    }

    public function admin_update_outlet_status(Request $request) {
        $outlet = Outlet::find($request->outlet_id);
        $outlet->flag = $request->outlet_status;
        $outlet->save();
        return;
    }

    public function outletlogin(Request $request) {
        
        $request->validate([
            'outlet_user_name' => 'required|string',
            'password' => 'required|string'
        ]);
        $credentials = request(['outlet_user_name', 'password']);
        if(!Auth::guard($this->guard)->attempt($credentials,true)) {
            return redirect()->intended('outlet_login')->withInput()->withErrors([
                'outlet_user_name' => [
                    "message" => "These credentials do not match our records."
                ]
            ]);
        } else {

            $getLoginOutlet = Outlet::where('id',Auth::guard('outlet')->id())->first();
        
            if ($getLoginOutlet->session_id != null) {
                Auth::guard('outlet')->logout();
                return redirect()->intended('outlet_login')->withInput()->withErrors([
                    'outlet_already_login' => [
                        "message" => "Another user already Logged In. Please contact Administrator."
                    ]
                ]);
            }
            $outlet = Outlet::where('id',Auth::guard('outlet')->id())
                            ->update(["session_id" => \Session::getId()]);
            $localIP = getHostByName(getHostName());

            $message = "Outlet : ".$getLoginOutlet->outlet_user_name.", System IP Address : ". $localIP.', DateTime : '.date("Y/m/d H:i:s").'.      ';
            Log::channel('outletloginlogs')->info($message);
            // return $this->index();
            return redirect('/outlet_dashboard');
        }
        
    }

    public function get_outlet(Request $request){
       
        $outlet = Outlet::where('id',$request->id)->first();
        if($outlet -> open_time){
            $outlet -> open_time = date('h:i A',strtotime($outlet -> open_time));
        }
        if($outlet -> close_time){
            $outlet -> close_time = date('h:i A',strtotime($outlet -> close_time));
        }

        return Array( 'outlet' =>$outlet);

    }

    public function get_outlet_list(Request $request) {

        $merchant_id = Auth::guard('merchant')->id();

        if ( $merchant_id != null ) {
                
            $columns = array( 
                0 =>'outlet_name',
                1 =>'outlet_user_name',
                2 =>'outlet_type',
                3 =>'action',
            );
            $totalData = Outlet::where('merchant_id', $merchant_id)->count();            
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {            
                $Outlet = Outlet::where('merchant_id', $merchant_id)->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            }else {
                $search = $request->input('search.value'); 
    
                $Outlet =  Outlet::where('merchant_id', $merchant_id)
                                ->orWhere('id','LIKE',"%{$search}%")
                                ->orWhere('outlet_user_name', 'LIKE',"%{$search}%")
                                ->orWhere('outlet_name', 'LIKE',"%{$search}%")
                                ->orWhere('outlet_type', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
    
                $totalFiltered = Outlet::where('merchant_id', $merchant_id)
                                ->orWhere('id','LIKE',"%{$search}%")
                                ->orWhere('outlet_user_name', 'LIKE',"%{$search}%")
                                ->orWhere('outlet_name', 'LIKE',"%{$search}%")
                                ->orWhere('outlet_type', 'LIKE',"%{$search}%")
                                ->count();
            }

        } else {
            $Outlet = array();
        }
        
        $data = array();
        if(!empty($Outlet))
        {
            foreach ($Outlet as $key=>$outlet)
            {
                
                $nestedData['outlet_name'] = $outlet->outlet_name;
                $nestedData['outlet_user_name'] = $outlet->outlet_user_name;
                $nestedData['outlet_type'] = $outlet->outlet_type;
                $nestedData['action'] = '<div class="action-group"><a title="View Outlet" class="view btn operations" onclick="getOutletView('.$outlet->id.')" data-toggle="modal" data-target="#viewOutletModal-1"> <i class="fa fa-eye"></i></a>
                                        <a title="Edit Outlet" class="btn operations" onclick="getOutlet('.$outlet->id.')" data-toggle="modal" data-target="#editOutletModal-1">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a title="Delete Outlet" data-attr="'.$outlet->id.'" onclick="delete_outlet('.$outlet->id.')" href="javascript:void(0)" class="btn operations"><i class="fa fa-trash-o"></i></a>
                                        <a title="Attachment Outlet" data-attr="'.$outlet->id.'" onclick="attachment_outlet('.$outlet->id.')" href="javascript:void(0)" class="btn operations"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
                                        <a title="Signout all user from outlet" data-attr="'.$outlet->id.'" onclick="signout_outlet('.$outlet->id.')" href="javascript:void(0)" class="btn operations"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                        <a title="Signout all app user from outlet" data-attr="'.$outlet->id.'" onclick="signout_app_outlet('.$outlet->id.')" href="javascript:void(0)" class="btn operations"><i class="fa fa-mobile" aria-hidden="true"></i></a>
                                        </div> ';
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);

    }

    public function create_outlet(Request $request){
        
        $merchant_id = Auth::guard('merchant')->id();
        $outlet = new Outlet();

        $filename = '';
        if($request->file('image_url')){
            $img_temp = $request->file('image_url');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            // Storage::disk('public')->put($filename, File::get($img_temp));
        }

        $filename_outlet_str = [];
        $sft = 'document';

        // dd( $request->file($sft) );

        // exit;
        if( !empty($request->file($sft)) ) {

            $img_temp_outlet = $request->file($sft);

            foreach($img_temp_outlet as $temp_outlet){
                // dd( $temp_outlet );
                // exit;
                $tmp_out = pathinfo( $temp_outlet -> getClientOriginalName(), PATHINFO_FILENAME);
                $extension = $temp_outlet -> getClientOriginalExtension();
                $filename_outlet  = $tmp_out.'-'.$request->outlet_name.'-'.rand(1111,9999).'.'.$extension;
                $image_path_outlet = "images/". $filename_outlet;
                if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                    $image_path_outlet = "public/images/". $filename_outlet;
                }
                move_uploaded_file( $temp_outlet, $image_path_outlet);
                array_push( $filename_outlet_str, $filename_outlet );
            }

        }
        
        // dd( $filename_outlet_str );
        // exit;

        $outlet -> outlet_user_name = $request -> outlet_user_name;
        $outlet -> password = Hash::make($request -> outlet_password);
        $outlet -> outlet_type = $request -> outlet_type;
        $outlet -> branch = $request -> branch;
        $outlet -> fssai_license = $request -> fssai_license;
        $outlet -> gst = $request -> gst;
        $outlet -> outlet_address = $request -> outlet_address;
        $outlet -> outlet_name = $request -> outlet_name; 
        $outlet -> shop_license = $request -> shop_license;
        $outlet -> gst_rate = $request -> gst_rate;
        $outlet -> delivery_charges = $request -> delivery_charges;
        $outlet -> merchant_id = $merchant_id;
        $outlet -> outlet_category_id = $request -> outlet_category_type;
        $outlet -> mobile = $request -> outlet_mobile;
        $outlet -> outlet_geoaddress = $request -> geo_address;
        $outlet -> outlet_latitude = $request -> address_latitude;
        $outlet -> outlet_longitude = $request -> address_longitude;
        $outlet -> flag = $request->flag;
        if($filename){
            // $outlet -> image_url = json_encode($filename);
            $outlet -> image_url = json_encode($request->outlet_image_name);
        }
        $outlet -> outlet_speciality = $request -> outlet_speciality;
        // $outlet -> booking_for_two = $request -> booking_for_two;
        if($filename_outlet_str){
            $outlet -> img_url = json_encode( $filename_outlet_str );
        }
        if(isset($request->is_cod)){
            $outlet -> is_cod = 1;
        } else {
            $outlet -> is_cod = 0;
        }

        if($request -> open_time){
            $outlet -> open_time = date('G:i',strtotime($request -> open_time));   
        }
        if($request -> close_time){
            $outlet -> close_time = date('G:i',strtotime($request -> close_time));
        }                
        
        $outlet -> save();
        //print_r($outlet);
        echo 'save';

    }

    public function edit_outlet(Request $request){

        $merchant_id = Auth::guard('merchant')->id();
        $outlet = Outlet::find($request->outlet_id_edit);
        
        $filename = '';
        if($request->file('ed_rest_img')){
            $img_temp = $request->file('ed_rest_img');
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            // Storage::disk('public')->put($filename, File::get($img_temp));
        }

        $filename_outlet_str = [];
        $sft = 'documented';

        // dd( $request->file($sft) );

        // exit;
        if( !empty($request->file($sft)) ) {

            $img_temp_outlet = $request->file($sft);

            foreach($img_temp_outlet as $temp_outlet){
                // dd( $temp_outlet );
                // exit;
                $tmp_out = pathinfo( $temp_outlet -> getClientOriginalName(), PATHINFO_FILENAME);
                $extension = $temp_outlet -> getClientOriginalExtension();
                $filename_outlet  = $tmp_out.'-'.$request->outlet_name_edit.'-'.rand(1111,9999).'.'.$extension;
                $image_path_outlet = "images/". $filename_outlet;
                if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                    $image_path_outlet = "public/images/". $filename_outlet;
                }
                move_uploaded_file( $temp_outlet, $image_path_outlet);
                array_push( $filename_outlet_str, $filename_outlet );
            }

        }
        
        // dd( $filename_outlet_str );
        // exit;

        $outlet -> outlet_user_name = $request -> outlet_user_name_edit;
        if( isset($request -> password_ed) && ( $outlet -> password != $request -> outlet_password_edit ) ) {
            $outlet -> password = Hash::make($request -> outlet_password_edit);
        }
        $outlet -> outlet_type = $request -> outlet_type_edit;
        $outlet -> branch = $request -> branch_edit;
        $outlet -> fssai_license = $request -> fssai_license_edit;
        $outlet -> gst = $request -> gst_edit;
        $outlet -> outlet_address = $request -> outlet_address_edit;
        $outlet -> outlet_name = $request->outlet_name_edit; 
        $outlet -> shop_license = $request -> shop_license_edit;
        $outlet -> gst_rate = $request -> gst_rate_edit;
        $outlet -> delivery_charges = $request -> delivery_charges_edit;
        // $outlet -> merchant_id = $merchant_id;
        $outlet -> outlet_category_id = $request -> outlet_category_type_edit;
        $outlet -> mobile = $request -> outlet_mobile_edit;
        $outlet -> outlet_geoaddress = $request -> geo_address_edit;
        
        if($request -> address_latitude_edit && $request -> address_latitude_edit != 0){
            $outlet -> outlet_latitude = $request -> address_latitude_edit;
        }
        if($request -> address_longitude_edit && $request -> address_longitude_edit != 0){
            $outlet -> outlet_longitude = $request -> address_longitude_edit;
        }
        
        $outlet -> flag = $request -> flag_edit;
        if ( !empty($filename) ) {
            $outlet -> image_url = json_encode($request->outlet_image_name);
        }
        if( !empty($filename_outlet_str) ) {
            $existing_array = json_decode($outlet->img_url);
            $filename_outlet_str = array_merge($existing_array,$filename_outlet_str);
            $outlet -> img_url = json_encode( $filename_outlet_str );
        }
        $outlet -> outlet_speciality = $request -> outlet_speciality_ed;
        // $outlet -> booking_for_two = $request -> booking_for_two_ed;
        if(isset($request->is_cod_edit)){
            $outlet -> is_cod = 1;
        } else {
            $outlet -> is_cod = 0;
        }

        if($request -> open_time_edit){
            $outlet -> open_time = date('G:i',strtotime($request -> open_time_edit));   
        }
        else{
            $outlet -> open_time = null;
        }
        if($request -> close_time_edit){
            $outlet -> close_time = date('G:i',strtotime($request -> close_time_edit));
        }
        else{
            $outlet -> close_time = null;
        }                

        $outlet -> save();
        //print_r($outlet);
        echo 'save';

    }

    public function delete_outlet(Request $request) {
        $merchant_id = Auth::guard('merchant')->id();
        $outlet = Outlet::find($request->outlet_id);
        $outlet->delete();
        return;
    }

    public function attachement_outlet(Request $request)
    {
        $outlet = Outlet::find($request->outlet_id);
        return response()->json([
                'attachements' => json_decode($outlet->img_url),
            ]);
    }

    public function outlet_check_username(Request $request) {
        // dd( $request );
        // exit;
        if($request->outlet_id != ""){
            $outlet = DB::table('outlets')
                    ->where('outlet_user_name', $request->outlet_user_name)
                    ->where('id', '!=', $request->outlet_id)
                    ->first();
        }
        else {
            $outlet = DB::table('outlets')
                    ->where('outlet_user_name', $request->outlet_user_name)
                    ->first();
        }
        
        
        if ( !empty($outlet) ) {
            return response()->json([
                'message' => 'User already Exist'
            ],200);
        } else {
            return response()->json([
                'message' => 'Username available'
            ],200);
        }

    }

    public function get_all_outlets() {
        $outlets = Outlet::where('merchant_id',Auth::guard('merchant')->id())->get(['id', 'outlet_name']);
        return $outlets;
    }

    public function setFCMToken($token)
    {   
        return $updateToken = Outlet::where('id', Auth::guard('outlet')->id())
              ->update(['firebase_token' => $token]);
    }

    public function destroyAllSession($id)
    {   
        return Outlet::where('id',$id)
                ->update(['session_id'=> null]);
    }
    public function destroyAllAppSession($id)
    {   
        return Outlet::where('id',$id)
                ->update(['app_session_id'=> null, 'app_firebase_token' => null]);
    }

}