<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $valid_ext_array = ["jpeg", "jpg", "png"];
    protected $successCode = '200';
    protected $badRequest = '400';
    protected $NotFound = '404';
    protected $NotAuthorized = '401';
    
    public function store_base64_image($image_name, $base64_image) {

        $tmp_image_name = explode(".", $image_name);
        $filename  = $tmp_image_name[0].'-'.rand(1111,9999) .'.'. $tmp_image_name[ count($tmp_image_name) - 1 ];
        $ext_check = $this->check_image_extension($filename);
        // exit;
            
        if ( $ext_check ) {
            
            if ( preg_match('/^data:image\/(\w+);base64,/', $base64_image) ) {
                
                $size_check = $this->getBase64ImageSize($base64_image);

                if ( $size_check < "1024" ) {

                    $data = substr($base64_image, strpos($base64_image, ',') + 1);

                    $data = base64_decode($data);
                    Storage::disk('public')->put($filename, $data);
                    return $filename;
                    
                } else {
                    return "size_error";
                }
                
            } else {
                return "upload_error";
            }
            
        } else {
            return "ext_error";
        }

    }

    public function getBase64ImageSize($base64Image){ //return memory size in B, KB, MB
        try{
            $size_in_bytes = (int) (strlen(rtrim($base64Image, '=')) * 3 / 4);
            $size_in_kb    = floor( $size_in_bytes / 1024 );
            $size_in_mb    = $size_in_kb / 1024;
    
            return $size_in_kb;
        }
        catch(Exception $e){
            return $e;
        }
    }

    public function check_image_mime_type($image) {
        $image_size = getimagesize($image);
        $tmp_image_name = explode("/", $image_size['mime']);
        $extension = $tmp_image_name[ count($tmp_image_name) - 1 ];
        
        if ( in_array($extension, $this->valid_ext_array) ) {
            return 1;
        } else {
            return 0;
        }

    }

    public function check_image_extension($image_name) {
        
        $tmp_image_name = explode(".", $image_name);
        $extension = $tmp_image_name[ count($tmp_image_name) - 1 ];
        
        if ( in_array($extension, $this->valid_ext_array) ) {
            return 1;
        } else {
            return 0;
        }

    }

    public function deleteFromStorage($image_name) {
        //Storage::disk('public')->delete($image_name);
        return 1;
    }

    public function remove_image(Request $request) {
        $table_name = trim( $request->table_name );
        $column_name = trim( $request->column_name );
        $row_id = trim( $request->row_id );
        $image_name = trim( $request->image_name );
        $image = DB::table($table_name)->where('id', $row_id)->pluck($column_name);
        if ( $image ) {
            $get_image_name = json_decode( $image[0] );
            // print_r($get_image_name);
            if ( $image_name === $get_image_name ) {
                $this->deleteFromStorage($get_image_name);
                $image = DB::table($table_name)->where('id', $row_id)->update(array($column_name=>NULL));
            }else if($image[0] == $image_name){
                $this->deleteFromStorage($image_name);
                $image = DB::table($table_name)->where('id', $row_id)->update(array($column_name=>NULL));
            } 
            
            else {
                $final_image_array = array();
                foreach( $get_image_name as $key => $img ) {
                    // echo $img;
                    // exit;
                    if ( $image_name === $img ) {
                        $this->deleteFromStorage($get_image_name[$key]);
                        // unset($get_image_name[$key]);
                    } else {
                        array_push($final_image_array, $img);
                    }
                }
                $image = DB::table($table_name)->where('id', $row_id)->update(array( $column_name=> json_encode( $final_image_array ) ));
                // exit;
            }
            return true;
        } else {
            return false;
        }
        
        // exit;
    }

    public function dbNulltoString($array) {
        
        

    }

}
