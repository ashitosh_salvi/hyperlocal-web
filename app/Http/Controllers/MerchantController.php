<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merchant;
use App\Outlet;
use App\Orders;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Response;

class MerchantController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    //     $this->middleware('role:ROLE_MERCHANT');
    // }

    protected $redirectTo = '/merchant_dashboard';
    protected $guard = 'merchant';

    public function __construct()
    {
        // $this->middleware('merchant');
    }

    public function showLoginForm(){

        // if(view()->exists('merchant.merchant_login')){
            return view('merchant.merchant_login');
        // }
        // return view('login');
    }

    public function merchantlogin(Request $request) {
        $request->validate([
            'user_name' => 'required|string',
            'password' => 'required|string'
        ],
        [
            'user_name.required' => 'Enter Your Username',
            'password.required' => 'Enter Your Password',
        ]);

        $credentials = request(['user_name', 'password']);

        // dd( Auth::guard($this->guard)->user() );
        if(!Auth::guard($this->guard)->attempt($credentials)) {
            return redirect()->back()->withInput()->withErrors([
                'user_name' => [
                    "message" => "These credentials do not match our records."
                ]
            ]);
        } else {
            return $this->index();
        }


    }

    public function index() {
        return view('merchant.merchant_dashboard');
    }
    
    public function create_merchant(Request $request){
        // dd($request);
        
        // exit;

        $merchant = new Merchant();
        

        $merchant -> merchant_name = $request -> merchant_name;
        $merchant -> user_name = $request -> user_name;
        $merchant -> first_name = $request -> first_name;
        $merchant -> last_name = $request -> last_name;
        $merchant -> email = $request -> email;
        $merchant -> address = $request -> address;
        $merchant -> state = $request -> state;
        $merchant -> city = $request -> city;
        $merchant -> country = $request -> country;
        $merchant -> pin_code = $request -> pin_code;
        $merchant -> contact = $request -> contact;
        $merchant -> password = Hash::make($request -> password);
        $merchant -> status = 0;
        // $merchant -> image_url = $filename;
        $merchant -> flag = 0;
                
        // dd( $request -> no_outlet );
        // exit;
        $merchant->save();
        
        $oulet_count =  $request -> outlet_count -1;
        $cntx = 0;
        
        if ( $request->no_outlet ) {    // If there are no outlets added in merchant
            if ( $oulet_count >= 0 ) {
                for($i = 0; $i <= $oulet_count; $i++ ){
                    $outlet = new Outlet();

                    $filename = '';
                    $imgsft = 'image_url'.$i;
                    $outlet_image_name = $request ->outlet_image_name_.$i;
                    $path = $request->file();
                    if($request->file($imgsft)){
                        $img_temp = $request->file($imgsft);
                        $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
                        $extension = $img_temp->getClientOriginalExtension();
                        $filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
                        // Store image under public folder inside images
                        Storage::disk('public')->put($filename, File::get($img_temp));
                    }

                    //echo $i;
                    $filename_outlet_str = [];
                    $sft = 'document'. $i;

                    // dd( $request );

                    if( !empty($request->file($sft)) ) {

                        $img_temp_outlet = $request->file($sft);

                        foreach($img_temp_outlet as $temp_outlet){
                            // dd( $temp_outlet );
                            // exit;
                            $tmp_out = pathinfo( $temp_outlet -> getClientOriginalName(), PATHINFO_FILENAME);
                            $extension = $temp_outlet -> getClientOriginalExtension();
                            $filename_outlet  = $tmp_out.'-'.rand(1111,9999).'.'.$extension;
                            $image_path_outlet = "images/". $filename_outlet;
                            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                                $image_path_outlet = "public/images/". $filename_outlet;
                            }
                            move_uploaded_file( $temp_outlet, $image_path_outlet);
                            array_push( $filename_outlet_str, $filename_outlet );
                        }

                    }
                
                    // dd( $filename_outlet_str );
                    // exit;

                    $outlet -> outlet_user_name = $request -> outlet_user_name[$i];
                    $outlet -> password = Hash::make($request -> outlet_password[$i]);
                    $outlet -> outlet_type = $request -> outlet_type[$i];
                    $outlet -> branch = $request -> branch[$i];
                    $outlet -> fssai_license = $request -> fssai_license[$i];
                    $outlet -> gst = $request -> gst[ $i ];
                    $outlet -> outlet_address = $request -> outlet_address[$i];
                    $outlet -> outlet_geoaddress = $request -> geo_address_add[$i];
                    $outlet -> outlet_latitude = $request -> address_latitude_add[$i];
                    $outlet -> outlet_longitude = $request -> address_longitude_add[$i];
                    $outlet -> outlet_name = $request -> outlet_name[$i]; 
                    $outlet -> shop_license = $request -> shop_license[$i];
                    $outlet -> gst_rate = $request -> gst_rate[$i];
                    $outlet -> delivery_charges = $request -> delivery_charges[$i];
                    $outlet -> merchant_id = $merchant->id;
                    $outlet -> outlet_category_id = $request -> outlet_category_type[$i];
                    $outlet -> mobile = $request -> outlet_mobile[$i];
                    $outlet -> flag = $request -> flag[$i];
                    $outlet -> outlet_speciality = $request -> outlet_speciality[$i];
                    // $outlet -> booking_for_two = $request -> booking_for_two[$i];
                    $outlet -> image_url = json_encode( $outlet_image_name );
                    $outlet -> img_url = json_encode( $filename_outlet_str );
                    $outlet -> is_cod = 1;

                    if($request -> open_time[$i]){
                        $outlet -> open_time = date('G:i',strtotime($request -> open_time[$i]));   
                    }
                    if($request -> close_time[$i]){
                        $outlet -> close_time = date('G:i',strtotime($request -> close_time[$i]));
                    }

                    $outlet -> save();
                    //print_r($outlet);
                    // echo 'save';
                }
            }
        }

        
        return;

    }

    public function merchant_list(){

        $list = Merchant::all();

        $data = Array(
            'list' => $list
        );
        return view('merchant.merchant_list')->with($data);
    }

    public function get_merchant(Request $request){
       
        $md = Merchant::where('id',$request->id)->first();

        $data = Merchant::Where('merchants.id',$request->id)
            ->join('outlets','outlets.merchant_id','=','merchants.id')->get();
        
        $data->map(function($outlet){
            if($outlet -> open_time){
                $outlet -> open_time = date('h:i A',strtotime($outlet -> open_time));
            }
            if($outlet -> close_time){
                $outlet -> close_time = date('h:i A',strtotime($outlet -> close_time));
            }

        });    

        return Array( 'merchant' =>$md, 'outlet' => $data);

    }

    public function get_merchant_list( Request $request ) {
        
        $columns = array( 
            0 => 'merchant_name',
            1 => 'user_name',
            2 =>'first_name',
            3 =>'email',
            4 =>'state',
            5 =>'country',
            6 =>'action',

        );
        $totalData = Merchant::count();            
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $Merchant = Merchant::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }else {
            $search = $request->input('search.value'); 
 
            $Merchant =  Merchant::where('id','LIKE',"%{$search}%")
                            ->orWhere('first_name', 'LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%")
                            ->orWhere('state', 'LIKE',"%{$search}%")
                            ->orWhere('city', 'LIKE',"%{$search}%")
                            ->orWhere('merchant_name', 'LIKE',"%{$search}%")
                            ->orWhere('user_name', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
 
            $totalFiltered = Merchant::where('id','LIKE',"%{$search}%")
                            ->orWhere('first_name', 'LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%")
                            ->orWhere('state', 'LIKE',"%{$search}%")
                            ->orWhere('city', 'LIKE',"%{$search}%")
                            ->orWhere('merchant_name', 'LIKE',"%{$search}%")
                            ->orWhere('user_name', 'LIKE',"%{$search}%")
                            ->count();
        }
        $data = array();
        if(!empty($Merchant))
        {
            foreach ($Merchant as $key=>$merch)
            {
                
                $nestedData['merchant_name']= $merch->merchant_name;
                $nestedData['user_name']= $merch->user_name;
                $nestedData['first_name'] = $merch->first_name;
                $nestedData['email'] = $merch->email;
                $nestedData['state'] = $merch->state;
                $nestedData['city'] = $merch->city;
                $nestedData['action'] = '<div class="action-group"><a title="View Merchant" class="view btn operations" onclick="getMerchantView('.$merch->id.')" data-toggle="modal" data-target="#viewMerchantModal-1"> <i class="fa fa-eye"></i></a>
                                        <a title="Edit Merchant" class="btn operations" onclick="getMerchant('.$merch->id.')" data-toggle="modal" data-target="#editMerchantModal-1">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a title="Delete Merchant" data-attr="'.$merch->id.'" onclick="delete_merchant('.$merch->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';
                $data[] = $nestedData;
            }
 
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
    }

    public function edit_merchant(Request $request) {
        // dd($request);
        
        // exit;

        $merchant = Merchant::find($request -> merchant_id_edit);
        
        $merchant -> merchant_name = $request -> merchant_name_ed;
        $merchant -> first_name = $request -> first_name_ed;
        $merchant -> last_name = $request -> last_name_ed;
        $merchant -> email = $request -> email_ed;
        $merchant -> address = $request -> address_ed;
        $merchant -> state = $request -> state_ed;
        $merchant -> city = $request -> city_ed;
        $merchant -> country = $request -> country_ed;
        $merchant -> pin_code = $request -> pin_code_ed;
        $merchant -> contact = $request -> contact_ed;
        if( isset($request -> password_ed) && ( $merchant -> password != $request -> password_ed ) ) {
            $merchant -> password = Hash::make($request -> password_ed);
        }
        $merchant -> status = 0;
        // if ( !empty($filename) ) {
        //     $merchant -> image_url = $filename;
        // }
        $merchant -> flag = 0;
        $merchant->save();

        $oulet_count =  $request -> outlet_count_edit;
        $cntx = 0;
        
        if ( $request->no_outlet_edit ) {    // If there are no outlets added in merchant
            if ( $oulet_count >= 0 ) {
                for($i = 0; $i < $oulet_count; $i++ ){


                    if ( $request->outlet_id[ $i ] != null ) {
                        $outlet = Outlet::find($request->outlet_id[ $i ]);
                        if ( isset($request -> outlet_password_ed[$i]) && ( $outlet -> password != $request -> outlet_password_ed[$i] ) ) {
                            $outlet -> password = Hash::make($request -> outlet_password_ed[$i]);
                        }
                    } else {
                        $outlet = new Outlet();
                        if ( isset($request -> outlet_password_ed[$i]) ) {
                            $outlet -> password = Hash::make($request -> outlet_password_ed[$i]);
                        }
                    }

                    $filename = '';
                    $imgsft = 'ed_rest_img'.$i;

                    if($request->file($imgsft)){
                        $img_temp = $request->file($imgsft);
                        $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
                        $extension = $img_temp->getClientOriginalExtension();
                        $filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
                        // Store image under public folder inside images
                        Storage::disk('public')->put($filename, File::get($img_temp));
                    }

                    $filename_outlet_str = [];
                    $sft = 'documented'. $i;

                    // dd( $request->file($sft) );

                    // exit;
                    if( !empty($request->file($sft)) ) {

                        $img_temp_outlet = $request->file($sft);

                        foreach($img_temp_outlet as $temp_outlet){
                            // dd( $temp_outlet );
                            // exit;
                            $tmp_out = pathinfo( $temp_outlet -> getClientOriginalName(), PATHINFO_FILENAME);
                            $extension = $temp_outlet -> getClientOriginalExtension();
                            $filename_outlet  = $tmp_out.'-'.rand(1111,9999).'.'.$extension;
                            $image_path_outlet = "images/". $filename_outlet;
                            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                                $image_path_outlet = "public/images/". $filename_outlet;
                            }
                            move_uploaded_file( $temp_outlet, $image_path_outlet);
                            array_push( $filename_outlet_str, $filename_outlet );
                        }

                    }
                
                    // dd( $filename_outlet_str );
                    // exit;
                    //return $request;
                    $outlet -> outlet_user_name = $request -> outlet_user_name_ed[$i];
                    //$outlet -> password = Hash::make($request -> outlet_password_ed[$i]);
                    $outlet -> outlet_type = $request -> outlet_type_ed[$i];
                    $outlet -> branch = $request -> branch_ed[$i];
                    $outlet -> fssai_license = $request -> fssai_license_ed[$i];
                    $outlet -> gst = $request -> gst_ed[ $i ];
                    $outlet -> outlet_address = $request -> outlet_address_ed[$i];
                    $outlet -> outlet_geoaddress = $request -> ed_geo_address_add[$i];
                    $outlet -> outlet_latitude = $request -> ed_address_latitude_add[$i];
                    $outlet -> outlet_longitude = $request -> ed_address_longitude_add[$i];
                    $outlet -> outlet_name = $request -> outlet_name_ed[$i]; 
                    $outlet -> shop_license = $request -> shop_license_ed[$i];
                    $outlet -> gst_rate = $request -> gst_rate_ed[$i];
                    $outlet -> delivery_charges = $request -> delivery_charges_ed[$i];
                    $outlet -> merchant_id = $request -> merchant_id_edit;
                    $outlet -> outlet_category_id = $request -> outlet_category_type_ed[$i];
                    $outlet -> mobile = $request -> outlet_mobile_ed[$i];
                    $outlet -> flag = $request -> flag_ed[$i];
                    $outlet -> is_cod = 1;
                    if ( !empty($filename) ) {
                        $outlet -> image_url = json_encode( $filename );
                    }
                    if( !empty($filename_outlet_str) ) {
                        $outlet -> img_url = json_encode( $filename_outlet_str );
                    }
                    $outlet -> outlet_speciality = $request -> outlet_speciality_ed[$i];
                    // $outlet -> booking_for_two = $request -> booking_for_two_ed[$i];
                    
                    if($request -> open_time_ed[$i]){
                        $outlet -> open_time = date('G:i',strtotime($request -> open_time_ed[$i]));   
                    }else{
                        $outlet -> open_time = null;
                    }
                    if($request -> close_time_ed[$i]){
                        $outlet -> close_time = date('G:i',strtotime($request -> close_time_ed[$i]));
                    }else{
                        $outlet -> close_time = null;
                    }
                    
                    $outlet->save();

                }
            }
        }
        
        return;
    }

    public function delete_merchant(Request $request) {
        $merchant = Merchant::find($request->merchant_id);
        $outlet = Outlet::where('merchant_id', $request->merchant_id)->delete();
        $merchant->delete();
        return;
    }

    public function check_username(Request $request) {
        // dd( $request );
        // exit;
        if($request->merchant_id != ''){
            $merchant = DB::table('merchants')
                        ->where('id', '!=' , $request->merchant_id)
                        ->where('user_name', $request->user_name)
                        ->first();
        }
        else {
            $merchant = DB::table('merchants')
                        ->where('user_name', $request->user_name)
                        ->first();
        }
        
        
        if ( !empty($merchant) ) {
            return response()->json([
                'message' => 'User already Exist'
            ],200);
        } else {
            return response()->json([
                'message' => 'Username available'
            ],200);
        }

    }
    
    function get_all_outlet_categories(Request $req) {
        if($req->outlet_type){
            $outlet_type = $req->outlet_type;
            $outlet_categories = DB::table('outlet_categories')->where('outlet_type',$outlet_type)->get(['id', 'category_name']);
            return $outlet_categories;
        }
        $outlet_categories = DB::table('outlet_categories')->get(['id', 'category_name']);
        return $outlet_categories;
    }

    function get_all_merchants() {
        $merchant = Merchant::get(['id', 'merchant_name']);
        return $merchant;
    }

    public function admin_merchant_report(Request $request)
    {
        // $riders = Rider::where('status', 1)->get();
        return view( 'reports.admin_merchant_report');
    }

    public function get_admin_merchant_report(Request $request)
    {
        $merchant = array();
        $columns = array( 
            0 =>'merchant_name',
            1 =>'contact'
        );
        $totalData = DB::table('merchants')
                        ->select('id','merchant_name','contact')
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        
        if(empty($request->input('search.value')))
        {            
            $merchant = DB::table('merchants')
                        ->select('id','merchant_name','contact')
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }else{

            $search = $request->input('search.value'); 

            $merchant = DB::table('merchants')
                        ->select('id','merchant_name','contact')
                        ->where('merchant_name','LIKE',"%{$search}%")
                        ->orWhere('contact', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = DB::table('merchants')
                            ->select('id','merchant_name','contact')
                            ->where('merchant_name','LIKE',"%{$search}%")
                            ->orWhere('contact', 'LIKE',"%{$search}%")
                            ->count();
        }
        
        if(!empty($merchant))
        {   
            foreach ($merchant as $key=>$single_merchant)
            {
                $single_merchant->order_count = Orders::where('merchant_id',$single_merchant->id)           
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])
                                    ->count();
                $single_merchant->total_amount = Orders::where('merchant_id',$single_merchant->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])
                                    ->sum('final_amount');
            }
        }
        
        $data = array();
        if(!empty($merchant))
        {
            foreach ($merchant as $key=>$merchant)
            {
                $nestedData['outlet_name'] = $merchant->merchant_name;
                $nestedData['outlet_user_name'] = $merchant->contact;
                $nestedData['count_of_order'] = '<a href="javascript:void(0);" onclick="viewMerchantDetails('.$merchant->id.',\''.$merchant->merchant_name.'\')">'.$merchant->order_count.'</a>';
                $nestedData['value_of_order'] = round($merchant->total_amount,2);
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
    }
    
    public function get_admin_merchant_details_report(Request $request,$merchant_id = null)
    {   
        $outlets = array();
        $columns = array( 
            0 =>'outlet_name'
        );
        $totalData = Outlet::where('merchant_id',$merchant_id)
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        
        if(empty($request->input('search.value')))
        {            
            $outlets = DB::table('outlets')
                        ->select('id','outlet_name')
                        ->where('merchant_id',$merchant_id)
                        ->offset($start)
                        ->limit($limit)
                        // ->orderBy($order,$dir)
                        ->get();
        }else{

            $search = $request->input('search.value'); 

            $outlets = DB::table('outlets')
                        ->select('id','outlet_name')
                        ->where('merchant_id',$merchant_id)
                        ->where('merchant_name','LIKE',"%{$search}%")
                        ->orWhere('contact', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        // ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = DB::table('outlets')
                            ->select('id','outlet_name')
                            ->where('merchant_id',$merchant_id)
                            ->where('merchant_name','LIKE',"%{$search}%")
                            ->orWhere('contact', 'LIKE',"%{$search}%")
                            ->count();
        }

        if(!empty($outlets))
        {   
            foreach ($outlets as $key=>$single_outlets)
            {
                $single_outlets->order_count = Orders::where('outlet_id',$single_outlets->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date]) 
                                    ->count();
                $single_outlets->total_amount = Orders::where('outlet_id',$single_outlets->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])
                                    ->sum('final_amount');
            }
        }
        $data = array();
        if(!empty($outlets))
        {
            foreach ($outlets as $key=>$outlet)
            {
                $nestedData['outlet_name']      = $outlet->outlet_name;
                $nestedData['count_of_order']   = $outlet->order_count;
                $nestedData['value_of_order']   = round($outlet->total_amount,2);
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
    }

    public function export_get_admin_merchant_report($from_date = null, $to_date = null,$search = null)
    {
        $filename = 'hyperlocal_order_history_'.$from_date.'_'.$to_date;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename.".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Merchant Name','Phone Number','Count Of Order','Value Of Order');

        $callback = function() use ($from_date,$to_date,$search,$columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            
            if(empty($search))
            {            
                $merchant = DB::table('merchants')
                            ->select('id','merchant_name','contact')
                            ->get();
            }else{

                $merchant = DB::table('merchants')
                            ->select('id','merchant_name','contact')
                            ->where('merchant_name','LIKE',"%{$search}%")
                            ->orWhere('contact', 'LIKE',"%{$search}%")
                            ->get();
            }
            
            if(!empty($merchant))
            {   
                foreach ($merchant as $key=>$single_merchant)
                {
                    $single_merchant->order_count = Orders::where('merchant_id',$single_merchant->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])  
                                    ->count();
                    $single_merchant->total_amount = Orders::where('merchant_id',$single_merchant->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])
                                    ->sum('final_amount');
                }
            }
            
            $data = array();
            if(!empty($merchant))
            {
                foreach ($merchant as $key=>$merchant)
                {
                    $nestedData['outlet_name'] = $merchant->merchant_name;
                    $nestedData['outlet_user_name'] = $merchant->contact;
                    $nestedData['count_of_order'] = $merchant->order_count;
                    $nestedData['value_of_order'] = round($merchant->total_amount,2);
                    $data[] = $nestedData;
                    fputcsv($file,$nestedData);
                }
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
        
    }

    public function export_get_admin_merchant_details_report($from_date = null, $to_date = null,$merchant_id = null)
    {   
        $merchant = Merchant::where('id',$merchant_id)->first();
        $filename = $merchant->merchant_name.'_'.$from_date.'_'.$to_date;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename.".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Outlet Name','Count Of Order','Value Of Order');

        $callback = function() use ($from_date,$to_date,$merchant_id,$columns)
        {   
            
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
                      
            $outlets = DB::table('outlets')
                        ->select('id','outlet_name')
                        ->where('merchant_id',$merchant_id)
                        ->get();
            

            if(!empty($outlets))
            {   
                foreach ($outlets as $key=>$single_outlets)
                {
                    $single_outlets->order_count = Orders::where('outlet_id',$single_outlets->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])
                                    ->count();
                    $single_outlets->total_amount = Orders::where('outlet_id',$single_outlets->id)
                                    ->whereRaw('date(created_at) >= ?',[$from_date])
                                    ->whereRaw('date(created_at) <= ?',[$to_date])
                                    ->sum('final_amount');
                }
            }

            $data = array();
            if(!empty($outlets))
            {
                foreach ($outlets as $key=>$outlet)
                {
                    $nestedData['outlet_name']      = $outlet->outlet_name;
                    $nestedData['count_of_order']   = $outlet->order_count;
                    $nestedData['value_of_order']   = round($outlet->total_amount,2);
                    $data[] = $nestedData;
                    fputcsv($file,$nestedData);
                }
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
        
    }

    //  public function get_admin_merchant_report(Request $request)
    // {
    //     $merchant = array();
    //     $totalData = DB::table('merchants')
    //                     ->join('outlets','outlets.merchant_id','=','merchants.id')
    //                     ->select('merchants.merchant_name','merchants.contact','outlets.id','outlets.outlet_name')
    //                     ->count();
    //     $totalFiltered = $totalData;
    //     $limit = $request->input('length');
    //     $start = $request->input('start');
        
    //     if(empty($request->input('search.value')))
    //     {            
    //         $merchant = DB::table('merchants')
    //                     ->join('outlets','outlets.merchant_id','=','merchants.id')
    //                     ->select('merchants.merchant_name','merchants.contact','outlets.id','outlets.outlet_name')
    //                     ->offset($start)
    //                     ->limit($limit)
    //                     ->get();
    //     }else{

    //         $search = $request->input('search.value'); 

    //         $merchant = DB::table('merchants')
    //                     ->join('outlets','outlets.merchant_id','=','merchants.id')
    //                     ->select('merchants.merchant_name','merchants.contact','outlets.id','outlets.outlet_name')
    //                     ->where('merchant_name','LIKE',"%{$search}%")
    //                     ->orWhere('contact', 'LIKE',"%{$search}%")
    //                     ->offset($start)
    //                     ->limit($limit)
    //                     ->get();

    //         $totalFiltered = DB::table('merchants')
    //                         ->join('outlets','outlets.merchant_id','=','merchants.id')
    //                         ->select('merchants.merchant_name','merchants.contact','outlets.id','outlets.outlet_name')
    //                         ->where('merchant_name','LIKE',"%{$search}%")
    //                         ->orWhere('contact', 'LIKE',"%{$search}%")
    //                         ->count();
    //     }
        
    //     if(!empty($merchant))
    //     {   
    //         foreach ($merchant as $key=>$single_merchant)
    //         {
    //             $single_merchant->order_count = Orders::where('outlet_id',$single_merchant->id)->count();
    //             $single_merchant->total_amount = Orders::where('outlet_id',$single_merchant->id)
    //                                 ->sum('total_amount');
    //         }
    //     }
        
    //     $data = array();
    //     if(!empty($merchant))
    //     {
    //         foreach ($merchant as $key=>$merchant)
    //         {
    //             $nestedData['outlet_name'] = $merchant->merchant_name;
    //             $nestedData['outlet_user_name'] = $merchant->contact;
    //             // $nestedData['outlet_type'] = $merchant->outlet_name;
    //             $nestedData['count_of_order'] = $merchant->order_count;
    //             $nestedData['value_of_order'] = $merchant->total_amount;
    //             $data[] = $nestedData;
    //         }
    //     }
    //     $json_data = array(
    //         "draw"            => intval($request->input('draw')),  
    //         "recordsTotal"    => intval($totalData),  
    //         "recordsFiltered" => intval($totalFiltered), 
    //         "data"            => $data   
    //     );

    //     echo json_encode($json_data);
    // }

}