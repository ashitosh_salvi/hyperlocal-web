<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
    //
    public function get_all_states() {
        $states = State::get(['id', 'state_name']);
        // dd( $states );
        return response()->json([
            'states' => $states
        ]);
    }

    public function get_state_by_name(Request $request) {
        $state_name = $request->state_name;
        $state = State::where('state_name', $state_name)->get(['id', 'state_name']);
        // dd($state);
        return response()->json([
            'state' => $state
        ]);
    }
    
}
