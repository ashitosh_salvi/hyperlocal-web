<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Food_category;
use App\Category;
use App\Food_section;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MenuItemController extends Controller
{
    public function create_menuitem(Request $request){
        $outlet_id = Auth::guard('outlet')->id();
        
        $menu_item = new Recipe(); 

        $filename = '';
        $path = $request->file('image_url_ed');
        if($request->image_url_ed){
            $img_temp = $request->image_url_ed;
            //$tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            //$filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            //Storage::disk('public')->put($filename, File::get($img_temp));
            // $filename = $this->store_base64_image('menuitem.jpeg',$img_temp);
            // $this->compressImage($img_temp,);

            // Getting file name
            $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp->getClientOriginalExtension();
            $filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Valid extension
            $valid_ext = array('png','jpeg','jpg');

            // Location
            $location = "images/".$filename;
            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                $location = "public/images/".$filename;
            }

            // file extension
            $file_extension = pathinfo($location, PATHINFO_EXTENSION);
            $file_extension = strtolower($file_extension);

            // Check extension
            if(in_array($file_extension,$valid_ext)){

                // Compress Image
                $this->compressImage($_FILES['image_url_ed']['tmp_name'],$location,50);

            }
        }
        


        $menu_item->outlet_id = $outlet_id;
        $menu_item->name = $request->name_ed;
        $menu_item->description = $request->description_ed;
        $menu_item->category_type = $request->category_type_ed;
        $menu_item->category_id = $request->category_id_ed;
        $menu_item->sub_category_id = $request->sub_category_id_ed;
        $menu_item->section = $request->section_ed;
        $menu_item->food_cat = $request->food_cat_ed;
        $menu_item->add_info = json_encode($request->add_info);
        $menu_item->availability = json_encode($request->availability);
        $menu_item->from_time = $request->from_time_ed;
        $menu_item->to_time = $request->to_time_ed;
        $menu_item->kot_group = $request->kot_group_ed;
        $menu_item->price = $request->price_ed;
        $menu_item->charges_gst = $request->charges_gst_ed;
        // $menu_item->special_instruction = $request->special_instruction;
        $menu_item->serving_time = $request->serving_time_ed;
        $menu_item->serves = $request->serves_ed;
        $menu_item->status = 1;
        $menu_item->flag = 0;
        $menu_item->image_url = $filename;

        if($request->from_time){
            $menu_item->from_time = date('G:i',strtotime($request->from_time));   
        }
        if($request->to_time){
            $menu_item->to_time = date('G:i',strtotime($request->to_time));
        }                 

        $menu_item->save();

        return ;


    }

    public function compressImage($source, $destination, $quality){

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

    }

    public function get_menuitem_list(Request $request){

        $outlet_id = Auth::guard('outlet')->id();

        if ( $outlet_id != null ) {

            // $list = Recipe::where('outlet_id', $outlet_id)->get();

            $columns = array( 
                0 =>'name',
                1 =>'section',
                2 =>'category_name',
                3 =>'spice_level',
                4 =>'from',
                5 =>'to',
                6 =>'price',
                7 =>'status',
                8 =>'action',
            );  
            $totalData = DB::table('recipes')
                        ->select('recipes.*','categories.category_name','food_category.id as food_cat_id','food_category.name as food_cat_name','food_section.id as food_section_id', 'food_section.name as food_section_name')
                        ->join('food_category','food_category.id','=','recipes.food_cat')
                        ->join('food_section','food_section.id','=','recipes.section')
                        ->join('categories','categories.id','=','recipes.category_id')
                        ->where('recipes.outlet_id', $outlet_id)->count();            
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            
            if(empty($request->input('search.value')))
            {            
                $menu_items = DB::table('recipes')
                                ->select('recipes.*','categories.category_name','food_category.id as food_cat_id','food_category.name as food_cat_name','food_section.id as food_section_id', 'food_section.name as food_section_name')
                                ->join('food_category','food_category.id','=','recipes.food_cat')
                                ->join('food_section','food_section.id','=','recipes.section')
                                ->join('categories','categories.id','=','recipes.category_id')
                                ->where('recipes.outlet_id', $outlet_id)
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
            }else {
                $search = $request->input('search.value'); 
    
                $menu_items =  DB::table('recipes')
                                ->select('recipes.*','categories.category_name','food_category.id as food_cat_id','food_category.name as food_cat_name','food_section.id as food_section_id', 'food_section.name as food_section_name')
                                ->join('food_category','food_category.id','=','recipes.food_cat')
                                ->join('food_section','food_section.id','=','recipes.section')
                                ->join('categories','categories.id','=','recipes.category_id')
                                ->where('recipes.outlet_id', $outlet_id)
                                ->where(function($query) use ($search){
                                    $query->where('recipes.name','LIKE',"%".$search."%")
                                        ->orWhere('food_section.name', 'LIKE',"%".$search."%")
                                        ->orWhere('food_category.name', 'LIKE',"%".$search."%")
                                        ->orWhere('recipes.spice_level', 'LIKE',"%".$search."%")
                                        ->orWhere('recipes.from_time', 'LIKE',"%".$search."%")
                                        ->orWhere('recipes.to_time', 'LIKE',"%".$search."%")
                                        ->orWhere('recipes.price', 'LIKE',"%".$search."%");
                                })
                                
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
    
                $totalFiltered = DB::table('recipes')
                                ->select('recipes.*','categories.category_name','food_category.id as food_cat_id','food_category.name as food_cat_name','food_section.id as food_section_id', 'food_section.name as food_section_name')
                                ->join('food_category','food_category.id','=','recipes.food_cat')
                                ->join('food_section','food_section.id','=','recipes.section')
                                ->join('categories','categories.id','=','recipes.category_id')
                                ->where('recipes.outlet_id', $outlet_id)
                                ->orWhere('recipes.name','LIKE',"%{$search}%")
                                ->orWhere('recipes.section', 'LIKE',"%{$search}%")
                                ->orWhere('recipes.food_cat', 'LIKE',"%{$search}%")
                                ->orWhere('recipes.spice_level', 'LIKE',"%{$search}%")
                                ->orWhere('recipes.from_time', 'LIKE',"%{$search}%")
                                ->orWhere('recipes.to_time', 'LIKE',"%{$search}%")
                                ->orWhere('recipes.price', 'LIKE',"%{$search}%")
                                ->count();
            }

        } 

           
        $data = array();
        if(!empty($menu_items))
        {
            foreach ($menu_items as $key=>$menu_item_v)
            {
                $nestedData['name'] = $menu_item_v->name;
                $nestedData['section_id'] = $menu_item_v->section;
                $nestedData['section'] = $menu_item_v->food_section_name;
                $nestedData['food_cat_id'] = $menu_item_v->food_cat;
                $nestedData['category_name'] = $menu_item_v->category_name;
                $nestedData['spice_level'] = $menu_item_v->spice_level;
                $nestedData['from'] = $menu_item_v->from_time ? date('h:i A',strtotime($menu_item_v->from_time)) : '';
                $nestedData['to'] = $menu_item_v->to_time ? date('h:i A',strtotime($menu_item_v->to_time)) : '';
                $nestedData['price'] = $menu_item_v->price;
                // $nestedData['status'] = '<input class="tgl tgl-ios tgl_checkbox" data-id="'.$menu_item_v->id.'" id="cb_'.$menu_item_v->id.'" type="checkbox" checked=""><label class="tgl-btn" for="cb_'.$menu_item_v->id.'"></label>';
                $nestedData['status']= '<input class="tgl tgl-ios tgl_checkbox" id="cb_'. $menu_item_v->id .'" name="menuitem_status" type="checkbox" data-attr="'. $menu_item_v->status .'" '. ( ( $menu_item_v->status == '1' ) ? "checked" : "" ) .' menuitem-id="'. $menu_item_v->id .'">
                <label class="tgl-btn" for="cb_'. $menu_item_v->id .'"></label>';
                $nestedData['action'] = '<div class="action-group"><a title="View Menu Item" class="view btn operations" onclick="getMenuItemView('.$menu_item_v->id.')" data-toggle="modal" data-target="#viewMenuItemModal-1"> <i class="fa fa-eye"></i></a><a title="Edit Menu Item" class="btn operations" onclick="getMenuItem('.$menu_item_v->id.')" data-toggle="modal" data-target="#editMenuItemModal-1"><i class="fa fa-edit"></i></a><a title="Delete Menu Item" data-attr="'.$menu_item_v->id.'" onclick="delete_menuitem('.$menu_item_v->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

        echo json_encode($json_data);
 
    }
    public function get_menuitem(Request $request){

        $pd = Recipe::where('id',$request->id)->first();
        $data = DB::table('recipes')
        ->select('recipes.*','categories.category_name','food_category.id as food_cat_id','food_category.name as food_cat_name','food_section.id as food_section_id', 'food_section.name as food_section_name')
        ->join('food_category','food_category.id','=','recipes.food_cat')
        ->join('food_section','food_section.id','=','recipes.section')
        ->join('categories','categories.id','=','recipes.category_id')
        ->where('recipes.id', $request->id)->first();
        $sc='';
        if($data){
            if($data->sub_category_id){
                $sc = Category::where('id',$data->sub_category_id)->first(['category_name']);
            }
            if($data->from_time){
                $data->from_time = date('h:i A',strtotime($data -> from_time));
            }   
            if($data->to_time){
                $data->to_time = date('h:i A',strtotime($data -> to_time));
            } 
        }

        return Array( 'menuitem' =>$data ,'old-data no use' => $pd ,'sub' => $sc);
    }

    public function menuitem_list(){

        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $food_cat = Food_category::all();
        $food_section = Food_section::all();
        $main_category = Category::where('outlet_id',$outlet_id)
                                ->where('outlet_type',$outlet_type)
                                ->where('priority', 1 )
                                ->get();
                                
        $data = Array(
            'food_cat' =>$food_cat,
            'food_section' => $food_section,
            'main_category' => $main_category
        );
        return view('merchant.menu_item_list')->with($data);
    }

    public function convert_revert_time($value)
    {
        $arr_time = explode(":",$value);
        //echo "<pre>"; print_r($arr_time); echo "</pre>";

        $time = "";

        if(isset($arr_time[0]) && $arr_time[0] != '' && isset($arr_time[1]) && $arr_time[1] != '')
        {
            $hr = $arr_time[0];
            $min = $arr_time[1];

            if($hr >= 12)
            {  $format = "PM"; }
            else
            {  $format = "AM"; }

            if($hr > 12)
            {  $hr = $hr - 12; }

            $time = $hr.":".$min." ".$format;            
        }

        return $time;
    }

    public function menuitem_grid(){

        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $food_cat = Food_category::all();
        $food_section = Food_section::all();
        $main_category = Category::where('outlet_id',$outlet_id)
                                ->where('outlet_type',$outlet_type)
                                ->where('priority', 1 )
                                ->get();

        $all_category_types = Category::select("category_type")
                                ->where('outlet_id',$outlet_id)
                                ->where('outlet_type',$outlet_type)
                                ->groupBy('category_type')
                                ->orderBy('category_type','DESC')
                                ->pluck('category_type')->toArray();
        
        $category_data = array();

        if(isset($all_category_types) && $all_category_types !='')
        {
            foreach ($all_category_types as $Catkey => $category_type) {            

                //echo "<hr>";
                //echo "<pre>"; print_r($category_type); echo "</pre>";                

                $all_parent_categories = Category::select("id as category_id","category_name")
                                    ->where('outlet_id',$outlet_id)
                                    ->where('outlet_type',$outlet_type)
                                    ->where('category_type',$category_type)
                                    ->where('status', 1)
                                    ->where(function($query) {
                                           return $query->whereNull('parent_category')
                                                ->orWhere('parent_category', '=', '');
                                    })
                                    ->get()->toArray();
                
                //echo "<pre>"; print_r($all_parent_categories); echo "</pre>";

                if(isset($all_parent_categories) && $all_parent_categories !='')
                {
                    foreach ($all_parent_categories as $ParentCatkey => $cat_data) {
                        
                        $category_id = $cat_data['category_id'];

                        //----parent_cat_menu---//

                        $parent_menus = DB::table('recipes')
                                ->select('recipes.id','recipes.name','recipes.status')
                                ->join('food_category','food_category.id','=','recipes.food_cat')
                                ->join('food_section','food_section.id','=','recipes.section')
                                ->join('categories','categories.id','=','recipes.category_id')
                                ->where('recipes.outlet_id', $outlet_id)
                                ->where('recipes.category_id', $category_id)
                                ->where(function($query) {
                                   return $query->whereNull('recipes.sub_category_id')
                                        ->orWhere('recipes.sub_category_id', '=', 0);
                                })
                                ->get()->toArray();  

                        //echo "<pre>"; print_r($parent_menus); echo "</pre>";
                        $all_parent_categories[$ParentCatkey]['parent_menus'] = $parent_menus;

                        //----sub_cat----//

                        $all_sub_categories = Category::select("id as sub_category_id","category_name")
                                    ->where('outlet_id',$outlet_id)
                                    ->where('outlet_type',$outlet_type)
                                    ->where('category_type',$category_type)
                                    ->where('parent_category',$category_id)
                                    ->where('status', 1)
                                    ->get()->toArray(); 

                        //echo "<pre>"; print_r($all_sub_categories); echo "</pre>";
                        $all_parent_categories[$ParentCatkey]['sub_category'] = $all_sub_categories;

                        if(isset($all_sub_categories) && $all_sub_categories !='')
                        {
                            foreach ($all_sub_categories as $SubCatkey => $sub_cat_data) {
                                
                                $sub_category_id = $sub_cat_data['sub_category_id'];

                                //----sub_cat_menu---//

                                $sub_menus = DB::table('recipes')
                                        ->select('recipes.id','recipes.name','recipes.status')
                                        ->join('food_category','food_category.id','=','recipes.food_cat')
                                        ->join('food_section','food_section.id','=','recipes.section')
                                        ->join('categories','categories.id','=','recipes.category_id')
                                        ->where('recipes.outlet_id', $outlet_id)
                                        ->where('recipes.category_id', $category_id)
                                        ->where('recipes.sub_category_id',$sub_category_id)
                                        ->get()->toArray(); 

                                //echo "<pre>"; print_r($sub_menus); echo "</pre>";

                                $all_parent_categories[$ParentCatkey]['sub_category'][$SubCatkey]['sub_menus'] = $sub_menus; 
                            }
                        }

                        //----------------//                                      

                        
                        
                    }
                }

                $category_data[] = array("cat_type" => $category_type, "parent_categories" => $all_parent_categories);
            }
        }

        //echo "<pre>"; print_r($category_data); echo "</pre>"; exit();
                                
        $data = Array(
            'food_cat' =>$food_cat,
            'food_section' => $food_section,
            'main_category' => $main_category,
            'category_data' => $category_data,
        );
        
        return view('merchant.menu_item_grid')->with($data);
    }

    public function convert_time($value)
    {
        $actual_time = "00:00:00";

        //echo "<hr>"; echo "<pre>"; print_r($value); echo "</pre>";                 

        $arr_time_format = explode(" ",$value);
        
        if(isset($arr_time_format[0]))
        {
            $time = $arr_time_format[0];

            $arr_time = explode(":",$time);

            if(isset($arr_time[0]) && $arr_time[0] !='' && isset($arr_time[1]) && $arr_time[1] !='')
            {               
                $hr = $arr_time[0];
                $min = $arr_time[1];

                if (strpos($value, 'pm') !== false || strpos($value, 'PM') !== false) 
                {
                    if($hr == 12){}
                    else{$hr = 12 + $hr;}
                }

                $actual_time = $hr.":".$min.":00"; 
            }
        }           
        

        return $actual_time;
    }

    public function edit_menuitem(Request $request){
        
        $menu_item = Recipe::find($request -> menuitem_id);
        
        $filename = '';
        $path = $request->file('image_url_ed');
        if($request->image_url_ed){
            $img_temp = $request->image_url_ed;
            // $tmp_out = pathinfo( $img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            // $extension = $img_temp->getClientOriginalExtension();
            // $filename  = $tmp_out.'-'.rand(1111,9999) .'.'. $extension;
            // Store image under public folder inside images
            // Storage::disk('public')->put($filename, File::get($img_temp));
            $filename = $this->store_base64_image('menuitem.jpeg',$img_temp);
            $menu_item -> image_url = $filename;
        }
        // print_r($request->add_info_ed);exit();
        $menu_item -> name = $request -> name_ed;
        $menu_item -> description = $request -> description_ed;
        $menu_item -> category_type = $request -> category_type_ed;
        $menu_item -> category_id = $request -> category_id_ed;
        $menu_item -> sub_category_id = $request -> sub_category_id_ed;
        $menu_item -> section = $request -> section_ed;
        $menu_item -> food_cat = $request -> food_cat_ed;
        $menu_item -> add_info = json_encode($request -> add_info_ed);
        $menu_item -> availability = json_encode($request -> availability_ed);

        if($request -> from_time_ed){
            $menu_item -> from_time = date('G:i',strtotime($request -> from_time_ed));   
        }else{
            $menu_item -> from_time = null;
        }
        if($request -> to_time_ed){
            $menu_item -> to_time = date('G:i',strtotime($request -> to_time_ed));
        }else{
            $menu_item -> to_time = null;
        }                 
        
        $menu_item -> spice_level = $request -> spice_level_ed;
        $menu_item -> kot_group = $request -> kot_group_ed;
        $menu_item -> price = $request -> price_ed;
        $menu_item -> charges_gst = $request -> charges_gst_ed;
        // $menu_item -> special_instruction = $request -> special_instruction_ed;
        $menu_item -> serving_time = $request -> serving_time_ed;
        $menu_item -> serves = $request -> serves_ed;
        
        

        $menu_item->save();

        return ;


    }

    public function get_recipe_cat_by_type(Request $request) {

        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;
        $category = DB::select('select id, parent_category, category_name from categories where parent_category IS NULL and category_type = ? and outlet_id = ? and outlet_type = ?', [$request->category_type,$outlet_id, $outlet_type]);
        return response()->json([
            'category' => $category
        ]);
    }

    public function get_recipe_subcat_by_cat_id(Request $request) {
        
        $cat_id = $request->cat_id;
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $sub_category = Category::where('parent_category', $cat_id)
                                ->where('priority', 2)
                                ->where('outlet_id', $outlet_id)
                                ->where('outlet_type', $outlet_type)
                                ->get(['id', 'category_name']);
        return response()->json([
            'sub_category' => $sub_category
        ]);
    }

    public function delete_menuitem(Request $request) {
        $menuitem = Recipe::find($request->menuitem_id);
        $menuitem->delete();
        return;
    }

    public function update_menuitem_status(Request $request) {
        $menuitem = Recipe::find($request->menuitem_id);

        if($request->menuitem_status == 0){
            $menuitem->status = $request->menuitem_status;
            $menuitem->flag = 1;
        }
        else{
            $menuitem->status = $request->menuitem_status;
            $menuitem->flag = 0;
        }

        $menuitem->save();
        return;
    }


}