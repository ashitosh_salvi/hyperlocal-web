<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Outlet;
use App\Roles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{

    public function index(){

        // dd( Auth::guard('merchant')->id() );

        $merchant_id = Auth::guard('merchant')->id();

        if ( $merchant_id != null ) {

            $list = Employee::where('merchant_id', $merchant_id)->get();

        } else {

            $list = Employee::all();
        }

        $data = Array(
            'list' => $list
        );

        return view('employee.employee_list')->with($data);
    }
    
    public function create_employee(Request $request){

        $merchant_id = Auth::guard('merchant')->id();
        $employee = new Employee();
        $filename = '';
        $path = $request->file('image_url');
        if($request->file('image_url')){
            $img_temp = $request->file('image_url');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            // $image_path = "images/".$filename
            // move_uploaded_file($img_temp,$image_path);
            // Valid extension
            $valid_ext = array('png','jpeg','jpg');
            // Location
            $location = "images/".$filename;

            // file extension
            $file_extension = pathinfo($location, PATHINFO_EXTENSION);
            $file_extension = strtolower($file_extension);

            // Check extension
            if(in_array($file_extension,$valid_ext)){
                // Compress Image
                $this->compressImage($_FILES['image_url_ed']['tmp_name'],$location,50);
            }
        }
        //return $request;
        $employee -> merchant_id = $merchant_id;
        $employee -> first_name = $request -> first_name;
        $employee -> last_name = $request -> last_name;
        $employee -> email = $request -> email;
        $employee -> address = $request -> address;
        $employee -> city = $request -> city;
        $employee -> state = $request -> state;
        $employee -> country = $request -> country;
        $employee -> pin_code = $request -> pin_code;
        $employee -> contact = $request -> contact;
        $employee -> pin = $request -> emp_pin;
        $employee -> role = $request -> role;
        $employee -> status = 1;
        $employee -> image_url = $filename;
        $employee -> flag = 0;
                
        $employee->save();
        
        echo 'save';
        return;

    }

    public function compressImage($source, $destination, $quality){

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

    }

    public function get_employee_list(Request $request){

        $merchant_id = Auth::guard('merchant')->id();

        // echo $merchant_id ;
        // exit;

        if ( $merchant_id != null ) {

            // $list = Employee::where('merchant_id', $merchant_id)->get();

            $columns = array( 
                0 =>'first_name',
                1 =>'last_name',
                2 =>'role',
                3 =>'email',
                4 =>'state',
                5 =>'city',
                5 =>'action',
            );  
            $totalData = Employee::where('merchant_id', $merchant_id)->count();            
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            
            if(empty($request->input('search.value')))
            {            
                $Employee = Employee::where('merchant_id', $merchant_id)->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            }else {
                $search = $request->input('search.value'); 
    
                $Employee =  Employee::where('merchant_id', $merchant_id)
                                    ->where(function($query) use ($search){
                                    $query->where('id','LIKE',"%{$search}%")
                                        ->orWhere('first_name', 'LIKE',"%{$search}%")
                                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                                        ->orWhere('role', 'LIKE',"%{$search}%")
                                        ->orWhere('email', 'LIKE',"%{$search}%")
                                        ->orWhere('state', 'LIKE',"%{$search}%")
                                        ->orWhere('city', 'LIKE',"%{$search}%");
                                    })
                                    ->offset($start)
                                    ->limit($limit)
                                    ->orderBy($order,$dir)
                                    ->get();
    
                $totalFiltered = Employee::where('merchant_id', $merchant_id)
                                        ->where(function($query) use ($search){
                                            $query->where('id','LIKE',"%{$search}%")
                                                ->orWhere('first_name', 'LIKE',"%{$search}%")
                                                ->orWhere('last_name', 'LIKE',"%{$search}%")
                                                ->orWhere('role', 'LIKE',"%{$search}%")
                                                ->orWhere('email', 'LIKE',"%{$search}%")
                                                ->orWhere('state', 'LIKE',"%{$search}%")
                                                ->orWhere('city', 'LIKE',"%{$search}%");
                                        })
                                
                                ->count();
            }

        } else {

            // $list = Employee::all();
 
            $columns = array( 
                    0 =>'first_name',
                    1 =>'last_name',
                    2 =>'role',
                    3 =>'email',
                    4 =>'state',
                    5 =>'city',
                    5 =>'action',
                );  
                $totalData = Employee::count();            
                $totalFiltered = $totalData;
                $limit = $request->input('length');
                $start = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                
                if(empty($request->input('search.value')))
                {            
                    $Employee = Employee::offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
                }else {
                    $search = $request->input('search.value'); 
        
                    $Employee =  Employee::where('id','LIKE',"%{$search}%")
                                    ->where(function($query) use ($search){
                                        $query->where('first_name', 'LIKE',"%{$search}%")
                                            ->orWhere('last_name', 'LIKE',"%{$search}%")
                                            ->orWhere('role', 'LIKE',"%{$search}%")
                                            ->orWhere('email', 'LIKE',"%{$search}%")
                                            ->orWhere('state', 'LIKE',"%{$search}%")
                                            ->orWhere('city', 'LIKE',"%{$search}%");
                                    })
                                    ->offset($start)
                                    ->limit($limit)
                                    ->orderBy($order,$dir)
                                    ->get();
        
                    $totalFiltered = Employee::where('id','LIKE',"%{$search}%")
                                        ->where(function($query) use ($search){
                                            $query->where('first_name', 'LIKE',"%{$search}%")
                                                ->orWhere('last_name', 'LIKE',"%{$search}%")
                                                ->orWhere('role', 'LIKE',"%{$search}%")
                                                ->orWhere('email', 'LIKE',"%{$search}%")
                                                ->orWhere('state', 'LIKE',"%{$search}%")
                                                ->orWhere('city', 'LIKE',"%{$search}%");
                                        })
                                        ->count();
                }
                
            }   
            
            $data = array();
            if(!empty($Employee))
            {
                foreach ($Employee as $key=>$employe)
                {
                    
                    $nestedData['first_name'] = $employe->first_name;
                    $nestedData['last_name'] = $employe->last_name;
                    $nestedData['role'] = $this->employee_role_by_id($employe->role);
                    $nestedData['email'] = $employe->email;
                    $nestedData['state'] = $employe->state;
                    $nestedData['city'] = $employe->city;
                    $nestedData['status']= '<input class="tgl tgl-ios tgl_checkbox" id="cb_'. ( $employe->id + 23 ) .'" name="employee_status" type="checkbox" data-attr="'. $employe->status .'" '. ( ( $employe->status == '1' ) ? "checked" : "" ) .' employee-id="'. $employe->id .'"><label class="tgl-btn" for="cb_'. ( $employe->id + 23 ) .'"></label>';
                    $nestedData['action'] = '<div class="action-group"><a title="View Employee" class="view btn operations" onclick="getEmployeeView('.$employe->id.')" data-toggle="modal" data-target="#viewEmployeeModal-1"> <i class="fa fa-eye"></i></a><a title="Edit Employee" class="btn operations" onclick="getEmployee('.$employe->id.')" data-toggle="modal" data-target="#editEmployeeModal-1"><i class="fa fa-edit"></i></a><!--<a title="Delete Employee" data-attr="'.$employe->id.'" onclick="delete_employee('.$employe->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a>--><a id="outlet_list'.$employe->id.'" title="Assign Outlet" class="btn operations" data-toggle="modal" onclick="getEmployeeOutlet('.$employe->id.', \''.$employe->first_name.' '.$employe->last_name.'\')" data-target="#getEmployeeOutletModal-1" data-array=\''.$employe->outlet_id.'\'><i class="fa fa-link"></i></a></div>';
                    $data[] = $nestedData;
                }
    
            }
            $json_data = array(
                "draw"            => intval($request->input('draw')),  
                "recordsTotal"    => intval($totalData),  
                "recordsFiltered" => intval($totalFiltered), 
                "data"            => $data   
            );

            echo json_encode($json_data);
 
    }

    public function get_employee(Request $request){
       
        $emp = Employee::where('id',$request->id)->first();
        
        return Array( 'employee' =>$emp);

    }

    public function edit_employee(Request $request) {

        $merchant_id = Auth::guard('merchant')->id();
        $employee = Employee::find($request -> employee_id_edit);

        $filename = '';
        $path = $request->file('ed_emp_img');

        if( !empty( $request->file('ed_emp_img') ) ){
            $img_temp = $request->file('ed_emp_img');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "images/".$filename;
            move_uploaded_file($img_temp,$image_path);
        }
        // $employee -> merchant_id = $merchant_id;
        $employee -> first_name = $request -> first_name_ed;
        $employee -> last_name = $request -> last_name_ed;
        $employee -> email = $request -> email_ed;
        $employee -> address = $request -> address_ed;
        $employee -> city = $request -> city_ed;
        $employee -> state = $request -> state_ed;
        $employee -> country = $request -> country_ed;
        $employee -> pin_code = $request -> pin_code_ed;
        $employee -> contact = $request -> contact_ed;
        // $employee -> pin = $request -> emp_pin_ed;
        $employee -> role = $request -> role_ed;
        // $employee -> status = 0;
        if ( !empty($filename) ) {
            $employee -> image_url = $filename;
        }
        $employee -> flag = 0;
        $employee->save();
    
        return;
    }

    public function delete_employee(Request $request) {
        $employee = Employee::find($request->employee_id);
        $employee->delete();
        return;
    }

    public function get_employee_role_by_id(Request $request) {
        $employee_role = $this->employee_role_by_id($request->role_id);
        return $employee_role;
    }

    public function employee_role_by_id($role_id) {

        $employee_role = DB::table('roles')->where('id', $role_id)->pluck('name');

        return $employee_role;
    }

    public function get_employee_roles() {
        $employee_roles = Roles::where('status','1')->get();
        return Array( 'employee_roles' => $employee_roles);
    }

    public function update_employee_status(Request $request) {
        $employee = Employee::find($request->employee_id);
        $employee->status = $request->employee_status;
        $employee->save();
        return;
    }

    public function assign_employee_outlet(Request $request) {
        // dd( $request );
        // exit;
        $employee = Employee::find($request->employee_id);
        if ( isset($request->outlet) ) {
            $outlet_list = json_encode( $request->outlet );
            $employee->outlet_id = $outlet_list;
        } else {
            $employee->outlet_id = NULL;
        }
        
        $employee->save();
        return;
    }

}