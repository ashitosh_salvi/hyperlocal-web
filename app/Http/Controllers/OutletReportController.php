<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Response;
use App\Merchant;
use App\Outlet;
use App\Orders;

class OutletReportController extends Controller
{
    public function individualOutlet(Request $request)
    {   
        $outlets = Outlet::all();
        return view( 'reports.outlet.individual_outlet',compact('outlets'));
    }

    public function getIndividualOutletReport(Request $request)
    {   
        $outlet_id  = $request->outlet;
        $from_date  = $request->from_date;
        $to_date    = $request->to_date;
        $merchant   = array();
        $columns    = array( 
            0 =>'order_id',
            1 =>'contact'
        );

        $totalData      = DB::select("SELECT o.order_id, c.first_name, c.last_name, c.mobile, o.discount,o.delivery_charges, o.tax, o.payment_mode, o.status FROM orders o, customers c WHERE date(o.created_at) >= '$from_date' AND date(o.created_at) <= '$to_date' and o.outlet_id  = $outlet_id and o.customer_id = c.id");
        $totalFiltered  = count($totalData);
        $limit          = $request->input('length');
        $start          = $request->input('start');
        $order          = $columns[$request->input('order.0.column')];
        $dir            = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')))
        {            
            $orders     = DB::select("SELECT o.order_id, c.first_name, c.last_name, c.mobile, o.discount,o.delivery_charges, o.tax, o.payment_mode, o.status,o.final_amount FROM orders o, customers c WHERE date(o.created_at) >= '$from_date' AND date(o.created_at) <= '$to_date' and o.outlet_id  = $outlet_id and o.customer_id = c.id LIMIT $start, $limit");
            // ORDER BY $order  $dir
        }
        else
        {
            $search         = $request->input('search.value');
            $orders         = DB::select("SELECT o.order_id, c.first_name, c.last_name, c.mobile, o.discount,o.delivery_charges, o.tax, o.payment_mode, o.status,o.final_amount  FROM orders o, customers c WHERE date(o.created_at) >= '$from_date' AND date(o.created_at) <= '$to_date' and o.outlet_id  = $outlet_id and o.customer_id = c.id and ( o.order_id LIKE '%$search%' OR o.status LIKE '%$search%' OR o.payment_mode LIKE '%$search%' OR c.first_name LIKE '%$search%' OR c.last_name LIKE '%$search%' OR c.mobile LIKE '%$search%' ) LIMIT $start, $limit"); 

            $totalFiltered_data  = DB::select("SELECT o.order_id, c.first_name, c.last_name, c.mobile, o.discount,o.delivery_charges, o.tax, o.payment_mode, o.status,o.final_amount  FROM orders o, customers c WHERE date(o.created_at) >= '$from_date' AND date(o.created_at) <= '$to_date' and o.outlet_id  = $outlet_id and o.customer_id = c.id and ( o.order_id LIKE '%$search%' OR o.status LIKE '%$search%' OR o.payment_mode LIKE '%$search%' OR c.first_name LIKE '%$search%' OR c.last_name LIKE '%$search%' OR c.mobile LIKE '%$search%' )");

            $totalFiltered      = count($totalFiltered_data);
        }
        
        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key=>$order)
            {   
                $nestedData['order_no']         = $order->order_id;
                $nestedData['name']             = $order->first_name." ".$order->last_name;
                $nestedData['contact_no']       = $order->mobile;
                $nestedData['order_value']      = "Rs. ".round($order->final_amount,2);
                $nestedData['discount']         = round($order->discount,2);
                $nestedData['delivery_charges'] = "Rs. ".$order->delivery_charges;
                $nestedData['gst']              = "Rs. ".round($order->tax,2);
                $nestedData['payment_mode']     = $order->payment_mode;
                $nestedData['status']           = $order->status;
                $data[]                         = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
    }

    public function exportIndividualOutletReport($from_date = null, $to_date = null,$search = null,$outlet_id = null)
    {   
        $outlet = Outlet::where('id',$outlet_id)->first();
        $filename = $outlet->outlet_name.'_'.$from_date.'_'.$to_date;
 
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename.".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Order No','Name','Contact No','Order Value','Discount','Delivery Charges','GST','Payment Mode','Status');

    
        $callback = function() use ($from_date,$to_date,$search,$outlet_id,$columns)
        {   
            
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            if ($search == '0')
            {   
                $orders     = DB::select("SELECT o.order_id, c.first_name, c.last_name, c.mobile, o.discount,o.delivery_charges, o.tax, o.payment_mode, o.status,o.final_amount FROM orders o, customers c WHERE date(o.created_at) >= '$from_date' AND date(o.created_at) <= '$to_date' and o.outlet_id  = $outlet_id and o.customer_id = c.id");
            }
            else
            {   
                $orders  = DB::select("SELECT o.order_id, c.first_name, c.last_name, c.mobile, o.discount,o.delivery_charges, o.tax, o.payment_mode, o.status,o.final_amount  FROM orders o, customers c WHERE date(o.created_at) >= '$from_date' AND date(o.created_at) <= '$to_date' and o.outlet_id  = $outlet_id and o.customer_id = c.id and ( o.order_id LIKE '%$search%' OR o.status LIKE '%$search%' OR o.payment_mode LIKE '%$search%' OR c.first_name LIKE '%$search%' OR c.last_name LIKE '%$search%' OR c.mobile LIKE '%$search%' )");
            }
            
            $data = array();
            if(!empty($orders))
            {   
                foreach ($orders as $key=>$order)
                {   
                    $nestedData['order_no']         = $order->order_id;
                    $nestedData['name']             = $order->first_name." ".$order->last_name;
                    $nestedData['contact_no']       = $order->mobile;
                    $nestedData['order_value']      = "Rs. ".round($order->final_amount,2);
                    $nestedData['discount']         = round($order->discount,2);
                    $nestedData['delivery_charges'] = "Rs. ".$order->delivery_charges;
                    $nestedData['gst']              = "Rs. ".round($order->tax,2);
                    $nestedData['payment_mode']     = $order->payment_mode;
                    $nestedData['status']           = $order->status;
                    fputcsv($file,$nestedData);
                }

            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
        
    }

    public function outletDailySales(Request $request)
    {   
        $outlets = Outlet::all();
        return view( 'reports.outlet.outlet_daily_sales',compact('outlets'));
    }

    public function getoutletDailySalesReport(Request $request)
    {   
        $outlet_id  = $request->outlet;
        $from_date  = $request->from_date;
        $to_date    = $request->to_date;
        $merchant   = array();
        $columns    = array( 
            0 =>'order_id',
            1 =>'contact'
        );

        $totalData      = DB::select("SELECT date(created_at) as date, count(id) as total_order, SUM(final_amount) as total_order_value, SUM(discount) as total_discount FROM `orders` where outlet_id =  $outlet_id and date(created_at) >= '$from_date' AND date(created_at) <= '$to_date'  GROUP BY date(created_at)");
        
        $totalFiltered  = count($totalData);
        $limit          = $request->input('length');
        $start          = $request->input('start');
        $order          = $columns[$request->input('order.0.column')];
        $dir            = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')))
        {            
            $orders     = DB::select("SELECT date(created_at) as date, count(id) as total_order, SUM(final_amount) as total_order_value, SUM(discount) as total_discount FROM `orders` where outlet_id =  $outlet_id and date(created_at) >= '$from_date' AND date(created_at) <= '$to_date'  GROUP BY date(created_at) LIMIT $start, $limit");
        }
        else
        {
            $search         = $request->input('search.value'); 
            $orders         = DB::select("SELECT date(created_at) as date, count(id) as total_order, SUM(final_amount) as total_order_value, SUM(discount) as total_discount FROM `orders` where outlet_id =  $outlet_id and date(created_at) >= '$from_date' AND date(created_at) <= '$to_date' AND date(created_at) LIKE '%$search%' GROUP BY date(created_at) LIMIT $start, $limit");

            $totalData  = DB::select("SELECT date(created_at) as date, count(id) as total_order, SUM(final_amount) as total_order_value, SUM(discount) as total_discount FROM `orders` where outlet_id =  $outlet_id and date(created_at) >= '$from_date' AND date(created_at) <= '$to_date' AND date(created_at) LIKE '%$search%'  GROUP BY date(created_at)");
            $totalFiltered      = count($totalData);
        }
        
        $data = array();
        $total_order = 0;
        $total_value = 0;
        if(!empty($orders))
        {   
            foreach ($orders as $key=>$order)
            {   
                $nestedData['date']             = $order->date;
                $nestedData['no_of_orders']     = $order->total_order;
                $nestedData['order_value']      = "Rs. ".round($order->total_order_value,2);
                $nestedData['discount']         = "Rs. ".round($order->total_discount,2);
                $data[]                         = $nestedData;
            }

            foreach ($totalData as $key => $all_order)
            {
                $total_order                    = $total_order + $all_order->total_order;
                $total_value                    = $total_value + $all_order->total_order_value;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data,
            "total_order"     => round($total_order,2),
            "total_value"     => "Rs. ".round($total_value,2)   
        );

        echo json_encode($json_data);
    }

    public function exportOutletDailySalesReport($from_date = null, $to_date = null,$search = null,$outlet_id = null)
    {   
        $outlet = Outlet::where('id',$outlet_id)->first();
        $filename = $outlet->outlet_name.'_'.$from_date.'_'.$to_date;

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename.".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Date','No Of Orders','Order Value','Discount');

        $callback = function() use ($from_date,$to_date,$search,$outlet_id,$columns)
        {   
            
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            if ($search == 0)
            {
                $orders     = DB::select("SELECT date(created_at) as date, count(id) as total_order, SUM(final_amount) as total_order_value, SUM(discount) as total_discount FROM `orders` where outlet_id =  $outlet_id  and date(created_at) >= '$from_date' AND date(created_at) <= '$to_date' GROUP BY date(created_at)");
            }
            else
            {
                $orders  = DB::select("SELECT date(created_at) as date, count(id) as total_order, SUM(final_amount) as total_order_value, SUM(discount) as total_discount FROM `orders` where outlet_id =  $outlet_id and date(created_at) >= '$from_date' AND date(created_at) <= '$to_date' AND date(created_at) LIKE '%$search%'  GROUP BY date(created_at)");
            }
                 
            $data = array();
            $total_order = 0;
            $total_value = 0;
            if(!empty($orders))
            {   
                foreach ($orders as $key=>$order)
                {   
                    $nestedData['date']             = $order->date;
                    $nestedData['no_of_orders']     = $order->total_order;
                    $nestedData['order_value']      = "Rs. ".round($order->total_order_value,2);
                    $nestedData['discount']         = "Rs. ".round($order->total_discount,2);
                    $data[]                         = $nestedData;

                    fputcsv($file,$nestedData);

                    $total_order                    = $total_order + $order->total_order;
                    $total_value                    = $total_value + $order->total_order_value;
                }

            }

            $total_array = ['Total',$total_order,$total_value,''];
            fputcsv($file,$total_array);
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
        
    }

}