<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Orders;
use App\Rider;
use App\Outlet;
use App\RidersAvaibility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use App\Notify;


class RiderApiController extends Controller
{
    //
    protected $redirectTo = '/rider';
    protected $guard = 'rider';

    public function signup(Request $request) {
        // dd($request);exit();
        $rider = Rider::whereMobile($request->mobile)->first();
        $rider_email = Rider::where('email', $request->email)->first();

        $image_upload_message = "";
        $address_proof_image_upload_message = "";
        $photo_id_image_upload_message = "";
        $other_image_upload_message = "";

        $validator = $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email'. ( ( $rider_email != null ) ? '|unique:riders,email,'.$rider_email->email : '' ),
            'mobile' => 'required|string|min:10|max:10'. ( ( $rider != null ) ? '|unique:riders,mobile,'.$rider->mobile : '' ),
            'password' => 'required|string|confirmed',
            'firebase_token' => 'required|string',
            'address_proof_image_url' => 'max:5120',

        ]);
        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }
        $rider = new Rider([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'password' => Hash::make($request->password),
            'status' => '0'
        ]);

        $rider->firebase_token = $request->firebase_token; 

        if ( isset($request->image_url) && isset($request->image_name) ) {
            $store_base64_image = $this->store_base64_image($request->image_name, $request->image_url);

            // echo $store_base64_image;
            // exit;
            
            if ( $store_base64_image == "upload_error" ) {
                $image_upload_message = 'Wrong Encoding. Unable to update Image';
            } else if ( $store_base64_image == "ext_error" ) {
                $image_upload_message = 'Image Name extension is invalid';
            } else if ( $store_base64_image == "size_error" ) {
                $image_upload_message = 'Image Size is grater than 1024 KB';
            } else {
                $rider->image_url = $store_base64_image;
                $image_upload_message = 'Image Uploaded Successful';
            }

        }

        $filename = '';
        $path = $request->file('address_proof_image_url');
        if(!empty($request->file('address_proof_image_url'))){
            $img_temp = $request->file('address_proof_image_url');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "public/images/".$filename;
            move_uploaded_file($img_temp,$image_path);
            
            $address_proof_image_upload_message = 'Image Uploaded Successful';
            $rider -> address_proof_image_url = $filename;
        }

        if(!empty($request->file('photo_id_image_url'))){
            $img_temp = $request->file('photo_id_image_url');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "public/images/".$filename;
            move_uploaded_file($img_temp,$image_path);
            
            $photo_id_image_upload_message = 'Image Uploaded Successful';
            $rider -> photo_id_image_url = $filename;
        }

        if(!empty($request->file('other_image_url'))){
            $img_temp = $request->file('other_image_url');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "public/images/".$filename;
            move_uploaded_file($img_temp,$image_path);
            
            $other_image_upload_message = 'Image Uploaded Successful';
            $rider -> other_image_url = $filename;
        }
            

        // if ( isset($request->photo_id_image_url) && isset($request->photo_id_image_name) ) {
        //     $photo_id_store_base64_image = $this->store_base64_image($request->photo_id_image_name, $request->photo_id_image_url);
            
        //     if ( $photo_id_store_base64_image == "upload_error" ) {
        //         $image_upload_message = 'Wrong Encoding. Unable to update Image';
        //     } else if ( $photo_id_store_base64_image == "ext_error" ) {
        //         $image_upload_message = 'Image Name extension is invalid';
        //     } else if ( $photo_id_store_base64_image == "size_error" ) {
        //         $image_upload_message = 'Image Size is grater than 100 KB';
        //     } else {
        //         $rider->photo_id_image_url = $photo_id_store_base64_image;
        //         $photo_id_image_upload_message = 'Image Uploaded Successful';
        //     }

        // }
        // if ( isset($request->other_image_url) && isset($request->other_image_name) ) {
        //     $other_store_base64_image = $this->store_base64_image($request->other_image_name, $request->other_image_url);
            
        //     if ( $other_store_base64_image == "upload_error" ) {
        //         $image_upload_message = 'Wrong Encoding. Unable to update Image';
        //     } else if ( $other_store_base64_image == "ext_error" ) {
        //         $image_upload_message = 'Image Name extension is invalid';
        //     } else if ( $other_store_base64_image == "size_error" ) {
        //         $image_upload_message = 'Image Size is grater than 100 KB';
        //     } else {
        //         $rider->other_image_url = $other_store_base64_image;
        //         $other_image_upload_message = 'Image Uploaded Successful';
        //     }

        // }

        $rider->save();
        
        return response()->json([
            'message' => 'Successfully created rider!',
            'image_upload_status' => $image_upload_message,
            'address_proof_image_upload_status' => $address_proof_image_upload_message,
            'photo_id_image_upload_status' => $photo_id_image_upload_message,
            'other_image_upload_status' => $other_image_upload_message
        ], $this->successCode);
        
    }

    public function check_status($mobile) {
        
        $rider = Rider::whereMobile($mobile)->pluck('status');

        if ( $rider ) {

            if ( $rider[0] ) {
                return 1;
            } else {
                return 0;
            }

        } else {
            return 0;
        }


    }

    public function login(Request $request)
    {
        if ( isset($request->mobile) && isset($request->password) ) {

            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10',
                'password' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors()->all()
                ], $this->badRequest);
            } else {
                    
                $credentials = request(['mobile', 'password']);

                if(!Auth::guard('rider')->attempt($credentials))
                    return response()->json([
                        'message' => 'The given data was invalid.',
                        'errors' => array('Unauthorized Mobile Number or Password')
                    ], $this->NotAuthorized);

                $check_status = $this->check_status($request->mobile);

                if ( $check_status ) {
                    
                    $rider = Rider::where('mobile', $request->mobile)->first();
                    $token = $rider->createToken('Laravel Personal Access Client')->accessToken;
                    $rider->image_root_path = URL::asset('/images') . "/";

                    $final_rider = array();
                    $get_rider = DB::table('riders')->where('mobile', $request->mobile)->get(['id', 'first_name', 'last_name', 'email', 'mobile', 'image_url', 'firebase_token']);
                    
                    foreach( $get_rider as $rider ) {
                        if(isset($request->firebase_token)){

                            $rider_temp = Rider::find($rider->id);
                            $rider_temp->firebase_token = $request->firebase_token;
                            $rider_temp->save();
                        }
                        $final_rider['id'] = empty( $rider->id ) ? '' : $rider->id;
                        $final_rider['first_name'] = empty( $rider->first_name ) ? '' : $rider->first_name;
                        $final_rider['last_name'] = empty( $rider->last_name ) ? '' : $rider->last_name;
                        $final_rider['email'] = empty( $rider->email ) ? '' : $rider->email;
                        $final_rider['mobile'] = empty( $rider->mobile ) ? '' : $rider->mobile;
                        $final_rider['city'] = empty( $rider->city ) ? '' : $rider->city;
                        $final_rider['state'] = empty( $rider->state ) ? '' : $rider->state;
                        $final_rider['country'] = empty( $rider->country ) ? '' : $rider->country;
                        $final_rider['pin_code'] = empty( $rider->pin_code ) ? '' : $rider->pin_code;
                        $final_rider['address'] = empty( $rider->address ) ? '' : $rider->address;
                        $final_rider['image_url'] = URL::asset('/images') . "/" . ( empty( $rider->image_url ) ? 'test.jpg' : $rider->image_url );
                        $final_rider['adhaar_image_url'] = URL::asset('/images') . "/" . ( empty( $rider->adhaar_image_url ) ? 'test.jpg' : $rider->adhaar_image_url );
                        $final_rider['pan_image_url'] = URL::asset('/images') . "/" . ( empty( $rider->pan_image_url ) ? 'test.jpg' : $rider->pan_image_url );
                        $final_rider['passport_image_url'] = URL::asset('/images') . "/" . ( empty( $rider->passport_image_url ) ? 'test.jpg' : $rider->passport_image_url );
                    }

                    return response()->json([
                        'message' => 'Successfully logged in',
                        'access_token' => $token,
                        'token_type' => 'Bearer',
                        // 'expires_at' => Carbon::parse(
                            // $tokenResult->token->expires_at
                        // )->toDateTimeString()
                        'data' => $final_rider
                    ]);

                } else {
                    return response([
                        'message' => 'Unauthorized Access',
                        'errors' => array('Rider status is unauthorized. Please contact admin.')
                    ], $this->NotAuthorized);
                }
                
            }

        }

    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        // $request->rider()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ], $this->successCode);
    }

    public function checkridermobile(Request $request) {
       
        if ( isset($request->mobile) ) {
            $rider_mobile = Rider::whereMobile($request->mobile)->first();
            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10'. ( ( $rider_mobile != null ) ? '|unique:riders,mobile,'.$rider_mobile->mobile : '' )
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => 'Mobile Status',
                    'data' => $validator->errors()->all()
                ], $this->successCode);
            } else {
                return response([
                    'message' => 'Account not found, Create new account'
                ], $this->badRequest);
            }
        }
        
    }

    public function riderNewPassword(Request $request) {
        
        if ( isset($request->mobile) && isset($request->new_password) ) {

            $rider_mobile = Rider::whereMobile($request->mobile)->first();
            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10'. ( ( $rider_mobile != null ) ? '|unique:riders,mobile,'.$rider_mobile->mobile : '' ),
                'new_password' => 'required|string'
            ]);

            if ($validator->fails()) {

                $customer = Rider::where('mobile', $request->mobile)->first();
                $customer -> password = Hash::make($request->new_password);
                $customer->save();

                return response()->json([
                    'message' => 'Successfully changed password'
                ], $this->successCode);
                
            } else {
                return response([
                    'message' => 'Moile Does not exists'
                ], $this->badRequest);
            }

        }

    }

    public function changeriderpassword(Request $request) {
        
        $validator = $validator = Validator::make($request->all(), [
            'mobile' => 'required|string',
            'password' => 'required|string',
            'new_password' => 'required|string'
        ]);  

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }

        $credentials = request(['mobile', 'password']);

        if(!Auth::guard('rider')->attempt($credentials))
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => array('Incorrect Old Password')
            ], $this->NotAuthorized);
        
        $rider = Rider::where('mobile', $request->mobile)->first();
        $rider -> password = Hash::make($request->new_password);
        $rider->save();

        $token = $request->user()->token();
        $token->revoke();
        return response()->json([
            'message' => 'Successfully changed password'
        ], $this->successCode);

    }

    public function rider(Request $request) {

        if ( isset($request->rider_id) ) {

            $user_check = Auth::guard('rider-api')->id();
            $rider_id = $request->rider_id;

         	$validator = $validator = Validator::make($request->all(), [
	            'address_proof_image_url' => 'max:5120',
	        ]);
	        if ($validator->fails()) {
	            return response([
	                'message' => 'The given data was invalid.',
	                'errors' => $validator->errors()->all()
	            ], $this->badRequest);
	        }

            $image_upload_message = "";
            $address_proof_image_upload_message = "";
            $photo_id_image_upload_message = "";
            $other_image_upload_message = "";

            if ( $user_check == $rider_id ) {
                $rider = Rider::whereId($rider_id)->first();
                // print_r($rider);
                // exit;
                if ( isset($request->first_name) ) {
                    $rider->first_name = $request->first_name;
                }
                if ( isset($request->last_name) ) {
                    $rider->last_name = $request->last_name;
                }
                if ( isset($request->email) ) {
                    $rider->email = $request->email;
                }
                if ( isset($request->image_url) && isset($request->image_name) ) {
                    $store_base64_image = $this->store_base64_image($request->image_name, $request->image_url);

                    // echo $store_base64_image;
                    // exit;
                    
                    if ( $store_base64_image == "upload_error" ) {
                        $image_upload_message = 'Wrong Encoding. Unable to update Image';
                    } else if ( $store_base64_image == "ext_error" ) {
                        $image_upload_message = 'Image Name extension is invalid';
                    } else if ( $store_base64_image == "size_error" ) {
                        $image_upload_message = 'Image Size is grater than 1024 KB';
                    } else {
                        $rider->image_url = $store_base64_image;
                        $image_upload_message = 'Image Uploaded Successful';
                    }

                }
                
                $filename = '';
                $path = $request->file('address_proof_image_url');
                if(!empty($request->file('address_proof_image_url'))){
                    $img_temp = $request->file('address_proof_image_url');
                    $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
                    $extension = $img_temp -> getClientOriginalExtension();
                    $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
                    $image_path = "images/".$filename;
                    move_uploaded_file($img_temp,$image_path);
                    
                    $address_proof_image_upload_message = 'Image Uploaded Successful';
                    $rider ->address_proof_image_url = $filename;
                }

                if(!empty($request->file('photo_id_image_url'))){
                    $img_temp = $request->file('photo_id_image_url');
                    $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
                    $extension = $img_temp -> getClientOriginalExtension();
                    $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
                    $image_path = "images/".$filename;
                    move_uploaded_file($img_temp,$image_path);
                    
                    $photo_id_image_upload_message = 'Image Uploaded Successful';
                    $rider ->photo_id_image_url = $filename;
                }

                if(!empty($request->file('other_image_url'))){
                    $img_temp = $request->file('other_image_url');
                    $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
                    $extension = $img_temp -> getClientOriginalExtension();
                    $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
                    $image_path = "images/".$filename;
                    move_uploaded_file($img_temp,$image_path);
                    
                    $other_image_upload_message = 'Image Uploaded Successful';
                    $rider ->other_image_url = $filename;
                }


                // if ( isset($request->photo_id_image_url) && isset($request->photo_id_image_name) ) {
                //     $photo_id_store_base64_image = $this->store_base64_image($request->photo_id_image_name, $request->photo_id_image_url);
                    
                //     if ( $photo_id_store_base64_image == "upload_error" ) {
                //         $image_upload_message = 'Wrong Encoding. Unable to update Image';
                //     } else if ( $photo_id_store_base64_image == "ext_error" ) {
                //         $image_upload_message = 'Image Name extension is invalid';
                //     } else if ( $photo_id_store_base64_image == "size_error" ) {
                //         $image_upload_message = 'Image Size is grater than 100 KB';
                //     } else {
                //         $rider->photo_id_image_url = $photo_id_store_base64_image;
                //         $photo_id_image_upload_message = 'Image Uploaded Successful';
                //     }

                // }
                // if ( isset($request->other_image_url) && isset($request->other_image_name) ) {
                //     $other_store_base64_image = $this->store_base64_image($request->other_image_name, $request->other_image_url);
                    
                //     if ( $other_store_base64_image == "upload_error" ) {
                //         $image_upload_message = 'Wrong Encoding. Unable to update Image';
                //     } else if ( $other_store_base64_image == "ext_error" ) {
                //         $image_upload_message = 'Image Name extension is invalid';
                //     } else if ( $other_store_base64_image == "size_error" ) {
                //         $image_upload_message = 'Image Size is grater than 100 KB';
                //     } else {
                //         $rider->other_image_url = $other_store_base64_image;
                //         $other_image_upload_message = 'Image Uploaded Successful';
                //     }

                // }





                $rider->save();
                $rider->image_root_path = URL::asset('/images') . "/";

                $final_rider = array();
                $get_rider = DB::table('riders')->where('id', $request->rider_id)->get();
                
                foreach( $get_rider as $ride ) {
                    $final_rider['id'] = empty( $ride->id ) ? '' : $ride->id;
                    $final_rider['first_name'] = empty( $ride->first_name ) ? '' : $ride->first_name;
                    $final_rider['last_name'] = empty( $ride->last_name ) ? '' : $ride->last_name;
                    $final_rider['email'] = empty( $ride->email ) ? '' : $ride->email;
                    $final_rider['mobile'] = empty( $ride->mobile ) ? '' : $ride->mobile;
                    $final_rider['city'] = empty( $ride->city ) ? '' : $ride->city;
                    $final_rider['state'] = empty( $ride->state ) ? '' : $ride->state;
                    $final_rider['country'] = empty( $ride->country ) ? '' : $ride->country;
                    $final_rider['pin_code'] = empty( $ride->pin_code ) ? '' : $ride->pin_code;
                    $final_rider['address'] = empty( $ride->address ) ? '' : $ride->address;
                    $final_rider['image_url'] = URL::asset('/images') . "/" . ( empty( $ride->image_url ) ? 'test.jpg' : $ride->image_url );
                    $final_rider['address_proof_image_url'] = URL::asset('/images') . "/" . ( empty( $ride->address_proof_image_url ) ? 'test.jpg' : $ride->address_proof_image_url );
                    $final_rider['photo_id_image_url'] = URL::asset('/images') . "/" . ( empty( $ride->photo_id_image_url ) ? 'test.jpg' : $ride->photo_id_image_url );
                    $final_rider['other_image_url'] = URL::asset('/images') . "/" . ( empty( $ride->other_image_url ) ? 'test.jpg' : $ride->other_image_url );
                }
                
                $message = 'Rider Profile Data Updated';
                
                return response()->json([
                    'message' => $message,
                    'data' => $final_rider,
                    'image_upload_status' => $image_upload_message,
                    'address_proof_image_upload_status' => $address_proof_image_upload_message,
                    'photo_id_image_upload_status' => $photo_id_image_upload_message,
                    'other_image_upload_status' => $other_image_upload_message
                ], $this->successCode);
            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }
    }

    public function riderActiveInactive(Request $request) {
        $validator = Validator::make($request->all(), [
            'rider_id' => 'required',
            'status' => 'required|string',
        ]);  

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }else {
            $rider = Rider::find($request->rider_id);
            if ($request->status == 'active') {
                $ride->active = 1;
                $message = "Rider Activated successfully.";
            }else{
                $ride->active = 0;
                $message = "Rider In-Activated successfully.";
            }
            $rider->save();
            
            return response()->json([
                'message' => $message,
            ], $this->successCode);
        }
    }

    public function riderOnlineOffline(Request $request)
    {   
        
        $validator = Validator::make($request->all(), [
            'rider_id' => 'required',
            'status' => 'required|string',
        ]);  

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }else {
            $rider = Rider::find($request->rider_id);
            
            $riders_avaibilities = new RidersAvaibility();
            $riders_avaibilities->rider_id = $request->rider_id;
            $riders_avaibilities->log_time = Carbon::now()->format('Y-m-d H:i:s');
            
            if ($request->status == 'online') {
                $rider->online = 1;
                $riders_avaibilities->state = 'online';
                $message = "Rider online successfully.";
            }else{
                $rider->online = 0;
                $riders_avaibilities->state = 'offline';
                $message = "Rider offline successfully.";
            }
            
            $riders_avaibilities->save();
            $rider->save();
            
            return response()->json([
                'message' => $message,
            ], $this->successCode);
        }
    }

    public function rider_notification(){
        $rider = Rider::find(auth()->user()->id);
        return $rider->notifications->take(20);
    }


    public function riderOrderReport(Request $request){
        $validator = Validator::make($request->all(), [
            'rider_id' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
        ]);  

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }else {
            $rider_id = $request->rider_id;
            $Orders = Orders::select(\DB::raw('outlet_id, count(outlet_id) as count'))
                            ->where('rider_id',$rider_id)
                            ->whereRaw('date(rider_assign_date) >= ?',[$request->from_date])
                            ->whereRaw('date(rider_assign_date) <= ?',[$request->to_date])
                            ->groupBy('outlet_id')->get();

            // orderdata in loop
            $output_array = [];
            foreach ($Orders as $key=>$order)
            {
                $outlet = Outlet::find($order->outlet_id);
                $outlet_name = $outlet['outlet_name'];

                $output_array[$key]['outlet'] = $outlet_name;
                $output_array[$key]['order_count'] = $order->count;
            }

            return response()->json([
                'data' => $output_array,
            ], $this->successCode);
        }
    }


    public function test(){
        
        $rider = Rider::find(auth()->user()->id);
        $rider->msgbody = '2222';
        $rider->msgtitle = '222';
        $rider->notify(new Notify);
        //return $rider;
        return $rider->notifications->take(20); 
    }


}