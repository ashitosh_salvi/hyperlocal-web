<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Offers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class OffersApiController extends Controller
{
    // Get All Orders
    public function index() {
        
        $orders = Orders::reorder('id', 'ASC')->get();
        // dd( $orders );
        return response()->json([
            'orders_list' => $orders
        ], 201);

    }

    public function get_offers_by_merchant_id(Request $request) {
        
        $customer_id = $request->merchant_id;
        $all_order_items = array();
        $past_temp = array();
        $current_temp = array();
        if ( 1 == 1 ) {
            // echo 'true';
            $get_orders = DB::select("select * from orders where customer_id = ? order by id desc", [$customer_id]);
            foreach ( $get_orders as $order ) {

                

                $get_order_items = DB::select("select * from order_items where order_id = ?", [$order->order_id]);
                $order->order_items = $get_order_items;
                if($order->status == 'pending'){
                    array_push( $current_temp, $order );
                }
                else if ($order->status == 'delivered'){
                    array_push( $past_temp, $order );
                }
                
            }

            $all_order_items['current_orders'] = $current_temp;
            $all_order_items['past_orders'] = $past_temp;

            return response()->json([
                'message' => 'Order List Retrieved',
                'data' => $all_order_items
            ], $this->successCode);
        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => array('Unauthorized User ID')
            ], $this->NotAuthorized);
        }

    }


    public function get_offer_list(){
        $offers = Offers::where('expiry_date', '>=', date('d/m/Y'))
                    ->where('status',1)
                    ->get();

        foreach($offers as $offer){
            $offer->offer_images = URL::asset('/images') . "/" . ( empty($offer->image_url) ? 'test.jpg' : $offer->image_url );
        }

        return response()->json([
            'message' => 'Offers List Retrieved',
            'data' => $offers,
        ], $this->successCode);

    } 

    
}
