<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\CustomerOutletFavourite;
use App\CustomerOutletRating;
use App\Http\Controllers\Controller;
use App\Offers;
use Illuminate\Http\Request;
use App\Outlet;
use App\Recipe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\CustomerAddress;

class OutletApiController extends Controller
{
    //
    public function index() {
        $outlets = Outlet::reorder('id', 'ASC')->get();
        return response()->json([
            'outlets_list' => $outlets
        ], $this->successCode);
    }

    function getDistance($latitude1, $longitude1, $latitude2, $longitude2)
    {
        $long1 = deg2rad($longitude1);
        $long2 = deg2rad($longitude2);
        $lat1 = deg2rad($latitude1);
        $lat2 = deg2rad($latitude2);
         
        //Haversine Formula
        $dlong = $long2 - $long1;
        $dlati = $lat2 - $lat1;
         
        $val = pow(sin($dlati/2),2)+cos($lat1)*cos($lat2)*pow(sin($dlong/2),2);
         
        $res = 2 * asin(sqrt($val));
         
        // $radius = 3958.756;
        $radius = 6378.8;
         
        return ($res*$radius); 
    }

    public function getoutletsbygeolocation(Request $request) {
        
        $latitude = $request->latitude;
        $longitude = $request->longitude;

        $outlets = DB::select('select * from outlets');

        $final_outlets = array();
        
        if ( !empty( $outlets ) ) {

            foreach ( $outlets as $outlet ) {
                // print_r( $outlet );
                $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);

                if ($distance <= 4) {
                    // echo "Within 100 kilometer radius";
                    array_push($final_outlets, $outlet);
                }
                //else {
                //     // echo "Outside 100 kilometer radius";
                // }

            } 

        }

        return response()->json([
            'outlets_list' => $this->shuffle_function($final_outlets)
        ], $this->successCode);

    }

    public function shuffle_function($list) { 
        if (!is_array($list)) return $list; 

        $keys = array_keys($list); 
        shuffle($keys); 
        $random = array(); 
        foreach ($keys as $key) { 
            $random[] = $list[$key]; 
        }
        return $random; 
    } 


    public function search_outlets_by_outlet_name(Request $request) {
        
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $customer_id = $request->customer_id;
        $outlet_name = $request->search_string;

        $outlets = DB::select('select * from outlets where flag = 0 and outlet_name like "%'. $outlet_name .'%"');
        
        $final_outlets = array();
        
        if ( !empty( $outlets ) ) {
            $temp = array();
            foreach ( $outlets as $outlet ) {

                $is_favourite = DB::table('customer_outlet_favourites')->where('customer_id', $customer_id)->where('outlet_id', $outlet->id)->count();

                $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);

                $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlet->id)->avg('rating');

                if($distance <= 4)
                {
                    $temp['id'] = empty($outlet->id) ? '' : strval($outlet->id);
                    $temp['outlet_distance'] = strval(number_format($distance)) . " km";
                    $temp['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
                    $temp['is_favourite'] = strval($is_favourite);
                    $temp['rating'] = empty($rating) ? '0' : strval( $rating );
                    $temp['outlet_name'] = $outlet->outlet_name;
                    $temp['outlet_category_id'] = empty($outlet->outlet_category_id) ? '' : strval($outlet->outlet_category_id);
                    $temp['merchant_id'] = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
                    $temp['image_url'] = URL::asset('/images') . "/" . ( empty( json_decode($outlet->image_url) ) ? 'test.jpg' : json_decode($outlet->image_url) );
                    $temp['outlet_speciality'] = empty($outlet->outlet_speciality) ? '' : strval($outlet->outlet_speciality);
                    $temp['booking_for_two'] = empty($outlet->booking_for_two) ? '' : strval($outlet->booking_for_two);
                    $temp['open_time'] = empty($outlet->open_time) ? '' : strval($outlet->open_time);
                    $temp['close_time'] = empty($outlet->close_time) ? '' : strval($outlet->close_time);
                    array_push($final_outlets, $temp);
                }
            }
        }
        if (!empty($final_outlets)) {
            $shuffle_array = $this->shuffle_function($final_outlets);
        }
        $shuffle_array = $final_outlets;
        
        return response()->json([
                'message' => 'Outlet List Retrieved',
                'data' => ['outlets_list'=>$shuffle_array]
            ], $this->successCode);
    }
    
    // public function search_outlets_by_outlet_name(Request $request) {
        
    //     $latitude = $request->latitude;
    //     $longitude = $request->longitude;
    //     $customer_id = $request->customer_id;
    //     $outlet_name = $request->search_string;

    //     $outlets = DB::select('select * from outlets where flag = 0 and outlet_name like "%'. $outlet_name .'%"');
        
    //     $final_outlets = array();
        
    //     if ( !empty( $outlets ) ) {
    //         $temp = array();
    //         foreach ( $outlets as $outlet ) {

    //             $is_favourite = DB::table('customer_outlet_favourites')->where('customer_id', $customer_id)->where('outlet_id', $outlet->id)->count();

    //             $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);

    //             $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlet->id)->avg('rating');

    //             $temp['id'] = empty($outlet->id) ? '' : strval($outlet->id);
    //             $temp['outlet_distance'] = strval(number_format($distance)) . " km";
    //             $temp['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
    //             $temp['is_favourite'] = strval($is_favourite);
    //             $temp['rating'] = empty($rating) ? '0' : strval( $rating );
    //             $temp['outlet_name'] = $outlet->outlet_name;
    //             $temp['outlet_category_id'] = empty($outlet->outlet_category_id) ? '' : strval($outlet->outlet_category_id);
    //             $temp['merchant_id'] = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
    //             $temp['image_url'] = URL::asset('/images') . "/" . ( empty( json_decode($outlet->image_url) ) ? 'test.jpg' : json_decode($outlet->image_url) );
    //             $temp['outlet_speciality'] = empty($outlet->outlet_speciality) ? '' : strval($outlet->outlet_speciality);
    //             $temp['booking_for_two'] = empty($outlet->booking_for_two) ? '' : strval($outlet->booking_for_two);
    //             $temp['open_time'] = empty($outlet->open_time) ? '' : strval($outlet->open_time);
    //             $temp['close_time'] = empty($outlet->close_time) ? '' : strval($outlet->close_time);

    //                 array_push($final_outlets, $temp);
                

    //         } 

    //     }

    //     return response()->json([
    //             'message' => 'Outlet List Retrieved',
    //             'data' => ['outlets_list'=>$final_outlets]
    //         ], $this->successCode);

    // }

    public function get_all_offers_by_geolocation(Request $request) {
        $latitude = $request->latitude;
        $longitude = $request->longitude;

        $final_offers = array();
        $get_offers = DB::table('offers')
                                ->where('expiry_date', '>=', date('d/m/Y'))
                                ->where('status',1)
                                ->get();
        $temp = array();
        foreach ($get_offers as $offer) {
            $offer->image_url = URL::asset('/images') . "/" . ( empty($offer->image_url) ? 'test.jpg' : $offer->image_url ); 
            // if($offer->outlet_id != '' || $offer->outlet_id != 'null'){
                // $get_outlets_id = json_decode($offer->outlet_id);

                // print_r($offer->outlet_id); echo '----';
                // foreach ($get_outlets_id as $outlet_id) {
                    // $outlet = DB::table('outlets')->where('id', $outlet_id)->get();

                    // if(count($outlet) > 0){

                        // $distance = $this->getDistance($outlet[0]->outlet_latitude, $outlet[0]->outlet_longitude, $latitude, $longitude);

                        // if ($distance < 100) {
                            // echo "Within 10 kilometer radius";
                            array_push($temp, $offer->image_url);
                        // } else {
                        //     // echo "Outside 10 kilometer radius";
                        // }
                    // }
                // }
                $final_offers['offers'] = $temp;
            // }
        }

        return response()->json([
            'message' => 'Offers List Retrieved',
            'data' => $final_offers,
        ], $this->successCode);       
    }
    public function get_outlet_categories() {
        $outlet_categories = DB::table("outlet_categories")->get(['id', 'category_name', 'image_url']);
        return response()->json([
            'message' => 'Outlet Category List Retrieved',
            'data' => $outlet_categories
        ], $this->successCode);
    }

    public function get_favourite_flag($outlet_id, $customer_id) {
        if ( $customer_id ) {
            // return 'true';
            $get_favourite_flag = CustomerOutletFavourite::where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->pluck('is_favourite');
            return empty( $get_favourite_flag[0] ) ? '0' : '1';
        } else {
            return '';
        }
    }

    public function get_avg_rating($outlet_id) {
        $get_avg_rating = CustomerOutletRating::where('outlet_id', $outlet_id)->get(['rating']);

        // print_r( $get_avg_rating );
        // exit;
        
        if ( count( $get_avg_rating ) == 0 ) {
            return "0";
        } else {
            // echo '1';
            $c = 0;
            $rate = 0;    
            // print_r($get_avg_rating);
            // exit;
            foreach($get_avg_rating as $rating) {
                $rate += ( 5 * $rating['rating'] );
                $c += 1;
                // echo $rating['rating'];
            }
            // echo $rate;
            // exit;
            $count = ( 5 * $c );
            $avg_rate = number_format( ($rate / $count), 1 );
        // echo $rate . '/' . $count;
        // exit;
            return $avg_rate;
        }
        exit;
    }

    public function get_outlets_by_category_id($outlet_category_id,$offset,$limit, $customer_id = 0,$address_id)
    {
        $outlets = DB::table('outlets')->where('outlet_category_id', $outlet_category_id)
            ->where('flag','0')
            // ->offset($offset)->limit($limit)
            ->get();

        $final_outlets = array();

        foreach($outlets as $outlet)
        {
        	$get_customer = CustomerAddress::find($address_id);
        	
            $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $get_customer->customer_latitude, $get_customer->customer_longitude);

            if($distance <= 4)
            {
                $out = array();

                if($outlet->outlet_type == 'product')
                {
                    $item_count = DB::table('products')->where('outlet_id', $outlet->id)->where('status', 1)->count();
                }
                else
                {
                    $item_count = DB::table('recipes')->where('outlet_id', $outlet->id)->where('status', 1)->count();
                }
                
                if($item_count > 0)
                {
                    
                    foreach($outlet as $key => $value)
                    {
                        if ($key == 'image_url')
                        {
                            $value = URL::asset('/images') . "/" . (empty($value) ? 'test.jpg' : json_decode($value));
                        }

                        $out[$key] = empty($value) ? '' : strval($value);
                        
                        if($customer_id != 0)
                        {
                            if($distance >= 1)
                            {
                                $out["outlet_distance"] = strval(number_format($distance)) . " km";
                            }
                            else
                            {
                                $out["outlet_distance"] = strval(number_format((float)$distance,3,'.','')) . " km";
                            }

                            $out["estimated_delivery_time"] = number_format((15 + ($distance * 60)/50))." Min";;
                        }
                        else
                        {
                            $out["outlet_distance"] = "0 KM";
                            $out["estimated_delivery_time"] = "0 Min";
                        }

                        if ( $key == 'id' )
                        {
                            $out["is_favourite"]=$this->get_favourite_flag($value, $customer_id);
                            $out["rating"]=$this->get_avg_rating($value);
                        }
                    }

                    array_push($final_outlets, $out);
                }
            }
            
        }
        if (!empty($final_outlets)) {
            return $this->shuffle_function($final_outlets);
        }
        return $final_outlets;
    }

    // public function get_outlets_by_category_id($outlet_category_id,$offset,$limit, $customer_id = 0,$address_id) {
    //     $outlets = DB::table('outlets')->where('outlet_category_id', $outlet_category_id)
    //         ->where('flag','0')
    //         ->offset($offset)->limit($limit)
    //         ->get();
    //     $final_outlets = array();
    //     foreach( $outlets as $outlet ) {
            

    //         $out = array();
    //         if($outlet->outlet_type == 'product'){
    //             $item_count = DB::table('products')->where('outlet_id', $outlet->id)->where('status', 1)->count();
    //         }else{
    //             $item_count = DB::table('recipes')->where('outlet_id', $outlet->id)->where('status', 1)->count();
    //         }
            
    //         if($item_count > 0){
                
    //             foreach( $outlet as $key => $value ) {
    //                 if ( $key == 'image_url' ) {
    //                     $value = URL::asset('/images') . "/" . ( empty($value) ? 'test.jpg' : json_decode( $value ) );
    //                 }
    //                 $out[$key] = empty($value) ? '' : strval( $value );
                    
    //                 if($customer_id != 0){

    //                     // $get_customer = DB::table('customers')->where('customers.id', $customer_id)
    //                     //                 ->join('customer_addresses','customers.id','=','customer_addresses.customer_id')
    //                     //                 ->first();

    //                     $get_customer = CustomerAddress::find($address_id);

    //                     $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $get_customer->customer_latitude, $get_customer->customer_longitude);
    //                     if($distance >= 1){
    //                         $out["outlet_distance"] = strval(number_format($distance)) . " km";
    //                     }else{
    //                         $out["outlet_distance"] = strval(number_format((float)$distance,3,'.','')) . " km";
    //                     }
    //                     $out["estimated_delivery_time"] = number_format((15 + ($distance * 60)/50))." Min";;
    //                 }
    //                 else{
    //                     $out["outlet_distance"] = "0 KM";
    //                     $out["estimated_delivery_time"] = "0 Min";
    //                 }

    //                 if ( $key == 'id' ) {            
    //                     $out["is_favourite"]=$this->get_favourite_flag($value, $customer_id);
    //                     $out["rating"]=$this->get_avg_rating($value);
    //                 }
    //             }
    //             array_push($final_outlets, $out);
    //         }
            
    //     }
    //     return $final_outlets;
    // }

    public function get_homepage_data(Request $request) {
            
        if ( isset($request->customer_id) ) {
            
            $user_check = Auth::guard('customer-api')->id();
            // dd($user_check);
            
            $address_id = $request->address_id;
            $customer_id = $request->customer_id;
            
            if ( $user_check == $customer_id ) {
                
                $cart_count = DB::table('cart_items')->where('customer_id', $customer_id)->count();
                
                $final_array = array();
                $offers = array();
                
                $get_offer = Offers::where('status','1')
                            ->where('expiry_date', '>=', date('d/m/Y'))
                            ->pluck('image_url')
                            ->take(12);
                
                foreach($get_offer as $go){
                    if($go){
                        $go  = URL::asset('/images') . "/" .$go;
                        array_push($offers,$go);
                    }
                }            

                $final_array['offers'] = $offers;
                
                $outlet_categories = DB::table("outlet_categories")->orderBy('priority', 'asc')->get(['id', 'category_name', 'image_url']);
                $final_outlet_categories = array();
                for( $i = 0; $i < count($outlet_categories); $i++ ) {
                    $single_outlet_categories = array();
                    // print_r($outlet_categories[$i]);
                    foreach( $outlet_categories[$i] as $key => $value ) {
                        if ( $key == 'image_url' ) {
                            $single_outlet_categories[$key] = URL::asset('/images') . "/" . ( empty($value) ? 'test.jpg' : $value );
                        } else {
                            $single_outlet_categories[$key] = strval( $value );
                        }
                    }
                    // print_r($single_outlet_categories);
                    // exit;
                    array_push($final_outlet_categories, $single_outlet_categories);
                }

                $final_array['outlet_categories'] = $final_outlet_categories;
                
                foreach( $outlet_categories as $outcat ) {
                    $get_outlets = $this->get_outlets_by_category_id($outcat->id,0,4, $customer_id,$address_id);
                    $final_array[$outcat->category_name] = $get_outlets;
                }
                
                $final_array['cart_count'] = $cart_count;
                // $final_array['image_root_path'] = URL::asset('/images') . "/";

                return response()->json([
                    'message' => 'Outlet Category List Retrieved',
                    'data' => $final_array
                ], $this->successCode);
                
            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }
        
    }

    public function get_outlets_by_category(Request $request) {
        
        if ( isset($request->customer_id) && isset($request->outlet_category_id) && isset($request->offset) ) {
            // $latitude = $request->latitude;
            // $longitude = $request->longitude;
            
            $user_check = Auth::guard('customer-api')->id();

            $outlet_category_id = $request->outlet_category_id;
            $offset = $request->offset;
            $customer_id = $request->customer_id;
            $address_id = $request->address_id;

            // $get_customer = DB::table('customers')->where('customers.id', $customer_id)
            //                             ->join('customer_addresses','customers.id','=','customer_addresses.customer_id')
            //                             ->first();
            
            
            $get_customer = CustomerAddress::find($address_id);
            
                                        
            if(!$get_customer){
                return response()->json([
                    'message' => 'Customer address not found',
                    'status' => false
                ], $this->successCode);
            }

            if ( $user_check == $customer_id ) {

                $get_outlets = $this->get_outlets_by_category_id($outlet_category_id,$offset,10, $customer_id,$address_id);
                
                return response()->json([
                    'message' => 'Outlet List Retrieved',
                    'data' => $get_outlets
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }
    }

    public function get_items_by_outlet_id_product(Request $request)
    {
        if ( isset($request->outlet_id) ){
            
            $outlet_id = $request->outlet_id;
            $customer_id = $request->customer_id;
            $outlet_type = $request->outlet_type;
            
            
            $food_category = array('1','2','3');
            if($outlet_type == 'recipe'){
                if($request->food_category){
                    $food_category = explode(',',$request->food_category);
                }
            }

            $all_cart_items = array();
            
            //$all_items['offer_list'] = $this->offer_by_outlet($outlet_id);
            $all_cart_items['user_rating'] = $this->get_avg_rating($outlet_id);
            $all_cart_items['category'] = array();

            $get_outlet_type = Outlet::where('id', $outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');

            if ( $get_outlet_type[0] == "product" ) {

                $final_product = array();
                $temp = array();
                // $all_category = DB::select('select * from categories where status = 1 and id in (select child_category_id from products where child_category_id <> "" and outlet_id = ? group by child_category_id)',[$outlet_id]);


                $all_category = DB::select('select * from categories where outlet_id = ? and priority = "1" and status = "1"',[$outlet_id]);
                foreach ($all_category as $category) {
                    
                    if($category->priority == 1){
                        
                        $get_all_products = DB::select('select * from products where outlet_id = ? and category_id = ? and status = "1"',[$outlet_id,$category->id]);
                    
                    
                    }
                    // elseif ($category->priority == 2) {
                    //     $get_all_products = DB::select('select * from products where outlet_id = ? and sub_category_id = ? and child_category_id IS NULL and status = "1"',[$outlet_id,$category->id]);   
                    // }
                    // else{
                    //     $get_all_products = DB::select('select * from products where outlet_id = ? and child_category_id = ? and status = "1"',[$outlet_id,$category->id]);
                    // }
                    // print_r($get_all_products);exit();
                    if(!empty($get_all_products)){

                        foreach ( $get_all_products as $rec ) {
                            $get_order_items = DB::table('products')->where('id', $rec->id)->first();

                            $get_item_qty = DB::table('cart_items')->where('item_id', $rec->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();

                            $final_product['item_id'] = empty($get_order_items->id) ? '' : strval( $get_order_items->id );
                            $final_product['item_name'] = empty($get_order_items->product_name) ? '' : strval( $get_order_items->product_name );
                            $final_product['category'] = $category->category_name;
                            $final_product['price'] = empty($get_order_items->price) ? '' : strval( $get_order_items->price );
                            $final_product['special_price'] = empty($get_order_items->special_price) ? '' : strval( $get_order_items->special_price );
                            $final_product['discount'] = empty($get_order_items->discount) ? '' : strval( $get_order_items->discount );
                            $final_product['image_url'] = URL::asset('/images') . "/" . ( empty($get_order_items->image_url) ? 'test.jpg' : $get_order_items->image_url );
                            $final_product['charges_gst'] = "";
                            $final_product['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                            $final_product['food_category'] = '';
                            array_push($temp, $final_product);
                        }
                        $all_cart_items['category'][$category->category_name] = $temp;
                    }
                    $temp = array();
                }

            } else {

                $final_recipe = array();
                $temp = array();

                // $all_category = DB::select('select * from categories where status = 1 and id in (select sub_category_id from recipes where sub_category_id <> "" and outlet_id = ? group by sub_category_id)',[$outlet_id]);

                $all_category = DB::select('select * from categories where outlet_id = ? and priority = "1" and status = "1"',[$outlet_id]);

                foreach ($all_category as $category) {

                    if($category->priority == 1){
                        
                        // $get_all_recipes = DB::select('select * from recipes where outlet_id = ? and category_id = ? 
                        //         and status = "1" and 
                        //     (("from_time" > ? and "to_time" < ? ) OR ("from_time" is null and "to_time" is null)) '
                        //     ,[$outlet_id,$category->id,strtotime('now'),strtotime('now')]);
                        
                        $get_all_recipes = DB::table('recipes')->where('outlet_id',$outlet_id)
                            ->where('status','1')
                            ->where('category_id',$category->id)
                            ->whereIn('food_cat',$food_category)
                            ->Where(function($q){
                                $q->where(function($q){
                                        $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                        ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                    })
                                    ->orWhere(function($q){
                                        $q->whereNull('from_time')
                                        ->whereNull('to_time');
                                    });        
                            })
                            
                            ->get();
                            
                    
                    }
                    // elseif ($category->priority == 2) {
                    //     $get_all_recipes = DB::select('select * from recipes where outlet_id = ? and sub_category_id = ? and status = "1"',[$outlet_id,$category->id]);
                    // }
                    if(!$get_all_recipes->isEmpty()){

                        foreach ( $get_all_recipes as $rec ) {
                            $get_order_items = DB::table('recipes')->where('id', $rec->id)->first();
                            $get_item_qty = DB::table('cart_items')->where('item_id', $rec->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();

                            $final_recipe['item_id'] = empty($get_order_items->id) ? '' : strval( $get_order_items->id );
                            $final_recipe['item_name'] = empty($get_order_items->name) ? '' : strval( $get_order_items->name );
                            $final_product['category'] = $category->category_name;
                            $final_recipe['price'] = empty($get_order_items->price) ? '' : strval( $get_order_items->price );
                            $final_recipe['special_price'] = empty($get_order_items->price) ? '' : strval( $get_order_items->price );
                            $final_recipe['discount'] = "";
                            $final_recipe['image_url'] = URL::asset('/images') . "/" . ( empty($get_order_items->image_url) ? 'test.jpg' : $get_order_items->image_url );
                            $final_recipe['charges_gst'] = empty($get_order_items->charges_gst) ? '' : strval( $get_order_items->charges_gst );
                            $final_recipe['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                            $final_recipe['food_category'] = empty($get_order_items->food_cat) ? '-1' : strval($get_order_items->food_cat);
                            

                            array_push($temp, $final_recipe);
                        }
                        $all_cart_items['category'][$category->category_name] = $temp;
                    }
                    $temp = array();
                }


            }

            return response()->json([
                'message' => 'Items List Retrieved',
                'data' => $all_cart_items
            ], $this->successCode);
            
        }
    }

    //Old code backup
    public function get_items_by_outlet_id(Request $request)
    {
        $get_outlet_type = Outlet::where('id', $request->outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');

            if ( $get_outlet_type[0] == "product" ) {
                return $this->get_items_by_outlet_id_product($request);
            }

        $da = Recipe::where('status','1')->pluck('category_id');

        $category = Category::where('outlet_id',$request->outlet_id)
                    ->where('priority','1')
                    ->where('status','1')
                    ->whereIn('id', $da)
                    ->where('outlet_type','recipe')
                    ->get(['id', 'category_name']);

        foreach($category as $cate){
            
                $cate->sub_category = Category::where('parent_category',$cate->id)
                                        ->where('outlet_type','recipe')
                                        ->where('status','1')
                                        ->get(['id','category_name']);

                $main_category_items = Recipe::where('category_id',$cate->id)
                                        ->where('status','1')
                                        ->where(function($q){
                                            $q->whereNull('sub_category_id')
                                                ->orWhere('sub_category_id','0');
                                        })
                                        ->Where(function($q){
                                            $q->where(function($q){
                                                    $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                                    ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                                })
                                                ->orWhere(function($q){
                                                    $q->whereNull('from_time')
                                                    ->whereNull('to_time');
                                                });        
                                        })
                                        ->get();

                foreach($main_category_items as $item){
                    $get_item_qty = DB::table('cart_items')->where('item_id', $item->id)->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->first();
                        $item['item_id'] = empty($item->id) ? '' : strval( $item->id );
                        $item['item_name'] = empty($item->name) ? '' : strval( $item->name );
                            
                        $item['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                        $item['image_url'] = URL::asset('/images') . "/" . ( empty($item->image_url) ? 'test.jpg' : $item->image_url );
                        $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                        $item['category'] = '';
                        $item['price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['special_price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['discount'] = "";
                        $item['charges_gst'] = empty($item->charges_gst) ? '' : strval( $item->charges_gst );
                }


                $cate->main_category_items = $main_category_items;                        

                foreach($cate->sub_category as $recipe){

                    $recipe->items = Recipe::where('sub_category_id',$recipe->id)
                                    ->where('status','1')
                                    ->Where(function($q){
                                        $q->where(function($q){
                                                $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                                ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                            })
                                            ->orWhere(function($q){
                                                $q->whereNull('from_time')
                                                ->whereNull('to_time');
                                            });        
                                    })
                                    ->get();
                    foreach($recipe->items as $item){
                        $get_item_qty = DB::table('cart_items')->where('item_id', $item->id)->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->first();
                        $item['item_id'] = empty($item->id) ? '' : strval( $item->id );
                        $item['item_name'] = empty($item->name) ? '' : strval( $item->name );
                            
                        $item['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                        $item['image_url'] = URL::asset('/images') . "/" . ( empty($item->image_url) ? 'test.jpg' : $item->image_url );
                        $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                        $item['category'] = '';
                        $item['price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['special_price'] = empty($item->price) ? '' : strval( $item->price );
                        $item['discount'] = "";
                        $item['charges_gst'] = empty($item->charges_gst) ? '' : strval( $item->charges_gst );
                    }
                }
        }

        //$all_items['offer_list'] = $this->offer_by_outlet($outlet_id);
        $all_items['user_rating'] = $this->get_avg_rating($request->outlet_id);
        $all_items['category'] = $category;
        
        return response()->json([
            'message' => 'Items List Retrieved',
            'data' => $all_items
        ], $this->successCode);
    }

    /**
    * @author Suyash Kachi
    * @param $request
    * @return item json by outlet id
    */
    public function get_items_by_outlet_id_bk(Request $request)
    {
        $outletType = Outlet::where('id', $request->outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');

        if ($outletType[0] == "product")
        {
            return $this->get_items_by_outlet_id_product($request);
        }

        $category = Category::where('outlet_id', $request->outlet_id)
                    ->where('priority','1')
                    ->where('status','1')
                    ->whereIn('id', function($query) { 
                        $query->select('category_id')->from('recipes')->where('status','1');
                    })
                    ->where('outlet_type','recipe')
                    ->get(['id', 'category_name']);

        $subcategory = Category::whereIn('parent_category', $category->pluck('id'))
        ->where('outlet_type','recipe')
        ->where('status','1')
        ->get(['id', 'category_name', 'parent_category']);

        $mainCategoryItems = Recipe::whereIn('category_id', $category->pluck('id'))
        ->where('status','1')
        ->where(function($query) {
            $query->whereNull('sub_category_id')
                ->orWhere('sub_category_id','0');
        })
        ->Where(function($query) {
            $query->where(function($query) {
                $query->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                    ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
            })
            ->orWhere(function($query) {
                $query->whereNull('from_time')
                ->whereNull('to_time');
            });
        })
        ->get();

        $itemsQuantity = DB::table('cart_items')->whereIn('item_id', $mainCategoryItems->pluck('id'))->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->get();

        foreach($category as $value)
        {            
            $value->sub_category = $subcategory->where('parent_category',$value->id)->values();

            foreach ($mainCategoryItems->where('category_id', $value->id)->values() as $item)
            {
                $item['item_id'] = empty($item->id) ? '' : strval($item->id);
                $item['item_name'] = empty($item->name) ? '' : strval($item->name);

                $get_item_details = DB::table('cart_items')->where('item_id', $item->id)->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->first();

                if(isset($get_item_details) && $get_item_details!='')
                {
                    $item['item_quantity'] = $get_item_details->item_quantity;
                }
                else
                {
                    $item['item_quantity'] = '0';
                } 

                /*if($itemsQuantity->isNotEmpty())
                {
                    $item['item_quantity'] = empty($itemsQuantity->where('item_id', $item->id)->first()->item_quantity) ? '0' : strval($itemsQuantity->where('item_id', $item->id)->first()->item_quantity);
                }
                else
                {
                    $item['item_quantity'] = '0';
                }*/
                

                $item['image_url'] = URL::asset('/images') . "/" . (empty($item->image_url) ? 'test.jpg' : $item->image_url);
                $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                $item['category'] = '';
                $item['price'] = empty($item->price) ? '' : strval($item->price);
                $item['special_price'] = empty($item->price) ? '' : strval($item->price);
                $item['discount'] = "";
                $item['charges_gst'] = empty($item->charges_gst) ? '' : strval($item->charges_gst);
            }

            $value->main_category_items = $mainCategoryItems->where('category_id', $value->id)->values();

            foreach($value->sub_category as $recipe)
            {
            	unset($recipe->parent_category);

                $recipe->items = Recipe::where('sub_category_id', $recipe->id)
                ->where('status','1')
                ->Where(function($q){
                    $q->where(function($q){
                        $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                        ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                        })
                        ->orWhere(function($q){
                            $q->whereNull('from_time')
                            ->whereNull('to_time');
                    });        
                })
                ->get();

                $itemsQuantity = DB::table('cart_items')->whereIn('item_id', $recipe->items->pluck('id'))->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->get();

                foreach($recipe->items as $item)
                {
                    $item['item_id'] = empty($item->id) ? '' : strval($item->id);
                    $item['item_name'] = empty($item->name) ? '' : strval($item->name);

                    $get_item_details = DB::table('cart_items')->where('item_id', $item->id)->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->first();

                    if(isset($get_item_details) && $get_item_details!='')
                    {
                        $item['item_quantity'] = $get_item_details->item_quantity;
                    }
                    else
                    {
                        $item['item_quantity'] = '0';
                    } 

                    /*if($itemsQuantity->isNotEmpty())
                    {
                        $item['item_quantity'] = empty($itemsQuantity->where('item_id', $item->id)->first()->item_quantity) ? '0' : strval($itemsQuantity->where('item_id', $item->id)->first()->item_quantity);
                    }
                    else
                    {
                        $item['item_quantity'] = '0';
                    }*/

                    $item['image_url'] = URL::asset('/images') . "/" . (empty($item->image_url) ? 'test.jpg' : $item->image_url);
                    $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                    $item['category'] = '';
                    $item['price'] = empty($item->price) ? '' : strval($item->price);
                    $item['special_price'] = empty($item->price) ? '' : strval($item->price);
                    $item['discount'] = "";
                    $item['charges_gst'] = empty($item->charges_gst) ? '' : strval($item->charges_gst);
                }
            }
        }

        $all_items['user_rating'] = $this->get_avg_rating($request->outlet_id);
        $all_items['category'] = $category;
        
        return response()->json([
            'message' => 'Items List Retrieved',
            'data' => $all_items
        ], $this->successCode);
    }

    //New code query updated
    /**
    * @author Suyash Kachi
    * @param $request
    * @return item json by outlet id
    */
    public function get_items_by_outlet_id_bk_240321(Request $request)
    {
        $outletType = Outlet::where('id', $request->outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');

        if ($outletType[0] == "product")
        {
            return $this->get_items_by_outlet_id_product($request);
        }

        $category = Category::where('outlet_id', $request->outlet_id)
                    ->where('outlet_type','recipe')
                    ->where('status','1')
                    ->whereExists(function($query) { 
                        $query->select(DB::raw(1))->from('recipes')
                        ->where('recipes.status','1')
                        ->where(function($query) {
                            $query->whereNull('categories.parent_category')
                            ->where('categories.priority', 1)
                            ->whereColumn('categories.id', 'recipes.category_id');
                        })
                        ->orWhere(function($query) {
                            $query->whereNotNull('categories.parent_category')
                            ->whereColumn('categories.parent_category', 'recipes.category_id');
                        });
                    })
                    ->get(['id', 'category_name', 'parent_category']);

        $mainCategoryItems = Recipe::whereExists(function($query) use($request) {
            $query->select('*')->from('categories')
            ->where('categories.outlet_id', $request->outlet_id)
            ->where('categories.priority', 1)
            ->where('categories.outlet_type','recipe')
            ->where('categories.status','1')
            ->whereColumn('categories.id', 'recipes.category_id');
        })
        ->where('recipes.status','1')
        ->where(function($query) {
            $query->whereNull('recipes.sub_category_id')
                ->orWhere('recipes.sub_category_id','0');
        })
        ->Where(function($query) {
            $query->where(function($query) {
                $query->where('recipes.from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                    ->where('recipes.to_time','>',date('G:i',strtotime('now')+60*60*5.5));
            })
            ->orWhere(function($query) {
                $query->whereNull('recipes.from_time')
                ->whereNull('recipes.to_time');
            });
        })
        ->get(['id','category_id', 'name', 'image_url', 'food_cat', 'price', 'charges_gst']);

        $itemsQuantity = DB::table('cart_items')->whereExists(function($query) use ($request) {
            $query->select(DB::raw(1))->from('recipes')
            ->whereColumn('recipes.id', 'cart_items.item_id')
            ->whereExists(function($query) use ($request) {
                $query->select(DB::raw('*'))->from('categories')
                ->where('outlet_id', $request->outlet_id)
                ->where('categories.priority', 1)
                ->where('categories.outlet_type','recipe')
                ->where('categories.status','1')
                ->whereColumn('categories.id', 'recipes.category_id');
            })
            ->where('recipes.status','1')
            ->where(function($query) {
                $query->whereNull('recipes.sub_category_id')
                ->orWhere('recipes.sub_category_id','0');
            })
            ->Where(function($query) {
                $query->where(function($query) {
                    $query->where('recipes.from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                        ->where('recipes.to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                })
                ->orWhere(function($query) {
                    $query->whereNull('recipes.from_time')
                    ->whereNull('recipes.to_time');
                });
            });
        })
        ->where('cart_items.customer_id', $request->customer_id)
        ->where('cart_items.outlet_id', $request->outlet_id)
        ->get(['id', 'item_id', 'item_quantity']);

        foreach($category as $value)
        {
            $value->sub_category = $category->where('parent_category',$value->id)->values();

            foreach ($mainCategoryItems->where('category_id', $value->id)->values() as $item)
            {
                $item['item_id'] = empty($item->id) ? '' : strval($item->id);
                $item['item_name'] = empty($item->name) ? '' : strval($item->name);

                if($itemsQuantity->isNotEmpty())
                {
                    $item['item_quantity'] = empty($itemsQuantity->where('item_id', $item->id)->first()->item_quantity) ? '0' : strval($itemsQuantity->where('item_id', $item->id)->first()->item_quantity);
                }
                else
                {
                    $item['item_quantity'] = '0';
                }

                $item['image_url'] = URL::asset('/images') . "/" . (empty($item->image_url) ? 'test.jpg' : $item->image_url);
                $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                $item['category'] = '';
                $item['price'] = empty($item->price) ? '' : strval($item->price);
                $item['special_price'] = empty($item->price) ? '' : strval($item->price);
                $item['discount'] = "";
                $item['charges_gst'] = empty($item->charges_gst) ? '' : strval($item->charges_gst);
                $item['description'] = $item->description;

                unset($item["id"]);
                unset($item["name"]);
                unset($item["food_cat"]);
                unset($item["charges_gst"]);
                unset($item["category"]);
            }

            $value->main_category_items = $mainCategoryItems->where('category_id', $value->id)->values();

            foreach($value->sub_category as $recipe)
            {
                unset($recipe->parent_category);

                $recipe->items = Recipe::where('sub_category_id', $recipe->id)
                ->where('status','1')
                ->Where(function($q){
                    $q->where(function($q){
                        $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                        ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                        })
                        ->orWhere(function($q){
                            $q->whereNull('from_time')
                            ->whereNull('to_time');
                    });        
                })
                ->get(['id', 'name', 'image_url', 'food_cat', 'price', 'charges_gst']);

                $itemsQuantity = DB::table('cart_items')->whereIn('item_id', $recipe->items->pluck('id'))->where('customer_id', $request->customer_id)->where('outlet_id', $request->outlet_id)->get(['id', 'item_id', 'item_quantity']);

                foreach($recipe->items as $item)
                {
                    $item['item_id'] = empty($item->id) ? '' : strval($item->id);
                    $item['item_name'] = empty($item->name) ? '' : strval($item->name); 

                    if($itemsQuantity->isNotEmpty())
                    {
                        $item['item_quantity'] = empty($itemsQuantity->where('item_id', $item->id)->first()->item_quantity) ? '0' : strval($itemsQuantity->where('item_id', $item->id)->first()->item_quantity);
                    }
                    else
                    {
                        $item['item_quantity'] = '0';
                    }

                    $item['image_url'] = URL::asset('/images') . "/" . (empty($item->image_url) ? 'test.jpg' : $item->image_url);
                    $item['food_category'] = empty($item->food_cat) ? '-1' : strval($item->food_cat);
                    $item['category'] = '';
                    $item['price'] = empty($item->price) ? '' : strval($item->price);
                    $item['special_price'] = empty($item->price) ? '' : strval($item->price);
                    $item['discount'] = "";
                    $item['charges_gst'] = empty($item->charges_gst) ? '' : strval($item->charges_gst);

                    unset($item["id"]);
	                unset($item["name"]);
	                unset($item["food_cat"]);
	                unset($item["charges_gst"]);
	                unset($item["category"]);
                }
            }
        }

        $all_items['user_rating'] = $this->get_avg_rating($request->outlet_id);
        $all_items['category'] = $category;
        
        return response()->json([
            'message' => 'Items List Retrieved',
            'data' => $all_items
        ], $this->successCode);
    }

    public function offer_by_outlet(Request $request){
        
        $outlet_id = $request->outlet_id;

        $offer = Offers::where('status','1')
                            ->whereRaw('STR_TO_DATE(expiry_date,"%d/%m/%Y") >= date(now())')
                            // ->where('expiry_date', '>=', date('d/m/Y'))
                            ->where('outlet_id', 'LIKE','%"'. $outlet_id .'"%')
                            ->get();
        // dd($offer);
                foreach($offer as $off){
                    if($off){
                        if($off->image_url){
                            $off->image_url = URL::asset('/images') . "/" .$off->image_url;
                        }else{
                            $off->image_url = "";
                        }
                    }
                }
        return response()->json([
            'message' => 'Offer List Retrieved',
            'data' => $offer
        ], $this->successCode);

    }

    public function search_by_item_name_outlet_id(Request $request) {
        if ( isset($request->outlet_id) ) {
            
            $outlet_id = $request->outlet_id;
            $customer_id = $request->customer_id;
            $item_name = $request->item_name;

            $all_cart_items = array();
            
            $all_cart_items['user_rating'] = $this->get_avg_rating($outlet_id);
            $temp = array();

            $get_outlet_type = Outlet::where('id', $outlet_id)
            ->where('flag','0')
            ->pluck('outlet_type');

            if ( $get_outlet_type[0] == "product" ) {

                $final_product = array();
                
                $get_all_products = DB::select('select * from products where outlet_id = "'. $outlet_id .'" and status = "1" and product_name like "%'. $item_name .'%"');
                foreach ( $get_all_products as $rec ) {
                    $get_order_items = DB::table('products')->where('id', $rec->id)->get();

                    foreach( $get_order_items as $it ) {
                        $get_item_qty = DB::table('cart_items')->where('item_id', $it->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();

                        $final_product['item_id'] = empty($it->id) ? '' : strval( $it->id );
                        $final_product['item_name'] = empty($it->product_name) ? '' : strval( $it->product_name );
                        $final_product['price'] = empty($it->price) ? '' : strval( $it->price );
                        $final_product['special_price'] = empty($it->special_price) ? '' : strval( $it->special_price );
                        $final_product['discount'] = empty($it->discount) ? '' : strval( $it->discount );
                        $final_product['image_url'] = URL::asset('/images') . "/" . ( empty($it->image_url) ? 'test.jpg' : $it->image_url );
                        $final_product['charges_gst'] = "";
                        $final_product['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                        $final_product['food_category'] = '';
                            
                    }
                    array_push($temp, $final_product);
                }
                $all_cart_items['items'] = $temp;


            } else {

                $final_recipe = array();

                //$get_all_recipes = DB::select('select * from recipes where outlet_id = "'. $outlet_id .'" and status = "1" and name like "%'. $item_name .'%"');
                
                $get_all_recipes = DB::table('recipes')
                            ->where('outlet_id',$outlet_id)
                            ->where('name','LIKE','%'. $item_name .'%')
                            ->where('status','1')
                            ->Where(function($q){
                                $q->where(function($q){
                                        $q->where('from_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                        ->where('to_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                    })
                                    ->orWhere(function($q){
                                        $q->whereNull('from_time')
                                        ->whereNull('to_time');
                                    });        
                            })
                            ->get();
                
                
                foreach ( $get_all_recipes as $rec ) {
                    $get_order_items = DB::table('recipes')->where('id', $rec->id)->get();

                    foreach( $get_order_items as $it ) {
                        $get_item_qty = DB::table('cart_items')->where('item_id', $it->id)->where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();
                        
                        $final_recipe['item_id'] = empty($it->id) ? '' : strval( $it->id );
                        $final_recipe['item_name'] = empty($it->name) ? '' : strval( $it->name );
                        $final_recipe['price'] = empty($it->price) ? '' : strval( $it->price );
                        $final_recipe['special_price'] = empty($it->price) ? '' : strval( $it->price );
                        $final_recipe['discount'] = "";
                        $final_recipe['image_url'] = URL::asset('/images') . "/" . ( empty($it->image_url) ? 'test.jpg' : $it->image_url );
                        $final_recipe['charges_gst'] = empty($it->charges_gst) ? '' : strval( $it->charges_gst );
                        $final_product['item_quantity'] = empty($get_item_qty->item_quantity) ? '0' : strval($get_item_qty->item_quantity);
                        $final_recipe['food_category'] = empty($it->food_cat) ? '' : strval($it->food_cat);
                            
                    }
                    array_push($temp, $final_recipe);
                }
                $all_cart_items['items'] = $temp;
                
            }

            return response()->json([
                'message' => 'Items List Retrieved',
                'data' => $all_cart_items
            ], $this->successCode);
            
        }
    }

}