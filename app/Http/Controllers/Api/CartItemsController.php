<?php

namespace App\Http\Controllers\Api;

use App\CartItem;
use App\TempOrders;
use App\TempOrderItems;
use App\Orders;
use App\OrderItems;
use App\OrderIdMaster;
use App\Http\Controllers\Controller;
use App\Outlet;
use App\Offers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Notifications\OrderRecived;

class CartItemsController extends Controller
{
    //
    // function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
    
    //     $earth_radius = 6371;
      
    //     $dLat = deg2rad($latitude2 - $latitude1);  
    //     $dLon = deg2rad($longitude2 - $longitude1);  
      
    //     $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
    //     $c = 2 * asin(sqrt($a));  
    //     $d = $earth_radius * $c;  
      
    //     return $d;  
    // }

    function getDistance($latitude1, $longitude1, $latitude2, $longitude2)
    {
        $long1 = deg2rad($longitude1);
        $long2 = deg2rad($longitude2);
        $lat1 = deg2rad($latitude1);
        $lat2 = deg2rad($latitude2);
         
        //Haversine Formula
        $dlong = $long2 - $long1;
        $dlati = $lat2 - $lat1;
         
        $val = pow(sin($dlati/2),2)+cos($lat1)*cos($lat2)*pow(sin($dlong/2),2);
         
        $res = 2 * asin(sqrt($val));
         
        // $radius = 3958.756;
        $radius = 6378.8;
         
        return ($res*$radius); 
    }
    
    public function get_items_in_cart_by_customer_id(Request $request) {
        

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;

            if ( $user_check == $customer_id ) {

                $get_cart_items = CartItem::where('customer_id', $request->customer_id)->get(['id', 'item_quantity', 'item_id', 'outlet_id', 'customer_id']);
                $my_cart = array();
                $all_cart_items = array();
                
                foreach( $get_cart_items as $items ) {

                        $final_product = array();
                        $final_product['item_id'] = empty($items->item_id) ? '0' : strval($items->item_quantity);
                        $final_product['outlet_id'] = empty($items->outlet_id) ? '0' : strval($items->outlet_id);
                        $final_product['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);
                        
                        array_push($all_cart_items, $final_product);
        
                }

                $my_cart['item_details'] = $all_cart_items;
                 

                return response()->json([
                    'message' => 'Cart List Retrieved',
                    'data' => $my_cart,
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function get_cart_data_by_customer_id(Request $request) {
        

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $outlet_gst = 0;

            if ( $user_check == $customer_id ) {

                $get_cart_items = CartItem::where('customer_id', $request->customer_id)->get(['id', 'item_quantity', 'item_id', 'outlet_id', 'customer_id']);
                $my_cart = array();
                $temp_outlet = array();
                $all_cart_items = array();
                $all_offers = array();
                $free_delivery = 0;
                $amount = 0;

                if(!$get_cart_items->isEmpty()){
                    $outlet = DB::table('outlets')->where('id',$get_cart_items[0]->outlet_id)->first();
                    $is_favourite = DB::table('customer_outlet_favourites')->where('customer_id', $customer_id)->where('outlet_id', $outlet->id)->count();

                    $get_customer = DB::table('customers')->where('customers.id', $customer_id)
                                        ->join('customer_addresses','customers.id','=','customer_addresses.customer_id')
                                        ->first();
                    
                    $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $get_customer->customer_latitude, $get_customer->customer_longitude);
                    // $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);
                    
                    $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlet->id)->avg('rating');

                    $temp_outlet['id']                      = empty($outlet->id) ? '' : strval($outlet->id);
                    $temp_outlet['outlet_distance']         = strval(number_format($distance)) . " km";
                    $temp_outlet['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
                    $temp_outlet['is_favourite']            = strval($is_favourite);
                    $temp_outlet['rating']                  = empty($rating) ? '0' : strval( $rating );
                    $temp_outlet['outlet_name']             = $outlet->outlet_name;
                    $temp_outlet['outlet_category_id']      = empty($outlet->outlet_category_id) ? '' : strval($outlet->outlet_category_id);
                    $temp_outlet['merchant_id']             = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
                    $temp_outlet['image_url']               = URL::asset('/images') . "/" . ( empty( json_decode($outlet->image_url) ) ? 'test.jpg' : json_decode($outlet->image_url) );
                    $temp_outlet['outlet_speciality']       = empty($outlet->outlet_speciality) ? '' : strval($outlet->outlet_speciality);
                    $temp_outlet['booking_for_two']         = empty($outlet->booking_for_two) ? '' : strval($outlet->booking_for_two);

                    //--apurva-code-start-(18 feb 2021)--//
                    $outlet_gst = $outlet->gst_rate;
                    //--apurva-code-end-(18 feb 2021)--//

                    $my_cart['outlet_details'] = $temp_outlet;
                }

                // $get_offers = DB::table('offers')
                //                 ->where('merchant_id',$temp_outlet['merchant_id'])
                //                 ->where('expiry_date', '>=', date('d/m/Y'))
                //                 ->where('status',1)
                //                 ->get();

                // if(!empty($get_offers)){
                //     foreach ($get_offers as $key => $offer_val) {
                //         if(in_array($temp_outlet['id'], json_decode($offer_val->outlet_id))) {
                //             array_push($all_offers, $offer_val);
                //             if($offer_val->offer_type == 'delivery_based'){
                //                 $free_delivery = 1;
                //             }
                //         }
                //     }
                // }
                if(!$get_cart_items->isEmpty()){
                    foreach( $get_cart_items as $items ) {

                        if ( $outlet->outlet_type == "product" ) {
            
                            // echo 'product';
                            $final_product = array();

                            $get_order_items = DB::table('products')->where('id', $items->item_id)->get(['id', 'product_name', 'price', 'special_price', 'discount', 'image_url']);
                            
                            foreach( $get_order_items as $it ) {
                                $final_product['item_id'] = empty($it->id) ? '0' : strval($it->id);
                                $final_product['item_name'] = empty($it->product_name) ? '0' : strval($it->product_name);
                                $final_product['price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_product['special_price'] = empty($it->special_price) ? '0' : strval($it->special_price);
                                $final_product['discount'] = empty($it->discount) ? '0' : strval($it->discount);
                                $final_product['image_url'] = URL::asset('/images') . "/" . ( empty( $it->image_url ) ? 'test.jpg' : $it->image_url );
                                $final_product['charges_gst'] = "";
                                $final_product['food_category'] = '';
                            }
                            $final_product['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);

                            $amount += $final_product['special_price'] * $final_product['item_quantity'];
                            
                            array_push($all_cart_items, $final_product);
            
                        } else {
            
                            // echo 'recipe';
                            $final_recipe = array();

                            $get_order_items = DB::table('recipes')->where('id', $items->item_id)->get(['id', 'name', 'price', 'charges_gst', 'image_url']);
                            
                            foreach( $get_order_items as $it ) {
                                $final_recipe['item_id'] = empty($it->id) ? '0' : strval($it->id);
                                $final_recipe['item_name'] = empty($it->name) ? '0' : strval($it->name);
                                $final_recipe['price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_recipe['special_price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_recipe['discount'] = "";
                                $final_recipe['image_url'] = URL::asset('/images') . "/" . ( empty( $it->image_url ) ? 'test.jpg' : $it->image_url );
                                $final_recipe['charges_gst'] = empty($it->charges_gst) ? '0' : strval($it->charges_gst);
                                $final_recipe['food_category'] = empty($it->food_cat) ? '' : strval($it->food_cat);
                            }
                            $final_recipe['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);

                            $amount += $final_recipe['price'] * $final_recipe['item_quantity'];

                            array_push($all_cart_items, $final_recipe);
            
                        }

                    }
                }

                $my_cart['item_details'] = $all_cart_items;
                // $my_cart['free_delivery_available'] = strval($free_delivery);
                // ----------Main calculation----------

                //--apurva-code-start-(18 feb 2021)--//
                $my_cart['outlet_gst'] = $outlet_gst;
                //--apurva-code-end-(18 feb 2021)--//
                
                $my_cart['amount'] = strval($amount);
                $my_cart['discount'] = strval(0);
                // $outlet_delivery_charges = empty($outlet->delivery_charges) ? '0' : strval($outlet->delivery_charges);
                // if($free_delivery){

                    // $my_cart['delivery_charges'] = '0';
                    
                // }
                // else {
                    $my_cart['delivery_charges'] = empty($outlet->delivery_charges) ? '0' : strval($outlet->delivery_charges);
                // }

                $my_cart['total_amount'] = strval($my_cart['amount'] + $my_cart['delivery_charges'] - $my_cart['discount']);
                
                $gst_rate = empty($outlet->gst_rate) ? '0' : strval($outlet->gst_rate);
                if(!$get_cart_items->isEmpty()){
                    if ( $outlet->outlet_type == "product" ) {
                        $my_cart['taxes'] = strval(0);
                    }
                    else{
                        $my_cart['taxes'] = strval(($my_cart['total_amount'] * $gst_rate)/100);
                    }
                }
                else{
                    $my_cart['taxes'] = strval(0);
                }

                $my_cart['final_amount'] = strval($my_cart['total_amount'] + $my_cart['taxes']);
                // if($free_delivery){
                //     $my_cart['total_saving'] = strval($my_cart['discount'] + $outlet_delivery_charges);
                // }
                // else{
                    $my_cart['total_saving'] = strval($my_cart['discount']);
                // }
                    
                $oid = empty($outlet->id) ? 'na' : $outlet->id;
                
                //whereRaw('date(expiry_date) >= ?',date('d/m/Y'))
                $Offers = Offers::whereRaw('STR_TO_DATE(expiry_date,"%d/%m/%Y") >= date(now())')
                // where('expiry_date', '>=', date('d/m/Y'))
                    ->where('status',1)
                    ->where('outlet_id', 'LIKE','%"'. $oid .'"%')
                    ->get();
                $offers = array();
                foreach($Offers as $offer){
                    $offe = array();
                    $offe['offer_images'] = URL::asset('/images') . "/" . ( empty($offer->image_url) ? 'test.jpg' : $offer->image_url );
                    $offe['promo_code'] = empty($offer->promo_code) ? '' : $offer->promo_code;
                    $offe['offer_type'] = empty($offer->offer_type) ? '' : $offer->offer_type;
                    $offe['discount_type'] = empty($offer->discount_type) ? '' : $offer->discount_type;
                    $offe['max_discount'] = empty($offer->max_discount) ? '' : $offer->max_discount;
                    $offe['min_amount'] = empty($offer->amount) ? '' : $offer->amount;
                    $offe['discount'] = empty($offer->discount) ? '' : $offer->discount;
                    
                    
                    array_push($offers, $offe);
                }

                $my_cart['offer_list'] = $offers;

                return response()->json([
                    'message' => 'Cart List Retrieved',
                    'data' => $my_cart,
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function create_temp_order_by_customer_id(Request $request) {
        

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $customer_addr_id = $request->customer_addr_id;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $request_discount = $request->discount;
            
            if ( $user_check == $customer_id ) {

                $get_cart_items = CartItem::where('customer_id', $request->customer_id)->get(['id', 'item_quantity', 'item_id', 'outlet_id', 'customer_id']);
                $my_cart = array();
                $temp_outlet = array();
                $all_cart_items = array();
                $all_offers = array();
                $free_delivery = 0;
                $amount = 0;
                $outlet_id = 0;

                if(!$get_cart_items->isEmpty()){
                    $outlet = DB::table('outlets')->where('id',$get_cart_items[0]->outlet_id)->first();
                    // print_r($outlet);exit();
                    $is_favourite = DB::table('customer_outlet_favourites')->where('customer_id', $customer_id)->where('outlet_id', $outlet->id)->count();

                    $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);

                    $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlet->id)->avg('rating');

                    $temp_outlet['id']                      = empty($outlet->id) ? '' : strval($outlet->id);
                    $temp_outlet['outlet_distance']         = strval(number_format($distance)) . " km";
                    $temp_outlet['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
                    $temp_outlet['is_favourite']            = strval($is_favourite);
                    $temp_outlet['rating']                  = empty($rating) ? '0' : strval( $rating );
                    $temp_outlet['outlet_name']             = $outlet->outlet_name;
                    $temp_outlet['outlet_category_id']      = empty($outlet->outlet_category_id) ? '' : strval($outlet->outlet_category_id);
                    $temp_outlet['merchant_id']             = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
                    $temp_outlet['image_url']               = URL::asset('/images') . "/" . ( empty( json_decode($outlet->image_url) ) ? 'test.jpg' : json_decode($outlet->image_url) );
                    $temp_outlet['outlet_speciality']       = empty($outlet->outlet_speciality) ? '' : strval($outlet->outlet_speciality);
                    $temp_outlet['booking_for_two']         = empty($outlet->booking_for_two) ? '' : strval($outlet->booking_for_two);

                    $outlet_id = $outlet->id;

                    $my_cart['outlet_details'] = $temp_outlet;
                }



                if(!$get_cart_items->isEmpty()){
                    foreach( $get_cart_items as $items ) {

                        if ( $outlet->outlet_type == "product" ) {
            
                            // echo 'product';
                            $final_product = array();

                            $get_order_items = DB::table('products')->where('id', $items->item_id)->get(['id', 'product_name', 'price', 'special_price', 'discount', 'image_url']);
                            
                            foreach( $get_order_items as $it ) {
                                $final_product['item_id'] = empty($it->id) ? '0' : strval($it->id);
                                $final_product['item_name'] = empty($it->product_name) ? '0' : strval($it->product_name);
                                $final_product['price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_product['special_price'] = empty($it->special_price) ? '0' : strval($it->special_price);
                                $final_product['discount'] = empty($it->discount) ? '0' : strval($it->discount);
                                $final_product['image_url'] = URL::asset('/images') . "/" . ( empty( $it->image_url ) ? 'test.jpg' : $it->image_url );
                                $final_product['charges_gst'] = "";
                            }
                            $final_product['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);

                            $amount += $final_product['special_price'] * $final_product['item_quantity'];
                            
                            array_push($all_cart_items, $final_product);
            
                        } else {
            
                            // echo 'recipe';
                            $final_recipe = array();

                            $get_order_items = DB::table('recipes')->where('id', $items->item_id)->get(['id', 'name', 'price', 'charges_gst', 'image_url']);
                            
                            foreach( $get_order_items as $it ) {
                                $final_recipe['item_id'] = empty($it->id) ? '0' : strval($it->id);
                                $final_recipe['item_name'] = empty($it->name) ? '0' : strval($it->name);
                                $final_recipe['price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_recipe['special_price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_recipe['discount'] = "";
                                $final_recipe['image_url'] = URL::asset('/images') . "/" . ( empty( $it->image_url ) ? 'test.jpg' : $it->image_url );
                                $final_recipe['charges_gst'] = empty($it->charges_gst) ? '0' : strval($it->charges_gst);
                            }
                            $final_recipe['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);

                            $amount += $final_recipe['price'] * $final_recipe['item_quantity'];

                            array_push($all_cart_items, $final_recipe);
            
                        }

                    }
                }
                
                // $get_offers = DB::table('offers')
                //                 ->where('merchant_id',$temp_outlet['merchant_id'])
                //                 ->where('expiry_date', '>=', date('d/m/Y'))
                //                 ->where('status',1)
                //                 ->get();
                
                // if(!empty($get_offers)){
                //     foreach ($get_offers as $key => $offer_val) {
                //         if(in_array($temp_outlet['id'], json_decode($offer_val->outlet_id))) {
                //             array_push($all_offers, $offer_val);
                //             if($offer_val->offer_type == 'delivery_based'){
                //                 if($amount >= $offer_val->amount){
                //                     $free_delivery = 1;
                //                 }   
                //             }
                //         }
                //     }
                // }


                $my_cart['item_details'] = $all_cart_items;
                
                //$my_cart['free_delivery_available'] = strval($free_delivery);
                // ----------Main calculation----------
                $my_cart['amount'] = strval($amount);
                
                $outlet_delivery_charges = empty($outlet->delivery_charges) ? '0' : strval($outlet->delivery_charges);
                $my_cart['delivery_charges'] = empty($outlet->delivery_charges) ? '0' : strval($outlet->delivery_charges);
                // if($free_delivery){
                //     $my_cart['discount'] = strval($outlet_delivery_charges);
                // }
                //else {
                    $my_cart['discount'] = strval($request_discount);
                //}
                
                $my_cart['total_amount'] = strval($my_cart['amount'] + $my_cart['delivery_charges'] - $my_cart['discount']);
                
                $gst_rate = empty($outlet->gst_rate) ? '0' : strval($outlet->gst_rate);
                if(!$get_cart_items->isEmpty()){
                    if ( $outlet->outlet_type == "product" ) {
                        $my_cart['taxes'] = strval(0);
                    }
                    else{
                        $my_cart['taxes'] = strval(($my_cart['total_amount'] * $gst_rate)/100);
                    }
                }
                else{
                    $my_cart['taxes'] = strval(0);
                }

                $my_cart['final_amount'] = strval($my_cart['total_amount'] + $my_cart['taxes']);
                
                $my_cart['total_saving'] = strval($my_cart['discount']);
                

                $outlet_time = DB::table('outlets')
                                ->where('id',$outlet_id)
                                ->Where(function($q){
                                    $q->where(function($q){
                                            $q->where('open_time','<',date('G:i',strtotime('now')+60*60*5.5))
                                            ->where('close_time','>',date('G:i',strtotime('now')+60*60*5.5));
                                        })
                                        ->orWhere(function($q){
                                            $q->whereNull('open_time')
                                            ->whereNull('close_time');
                                        });        
                                })
                                ->get();
                if($outlet_time->isEmpty()){
                    return response()->json([
                        'status' => false,
                        'message' => 'Curently the  Outlet is closed',
                        
                    ], $this->successCode);
                }





                // ----------- Delete Previous Temp Orders---------
                $get_temp_orders = TempOrders::where('customer_id',$customer_id)->get();
                
                foreach ($get_temp_orders as $key => $order_val) {
                    $delete_items = TempOrderItems::where('order_id',$order_val->id)->delete();
                }

                $delete_orders = TempOrders::where('customer_id',$customer_id)->delete();

              
                // ----------- create Temp Order ---------
                $temp_order = new TempOrders();
                $temp_order -> merchant_id  = $outlet->merchant_id;
                $temp_order -> outlet_id    = $outlet->id;
                // if($free_delivery){
                //     $temp_order -> offer_type   = "delivery_based";
                // }
                $temp_order -> amount           = $my_cart['amount'];
                $temp_order -> delivery_charges = $my_cart['delivery_charges'];
                $temp_order -> discount         = $my_cart['discount'];
                $temp_order -> total_amount     = $my_cart['total_amount'];
                $temp_order -> tax              = $my_cart['taxes'];
                $temp_order -> final_amount     = $my_cart['final_amount'];
                $temp_order -> total_saving     = $my_cart['total_saving'];
                $temp_order -> customer_id      = $customer_id;
                $temp_order -> customer_addr_id = $customer_addr_id;
                $temp_order -> status           = 'Pending';
                $temp_order -> flag             = 0;

                $temp_order ->save();

                $my_cart['temp_order_id'] = $temp_order->id;
                // print_r($temp_order->id);exit();

                foreach ($my_cart['item_details'] as $key => $items) {
                    // print_r($items);
                    $temp_order_item = new TempOrderItems();
                    $temp_order_item -> item_id = $items['item_id'];
                    $temp_order_item -> item_code = $items['item_id'];
                    $temp_order_item -> item_name = $items['item_name'];
                    $temp_order_item -> item_price = $items['price'];
                    $temp_order_item -> item_qty = $items['item_quantity'];
                    $temp_order_item -> item_img = $items['image_url'];
                    $temp_order_item -> order_id = $temp_order->id;
                    $temp_order_item -> save();

                }


                return response()->json([
                    'message' => 'Temp Order Generated',
                    'status' => true,
                    'data' => array('temp_order_id'=> $temp_order->id ),
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function order_pre_finalize(Request $request) {
        

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $temp_order_id = $request->temp_order_id;
            $outlet_id = '';
            if ( $user_check == $customer_id ) {


                $get_cart_items = CartItem::where('customer_id', $request->customer_id)->get(['id', 'item_quantity', 'item_id', 'outlet_id', 'customer_id']);
                $my_cart = array();
                $temp_outlet = array();
                $all_cart_items = array();
                $all_offers = array();
                $free_delivery = 0;
                $amount = 0;
                $merchant_id = 0;

                if(!$get_cart_items->isEmpty()){
                    $outlet = DB::table('outlets')->where('id',$get_cart_items[0]->outlet_id)->first();
                    // $is_favourite = DB::table('customer_outlet_favourites')->where('customer_id', $customer_id)->where('outlet_id', $outlet->id)->count();

                    // $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);

                    // $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlet->id)->avg('rating');

                    $temp_outlet['id']                      = empty($outlet->id) ? '' : strval($outlet->id);
                    // $temp_outlet['outlet_distance']         = strval(number_format($distance)) . " km";
                    // $temp_outlet['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
                    // $temp_outlet['is_favourite']            = strval($is_favourite);
                    // $temp_outlet['rating']                  = empty($rating) ? '0' : strval( $rating );
                    // $temp_outlet['outlet_name']             = $outlet->outlet_name;
                    // $temp_outlet['outlet_category_id']      = empty($outlet->outlet_category_id) ? '' : strval($outlet->outlet_category_id);
                    $temp_outlet['merchant_id']             = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
                    // $temp_outlet['image_url']               = URL::asset('/images') . "/" . ( empty( json_decode($outlet->image_url) ) ? 'test.jpg' : json_decode($outlet->image_url) );
                    // $temp_outlet['outlet_speciality']       = empty($outlet->outlet_speciality) ? '' : strval($outlet->outlet_speciality);
                    // $temp_outlet['booking_for_two']         = empty($outlet->booking_for_two) ? '' : strval($outlet->booking_for_two);
                    $temp_outlet['cod']         = strval($outlet->is_cod);
                    $my_cart['outlet_details'] = $temp_outlet;

                    $outlet_id = $outlet->id;
                }



                $get_order = TempOrders::where('id', $temp_order_id)->first();
                
                if(!empty($get_order)){
                    if($get_order->customer_id == $customer_id){
                        $my_cart['offer_type'] = empty($get_order->offer_type) ? '' : strval($get_order->offer_type);
                        // ----------Main calculation----------
                        $my_cart['amount'] = empty($get_order->amount) ? '' : strval($get_order->amount);
                        $my_cart['discount'] = empty($get_order->discount) ? '0' : strval($get_order->discount);
                        if($my_cart['discount'] != 0){
                            $my_cart['discount'] = "-".$my_cart['discount']; 
                        }
                        $my_cart['delivery_charges'] = empty($get_order->delivery_charges) ? '0' : strval($get_order->delivery_charges);


                        $my_cart['total_amount'] = empty($get_order->total_amount) ? '0' : strval($get_order->total_amount);
                        
                        $my_cart['taxes'] = empty($get_order->tax) ? '0' : strval($get_order->tax);

                        $my_cart['final_amount'] = empty($get_order->final_amount) ? '0' : strval($get_order->final_amount);
                        
                        $my_cart['total_saving'] = empty($get_order->total_saving) ? '0' : strval($get_order->total_saving);
                        
                        $outlet_id = $get_order->outlet_id;
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Invalid Temporary Order Id',

                    ], $this->successCode);
                }

            
                $Offers = Offers::whereRaw('STR_TO_DATE(expiry_date,"%d/%m/%Y") >= date(now())')
                // where('expiry_date', '>=', date('d/m/Y'))
                    ->where('status',1)
                    ->where('outlet_id', 'LIKE','%'. $outlet_id .'%')
                    ->get();
                $offers = array();
                foreach($Offers as $offer){
                    $offe = array();
                    $offe['offer_images'] = URL::asset('/images') . "/" . ( empty($offer->image_url) ? 'test.jpg' : $offer->image_url );
                    $offe['promo_code'] = empty($offer->promo_code) ? '' : $offer->promo_code;
                    $offe['offer_type'] = empty($offer->offer_type) ? '' : $offer->offer_type;
                    array_push($offers, $offe);
                }
                
                $my_cart['offer_list'] = $offers;

                return response()->json([
                    'message' => 'Temp Order Generated',
                    'data' => $my_cart,                    

                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }
    public function generateOrderId(){
        $get_data = OrderIdMaster::first();

        $new_id = str_pad($get_data->order_id, 8, '0', STR_PAD_LEFT);
        DB::update('update order_id_master set order_id = order_id + 1');
        return 'HPL2021-'.$new_id;
    }
    public function place_order(Request $request) {
        
        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $temp_order_id = $request->temp_order_id;
            $payment_mode = $request->payment_mode;
            $transaction_id = $request->transaction_id;
            $get_final_amount = $request->final_amount;
            $special_instruction = $request->special_instruction;

            if ( $user_check == $customer_id ) {

                $get_cart_items = CartItem::where('customer_id', $request->customer_id)->get(['id', 'item_quantity', 'item_id', 'outlet_id', 'customer_id']);
                $my_cart = array();
                $temp_outlet = array();
                $all_cart_items = array();
                $all_offers = array();
                $free_delivery = 0;
                $amount = 0;

                $temp_order = TempOrders::where('id', $temp_order_id)->first();
                
                if(!empty($temp_order)){
                    if($temp_order->customer_id == $customer_id){
                        
                        $order = new Orders();
                        $order -> order_id = $this->generateOrderId();
                        $order -> merchant_id = $temp_order->merchant_id;
                        $order -> outlet_id = $temp_order->outlet_id;
                        $order -> promocode = $temp_order->promocode;
                        $order -> offer_type = $temp_order->offer_type;
                        $order -> amount = $temp_order->amount;
                        $order -> delivery_charges = $temp_order->delivery_charges;
                        $order -> discount = $temp_order->discount;
                        $order -> total_amount = $temp_order->total_amount;
                        $order -> tax = $temp_order->tax;
                        $order -> final_amount = $temp_order->final_amount;
                        $order -> total_saving = $temp_order->total_saving;
                        $order -> customer_id = $temp_order->customer_id;
                        $order -> special_instruction = $special_instruction;
                        $order -> customer_addr_id = $temp_order->customer_addr_id;
                        $order -> payment_mode = $payment_mode;
                        if($transaction_id != ''){
                            $order -> transaction_id = $transaction_id;
                        }
                        $order -> status = 'pending';
                        $order -> created_at = date("Y-m-d h:i:sa");
                        if($request -> dinein){
                            $order -> dinein = $request -> dinein;
                        }
                        if($request -> table_no){
                            $order -> table_no = $request -> table_no;
                        }
                            
                        $order -> save();

                        $temp_order_item = TempOrderItems::where('order_id', $temp_order_id)->get();
                        if($temp_order_item->isNotEmpty()){
                            foreach ($temp_order_item as $key => $value) {
                                $order_item = new OrderItems();
                                $order_item -> order_id = $order -> order_id;
                                $order_item -> item_id = $value -> item_id;
                                $order_item -> item_name = $value -> item_name;
                                $order_item -> item_code = $value -> item_code;
                                $order_item -> item_qty = $value -> item_qty;
                                $order_item -> item_price = $value -> item_price;
                                $order_item -> item_img = $value -> item_img;
                                $order_item -> created_at = date("Y-m-d h:i:sa");
                                $order_item -> save();
                            }    
                        }

                        $auth = Outlet::find($temp_order->outlet_id);
                        $auth->order_id = $order->id;
                        $auth->notify(new OrderRecived);

                        // send notification to outlet app
                        if ($auth->app_firebase_token != null) {
                            $this->order_notification_to_outlet_app($auth->app_firebase_token, $order->id, $temp_order->outlet_id);
                        }
                        
                        // ----------- Delete Previous Temp Orders---------
                        $get_temp_orders = TempOrders::where('customer_id',$customer_id)->get();
                        
                        foreach ($get_temp_orders as $key => $order_val) {
                            $delete_items = TempOrderItems::where('order_id',$order_val->id)->delete();
                        }

                        $delete_orders = TempOrders::where('customer_id',$customer_id)->delete();

                        // ------]-----]--]-]----]--]]]
                        return response()->json([
                            'message' => 'Order Generated Successfully',
                            'order_id' => $order->order_id
                        ], $this->successCode);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Invalid Temporary Order Id',

                    ], $this->successCode);
                }

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function order_notification_to_outlet_app($token, $order_id, $outlet_id)
    {
        $API_ACCESS_KEY = "AAAA8um-vn8:APA91bGCB3wCj6z1XWkURx9dvJVCcjnc03Pflkx-nEWsG7aRozx2LAqbYXGrYo7v8wGMtmtYsqijrklfVvqOXQ-50tx3xCEGcJwom6V0kbfPPZKw84lRXh3DrfpdYn5TbvpGeDKwYaWQ";
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        // $body = '{"order_id":'.$order_id.',"outlet_id":'.$outlet_id.'}';
        $body = [
            "order_id"  => $order_id,
            "outlet_id" => $outlet_id
        ];

        $notification = [
            'title' =>'New Order Received',
            'body' => $body,
            'icon' =>'myIcon', 
            'sound' => 'ring'
        ];
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'to'        => $token, //single token
            // 'notification' => $notification,
            'priority'=>'high',
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);


        return true;
    }

    public function apply_promo_code(Request $request) {
        

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $temp_order_id = $request->temp_order_id;
            $promo_code = $request->promo_code;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $user_offer_type = $request->offer_type;

            if ( $user_check == $customer_id ) {

                $get_order = TempOrders::where('id',$temp_order_id)->first();

                $my_cart = array();
                $temp_outlet = array();
                $all_offers = array();
                $free_discount_on_promo_code = 0;
                $apply_delivery_based = 0;

                if(!empty($get_order)){

                    // if($get_order->promocode != ''){
                    //     return response()->json([
                    //     'message' => 'Promo Code Already Applied!',
                    // ], $this->successCode);
                    // }

                    $outlet = DB::table('outlets')->where('id',$get_order->outlet_id)->first();
                    // $is_favourite = DB::table('customer_outlet_favourites')->where('customer_id', $customer_id)->where('outlet_id', $outlet->id)->count();

                    // $distance = $this->getDistance($outlet->outlet_latitude, $outlet->outlet_longitude, $latitude, $longitude);

                    // $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlet->id)->avg('rating');

                    $temp_outlet['id']                      = empty($outlet->id) ? '' : strval($outlet->id);
                    // $temp_outlet['outlet_distance']         = strval(number_format($distance)) . " km";
                    // $temp_outlet['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
                    // $temp_outlet['is_favourite']            = strval($is_favourite);
                    // $temp_outlet['rating']                  = empty($rating) ? '0' : strval( $rating );
                    // $temp_outlet['outlet_name']             = $outlet->outlet_name;
                    // $temp_outlet['outlet_category_id']      = empty($outlet->outlet_category_id) ? '' : strval($outlet->outlet_category_id);
                    $temp_outlet['merchant_id']             = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
                    $temp_outlet['cod']             = empty($outlet->is_cod) ? '' : strval($outlet->is_cod);
                    // $temp_outlet['image_url']               = URL::asset('/images') . "/" . ( empty( json_decode($outlet->image_url) ) ? 'test.jpg' : json_decode($outlet->image_url) );
                    // $temp_outlet['outlet_speciality']       = empty($outlet->outlet_speciality) ? '' : strval($outlet->outlet_speciality);
                    // $temp_outlet['booking_for_two']         = empty($outlet->booking_for_two) ? '' : strval($outlet->booking_for_two);

                    $my_cart['outlet_details'] = $temp_outlet;
                }
                else{
                    return response()->json([
                        'message' => 'Invalid Order Id!',
                    ], $this->successCode);
                }

                $get_offers = DB::table('offers')
                                ->where('merchant_id',$temp_outlet['merchant_id'])
                                ->whereRaw('STR_TO_DATE(expiry_date,"%d/%m/%Y") >= date(now())')
                                // ->where('expiry_date', '>=', date('d/m/Y'))
                                ->where('status',1)
                                ->get();
                if(!empty($get_offers)){
                    foreach ($get_offers as $key => $offer_val) {
                        if(in_array($temp_outlet['id'], json_decode($offer_val->outlet_id))) {
                            array_push($all_offers, $offer_val);
                            if($offer_val->offer_type == 'promo_code_based' && $user_offer_type == 'promo_code_based'){
                                if($offer_val->promo_code == $promo_code){
                                    $free_discount_on_promo_code = 1;
                                    $applied_offer = $offer_val;

                                }   
                            }
                            if($user_offer_type == 'delivery_based' && $offer_val->offer_type == 'delivery_based'){
                                $free_discount_on_promo_code = 1;
                                $apply_delivery_based = 1;
                                $applied_offer = $offer_val;
                            }
                        }
                    }
                }

                
                if($free_discount_on_promo_code == 1){
                    if($get_order->amount >= $applied_offer->amount){

                        if($applied_offer->discount_type == 'percentage'){

                            $new_discount = strval(($get_order->amount * $applied_offer->discount)/100);
                            $maximum = 0;
                            if($applied_offer->max_discount){
                                $maximum = $applied_offer->max_discount;
                            }
                            if($new_discount  > $maximum ){
                                $new_discount = $maximum;
                            }
                            $new_total_amount = strval($get_order->amount + $outlet->delivery_charges - $new_discount);
                        }
                        elseif ($applied_offer->discount_type == 'flat_amount') {
                            $new_discount = strval($applied_offer->discount);
                            $new_total_amount = strval($get_order->amount + $outlet->delivery_charges - $new_discount);
                        }
                        

                        $set_delivery_charges = $outlet->delivery_charges;
                        if($apply_delivery_based == 1){
                            $set_delivery_charges = '0';
                            $new_total_amount = strval($get_order->amount);
                            $new_discount = $outlet->delivery_charges;
                            $new_total_saving = strval($new_discount);
                            $promo_code = '';
                        }

                        $gst_rate = empty($outlet->gst_rate) ? '0' : strval($outlet->gst_rate);
                        if ( $outlet->outlet_type == "product" ) {
                            $new_taxes = strval(0);
                        }
                        else{
                            $new_taxes = strval(($new_total_amount * $gst_rate)/100);
                        }

                        $new_final_amount = $new_total_amount + $new_taxes;
                        $new_total_saving = strval($new_discount);
                        

                        $temp_order = TempOrders::find($temp_order_id);
                        $temp_order -> offer_type       = $user_offer_type;
                        $temp_order -> promocode       = $promo_code;
                        $temp_order -> amount           = $get_order->amount;
                        $temp_order -> delivery_charges = $set_delivery_charges;
                        $temp_order -> discount         = $new_discount;
                        $temp_order -> total_amount     = $new_total_amount;
                        $temp_order -> tax              = $new_taxes;
                        $temp_order -> final_amount     = $new_final_amount;
                        $temp_order -> total_saving     = $new_total_saving;

                        $temp_order ->save();    
                        $my_cart['offer_type'] = strval("promo_code_based");
                        $my_cart['amount'] = strval($get_order->amount);
                        $my_cart['promo_code'] =strval($promo_code);
                        $my_cart['discount'] = "-".strval($new_discount);
                        $my_cart['delivery_charges'] = strval($outlet->delivery_charges);
                        $my_cart['total_amount'] = strval($new_total_amount);
                        $my_cart['taxes'] = strval($new_taxes);
                        $my_cart['final_amount'] = strval($new_final_amount);
                        $my_cart['total_saving'] = strval($new_total_saving);
                        
                        return response()->json([
                            'message' => 'Promo Code Applied Successfully',
                            'data' => $my_cart,
                        ], $this->successCode);
                    }
                    else{
                        return response()->json([
                            'message' => 'You need to buy minimum Rs.'. $applied_offer->amount .' order to Apply this Promo Code',
                        ], $this->successCode);
                    }
                }
                else{
                    return response()->json([
                        'message' => 'Invalid Promo Code!',
                    ], $this->successCode);
                }
                
            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function remove_offer_from_cart(Request $request) {
        

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $temp_order_id = $request->temp_order_id;
            
            if ( $user_check == $customer_id ) {

                $get_cart_items = CartItem::where('customer_id', $request->customer_id)->get(['id', 'item_quantity', 'item_id', 'outlet_id', 'customer_id']);
                $my_cart = array();
                $temp_outlet = array();
                $all_cart_items = array();
                $all_offers = array();
                $free_delivery = 0;
                $amount = 0;

                if(!$get_cart_items->isEmpty()){
                    $outlet = DB::table('outlets')->where('id',$get_cart_items[0]->outlet_id)->first();
                    
                    $temp_outlet['id']                      = empty($outlet->id) ? '' : strval($outlet->id);
                    $temp_outlet['merchant_id']             = empty($outlet->merchant_id) ? '' : strval($outlet->merchant_id);
                    $temp_outlet['cod'] = empty($outlet->is_cod) ? '0' : strval($outlet->is_cod);
                    $my_cart['outlet_details'] = $temp_outlet;
                }



                if(!$get_cart_items->isEmpty()){
                    foreach( $get_cart_items as $items ) {

                        if ( $outlet->outlet_type == "product" ) {
            
                            // echo 'product';
                            $final_product = array();

                            $get_order_items = DB::table('products')->where('id', $items->item_id)->get(['id', 'product_name', 'price', 'special_price', 'discount', 'image_url']);
                            
                            foreach( $get_order_items as $it ) {
                                $final_product['price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_product['special_price'] = empty($it->special_price) ? '0' : strval($it->special_price);
                            }
                            $final_product['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);

                            $amount += $final_product['special_price'] * $final_product['item_quantity'];
                            
                            array_push($all_cart_items, $final_product);
            
                        } else {
            
                            // echo 'recipe';
                            $final_recipe = array();

                            $get_order_items = DB::table('recipes')->where('id', $items->item_id)->get(['id', 'name', 'price', 'charges_gst', 'image_url']);
                            
                            foreach( $get_order_items as $it ) {
                                $final_recipe['price'] = empty($it->price) ? '0' : strval($it->price);
                                $final_recipe['special_price'] = empty($it->price) ? '0' : strval($it->price);
                                
                            }
                            $final_recipe['item_quantity'] = empty($items->item_quantity) ? '0' : strval($items->item_quantity);

                            $amount += $final_recipe['price'] * $final_recipe['item_quantity'];

                            array_push($all_cart_items, $final_recipe);
            
                        }

                    }
                }

                $my_cart['offer_type'] = "";
                // ----------Main calculation----------
                $my_cart['amount'] = strval($amount);
                $my_cart['discount'] = strval(0);
                $outlet_delivery_charges = empty($outlet->delivery_charges) ? '0' : strval($outlet->delivery_charges);
                
                $my_cart['delivery_charges'] = empty($outlet->delivery_charges) ? '0' : strval($outlet->delivery_charges);

                $my_cart['total_amount'] = strval($my_cart['amount'] + $my_cart['delivery_charges'] - $my_cart['discount']);
                
                $gst_rate = empty($outlet->gst_rate) ? '0' : strval($outlet->gst_rate);
                if(!$get_cart_items->isEmpty()){
                    if ( $outlet->outlet_type == "product" ) {
                        $my_cart['taxes'] = strval(0);
                    }
                    else{
                        $my_cart['taxes'] = strval(($my_cart['total_amount'] * $gst_rate)/100);
                    }
                }
                else{
                    $my_cart['taxes'] = strval(0);
                }

                $my_cart['final_amount'] = strval($my_cart['total_amount'] + $my_cart['taxes']);
                
                $my_cart['total_saving'] = strval($my_cart['discount']);
                
                

                

              
                // ----------- create Temp Order ---------
                $temp_order = TempOrders::find($temp_order_id);
                $temp_order -> merchant_id  = $outlet->merchant_id;
                $temp_order -> outlet_id    = $outlet->id;
                $temp_order -> offer_type   = $my_cart['offer_type'];
                $temp_order -> promocode   = "";
                $temp_order -> amount           = $my_cart['amount'];
                $temp_order -> delivery_charges = $my_cart['delivery_charges'];
                $temp_order -> discount         = $my_cart['discount'];
                $temp_order -> total_amount     = $my_cart['total_amount'];
                $temp_order -> tax              = $my_cart['taxes'];
                $temp_order -> final_amount     = $my_cart['final_amount'];
                $temp_order -> total_saving     = $my_cart['total_saving'];
                $temp_order -> customer_id      = $customer_id;
                $temp_order -> status           = 'Pending';
                $temp_order -> flag             = 0;

                $temp_order ->save();

                $my_cart['temp_order_id'] = $temp_order->id;
                // print_r($temp_order);exit();

                return response()->json([
                    'message' => 'Offer Remove Successfully',
                    'data' => $my_cart,
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function add_to_cart(Request $request) {

        if ( isset($request->item_quantity) && isset($request->customer_id) && isset($request->outlet_id) && isset($request->item_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;

            if ( $user_check == $customer_id ) {
            
                $item_quantity = $request->item_quantity;
                $customer_id = $request->customer_id;
                $outlet_id = $request->outlet_id;
                $item_id = $request->item_id;

                $get_outlet_id_count = CartItem::where('outlet_id', $outlet_id)->where('customer_id', $customer_id)->count();
                $get_item_count = CartItem::where('item_id', $item_id)->where('outlet_id', $outlet_id)->where('customer_id', $customer_id)->count();
                $get_cart_count = CartItem::where('customer_id', $customer_id)->count();

                
                if ( $get_outlet_id_count != 0 || $get_cart_count == 0 ) {

                    if ( $get_item_count == 0 ) {

                        $cartItem = new CartItem([
                            'item_quantity' => $item_quantity,
                            'customer_id' => $customer_id,
                            'outlet_id' => $outlet_id,
                            'item_id' => $item_id,
                        ]);
                        $cartItem->save();

                        // $cart = $this->get_cart_count();

                        return response()->json([
                            'message' => 'Item added to cart Successfully!',
                            // 'data' => $cart
                        ], $this->successCode);

                    } else {
                        $get_item_detail = CartItem::where('item_id', $item_id)->where('outlet_id', $outlet_id)->where('customer_id', $customer_id)->first();
                        
                        $get_item_detail = json_decode($get_item_detail);
                        if($item_quantity == 0){
                            $empty_cart = CartItem::where('id', $get_item_detail->id)->delete();

                            return response()->json([
                                'message' => 'Item is Deleted Successfully!',
                            ], $this->successCode);  
                        }
                        else{
                            $cartItem = CartItem::find($get_item_detail->id);
                            $cartItem->item_quantity = $item_quantity;
                            $cartItem->customer_id = $customer_id;
                            $cartItem->outlet_id = $outlet_id;
                            $cartItem->item_id = $item_id;
                            $cartItem->save();  
                            
                            return response()->json([
                                'message' => 'Item is Updated Successfully!',
                            ], $this->successCode);   
                        }
                        
                    }

                } else {
                    return response()->json([
                        'message' => 'You can only order from one outlet at a time!',
                    ], $this->successCode);
                }

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }

        }

    }

    public function remove_item_from_cart(Request $request) {

        if ( isset($request->customer_id) && isset($request->outlet_id) && isset($request->item_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;

            if ( $user_check == $customer_id ) {
            
                $customer_id = $request->customer_id;
                $outlet_id = $request->outlet_id;
                $item_id = $request->item_id;

                $get_outlet_id_count = CartItem::where('outlet_id', $outlet_id)->where('customer_id', $customer_id)->count();
                $get_item_count = CartItem::where('item_id', $item_id)->where('outlet_id', $outlet_id)->where('customer_id', $customer_id)->count();
                $get_cart_count = CartItem::where('customer_id', $customer_id)->count();

                
                

                    if ( $get_item_count == 0 ) {

                        return response()->json([
                            'message' => 'Item Not Found For Delete!',
                            // 'data' => $cart
                        ], $this->successCode);

                    } else {
                        $get_item_detail = CartItem::where('item_id', $item_id)->where('outlet_id', $outlet_id)->where('customer_id', $customer_id)->first();
                        
                        $get_item_detail = json_decode($get_item_detail);
                        
                        $empty_cart = CartItem::where('id', $get_item_detail->id)->delete();

                        return response()->json([
                            'message' => 'Item is Removed Successfully!',
                        ], $this->successCode);  
                        
                        
                    }

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }

        }

    }

    public function empty_cart(Request $request) {
        
        if ( isset($request->customer_id) ) {
            
            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;

            if ( $user_check == $customer_id ) {
                
                $empty_cart = CartItem::where('customer_id', $customer_id)->delete();

                return response()->json([
                    'message' => 'Cart Cleared Successfully!',
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }

        }
    }

    public function get_cart_count(Request $request) {

        if ( isset($request->customer_id) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;

            if ( $user_check == $customer_id ) {

                $get_cart_items_count = CartItem::where('customer_id', $request->customer_id)->count();

                return response()->json([
                    'message' => 'Cart List Retrieved',
                    'data' => $get_cart_items_count
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

}