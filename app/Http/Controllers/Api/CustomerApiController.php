<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\CustomerAddress;
use App\CustomerOutletFavourite;
use App\CustomerOutletRating;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class CustomerApiController extends Controller
{
    //
    protected $redirectTo = '/customer';
    protected $guard = 'customer';

    /**
     * Create customer
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        // print_r( $request->first_name );

        // exit;
        $customer_mobile = Customer::whereMobile($request->mobile)->first();
        $customer_email = Customer::where('email', $request->email)->first();

        // print_r( $customer_email->email );
        // exit;

        $validator = $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email'. ( ( $customer_email != null ) ? '|unique:customers,email,'.$customer_email->email : '' ),
            'mobile' => 'required|string|min:10|max:10'. ( ( $customer_mobile != null ) ? '|unique:customers,mobile,'.$customer_mobile->mobile : '' ),
            'password' => 'required|string',
            'firebase_token' => 'required|string'
        ]);
        // print_r( $validator->errors() );
        // exit;
        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }
        $customer = new Customer([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'password' => Hash::make($request->password),
            'firebase_token' => $request->firebase_token
        ]);
        $customer->save();

        $customer = Customer::where('mobile', $request->mobile)->first();
        $token = $customer->createToken('Laravel Personal Access Client')->accessToken;

        $final_customer = array();
        $get_customer = DB::table('customers')->where('mobile', $request->mobile)->get(['id', 'first_name', 'last_name', 'email', 'mobile', 'image_url', 'firebase_token']);
        foreach( $get_customer as $cust ) {
            $final_customer['id'] = $cust->id;
            $final_customer['first_name'] = $cust->first_name;
            $final_customer['last_name'] = $cust->last_name;
            $final_customer['email'] = $cust->email;
            $final_customer['mobile'] = $cust->mobile;
            $final_customer['image_url'] = URL::asset('/images') . "/" . ( empty( $cust->image_url ) ? 'test.jpg' : $cust->image_url );
            // $final_customer['firebase_token'] = empty( $cust->firebase_token ) ? '' : $cust->firebase_token;
        }
        
        return response()->json([
            'message' => 'Successfully created customer!',
            'access_token' => $token,
            'token_type' => 'Bearer',
            'data' => $final_customer
        ], $this->successCode);
    }

    public function login(Request $request)
    {

        $validator = $validator = Validator::make($request->all(), [
            'mobile' => 'required|string',
            'password' => 'required|string',
            'firebase_token' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        } else {
                
            $credentials = request(['mobile', 'password']);

            if(!Auth::guard('customer')->attempt($credentials))
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized Mobile or Password')
                ], $this->NotAuthorized);
            
            $customer = Customer::where('mobile', $request->mobile)->first();
            $token = $customer->createToken('Laravel Personal Access Client')->accessToken;

            $customer->firebase_token = $request->firebase_token;
            $customer->save();

            $final_customer = array();
            $get_customer = DB::table('customers')->where('mobile', $request->mobile)->get(['id', 'first_name', 'last_name', 'email', 'mobile', 'image_url', 'firebase_token']);
            foreach( $get_customer as $cust ) {
                $final_customer['id'] = $cust->id;
                $final_customer['first_name'] = $cust->first_name;
                $final_customer['last_name'] = $cust->last_name;
                $final_customer['email'] = $cust->email;
                $final_customer['mobile'] = $cust->mobile;
                $final_customer['image_url'] = URL::asset('/images') . "/" . ( empty( $cust->image_url ) ? 'test.jpg' : $cust->image_url );
                // $final_customer['firebase_token'] = empty( $cust->firebase_token ) ? '' : $cust->firebase_token;
            }
            
            return response()->json([
                'message' => 'Successfully logged in',
                'access_token' => $token,
                'token_type' => 'Bearer',
                // 'expires_at' => Carbon::parse(
                    // $tokenResult->token->expires_at
                // )->toDateTimeString()
                'data' => $final_customer
            ]);
            
        }

    }

    /**
     * Logout customer (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        // $request->customer()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ], $this->successCode);
    }

    public function update_customer_details(Request $request) {

        $user_check = Auth::guard('customer-api')->id();
        $customer_id = $request->customer_id;

        // echo $user_check;
        // echo $customer_id;
        // exit;
        $image_upload_message = "";

        if ( $user_check == $customer_id ) {
            $customer = Customer::whereId($customer_id)->first();
            // print_r($customer);
            // exit;
            if ( isset($request->first_name) ) {
                $customer->first_name = $request->first_name;
            }
            if ( isset($request->last_name) ) {
                $customer->last_name = $request->last_name;
            }
            if ( isset($request->email) ) {
                $customer->email = $request->email;
            }
            if ( isset($request->image_url)) {
                $store_base64_image = $this->store_base64_image("test.png", $request->image_url);

                // echo $store_base64_image;
                // exit;
                
                if ( $store_base64_image == "upload_error" ) {
                    $image_upload_message = 'Wrong Encoding. Unable to update Image';
                } else if ( $store_base64_image == "ext_error" ) {
                    $image_upload_message = 'Image Name extension is invalid';
                } else if ( $store_base64_image == "size_error" ) {
                    $image_upload_message = 'Image Size is grater than 100 KB';
                } else {
                    $customer->image_url = $store_base64_image;
                    $image_upload_message = 'Image Uploaded Successful';
                }

            }

            $customer->save();

            $get_customer = Customer::whereId($customer_id)->first();
            $get_customer->image_url = URL::asset('/images') . "/" . ( empty($get_customer->image_url) ? 'test.jpg' : $get_customer->image_url );
            $get_customer->ref_code = '';
            foreach ($get_customer as $key => $value) {
                $get_customer[$key] = empty($value) ? '' : strval($value);
            }

            return response()->json([
                'message' => 'Customer Profile Data Updated',
                'data' => $get_customer,
                'image_upload_status' => $image_upload_message
            ], $this->successCode);
        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => array('Unauthorized User ID')
            ], $this->NotAuthorized);
        }

    }

    public function checkemailmobile(Request $request) {
        if ( isset($request->mobile) && isset($request->email) ) {
        
            $customer_mobile = Customer::whereMobile($request->mobile)->first();
            $customer_email = Customer::where('email', $request->email)->first();
            
            $validator = $validator = Validator::make($request->all(), [
                'email' => 'required|string|email'. ( ( $customer_email != null ) ? '|unique:customers,email,'.$customer_email->email : '' ),
                'mobile' => 'required|string|min:10|max:10'. ( ( $customer_mobile != null ) ? '|unique:customers,mobile,'.$customer_mobile->mobile : '' )
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => 'Email and Mobile is already Existed'
                ], $this->badRequest);
            } else {
                return response([
                    'message' => 'Email and Moile Does not exists'
                ], $this->successCode);
            }

        }

    }
    
    public function checkmobile(Request $request) {
        
        if ( isset($request->mobile) ) {
            $customer_mobile = Customer::whereMobile($request->mobile)->first();
            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10'. ( ( $customer_mobile != null ) ? '|unique:customers,mobile,'.$customer_mobile->mobile : '' )
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => 'Mobile Status',
                    'data' => $validator->errors()->all()
                ], $this->successCode);
            } else {
                return response([
                    'message' => 'Account not found, Create new account'
                ], $this->badRequest);
            }

        }
        
    }

    public function customerNewPassword(Request $request) {
        
        if ( isset($request->mobile) && isset($request->new_password) ) {

            $customer_mobile = Customer::whereMobile($request->mobile)->first();
            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10'. ( ( $customer_mobile != null ) ? '|unique:customers,mobile,'.$customer_mobile->mobile : '' ),
                'new_password' => 'required|string'
            ]);

            if ($validator->fails()) {

                $customer = Customer::where('mobile', $request->mobile)->first();
                $customer -> password = Hash::make($request->new_password);
                $customer->save();

                return response()->json([
                    'message' => 'Successfully changed password'
                ], $this->successCode);
                
            } else {
                return response([
                    'message' => 'Moile Does not exists'
                ], $this->badRequest);
            }

        }

    }

    public function customer_mark_favourite_outlet(Request $request) {

        $user_check = Auth::guard('customer-api')->id();

        $validator = $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'outlet_id' => 'required|integer',
            'is_favourite' => 'required|integer|min:0|max:1'
        ]);
        $customer_id = $request->customer_id;
        $outlet_id = $request->outlet_id;
        $is_favourite = $request->is_favourite;

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all()
            ], $this->badRequest);
        }

        if ( $user_check == $customer_id ) {

            $is_exist = CustomerOutletFavourite::where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();
            $message = "";

            if ( $is_exist ) {
                if ( $is_favourite == 0 ) {
                    $is_exist -> delete();
                    $message = 'Outlet removed as Favourite';
                }
            } else {
                if ( $is_favourite == 1 ) {
                    $mark_favourite = new CustomerOutletFavourite();
                    $mark_favourite -> customer_id = $customer_id;
                    $mark_favourite -> outlet_id = $outlet_id;
                    $mark_favourite -> is_favourite = $is_favourite;
                    $mark_favourite -> save();
                    $message = 'Outlet Marked as Favourite';
                }
            }

            return response()->json([
                'message' => $message
            ], $this->successCode);

        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => array('Unauthorized User ID')
            ], $this->NotAuthorized);
        }

    }

    function getDistance($latitude1, $longitude1, $latitude2, $longitude2) { // Below function that takes two sets of longitude and latitude co-ordinates and returns the distance between the two.
    
        $earth_radius = 6371;
      
        $dLat = deg2rad($latitude2 - $latitude1);  
        $dLon = deg2rad($longitude2 - $longitude1);  
      
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));  
        $d = $earth_radius * $c;  
      
        return $d;  
    }

    public function get_customer_favourite_outlets(Request $request) {

        $user_check = Auth::guard('customer-api')->id();

        $validator = $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
        ]);
        
        $customer_id = $request->customer_id;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $all_outlets = array();

        if ($validator->fails()) {
            return response([
                'message' => 'The given data was invalid.',
                'errors' => $validator->errors()->all(),
            ], $this->badRequest);
        }

        if ( $user_check == $customer_id ) {

            $get_outlets_id = DB::select("select outlet_id from customer_outlet_favourites where customer_id = ? order by outlet_id desc", [$customer_id]);
            $temp = array();
            foreach ($get_outlets_id as $value) {

                $outlets = DB::table('outlets')->where('id', $value->outlet_id)->first();
                if(!empty($outlets)){
                    $distance = $this->getDistance($outlets->outlet_latitude, $outlets->outlet_longitude, $latitude, $longitude);

                    $rating = DB::table('customer_outlet_ratings')->where('outlet_id', $outlets->id)->avg('rating');
                    
                    $temp['id'] = empty($outlets->id) ? '' : strval($outlets->id);
                    $temp['outlet_distance'] = strval(number_format($distance)) . " km";
                    $temp['estimated_delivery_time'] = number_format((15 + ($distance*60)/50))." Min";
                    $temp['is_favourite'] = "1";
                    $temp['rating'] = empty($rating) ? '0' : strval( $rating );
                    $temp['outlet_name'] = $outlets->outlet_name;
                    $temp['outlet_category_id'] = empty($outlets->outlet_category_id) ? '' : strval($outlets->outlet_category_id);
                    $temp['merchant_id'] = empty($outlets->merchant_id) ? '' : strval($outlets->merchant_id);
                    $temp['image_url'] = URL::asset('/images') . "/" . ( empty( json_decode($outlets->image_url) ) ? 'test.jpg' : json_decode($outlets->image_url) );
                    $temp['outlet_speciality'] = empty($outlets->outlet_speciality) ? '' : strval($outlets->outlet_speciality);
                    $temp['booking_for_two'] = empty($outlets->booking_for_two) ? '' : strval($outlets->booking_for_two);
                    

                    array_push($all_outlets, $temp);
                }
                
                
            }

            return response()->json([
                'message' => 'Favorite Outlet List Retrieved',
                'data' => $all_outlets
            ], $this->successCode);

        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => array('Unauthorized User ID')
            ], $this->NotAuthorized);
        }

    }

    public function customer_rate_outlet(Request $request) {

        if ( isset($request->customer_id) && isset($request->outlet_id) && isset($request->rating) ) {

            $user_check = Auth::guard('customer-api')->id();

            $validator = $validator = Validator::make($request->all(), [
                'customer_id' => 'required|integer',
                'outlet_id' => 'required|integer',
                'rating' => 'required|integer|min:1|max:5'
            ]);
            $customer_id = $request->customer_id;
            $outlet_id = $request->outlet_id;
            $rating = $request->rating;

            if ($validator->fails()) {
                return response([
                    'message' => 'The given data was invalid.',
                    'errors' => $validator->errors()->all()
                ], $this->badRequest);
            }

            if ( $user_check == $customer_id ) {

                $is_exist = CustomerOutletRating::where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->first();
                $message = "";

                if ( $is_exist ) {
                    $mark_rating = $is_exist -> rating = $rating;
                    $is_exist -> save();
                    $message = 'Ratings updated Successfully';
                } else {
                    $mark_rating = new CustomerOutletRating();
                    $mark_rating -> customer_id = $customer_id;
                    $mark_rating -> outlet_id = $outlet_id;
                    $mark_rating -> rating = $rating;
                    $mark_rating -> save();
                    $message = 'Ratings added Successfully';
                }

                $get_rating = CustomerOutletRating::where('customer_id', $customer_id)->where('outlet_id', $outlet_id)->get(['id', 'customer_id', 'outlet_id', 'rating']);

                return response()->json([
                    'message' => $message,
                    'data' => $get_rating
                ], $this->successCode);
                
            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }
    }

    public function add_customer_address(Request $request) {

        if ( isset($request->customer_id) && isset($request->customer_latitude) && isset($request->customer_longitude) ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $customer_address = $request->customer_address;
            $your_location = $request->your_location;
            $floor = $request->floor;
            $how_to_reach = $request->how_to_reach;
            $customer_latitude = $request->customer_latitude;
            $customer_longitude = $request->customer_longitude;
            
            if ( $user_check == $customer_id ) {

                // $add_customer_address = DB::insert('insert into customer_addresses(customer_id, customer_address, customer_latitude, customer_longitude) values(?, ?, ?, ?)', [$customer_id, $customer_address, $customer_latitude, $customer_longitude]);
                
                $add_customer_address = new CustomerAddress(); 

                $add_customer_address -> customer_id = $customer_id;
                $add_customer_address -> your_location = $your_location;
                $add_customer_address -> floor = $floor;
                $add_customer_address -> how_to_reach = $how_to_reach;
                $add_customer_address -> customer_address = $customer_address;
                $add_customer_address -> customer_latitude = $customer_latitude;
                $add_customer_address -> customer_longitude = $customer_longitude;
                $add_customer_address -> isdelete = 0;

                $add_customer_address->save();

                return response()->json([
                    'message' => 'Customer Address Added Successfully',
                    'data' => ['address_id' => $add_customer_address->id]
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }
    }

    public function update_customer_addresses(Request $request) {

        if ( isset($request->customer_id)  ) {

            $user_check = Auth::guard('customer-api')->id();
            $customer_id = $request->customer_id;
            $address_id = $request->address_id;
            $customer_address = $request->customer_address;
            $your_location = $request->your_location;
            $floor = $request->floor;
            $how_to_reach = $request->how_to_reach;
            $customer_latitude = $request->customer_latitude;
            $customer_longitude = $request->customer_longitude;
            
            if ( $user_check == $customer_id ) {

                $update_customer_address = DB::update('update customer_addresses SET customer_address = ?, your_location = ?, floor = ?, how_to_reach = ?, customer_latitude = ?, customer_longitude = ? WHERE customer_id = ? and id = ?', [$customer_address, $your_location, $floor, $how_to_reach, $customer_latitude, $customer_longitude, $customer_id, $address_id]);


                return response()->json([
                    'message' => 'Customer Address Updated Successfully',
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }
    }

    public function delete_customer_addresses(Request $request) {

        if ( isset($request->customer_id) && isset($request->address_id) ) {
            
            $user_check = Auth::guard('customer-api')->id();
            $address_id = $request->address_id;
            $customer_id = $request->customer_id;
            
            if ( $user_check == $customer_id ) {

                //$delete_customer_address = DB::delete('delete from customer_addresses WHERE customer_id = ? and id = ?', [$customer_id, $address_id]);

                $user = CustomerAddress::find($address_id);
                $user->isdelete = 1;
                $user->save();

                return response()->json([
                    'message' => 'Customer Address Deleted Successfully',
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
            
        }
    }

    public function get_customer_addresses(Request $request) {

        if ( isset($request->customer_id) ) {
            $user_check = Auth::guard('customer-api')->id();
            
            $customer_id = $request->customer_id;

            if ( $user_check == $customer_id ) {

                $get_customer_addresses = DB::table('customer_addresses')
                            ->where('customer_id', $customer_id)
                            ->where('isdelete','0')
                            ->get(['id', 'customer_address', 'your_location', 'floor', 'how_to_reach', 'customer_latitude', 'customer_longitude']);

                foreach ($get_customer_addresses as $key => $value) {

                    $get_customer_addresses[$key]->id = empty($value->id) ? '' : strval($value->id);
                    $get_customer_addresses[$key]->customer_address = empty($value->customer_address) ? '' : strval($value->customer_address);
                    $get_customer_addresses[$key]->your_location = empty($value->your_location) ? '' : strval($value->your_location);
                    $get_customer_addresses[$key]->floor = empty($value->floor) ? '' : strval($value->floor);
                    $get_customer_addresses[$key]->how_to_reach = empty($value->how_to_reach) ? '' : strval($value->how_to_reach);
                    $get_customer_addresses[$key]->customer_latitude = empty($value->customer_latitude) ? '' : strval($value->customer_latitude);
                    $get_customer_addresses[$key]->customer_longitude = empty($value->customer_longitude) ? '' : strval($value->customer_longitude);
                    
                }
                  
                return response()->json([
                    'message' => 'Customer Address Retrieved',
                    'data' => $get_customer_addresses
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized User ID')
                ], $this->NotAuthorized);
            }
        }

    }

}