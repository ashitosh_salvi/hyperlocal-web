<?php

namespace App\Http\Controllers\Api\Outlet\Auth;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\RidersAvaibility;
use App\Outlet;
use App\Orders;
use App\Rider;
use App\Notify;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if ( isset($request->outlet_user_name) && isset($request->password) && isset($request->device_id) && isset($request->firebase_token) ) 
        {   
            #validation check here
            $validator = $validator = Validator::make($request->all(), [
                'outlet_user_name'  => 'required|string',
                'password'          => 'required|string',
                'device_id'         => 'required|string',
                'firebase_token'    => 'required|string'
            ]);
            
            if($validator->fails())
            {   
                #validation fails
                return response([
                    'message'   => 'The given data was invalid.',
                    'errors'    => $validator->errors()->all()
                ], $this->badRequest);
            }
            else
            {
                #validation success
                $credentials = request(['outlet_user_name', 'password']);
                if (!Auth::guard('outlet')->attempt($credentials))
                {
                    # login failed
                    return response()->json([
                        'message'   => 'The given data was invalid.',
                        'errors'    => array('Unauthorized Outlet user name or Password')
                    ], $this->NotAuthorized);
                }

                $Outlet                     = Outlet::where('outlet_user_name', $request->outlet_user_name)->first();

                if ($Outlet->app_session_id != null) {
                    #already login error
                    return response([
                        'message'   => 'Another user already Logged In. Please contact Administrator.',
                        'errors'    => $validator->errors()->all()
                    ], $this->badRequest);
                }

                $token                      = $Outlet->createToken('Laravel Personal Access Client')->accessToken;
                $Outlet->app_firebase_token = $request->firebase_token;
                $Outlet->app_session_id     = $request->device_id;
                $Outlet->save();

                return response()->json([
                    'message'       => 'Successfully logged in',
                    'access_token'  => $token,
                    'outlet'        => $Outlet,
                    'token_type'    => 'Bearer'
                ]);
            }
        }
        #all data required
        return response([
            'message'   => 'The given data was invalid.',
            'errors'    => '500'
        ], $this->badRequest);
    }

    public function logout(Request $request)
    {   
        $token = $request->user()->token();
        $outlet_id      = $request->outlet_id;
        $token->revoke();

        $Outlet                     = Outlet::where('id', $outlet_id)->first();
        $Outlet->app_firebase_token = null;
        $Outlet->app_session_id     = null;
        $Outlet->save();

        return response()->json([
            'message' => 'Successfully logged out'
        ], $this->successCode);
    }

    public function logoutOutlet(Request $request)
    {
        if ( isset($request->outlet_user_name) && isset($request->password) && isset($request->device_id) && isset($request->firebase_token) ) 
        {   
            #validation check here
            $validator = $validator = Validator::make($request->all(), [
                'outlet_user_name'  => 'required|string',
                'password'          => 'required|string',
                'device_id'         => 'required|string',
                'firebase_token'    => 'required|string'
            ]);
            
            if($validator->fails())
            {   
                #validation fails
                return response([
                    'message'   => 'The given data was invalid.',
                    'errors'    => $validator->errors()->all()
                ], $this->badRequest);
            }
            else
            {
                #validation success
                $credentials = request(['outlet_user_name', 'password']);
                if (!Auth::guard('outlet')->attempt($credentials))
                {
                    # login failed
                    return response()->json([
                        'message'   => 'The given data was invalid.',
                        'errors'    => array('Unauthorized Outlet user name or Password')
                    ], $this->NotAuthorized);
                }

                $Outlet                     = Outlet::where('outlet_user_name', $request->outlet_user_name)->first();

                $token                      = $Outlet->createToken('Laravel Personal Access Client')->accessToken;
                $Outlet->app_firebase_token = $request->firebase_token;
                $Outlet->app_session_id     = $request->device_id;
                $Outlet->save();

                return response()->json([
                    'message'       => 'Successfully logged in',
                    'access_token'  => $token,
                    'outlet'        => $Outlet,
                    'token_type'    => 'Bearer'
                ]);
            }
        }
        #all data required
        return response([
            'message'   => 'The given data was invalid.',
            'errors'    => '500'
        ], $this->badRequest);
    }
}