<?php
namespace App\Http\Controllers\Api\Outlet;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\OrderRecived;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\RidersAvaibility;
use App\Customer;
use App\Outlet;
use App\Orders;
use App\Rider;
use App\Notify;

class OutletApiController extends Controller
{
    
    public function pendingOrder(Request $request)
    {
        #validation check here
        $validator = Validator::make($request->all(), [
            'outlet_id'   => 'required',
            'start'       => 'required',
            'end'         => 'required',
            'device_id'   => 'required',
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {   
            #check device is logged in or logout
            if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            {
                #return response
                return response([
                    'message'   => 'Need to logout.',
                    'errors'    => '101',
                ], $this->badRequest);
            }

            #get outlet
            $outlet = Outlet::where('id',$request->outlet_id)->count();
            if($outlet > 0)
            {
                #get pending orders
                $orders = Orders::with('orderList','customer','customer_address')
                                ->where('status','pending')
                                ->where('outlet_id',$request->outlet_id)
                                ->orderBy('created_at', 'desc')
                                ->skip($request->start)
                                ->take($request->end)
                                ->get();
                $order_map = $orders->map(function($order){
                    $order->created_at = date('Y-m-d H:i:s',strtotime(Carbon::parse($order->created_at))+60*60*5.5);
                    // $order->created_at = Carbon::parse($order->created_at)->format('Y-m-d');
                    
                });
                #return response
                return response([
                    'message'   => 'Order List Retrieved .',
                    'success'   => '200',
                    'orders'    => $orders
                ], $this->badRequest);
            }
            else 
            {
                #validation fails
                return response([
                    'message'   => 'Invalid outlet.',
                    'errors'    => '404'
                ], $this->badRequest);
            }
        }
    }

    public function inProgress(Request $request)
    {
        #validation check here
        $validator = Validator::make($request->all(), [
            'outlet_id'   => 'required',
            'start'       => 'required',
            'end'         => 'required',
            'device_id'   => 'required',
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {   
            #check device is logged in or logout
            if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            {
                #return response
                return response([
                    'message'   => 'Need to logout.',
                    'errors'    => '101',
                ], $this->badRequest);
            }
            
            #get outlet
            $outlet = Outlet::where('id',$request->outlet_id)->count();
            if($outlet > 0)
            {
                #get pending orders
                $orders = Orders::with('orderList','customer','customer_address','rider')
                                ->whereIn('status',['pickedup','accepted','reachedlocation','assigned','ready'])
                                ->where('outlet_id',$request->outlet_id)
                                ->orderBy('created_at', 'desc')
                                ->skip($request->start)
                                ->take($request->end)
                                ->get();
                $order_map = $orders->map(function($order){
                    $order->created_at = date('Y-m-d H:i:s',strtotime(Carbon::parse($order->created_at))+60*60*5.5);
                });
                #return response
                return response([
                    'message'   => 'Order List Retrieved .',
                    'success'   => '200',
                    'orders'    => $orders
                ], $this->badRequest);
            }
            else 
            {
                #validation fails
                return response([
                    'message'   => 'Invalid outlet.',
                    'errors'    => '404'
                ], $this->badRequest);
            }
        }
    }

    public function pastOrder(Request $request)
    {
        #validation check here
        $validator = Validator::make($request->all(), [
            'outlet_id'   => 'required',
            'start'       => 'required',
            'end'         => 'required',
            'device_id'   => 'required',
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {   
            #check device is logged in or logout
            if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            {
                #return response
                return response([
                    'message'   => 'Need to logout.',
                    'errors'    => '101',
                ], $this->badRequest);
            }

            #get outlet
            $outlet = Outlet::where('id',$request->outlet_id)->count();
            if($outlet > 0)
            {
                #get pending orders
                $orders = Orders::with('orderList','customer','customer_address')
                                ->whereIn('status',['rejected','delivered'])
                                ->where('outlet_id',$request->outlet_id)
                                ->orderBy('created_at', 'desc')
                                ->skip($request->start)
                                ->take($request->end)
                                ->get();
                $order_map = $orders->map(function($order){
                    $order->created_at = date('Y-m-d H:i:s',strtotime(Carbon::parse($order->created_at))+60*60*5.5);
                    // $order->created_at = date('d/m/Y h:i A', strtotime(Carbon::parse($created_date))+60*60*5.5);
                });
                #return response
                return response([
                    'message'   => 'Order List Retrieved .',
                    'success'   => '200',
                    'orders'    => $orders
                ], $this->badRequest);
            }
            else 
            {
                #validation fails
                return response([
                    'message'   => 'Invalid outlet.',
                    'errors'    => '404'
                ], $this->badRequest);
            }
        }
    }

    public function getOrder(Request $request)
    {
        #validation check here
        $validator = Validator::make($request->all(), [
            'order_id'   => 'required'
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {   
            #get pending orders
            $orders = Orders::with('orderList','customer','customer_address')
                            ->where('id',$request->order_id)
                            ->first();

            $orders->created_at = date('Y-m-d H:i:s',strtotime(Carbon::parse($orders->created_at))+60*60*5.5);
                            
            #return response
            return response([
                'message'   => 'Order List Retrieved .',
                'success'   => '200',
                'orders'    => $orders
            ], $this->badRequest);
           
        }
    }

    public function updateStatus(Request $request)
    {
        #validation check here
        $validator = Validator::make($request->all(), [
            'outlet_id'   => 'required',
            'order_id'    => 'required',
            'status'      => 'required',
            'device_id'   => 'required',
        ]);
        
        if ($validator->fails()) {
            # validation fails
            return response([
                'message' => 'The given data was invalid.',
                'errors'  => $validator->errors()->all()
            ],$this->badRequest);
        }
        else
        {   
            #check device is logged in or logout
            if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            {
                #return response
                return response([
                    'message'   => 'Need to logout.',
                    'errors'    => '101',
                ], $this->badRequest);
            }

            #check outlet
            $outlet = Outlet::where('id',$request->outlet_id)->count();
            if($outlet > 0)
            {
                #get pending orders
                $order          = Orders::where('id',$request->order_id)
                                ->where('outlet_id',$request->outlet_id)
                                ->first();
                if ($order->status == 'cancelled') 
                {
                    #return response
                    return response([
                        'message'   => 'Order is cancelled, Not possible to change status.',
                        'success'   => '200'
                    ], $this->badRequest);
                }
                else 
                {
                    $order->status      = $request->status;
                    $customer           = Customer::find($order->customer_id);
                    $outlet             = Outlet::find($order->outlet_id);
                    $customer->order_id = strval($order->id);
                    $rider              = new Rider();
                    if($order->rider_id)
                    {
                        $rider          = Rider::find($order->rider_id);
                    }
                    if($outlet->outlet_type == 'product')
                    {
                        $order_type     = 'order';
                    }
                    else
                    {
                        $order_type     = 'food';
                    }

                    if($request->status == 'rejected')
                    {
                        $customer->msgtitle = 'Order Rejected.';
                        if($order->payment_mode == 'cod')
                        {
                            $customer->msgbody = 'Dear '. $customer->first_name .', We are sorry but '. $outlet->outlet_name .' has rejected your order. Please check other which can serve you at the moment.';
                        }
                        else
                        {
                            $customer->msgbody = 'Dear '. $customer->first_name .', We are sorry but '. $outlet->outlet_name .' has rejected your order. Please check other Outlet which can serve you at the moment. Your paid order amount ₹'. $order->final_amount .' will get refunded in 8-10 business day.';
                        }
                        if($order->rider_id)
                        {
                            $rider->msgbody = 'Hi Rider, You order - '.$order->order_id.' has been rejected';
                            $rider->msgtitle = 'Order Rejected.';
                            $rider->order_id = strval($order->id);
                            try{
                                $rider->notify(new Notify);
                            }
                            catch(\Exception $ex){
                                $exp = $ex->getMessage();
                            }
                        } 
                    }
                    if($request->status == 'accepted')
                    {
                        $order->confirm_date = date("Y-m-d h:i:sa");
                        $customer->msgtitle = 'Order Confirm.';

                        if($order->payment_mode == 'cod')
                        {
                            $customer->msgbody = 'Dear '. $customer->first_name .', Thank you for ordering with '. $outlet->outlet_name .'. Your order is currently being prepared & we will keep you updated as soon as your '.$order_type.' is on its way. Request you to keep cash ₹'. $order->final_amount .' ready.';
                        }
                        else
                        {
                            $customer->msgbody = 'Dear '. $customer->first_name .', Thank you for ordering with '. $outlet->outlet_name .'. We have received your payment ₹'. $order->final_amount .' for '.$order->order_id.'. Your order is currently being prepared & we will keep you updated as soon as your '.$order_type.' is on its way.';
                        }  
                    }
                    if($request->status == 'pickedup')
                    {
                        $order->out_for_delivery_date = date("Y-m-d h:i:sa");    
                        $customer->msgtitle           = 'Order Out for Delivery.';

                        if($order->payment_mode == 'cod')
                        {  
                            $customer->msgbody = 'Dear '. $customer->first_name .', your food is currently on its way to you by our Rider '.$rider->first_name.'. Request you to keep cash ₹'.$order->final_amount.' ready.';
                        }
                        else
                        {
                            $customer->msgbody ='Dear '. $customer->first_name .', your food is currently on its way to you by our Rider '.$rider->first_name.'.';
                        }
                    }
                    else if($request->status == 'reachedlocation')
                    {
                        $order->reached_location_date = date("Y-m-d h:i:sa");
                    }
                    else if($request->status == 'delivered')
                    {
                        $order -> delivered_date = date("Y-m-d h:i:sa");   
                        $customer->msgtitle = 'Order Delivered';
                        if($order->payment_mode == 'cod')
                        {
                            $customer->msgbody = 'Thank you for Using HyperLocal App. We have received the Order amount ₹'.$order->final_amount.'. Hope you enjoy your order.';                
                        }
                        else
                        {
                            $customer->msgbody = 'Thank you for Using HyperLocal App - We hope you enjoy your order.';
                        }
                    }
                    
                    if($request->status == 'ready'){
                        $order->ready_datetime = date("Y-m-d h:i:sa");  
                        $customer->msgtitle = 'Order Ready';

                        if($order->payment_mode == 'cod'){
                            $customer->msgbody = 'Dear '. $customer->first_name .', Your order is ready.';                
                        }else{
                            $customer->msgbody = 'Dear '. $customer->first_name .', Your order is ready.';   
                        }

                    }
                    
                    if($customer->msgtitle)
                    {
                        try
                        {
                            $customer->notify(new Notify);
                        }
                        catch(\Exception $ex)
                        {
                            $expc = $ex->getMessage();
                        }
                    }
                    $order->save();
                    #return response
                    return response([
                        'message'   => 'Order status updated successfully.',
                        'success'   => '200'
                    ], $this->badRequest);
                }
            }
            else 
            {
                #validation fails
                return response([
                    'message'   => 'Invalid outlet.',
                    'errors'    => '404'
                ], $this->badRequest);
            }
        }

    }

    public function checkoutletmobile(Request $request) {
       
        if ( isset($request->mobile) ) {
            $rider_mobile = Outlet::whereMobile($request->mobile)->first();
            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10'. ( ( $rider_mobile != null ) ? '|unique:outlets,mobile,'.$rider_mobile->mobile : '' )
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => 'Mobile Status',
                    'data' => $validator->errors()->all()
                ], $this->successCode);
            } else {
                return response([
                    'message' => 'Account not found, Create new account'
                ], $this->badRequest);
            }
        }
        
    }

    public function outletNewPassword(Request $request) {
        
        if ( isset($request->mobile) && isset($request->new_password) ) {

            $outlet_mobile = Outlet::whereMobile($request->mobile)->first();
            
            $validator = $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:10'. ( ( $outlet_mobile != null ) ? '|unique:outlets,mobile,'.$outlet_mobile->mobile : '' ),
                'new_password' => 'required|string'
            ]);
            
            if ($validator->fails()) {
                $customer = Outlet::where('mobile', $request->mobile)->first();
                $customer -> password = Hash::make($request->new_password);
                $customer->save();
                
                return response()->json([
                    'message' => 'Successfully changed password'
                ], $this->successCode);
                
            } else {
                return response([
                    'message' => 'Moile Does not exists'
                ], $this->badRequest);
            }

        }

    }
    
    public function outlet_notification(Request $request){
        $outlet = Outlet::find($request->id);
        $outlet->msgbody = '2222';
        $outlet->msgtitle = 'New Order Received';
        $outlet->notify(new Notify);
        return $outlet->notifications->take(20);
    }

    public function outletOpenClose(Request $request){
        #validation check here
        $validator = Validator::make($request->all(), [
            'outlet_id'   => 'required',
            'status'      => 'required',
            'device_id'   => 'required',
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {
            #check device is logged in or logout
            if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            {
                #return response
                return response([
                    'message'   => 'Need to logout.',
                    'errors'    => '101',
                ], $this->badRequest);
            }

            #Update outlet status
            $outlet = Outlet::where('id',$request->outlet_id)->first();
            if (empty($outlet)) {
                #return response
                return response([
                    'message'   => 'No outlet find',
                    'errors'    => '404',
                ], $this->badRequest);
            }
            else
            {   
                $outlet_status = 0;
                if ($request->status == 'inactive') 
                {
                    $outlet_status = 1;
                }
                $outlet->flag = $outlet_status;
                $outlet->save();

                #return response
                return response([
                    'message'   => 'Outlet updated successfully',
                    'errors'    => '200',
                ], '200');
            }

        }
    }

    public function outletFlag(Request $request){
        #validation check here
        $validator = Validator::make($request->all(), [
            'outlet_id'   => 'required',
            'device_id'   => 'required',
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {
            #check device is logged in or logout
            if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            {
                #return response
                return response([
                    'message'   => 'Need to logout.',
                    'errors'    => '101',
                ], $this->badRequest);
            }

            #Update outlet status
            $outlet = Outlet::where('id',$request->outlet_id)->first();
            if (empty($outlet)) {
                #return response
                return response([
                    'message'   => 'No outlet find',
                    'errors'    => '404',
                ], $this->badRequest);
            }
            else
            {   
                #return response
                return response([
                    'message'   => 'Outlet flag',
                    'data'   => ["flag" => $outlet->flag],
                    'errors'    => '200',
                ], '200');
            }

        }
    }

    public function test_notification(Request $request){
        $API_ACCESS_KEY = "AAAA8um-vn8:APA91bGCB3wCj6z1XWkURx9dvJVCcjnc03Pflkx-nEWsG7aRozx2LAqbYXGrYo7v8wGMtmtYsqijrklfVvqOXQ-50tx3xCEGcJwom6V0kbfPPZKw84lRXh3DrfpdYn5TbvpGeDKwYaWQ";
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token='coRTrnwATVyY8_DynDy-lt:APA91bGlF2pDEvTrS2OwzeG78uUNa6grjGHVqEz5x2lXRhRSwqLoAImIMXQ2a9dODBzx-PRvSpxJCHiH-UafikMweRWqsiB5eaLXDq-BPvWADKjNxwK-bOjFnCUhv8QVo1kJmlZrLc0l';

        $notification = [
            'title' =>'New Order Received',
            'body' => '{"order_id":100,"outlet_id":2}',
            'icon' =>'myIcon', 
            'sound' => 'mySound'
        ];
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);


        echo $result;
    }

    public function checkLogin($device_id, $outlet_id)
    {
        #get outlet
        $outlet = Outlet::where('id',$outlet_id)
                        ->where('app_session_id',$device_id)
                        ->count();
        if ($outlet > 0) {
            return true;
        }
        return false;
    }

}