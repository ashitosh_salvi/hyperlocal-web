<?php

namespace App\Http\Controllers\Api\Outlet;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\OrderRecived;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\RidersAvaibility;
use App\Customer;
use App\Outlet;
use App\Orders;
use App\Rider;
use App\Notify;

class RiderApiController extends Controller
{
    public function allRiders(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'merchant_id' => 'required'
        ]);

        if ($validator->fails())
        {
            # validation fails
            return response([
                'message' => 'The given data was invalid.',
                'errors'  => $validator->errors()->all()
            ],$this->badRequest);
        }
        else
        {
            $merchant_id    = $request->merchant_id;
            $riders         = array();
            $get_riders     = DB::table('riders')
                                ->where('status', 1)
                                ->where('online', 1)
                                ->get(['id', 'first_name', 'last_name', 'merchant_id']);
            foreach( $get_riders as $rider )
            {
                if ( $rider->merchant_id != null ) 
                {
                    $merchant_ids = json_decode( $rider->merchant_id );
                    if (is_array($merchant_ids) || is_object($merchant_ids))
                    {
                        foreach( $merchant_ids as $key => $val )
                        {
                            if ( $val == $merchant_id )
                            {
                                array_push($riders, $rider);
                            }
                        }
                    }
                } 
            }
            return response([
                'message'   => 'Rider list.',
                'success'   => '200',
                'riders'    => $riders
            ]);
        }
    }

    public function assign_rider_order(Request $request) 
    {
        #validation check here
        $validator = Validator::make($request->all(), [
            'riders'            => 'required',
            'order_id'          => 'required',
            'device_id'         => 'required',
            'outlet_id'         => 'required',
        ]);

        if($validator->fails())
        {   
            #validation fails
            return response([
                'message'   => 'The given data was invalid.',
                'errors'    => $validator->errors()->all()
            ], $this->badRequest);
        }
        else
        {
            // #check device is logged in or logout
            // if ($this->checkLogin($request->device_id,$request->outlet_id) == false) 
            // {
            //     #return response
            //     return response([
            //         'message'   => 'Need to logout.',
            //         'errors'    => '101',
            //     ], $this->badRequest);
            // }
            date_default_timezone_set('Asia/Kolkata');
            $order                      = Orders::find($request->order_id);
            $order->rider_id            = $request->riders;
            $order->rider_assign_date   = date("Y-m-d h:i:s"); 
            $order->status              = 'assigned';
            $order->save();

            $rider              = Rider::find($request->riders);
            $rider->msgbody     = 'Hi Rider, You have received order - '.$order->order_id;
            $rider->msgtitle    = 'Order Assign.';
            $rider->order_id    = strval($order->id);
            try{
                $rider->notify(new Notify);
            }
            catch(\Exception $ex){
                //
            }
            #return response
            return response([
                'message'   => 'Rider assigned successfully',
                'errors'    => '200',
            ], '200');
        }
    }
}