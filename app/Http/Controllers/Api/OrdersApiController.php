<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Orders;
use App\Rider;
use App\Notify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Customer;
use App\Outlet;

class OrdersApiController extends Controller
{
    // Get All Orders
    public function index() {
        
        $orders = Orders::reorder('id', 'ASC')->get();
        // dd( $orders );
        return response()->json([
            'orders_list' => $orders
        ], 201);

    }

    public function get_orders_by_customer_id(Request $request) {
        
        $user_check = Auth::guard('customer-api')->id();
        $customer_id = $request->customer_id;
        $all_order_items = array();
        $past_temp = array();
        $current_temp = array();
        if ( $user_check == $customer_id ) {
            // echo 'true';
            $get_orders = DB::select("select * from orders where customer_id = ? order by id desc", [$customer_id]);
            foreach ( $get_orders as $order ) {
                // print_r($order);
                $temp = array();
                $sub_temp = array();
                $get_outlets = DB::table('outlets')->where('id', $order->outlet_id)->first();

                $temp['outlet_name']        = $get_outlets->outlet_name ?? '';
                $temp['id']                 = strval($order->id); 
                $temp['order_id']           = empty($order->order_id) ? '' : strval($order->order_id);
                $temp['order_date_time']    = date('d/m/Y h:i:s A', strtotime($order->created_at)+60*60*5.5);
                $temp['order_status']       = empty($order->status) ? '' : strval($order->status);
                $temp['delivery_charges']   = empty($order->delivery_charges) ? '' : strval($order->delivery_charges);
                $temp['discount']           = empty($order->discount) ? '' : strval($order->discount);
                $temp['coupon_code']        = empty($order->promocode) ? '' : strval($order->promocode);
                $temp['tax']                = empty($order->tax) ? '' : strval($order->tax);
                $temp['total_amount']       = empty($order->final_amount) ? '' : strval($order->final_amount);//as per req in apk
                $temp['final_amount']       = empty($order->total_amount) ? '' : strval($order->total_amount);
                $temp['payment_mode']       = empty($order->payment_mode) ? '' : strval($order->payment_mode);
                $temp['special_instruction']      = empty($order->special_instruction) ? '' : strval($order->special_instruction);
                
                $temp['outlet_mobile']      = empty($get_outlets->mobile) ? '' : strval($get_outlets->mobile);
                $sub_temp['pending']        = date('d/m/Y h:i A', strtotime($order->created_at)+60*60*5.5);
                $sub_temp['confirmed']      = empty($order->confirm_date) ? '' : date('d/m/Y h:i A', strtotime($order->confirm_date)+60*60*5.5);
                $sub_temp['out_for_delivery'] = empty($order->out_for_delivery_date) ? '' : date('d/m/Y h:i A', strtotime($order->out_for_delivery_date)+60*60*5.5);
                $sub_temp['delivered']      = empty($order->delivered_date) ? '' : date('d/m/Y h:i A', strtotime($order->delivered_date)+60*60*5.5); 
                $sub_temp['ready']      = empty($order->ready_datetime) ? '' : date('d/m/Y h:i A', strtotime($order->ready_datetime)+60*60*5.5);
                $temp['delivery_details']   = $sub_temp;
                
                $get_order_items = DB::select("select * from order_items where order_id = ?", [$order->order_id]);
                if(count($get_order_items) > 0){
                    foreach ( $get_order_items as $order_item_key => $order_item ) {
                        
                        foreach ( $order_item as $oi_key => $oi_val ) {
                            $get_order_items[$order_item_key]->$oi_key = empty($oi_val) ? '' : strval($oi_val);
                        }
                    }
                }

                // if(count($get_order_items) > 0){
                //     if($get_outlets->outlet_type == 'recipe'){
                //         foreach ( $get_order_items as $order_item_key => $order_item ) {
                //             foreach ( $order_item as $oi_key => $oi_val ) {
                //                 $get_order_items[$order_item_key]->$oi_key = empty($oi_val) ? '' : strval($oi_val);
                //             }
                //             $get_food_category = DB::table('recipe')->where('id', $order_item->order_item->item_id)->first();
                //             $get_order_items[$order_item_key]->food_catgory = $get_food_category->food_cat;
                //         }
                //     }
                //     else{
                //         foreach ( $get_order_items as $order_item_key => $order_item ) {
                //             foreach ( $order_item as $oi_key => $oi_val ) {
                //                 $get_order_items[$order_item_key]->$oi_key = empty($oi_val) ? '' : strval($oi_val);
                //             }
                //             $get_order_items[$order_item_key]->food_catgory = '';
                //         }
                //     }
                // }


                $temp['order_items'] = $get_order_items;
                
                if ($order->status == 'delivered' || $order->status == 'cancelled' || $order->status == 'rejected'){
                    array_push( $past_temp, $temp );
                }else{
                    array_push( $current_temp, $temp );
                }
                
            }

            $all_order_items['current_orders'] = $current_temp;
            $all_order_items['past_orders'] = $past_temp;

            return response()->json([
                'message' => 'Order List Retrieved',
                'data' => $all_order_items
            ], $this->successCode);
        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => array('Unauthorized User ID')
            ], $this->NotAuthorized);
        }

    }

    // public function ordersByRiderID(Request $request) {
    //     if( isset($request->rider_id) ) {
            
    //         $rider_id = $request->rider_id;

    //         $user_check = Auth::guard('rider-api')->id();
            
    //         if ( $user_check == $rider_id ) {
                
    //             $get_orders = Orders::where('rider_id',$user_check)
    //                 ->leftJoin('outlets','outlets.id','orders.outlet_id')
    //                 ->leftJoin('customers','customers.id','orders.customer_id')
    //                 ->leftJoin('customer_addresses','customer_addresses.id','orders.customer_addr_id')
    //                 ->orderBy('orders.id','DESC')
    //                 ->get(['orders.id as current_order_id','orders.created_at as order_date_time' ,'orders.*', 'outlets.*', 'customer_addresses.*', 'customers.*']);
    //             return response()->json([
    //                 'message' => 'Order List Retrieved',
    //                 'data' => $get_orders,
    //             ], $this->successCode);

    //         } else {
    //             return response()->json([
    //                 'message' => 'The given data was invalid.',
    //                 'errors' => array('Unauthorized Rider ID')
    //             ], $this->NotAuthorized);
    //         }

    //     }
    // }

    public function ordersByRiderID(Request $request) {
        if( isset($request->rider_id) ) {
           
            $rider_id = $request->rider_id;

            $user_check = Auth::guard('rider-api')->id();
           
            if ( $user_check == $rider_id ) {
               
                $get_orders = Orders::where('rider_id',$user_check)
                    ->leftJoin('outlets','outlets.id','orders.outlet_id')
                    ->leftJoin('customers','customers.id','orders.customer_id')
                    ->leftJoin('customer_addresses','customer_addresses.id','orders.customer_addr_id')
                    ->orderBy('orders.rider_assign_date','DESC')
                    ->get(['orders.id as current_order_id','orders.created_at as order_date_time' ,'orders.*', 'outlets.*', 'customer_addresses.*', 'customers.*' , 'outlets.mobile as outlet_mobile', 'customers.mobile as customer_mobile']);

                foreach ($get_orders as $key => $orders) {
                    $get_order_items = DB::select("select * from order_items where order_id = ?", [$orders->order_id]);
                    if(count($get_order_items) > 0){
                        foreach ( $get_order_items as $order_item_key => $order_item ) {
                           
                            foreach ( $order_item as $oi_key => $oi_val ) {
                                $get_order_items[$order_item_key]->$oi_key = empty($oi_val) ? '' : strval($oi_val);
                            }
                        }
                    }
                    $get_orders[$key]['order_items'] = $get_order_items;
                }
                return response()->json([
                    'message' => 'Order List Retrieved',
                    'data' => $get_orders,
                ], $this->successCode);

            } else {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => array('Unauthorized Rider ID')
                ], $this->NotAuthorized);
            }

        }
    }

    public function orderUpdate(Request $request ){
        
        $request->validate(['order_id' => 'required']);
        
        $order = Orders::find($request -> order_id);
        
        $customer = Customer::find($order->customer_id);
        $customer->order_id = strval($order->id);
        $outlet = Outlet::find($order->outlet_id);
        $rider = new Rider();
        if($order->rider_id){
            $rider = Rider::find($order->rider_id);
        }

        if($outlet->outlet_type == 'product'){
            $order_type = 'order';
        }else{
            $order_type = 'food';
        }

        $flag = false;
        if(!$order){
            return 'invalid order data';
        }
        if($request -> status){
            if($request -> status == 'cancelled'){
                $customer->msgtitle = 'Order Cancelled';
                $flag = true; 
                if($order->payment_mode == 'cod'){
                    $customer->msgbody = 'Dear '. $customer->first_name .', As requested we have cancelled your order with '. $outlet->outlet_name .'.';
                }else{
                    $customer->msgbody = 'Dear '. $customer->first_name .', As requested we have cancelled your order with'. $outlet->outlet_name .'. Your paid order amount ₹'. $order->final_amount .' will get refunded in 8-10 business day.';
                }

                if($order->rider_id){

                    $rider->msgbody = 'Hi Rider, You order - '.$order->order_id.' has  cancelled';
                    $rider->msgtitle = 'Order Cancelled.';

                    $rider->order_id = strval($order->id);

                    try{
                        $rider->notify(new Notify);
                    }
                    catch(\Exception $ex){
                        return $ex->getMessage();
                    }

                }
            }
            if($request -> status == 'pickedup'){
                $order -> out_for_delivery_date = date("Y-m-d h:i:sa");
                $customer->msgtitle = 'Order Out for Delivery';
                $flag = true; 
                if($order->payment_mode == 'cod'){  
                    $customer->msgbody = 'Dear '. $customer->first_name .', your food is currently on its way to you by our Rider '.$rider->first_name.'. Request you to keep cash ₹'.$order->final_amount.' ready.';
                }else{
                    $customer->msgbody ='Dear '. $customer->first_name .', your food is currently on its way to you by our Rider '.$rider->first_name.'.';
                }   
            }
            else if($request -> status == 'reachedlocation'){
                $order -> reached_location_date = date("Y-m-d h:i:sa");
                $flag = false;   
            }
            else if($request -> status == 'delivered'){
                $order -> delivered_date = date("Y-m-d h:i:sa");
                $customer->msgtitle = 'Order Delivered';
                $flag = true;
                if($order->payment_mode == 'cod'){
                    $customer->msgbody = 'Thank you for Using HyperLocal App. We have received the Order amount ₹'.$order->final_amount.'. Hope you enjoy your order.';                
                }else{
                    $customer->msgbody = 'Thank you for Using HyperLocal App - We hope you enjoy your order.';
                }
            }
            $order -> status = $request ->  status;
        }
        if($request -> rating){
            $order -> rating = $request -> rating;
        }
        if($request -> feedback){
            $order -> feedback = $request -> feedback;
        }

        try{
            if($flag){
                $customer->notify(new Notify);
            }
        }
        catch(\Exception $ex){
            $expc = $ex->getMessage();
        }

        if($order->save()){
            return response()->json([
                'message' => 'Order updated',
                'data' => $order
            ],$this -> successCode);
        }else{
            return response()->json([
                'message' => 'Order not updated',
                'errors' => true 
            ]);
        }

    }
    
    public function get_order_details(Request $request){

        $request->validate(['order_id' => 'required']);

        $get_orders = Orders::where('orders.id',$request->order_id)
                    ->leftJoin('outlets','outlets.id','orders.outlet_id')
                    ->leftJoin('customers','customers.id','orders.customer_id')
                    ->leftJoin('customer_addresses','customer_addresses.id','orders.customer_addr_id')
                    ->first(['orders.id as current_order_id', 'orders.*', 'outlets.*', 'customer_addresses.*', 'customers.*', 'outlets.mobile as outlet_mobile', 'customers.mobile as customer_mobile']);

        return response()->json([
            'message' => 'Order details',
            'data' => $get_orders
        ],$this -> successCode);
    }

}