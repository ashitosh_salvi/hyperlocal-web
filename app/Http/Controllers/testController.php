<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class testController extends Controller
{
    public function index()
    {
        return view('firebase.firebase');
    }

    public function sendNotification()
    {
        $url ="https://fcm.googleapis.com/fcm/send";

        $fields=array(
            "to"=>"d-5MinqMwWIgfPeRNODHCR:APA91bGA3QzFvd0swnepnqG7QtIJMOxGU5E6ljBFNBe6wT9EEpK8qY05yMhDMdq3xWooYSRncIn7tMUtQds3dcwRjRDFylyiWrnLYP6DtKKGTuTbutPH482rmpfiMdmsqN-P1ivJZIKO",
            "notification"=>array(
                "body"=>"Test Notificaton Body",
                "title"=>"Test Notificaton Title",
                "icon"=>"https://www.hyperlocalonline.in/assets/images/app_name.png",
                "click_action"=>"https://www.hyperlocalonline.in/hladmin/orders_list"
            )
        );

        $headers=array(
            'Authorization: key=AAAAFsT12I8:APA91bEksY6D9J3AK9SWvjUBea9qzNNVm7qJZIjCgqgKKf7l5WG_7APxh6VkBqnoAswhf845UOzKn-QoXj7TQ95T7ELxuN_CSg5JKiGXkcyMvfnf4ydTiZ61kOV5Y-BdGv9_omUxZVWh',
            'Content-Type:application/json'
        );

        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($fields));
        $result=curl_exec($ch);
        curl_close($ch);
        dd($result);
    }

    public function routeDistance(Request $request)
    {   
        $lat1   = $request->lat1;
        $long1  = $request->long1;
        $lat2   = $request->lat2;
        $long2  = $request->long2;

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        dd($response_a);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

        return array('distance' => $dist, 'time' => $time);
    }
}