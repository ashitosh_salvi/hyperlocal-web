<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

  public function __construct()
    {
        $this->Category = new Category;
        $this->title = 'Category';
        $this->path = 'category/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $outlet_id = Auth::guard('outlet')->id();
        // dd( Auth::guard('outlet')->user()->outlet_type );
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;
	    $data =$this->Category->getData();
        return view($this->path.'index')->with(compact('data'));
    }

    //// get category 
    public function getCategory(Request $request)
    {
        $data =$this->Category->getData($request->priority);
        return response()->json(['status'=>true,'data'=>$data,'message'=>'Category List']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd( $request );
        // exit;
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $validator = \Validator::make($request->all(), [
            'category_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status'=>false,'data' => ['error'=>$validator->errors()->all()],'message'=>'Validation Error']);
        }


        $cat_exist = Category::where('category_name', $request->category_name)->where('outlet_type', $outlet_type)->where('outlet_id', $outlet_id)->first();

        if($cat_exist)
        {
            return  response()->json(['status'=>false,'data' =>['errors'=>["category_name"=>'Category Already Exists']]]);
        }

        if($request->parent_category==null){
            $request->request->add(['priority' =>1]);
          }else{
          $names = $this->Category->setPriority($request->parent_category);
          foreach($names as $key=>$value){
             foreach($value as $key_1=>$value_2)
             {
                 if($key_1='grand_parent_name'){
                     if($value_2!="" || $value_2!=null){
                        $request->request->add(['priority' =>3]);
                     }else{
                        $request->request->add(['priority' =>2]);
                     }
   
                 }
             }
           }
           
           $parent_data = Category::where(['id'=>$request->parent_category])->get();

           $request->request->add(['status' =>$parent_data[0]->status]);

          }

        if ( $outlet_id != null ) {
            $request->request->add(['outlet_id' => $outlet_id]);
            $request->request->add(['outlet_type' => $outlet_type]);
        }

        $category=$this->Category->storeData($request->all());
        return response()->json(['status'=>true,'data' =>[],'message'=>'Category Added Sucessfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;
        $outlet_id = Auth::guard('outlet')->user()->id;
        $validator = \Validator::make($request->all(), [
            'category_name' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status'=>false,'data' => ['error'=>$validator->errors()->all()],'message'=>'Validation Error']);
        }
        $idd=array($id);
        $cat_exist = Category::whereNotIn("id",$idd)->where('category_name', $request->category_name)->where('outlet_id', $outlet_id)->where('outlet_type', $outlet_type)->first();
        if($cat_exist)
        {
            return  response()->json(['status'=>false,'data' =>['errors'=>["category_name"=>'Category Already Exists']]]);
        }

        if($request->parent_category==null){
            $request->request->add(['priority' =>1]);
          }else{
          $names = $this->Category->setPriority($request->parent_category);
          foreach($names as $key=>$value){
             foreach($value as $key_1=>$value_2)
             {
                 if($key_1='grand_parent_name'){
                     if($value_2!="" || $value_2!=null){
                        $request->request->add(['priority' =>3]);
                     }else{
                        $request->request->add(['priority' =>2]);
                     }
   
                 }
             }
           }
          }
          $category=$this->Category->updateData($id, $request->only('category_name','parent_category','description','priority'));
          return response()->json(['status'=>true,'data' =>[],'message'=>'Category Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        
      $data = Category::where(['id'=>$id])->get();

      $parent_data = Category::where(['id'=>$data[0]->parent_category])->get();
         

      if ( $data[0]->parent_category == null ) {

          if($request->status==0) {
              $status=1;
              $status_code="Activated";


              $p_id=$id;
              
              $child_data=Category::where(['parent_category'=>$p_id])->get();

              foreach($child_data as $key=>$value){

                $child = Category::where(['id'=>$value->id])->update(['status'=>$status]); // category update
                
                if($value->priority == 1){
                  $products = Product::where(['category_id'=>$value->id])->where(['flag'=> '0'])->update(['status'=>$status]); //product update
                }
                elseif ($value->priority == 2) {
                  $products = Product::where(['sub_category_id'=>$value->id])->where(['flag'=> '0'])->update(['status'=>$status]); //product update
                }
                elseif ($value->priority == 3) {
                  $products = Product::where(['child_category_id'=>$value->id])->where(['flag'=> '0'])->update(['status'=>$status]); //product update
                }
                

                $sub_child_data=Category::where(['parent_category'=>$value->id])->get();        
                  foreach($sub_child_data as $key_1=>$value_1){

                      $child = Category::where(['id'=>$value_1->id])->update(['status'=>$status]);  //category update  
                      if($value_1->priority == 1){
                        $products = Product::where(['category_id'=>$value_1->id])->where(['flag'=> '0'])->update(['status'=>$status]); //product update
                      }
                      elseif ($value_1->priority == 2) {
                        $products = Product::where(['sub_category_id'=>$value_1->id])->where(['flag'=> '0'])->update(['status'=>$status]); //product update
                      }
                      elseif ($value_1->priority == 3) {
                        $products = Product::where(['child_category_id'=>$value_1->id])->where(['flag'=> '0'])->update(['status'=>$status]); //product update
                      }
                      
                  }
              }

          } else {
              $status=0;
              $status_code="Deactivated";
              
              $p_id=$id;
              
              $child_data=Category::where(['parent_category'=>$p_id])->get();        
              
              foreach($child_data as $key=>$value){

                $child=Category::where(['id'=>$value->id])->update(['status'=>$status]); // category update

                if($value->priority == 1){
                  $products = Product::where(['category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                }
                elseif ($value->priority == 2) {
                  $products = Product::where(['sub_category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                }
                elseif ($value->priority == 3) {
                  $products = Product::where(['child_category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                }
                
                $sub_child_data=Category::where(['parent_category'=>$value->id])->get();        
                foreach($sub_child_data as $key_1=>$value_1){
  
                  $child=Category::where(['id'=>$value_1->id])->update(['status'=>$status]);     //category update  

                  if($value_1->priority == 1){
                      $products = Product::where(['category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                    }
                    elseif ($value_1->priority == 2) {
                      $products = Product::where(['sub_category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                    }
                    elseif ($value_1->priority == 3) {
                      $products = Product::where(['child_category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                    }

                }
              }

          }
          
          $this->Category->updateData($id,['status'=>$status]);
          $products = Product::where(['category_id'=>$id])->where(['flag'=> 0])->update(['status'=>$status]);
          if($data[0]->priority == 1){
              $products = Product::where(['category_id'=>$data[0]->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
            }
            elseif ($data[0]->priority == 2) {
              $products = Product::where(['sub_category_id'=>$data[0]->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
            }
            elseif ($data[0]->priority == 3) {
              $products = Product::where(['child_category_id'=>$data[0]->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
            }
          return response()->json(['status'=> true ,'message'=>'Category '.$status_code.' Sucessfully.']);

      } else {

          if ( $parent_data[0]->status == 0 ) {
              return response()->json(['status'=> false ,'message'=>'Activate Parent Category first.']);
          } else {
          
              if($request->status==0) {
                  $status=1;
                  $status_code="Activated";
                  

                  $p_id=$id;
                  
                  $child_data=Category::where(['parent_category'=>$p_id])->get();

                  foreach($child_data as $key=>$value){

                      
                  $child=Category::where(['id'=>$value->id])->update(['status'=>$status]);    //category update
                  
                  if($value->priority == 1){
                    $products = Product::where(['category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                  }
                  elseif ($value->priority == 2) {
                    $products = Product::where(['sub_category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                  }
                  elseif ($value->priority == 3) {
                    $products = Product::where(['child_category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                  }

                  $sub_child_data=Category::where(['parent_category'=>$value->id])->get();        
                      foreach($sub_child_data as $key_1=>$value_1){
        
                          $child=Category::where(['id'=>$value_1->id])->update(['status'=>$status]);    //category update

                          if($value_1->priority == 1){
                            $products = Product::where(['category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                          }
                          elseif ($value_1->priority == 2) {
                            $products = Product::where(['sub_category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                          }
                          elseif ($value_1->priority == 3) {
                            $products = Product::where(['child_category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                          }

                      }
                  }

              } else {
                  $status=0;
                  $status_code="Deactivated";
                  
                  
                  $p_id=$id;
                   
                  $child_data=Category::where(['parent_category'=>$p_id])->get();        
                  
                  foreach($child_data as $key=>$value){

                      $child=Category::where(['id'=>$value->id])->update(['status'=>$status]);    //category update
                      if($value->priority == 1){
                        $products = Product::where(['category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                      }
                      elseif ($value->priority == 2) {
                        $products = Product::where(['sub_category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                      }
                      elseif ($value->priority == 3) {
                        $products = Product::where(['child_category_id'=>$value->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                      }
                      
                      $sub_child_data=Category::where(['parent_category'=>$value->id])->get();        
                      foreach($sub_child_data as $key_1=>$value_1){
       
                        $child=Category::where(['id'=>$value_1->id])->update(['status'=>$status]);    //category update
                        if($value_1->priority == 1){
                          $products = Product::where(['category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                        }
                        elseif ($value_1->priority == 2) {
                          $products = Product::where(['sub_category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                        }
                        elseif ($value_1->priority == 3) {
                          $products = Product::where(['child_category_id'=>$value_1->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
                        }      
                      }
                  }
              
              }
              $this->Category->updateData($id,['status'=>$status]);
              if($data[0]->priority == 1){
                $products = Product::where(['category_id'=>$data[0]->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
              }
              elseif ($data[0]->priority == 2) {
                $products = Product::where(['sub_category_id'=>$data[0]->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
              }
              elseif ($data[0]->priority == 3) {
                $products = Product::where(['child_category_id'=>$data[0]->id])->where(['flag'=> 0])->update(['status'=>$status]); //product update
              }
              return response()->json(['status'=> true , 'message'=>'Category '.$status_code." Sucessfully."]);
          }
      }
    }

    public function get_category_product_view() {
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;
        $category = DB::select('select id, parent_category, category_name from categories where status = 1 and outlet_id = ? and outlet_type = ?', [$outlet_id, $outlet_type]);
        return response()->json([
            'category' => $category
        ]);
    }    

    
}