<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offers;
use App\Outlet;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class OfferController extends Controller
{
    public function create_offer(Request $request){
        
        $merchant_id = Auth::guard('merchant')->id();

        $offer = new Offers(); 

        $filename = '';
        $path = $request->file('image_url');
        
        if($request->file('image_url')){
            
            $img_temp = $request->file('image_url');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            // $image_path = "images/".$filename;
            // move_uploaded_file($img_temp,$image_path); 
            
            // Valid extension
            $valid_ext = array('png','jpeg','jpg');
            // Location
            $location = "images/".$filename;
            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                $location = "public/images/".$filename;
            }
            
            // file extension
            $file_extension = pathinfo($location, PATHINFO_EXTENSION);
            $file_extension = strtolower($file_extension);

            // Check extension
            if(in_array($file_extension,$valid_ext)){
                // Compress Image
                $this->compressImage($_FILES['image_url']['tmp_name'],$location,70);
            }
        }

        $max_discount = 0;
        if ($request -> max_discount != null) {
            $max_discount = $request -> max_discount;
        }

        $amount = 0;
        if ($request -> amount != null) {
            $amount = $request -> amount;
        }

        $offer -> offer_name = $request -> offer_name;
        $offer -> offer_type = $request -> offer_type;
        $offer -> offer_name = $request -> offer_name;
        $offer -> offer_description = $request -> offer_description;
        $offer -> merchant_id = $merchant_id;
        $offer -> outlet_id = json_encode( $request -> outlet_id );
        $offer -> promo_code = $request -> promo_code;
        $offer -> discount_type = $request -> discount_type;
        $offer -> discount = $request -> discount;
        $offer -> amount = $amount;
        $offer -> expiry_date = $request -> expiry_date;
        $offer -> max_discount = $max_discount;
        if ( !empty($filename) ) {
            $offer -> image_url = $filename;
        }
        $offer -> status = 1;
        $offer -> flag = 0;
        
        $offer->save();

        return ;


    }

    public function compressImage($source, $destination, $quality){

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

    }

    public function get_offer_list(Request $request){

        $merchant_id = Auth::guard('merchant')->id();

        if ( $merchant_id != null ) {

            $columns = array( 
                0 =>'id',
                1 =>'offer_name',
                2 =>'offer_type',
                3 =>'promo_code',
                4 =>'status',
                5 =>'action',
            );  
            $totalData = Offers::where('merchant_id', $merchant_id)->count();            
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            
            if(empty($request->input('search.value')))
            {            
                $offers = Offers::where('merchant_id', $merchant_id)->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
            }else {
                $search = $request->input('search.value'); 

                $offers =  Offers::where('merchant_id', $merchant_id)
                                    ->where(function($query) use ($search){
                                    $query->where('id','LIKE',"%{$search}%")
                                        ->orWhere('offer_name', 'LIKE',"%{$search}%")
                                        ->orWhere('offer_type', 'LIKE',"%{$search}%")
                                        ->orWhere('promo_code', 'LIKE',"%{$search}%");
                                    })
                                    ->offset($start)
                                    ->limit($limit)
                                    ->orderBy($order,$dir)
                                    ->get();

                $totalFiltered = Offers::where('merchant_id', $merchant_id)
                                    ->where(function($query) use ($search){
                                    $query->where('id','LIKE',"%{$search}%")
                                        ->orWhere('offer_name', 'LIKE',"%{$search}%")
                                        ->orWhere('offer_type', 'LIKE',"%{$search}%")
                                        ->orWhere('promo_code', 'LIKE',"%{$search}%");
                                    })
                                    ->count();
            }
        }

    
        $data = array();
        if(!empty($offers))
        {
            foreach ($offers as $key=>$offer_v)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['offer_name'] = $offer_v->offer_name;
                $nestedData['offer_type'] = $offer_v->offer_type;
                $nestedData['offer_description'] = $offer_v->offer_description;
                $nestedData['promo_code'] = $offer_v->promo_code;
                $nestedData['status'] = '<input class="tgl tgl-ios tgl_checkbox offer_status" id="cb_'. $offer_v->id .'" name="offer_status" type="checkbox" data-status="'. $offer_v->status .'" '. ( ( $offer_v->status == '1' ) ? "checked" : "" ) .' data-id="'. $offer_v->id .'"><label class="tgl-btn" for="cb_'. $offer_v->id .'"></label>';
                $nestedData['action'] = '<div class="action-group"><a title="View Menu Item" class="view btn operations" onclick="getOfferView('.$offer_v->id.')" data-toggle="modal" data-target="#viewOfferModal-1"> <i class="fa fa-eye"></i></a><a title="Edit Menu Item" class="btn operations" onclick="getOffer('.$offer_v->id.')" data-toggle="modal" data-target="#editOfferModal-1"><i class="fa fa-edit"></i></a><a title="Delete Menu Item" data-attr="'.$offer_v->id.'" onclick="delete_offer('.$offer_v->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
 
    }
    public function get_offer(Request $request){

        $pd = Offers::where('id',$request->id)->first();

        return Array( 'offer' =>$pd);
    }

    public function offer_list(){
        $merchant_id = Auth::guard('merchant')->id();

        $list = Outlet::where('merchant_id',$merchant_id)
                    ->get();

        $data = Array(
            'outlet_list' => $list
        );
        return view('merchant.offer_list')->with($data);
    }

    public function edit_offer(Request $request){
        
        $merchant_id = Auth::guard('merchant')->id();
        $offer = Offers::find($request -> offer_id);

        $filename = '';
        $path = $request->file('image_url_ed');
        
        if( !empty( $request->file('image_url_ed') ) ){
            
            $img_temp = $request->file('image_url_ed');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "images/".$filename;
            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                $image_path = "public/images/".$filename;
            }
           
            move_uploaded_file($img_temp,$image_path);           
        }
        
        
        $offer -> offer_name = $request -> offer_name_ed;
        $offer -> offer_type = $request -> offer_type_ed;
        $offer -> offer_name = $request -> offer_name_ed;
        $offer -> offer_description = $request -> offer_description_ed;
        $offer -> outlet_id = json_encode( $request -> outlet_id_ed);
        $offer -> promo_code = $request -> promo_code_ed;
        $offer -> discount_type = $request -> discount_type_ed;
        $offer -> discount = $request -> discount_ed;
        $offer -> amount = $request -> amount_ed;
        $offer -> expiry_date = $request -> expiry_date_ed;
        $offer -> max_discount = $request -> max_discount_ed;
        

        if ( !empty($filename) ) {
            $offer -> image_url = $filename;
        }

        $offer->save();

        return ;


    }

    public function delete_offer(Request $request) {
        $offer = Offers::find($request->offer_id);
        $offer->delete();
        return;
    }

    public function update_offer_status(Request $request) {
        $offer = Offers::find($request->offer_id);
        $offer->status = $request->offer_status;
        $offer->save();
        return;
    }

    public function check_offer_name(Request $request) {
        // dd( $request );
        // exit;
        $merchant_id = Auth::guard('merchant')->id();

        if($request->offer_id != ""){
            $outlet = DB::table('offers')
                    ->where('offer_name', $request->offer_name)
                    ->where('merchant_id', $merchant_id)
                    ->where('id', '!=', $request->offer_id)
                    ->first();
        }
        else {
            $outlet = DB::table('offers')
                    ->where('offer_name', $request->offer_name)
                    ->where('merchant_id', $merchant_id)
                    ->first();
        }
        
        
        if ( !empty($outlet) ) {
            return response()->json([
                'message' => 'Offer already Exist'
            ],200);
        } else {
            return response()->json([
                'message' => 'Offername available'
            ],200);
        }

    }


}