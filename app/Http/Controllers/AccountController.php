<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounts;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function create_account(Request $request)
    {        
        $merchant_id = Auth::guard('merchant')->id();

        if($request -> tnc_accepted == '1'){
            $tnc_accepted = "true";$tnc = "1";
        }else{ $tnc_accepted = "false"; $tnc = "0"; }  

        // $postData = '{
        //     "name":"'.$request->name.'",
        //     "email":"'.$request->email.'",
        //     "tnc_accepted":"'.$tnc_accepted.'",,
        //     "account_details":{
        //        "business_name":"'.$request->business_name.'",
        //        "business_type":"'.$request->business_type.'",
        //     },
        //     "bank_account":{
        //        "ifsc_code":"'.$request->ifsc_code.'",
        //        "beneficiary_name":"'.$request->beneficiary_name.'",
        //        "account_type":"'.$request->account_type.'",
        //        "account_number":"'.$request->account_number.'",
        //     }
        // }';
        // var_dump($postData);die();

        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => 'https://api.razorpay.com/v1/beta/accounts',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => '',
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => true,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS => $postData,
        //     CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json',
        //     'Authorization: Basic cnpwX2xpdmVfYUlpYkVIR3E5WVlmUVY6Q3FaTUdkVzEycTBTVUJNZU1tcGljSnNp'
        //     ),
        // ));
        
        // $response = curl_exec($curl);   
        // //Print error if any
        // if(curl_errno($ch))
        // {
        //     echo 'error:' . curl_error($ch);
        // }   
        // else{
        //     echo $response;
        //     $data = json_decode($response, true); 
        //     echo $data[0]->error;
        // }  
        // curl_close($curl);
        

        //if($data['id'] != ''){
            $account = new accounts(); 
            $account -> merchant_id = $merchant_id;
            $account -> rozorpay_account_id = "acc_dammyid";
            // $account -> rozorpay_account_id = $data['id'];
            $account -> name = $request -> name;
            $account -> email = $request -> email;
            $account -> tnc_accepted = $tnc;
            $account -> business_name = $request -> business_name;
            $account -> business_type = $request -> business_type;
            $account -> ifsc_code = $request -> ifsc_code;
            $account -> beneficiary_name = $request -> beneficiary_name; 
            $account -> account_number = $request -> account_number; 
            $account -> account_type = $request -> account_type;
            $account -> status = "Activated";
            //$account -> status = $data['activation_details']['status'];        
            $account->save();
        //}       

        return ;
    }

    public function get_account_list(Request $request)
    {
        $merchant_id = Auth::guard('merchant')->id();

        if ( $merchant_id != null ) {

            $columns = array( 
                0 =>'id',
                1 =>'rozorpay_account_id',
                2 =>'name',
                3 =>'email',
                4 =>'account_number',
                5 =>'status',
                6 =>'action',
            );  
            $totalData = Accounts::where('merchant_id', $merchant_id)->count();            
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            
            // if(empty($request->input('search.value')))
            // {            
                $accounts = Accounts::where('merchant_id', $merchant_id)->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            //}
        }

    
        $data = array();
        if(!empty($accounts))
        {
            foreach ($accounts as $key=>$account)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['rozorpay_account_id'] = $account->rozorpay_account_id;
                $nestedData['name'] = $account->name;
                $nestedData['email'] = $account->email;
                $nestedData['account_number'] = $account->account_number;
                $nestedData['status'] = $account->status;
                $nestedData['action'] = '<div class="action-group"><a title="View Account Detail" class="view btn operations" onclick="getAccountView('.$account->id.')" data-toggle="modal" data-target="#viewAccountModal-1"><i class="fa fa-eye"></i></a>  <a title="Delete Menu Item" data-attr="'.$account->id.'" onclick="delete_account('.$account->id.')" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
 
    }
    public function get_account(Request $request){

        $pd = accounts::where('id',$request->id)->first();

        return Array( 'account' =>$pd);
    }

    public function account_list()
    {
        $merchant_id = Auth::guard('merchant')->id();

        $list = Accounts::where('merchant_id',$merchant_id)->get();

        $data = Array(
            'account_list' => $list
        );
        return view('merchant.account_list')->with($data);
    }

    public function edit_account(Request $request){
        
        $merchant_id = Auth::guard('merchant')->id();
        $account = accounts::find($request -> account_id);

        $filename = '';
        $path = $request->file('image_url_ed');
        
        if( !empty( $request->file('image_url_ed') ) ){
            
            $img_temp = $request->file('image_url_ed');
            $tmp = pathinfo($img_temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $img_temp -> getClientOriginalExtension();
            $filename  = $tmp.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "images/".$filename;
            if ($_SERVER['SERVER_NAME'] != '127.0.0.1') {
                $image_path = "public/images/".$filename;
            }
           
            move_uploaded_file($img_temp,$image_path);           
        }
        
        
        $account -> account_name = $request -> account_name_ed;
        $account -> account_type = $request -> account_type_ed;
        $account -> account_name = $request -> account_name_ed;
        $account -> account_description = $request -> account_description_ed;
        $account -> outlet_id = json_encode( $request -> outlet_id_ed);
        $account -> promo_code = $request -> promo_code_ed;
        $account -> discount_type = $request -> discount_type_ed;
        $account -> discount = $request -> discount_ed;
        $account -> amount = $request -> amount_ed;
        $account -> expiry_date = $request -> expiry_date_ed;
        $account -> max_discount = $request -> max_discount_ed;
        

        if ( !empty($filename) ) {
            $account -> image_url = $filename;
        }

        $account->save();

        return ;


    }

    public function delete_account(Request $request) {
        $account = accounts::find($request->account_id);
        $account->delete();
        return;
    }

    public function check_account_email(Request $request)
    {
        $merchant_id = Auth::guard('merchant')->id();

        if($request->account_id != ""){
            $data = DB::table('accounts')
                    ->where('email', $request->email)
                    ->where('merchant_id', $merchant_id)
                    ->where('id', '!=', $request->account_id)
                    ->first();
        }
        else {
            $data = DB::table('accounts')
                    ->where('email', $request->email)
                    ->where('merchant_id', $merchant_id)
                    ->first();
        }
        
        
        if ( !empty($data) ) {
            return response()->json([
                'message' => 'Account email already Exist'
            ],200);
        } else {
            return response()->json([
                'message' => 'Account email available'
            ],200);
        }

    }

}