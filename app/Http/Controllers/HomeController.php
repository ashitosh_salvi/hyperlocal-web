<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
//use App\Role;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        //$this->middleware('role:ROLE_SUPERADMIN');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd( Auth::user()->id );
        // $check_role = DB::select('select r.name from role_user as ru INNER JOIN roles as r on ru.role_id = r.id WHERE user_id = ?', [Auth::user()->id]);
        // dd( $check_role );
        // if ( $check_role[0]->name == "ROLE_SUPERADMIN" ) {
            return view('welcome');
        // } elseif ( $check_role[0]->name == "ROLE_MERCHANT" ) {
        //     return view('merchant.merchant_dashboard');
        // }

    }
}
