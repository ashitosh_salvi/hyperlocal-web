<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function create_product(Request $request){

        $outlet_id = Auth::guard('outlet')->id();

        $product = new Product(); 

        $filename = [];
        $img = $request->file('images');
        foreach($img as $temp){
            
            $tmp_out = pathinfo( $temp -> getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $temp -> getClientOriginalExtension();
            $filename_product  = $tmp_out.'-'.rand(1111,9999).'.'.$extension;
            $image_path = "images/". $filename_product;
            move_uploaded_file( $temp, $image_path);
            array_push( $filename, $filename_product );
        }


        $product -> outlet_id = $outlet_id;
        $product -> product_name = $request -> product_name;
        $product -> brand = $request -> brand;
        $product -> category_id = $request -> category_id;
        $product -> sub_category_id = $request -> sub_category_id;
        $product -> child_category_id = $request -> child_category_id;
        // $product -> discount = $request -> discount;
        $product -> manufacturer = $request -> manufacturer;
        $product -> price = $request -> price;
        $product -> quantity = '0';
        $product -> sku_unit = $request -> sku_unit;
        $product -> special_price = $request -> special_price;  
        $product -> description = $request -> description;
        $product -> status = 1;
        $product -> flag = 0;
        $product -> image_url = implode( ",", $filename );

        $product->save();

        return ;


    }

    public function get_product_list(Request $request) {

        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        if ( $outlet_id != null ) {
        
            $columns = array( 
                0 =>'product_name',
                1 =>'description',
                2 =>'brand',
                3 =>'price',
                4 =>'manufacturer',
                5 =>'status',
                6 =>'action',
            );
            $totalData = Product::where('outlet_id', $outlet_id)->count();
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            if(empty($request->input('search.value'))) {            
                $Products = Product::where('outlet_id', $outlet_id)->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            } else {

                $search = $request->input('search.value'); 
    
                $Products =  Product::where('outlet_id', $outlet_id)
                                ->orWhere('product_name','LIKE',"%{$search}%")
                                ->orWhere('description', 'LIKE',"%{$search}%")
                                ->orWhere('brand', 'LIKE',"%{$search}%")
                                ->orWhere('price', 'LIKE',"%{$search}%")
                                ->orWhere('manufacturer', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
    
                $totalFiltered = Product::where('outlet_id', $outlet_id)
                                ->orWhere('product_name','LIKE',"%{$search}%")
                                ->orWhere('description', 'LIKE',"%{$search}%")
                                ->orWhere('brand', 'LIKE',"%{$search}%")
                                ->orWhere('price', 'LIKE',"%{$search}%")
                                ->orWhere('manufacturer', 'LIKE',"%{$search}%")
                                ->count();

            }
        } else {
            $Products = array();
        }
        $data = array();
        if(!empty($Products)) {
            foreach ($Products as $key=>$product) {
                
                $nestedData['product_name'] = $product->product_name;
                $nestedData['description'] = $product->description;
                $nestedData['brand'] = $product->brand;
                $nestedData['price'] = $product->price;
                $nestedData['manufacturer'] = $product->manufacturer;
                $nestedData['status']= '<input class="tgl tgl-ios tgl_checkbox" id="cb_'. $product->id .'" name="product_status" type="checkbox" data-attr="'. $product->status .'" '. ( ( $product->status == '1' ) ? "checked" : "" ) .' product-id="'. $product->id .'">
                <label class="tgl-btn" for="cb_'. $product->id .'"></label>';
                $nestedData['action']= '<div class="action-group"><a title="View Product" class="view btn operations" onclick="getProductView( '. $product->id .' )" data-toggle="modal" data-target="#"> <i class="fa fa-eye"></i></a>
                <a title="Edit Product" class="btn operations" onclick="getProduct( '. $product->id .' )" data-toggle="modal" data-target="#">
                    <i class="fa fa-edit"></i>
                </a>
                <a title="Delete Product" href="javascript:void(0)" onclick="delete_product( '. $product->id .' )" class="btn operations"><i class="fa fa-trash-o"></i></a></div>';

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);
    }

    public function get_product(Request $request){

        $pd = Product::where('id',$request->id)->first();

        return Array( 'product' =>$pd);
    }

    public function product_list(){

        $list = Product::all();

        $data = Array(
            'list' => $list
        );
        return view('merchant.product_list')->with($data);
    }

    public function edit_product(Request $request){

        $product = Product::find($request -> product_id);
        
        if($request->hasFile('images_edit')){
            $filename = [];
            $img = $request->file('images_edit');
            foreach($img as $temp){
                
                $tmp_out = pathinfo( $temp -> getClientOriginalName(), PATHINFO_FILENAME);
                $extension = $temp -> getClientOriginalExtension();
                $filename_product  = $tmp_out.'-'.rand(1111,9999).'.'.$extension;
                $image_path = "images/". $filename_product;
                move_uploaded_file( $temp, $image_path);
                array_push( $filename, $filename_product );
            }
            $product -> image_url = implode( ",", $filename );
        }
        
        $product -> product_name = $request -> product_name_ed;
        $product -> brand = $request -> brand_ed;
        $product -> category_id = $request -> category_id_ed;
        // $product -> discount = $request -> discount_ed;
        $product -> manufacturer = $request -> manufacturer_ed;
        $product -> price = $request -> price_ed;
        $product -> sub_category_id = $request -> sub_category_id_ed;
        $product -> child_category_id = $request -> child_category_id_ed;
        $product -> quantity = '0';
        $product -> sku_unit = $request -> sku_unit_ed;
        $product -> special_price = $request -> special_price_ed;  
        $product -> description = $request -> description_ed;
        

        $product->save();

        return ;


    }

    public function get_grossery_cat() {
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;
        $category = Category::where('priority', 1)
                                ->where('outlet_id', $outlet_id)
                                ->where('outlet_type', $outlet_type)
                                ->where('status', 1)
                                ->get();
        // $category = DB::select('select id, parent_category, category_name from categories where status = 1 and outlet_id = ? and outlet_type = ?', [$outlet_id, $outlet_type]);
        return response()->json([
            'category' => $category
        ]);
    } 

    public function get_grossery_subcat_by_cat_id(Request $request) {
        
        $cat_id = $request->cat_id;
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $sub_category = Category::where('parent_category', $cat_id)
                                ->where('priority', 2)
                                ->where('outlet_id', $outlet_id)
                                ->where('outlet_type', $outlet_type)
                                ->get(['id', 'category_name']);
        return response()->json([
            'sub_category' => $sub_category
        ]);
    }

    public function get_grossery_childcat_by_subcat_id(Request $request) {
        
        $cat_id = $request->cat_id;
        $outlet_id = Auth::guard('outlet')->id();
        $outlet_type = Auth::guard('outlet')->user()->outlet_type;

        $child_category = Category::where('parent_category', $cat_id)
                                ->where('priority', 3)
                                ->where('outlet_id', $outlet_id)
                                ->where('outlet_type', $outlet_type)
                                ->get(['id', 'category_name']);
        return response()->json([
            'child_category' => $child_category
        ]);
    }

    public function delete_product(Request $request) {
        $product = Product::find($request->product_id);
        $product->delete();
        return;
    }

    public function update_product_status(Request $request) {
        $product = Product::find($request->product_id);
        if($request->product_status == 0){
            $product->status = $request->product_status;
            $product->flag = 1;
        }
        else{
            $product->status = $request->product_status;
            $product->flag = 0;
        }
        
        $product->save();
        return;
    }

}
