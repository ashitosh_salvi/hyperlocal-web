<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    //
    public function get_all_cities_by_state_id(Request $request) {
        
        $state_id = $request->state_id;

        $cities = City::where('city_state', $state_id)->get(['id', 'city_name']);
        return response()->json([
            'cities' => $cities
        ]);
    }

    public function get_city_by_name(Request $request) {
        $city_name = $request->city_name;
        $city = City::where('city_name', $city_name)->get(['id', 'city_name']);
        return response()->json([
            'city' => $city
        ]);
    }

}
