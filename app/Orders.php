<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Orders extends Model
{
    use HasApiTokens;
    
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id', 'promocode', 'amount'
    ];
    
    protected $table = 'orders';

    public function orderList()
    {
        return $this->hasMany('App\OrderItems','order_id','order_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer','customer_id');
    }

    public function rider()
    {
        return $this->belongsTo('App\Rider','rider_id');
    }

    public function customer_address()
    {
        return $this->belongsTo('App\CustomerAddress','customer_addr_id');
    }

}