<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Roles extends Model
{
    use HasApiTokens;
    
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];
    
    protected $table = 'roles';

}
