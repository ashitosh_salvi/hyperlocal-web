<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;

class Notify extends Notification
{
    public function via($notifiable)
    {
        return [FcmChannel::class,'database'];
    }

    public function toFcm($notifiable)
    {
        return FcmMessage::create()
            ->setData(['title' => $notifiable->msgtitle, 'body' => $notifiable->msgbody , 'order_id' => $notifiable->order_id , 'click_action' => 'FLUTTER_NOTIFICATION_CLICK' ])
            ->setNotification(\NotificationChannels\Fcm\Resources\Notification::create()
                ->setTitle($notifiable->msgtitle)
                ->setBody($notifiable->msgbody)
                ->setImage(''))
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setNotification(AndroidNotification::create()->setColor('#0A0A0A'))
            )->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios')));
    }

    public function toArray($notifiable)
    {
        return [
            'title' => $notifiable->msgtitle,
            'body' => $notifiable->msgbody,
            'order_id' => $notifiable->order_id,
        ];
    }


}