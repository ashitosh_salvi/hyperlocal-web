<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Merchant extends Authenticatable
{
    //
    protected $fillable = [
        'user_name', 'first_name', 'last_name', 'email', 'password', 'address', 'city', 'state', 'country', 'pin_code', 'contact', 'status', 'flag'
    ];

    // protected $hidden = [
    //     'password',
    // ];
    
}
