<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Rider extends Authenticatable
{
    use HasApiTokens, Notifiable;
    //
    protected $fillable = [
        'first_name', 'last_name','email', 'mobile', 'password', 'status', 'image_url', 'address', 'city', 'state', 'country', 'pin_code', 'flag'
    ];
    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function routeNotificationForFcm(){
        return $this->firebase_token;
    }

}
