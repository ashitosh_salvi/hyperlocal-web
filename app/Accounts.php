<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Accounts extends Model
{
    use HasApiTokens;
    
    // Primary Key
    protected $primaryKey = 'id';

    protected $fillable = [
        'merchant_id','rozorpay_account_id','name','email','tnc_accepted','business_name','business_type','ifsc_code',
        'beneficiary_name','account_number','account_type','status'
    ];
    
    protected $table = 'accounts';

}
