<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth'
], function(){

    // Route::get('getorders', 'Api\OrdersApiController@index');
    Route::get('getoutlets', 'Api\OutletApiController@index');
    Route::get('get_outlet_categories', 'Api\OutletApiController@get_outlet_categories');
    Route::post('getoutletsbygeolocation', 'Api\OutletApiController@getoutletsbygeolocation');
    Route::post('search_outlets_by_outlet_name', 'Api\OutletApiController@search_outlets_by_outlet_name');
    Route::post('customerlogin', 'Api\CustomerApiController@login');
    Route::post('customersignup', 'Api\CustomerApiController@signup');
    Route::post('checkmobile', 'Api\CustomerApiController@checkmobile');
    Route::post('checkemailmobile', 'Api\CustomerApiController@checkemailmobile');
    Route::post('customerNewPassword', 'Api\CustomerApiController@customerNewPassword');
    
    Route::post('riderlogin', 'Api\RiderApiController@login');
    Route::post('ridersignup', 'Api\RiderApiController@signup');
    Route::post('checkridermobile', 'Api\RiderApiController@checkridermobile');
    Route::post('riderNewPassword', 'Api\RiderApiController@riderNewPassword');


    // All customer routes with auth token
    Route::group([
        'middleware' => 'auth:api'
    ], function(){
        
        

    });

    // All customer routes with auth token
    Route::group([
        // 'namespace' => 'Rider', 
        // 'prefix' => 'customer/v1', 
        'middleware' => 'auth:customer-api'
    ], function() {

        // Home Page Route
        Route::post('get_homepage_data', 'Api\OutletApiController@get_homepage_data');

        // define your routes here for the "customer"
        Route::post('get_outlets_by_category', 'Api\OutletApiController@get_outlets_by_category');
        Route::post('get_orders_by_customer_id', 'Api\OrdersApiController@get_orders_by_customer_id');
        Route::post('update_customer_details', 'Api\CustomerApiController@update_customer_details');
        Route::post('customer_mark_favourite_outlet', 'Api\CustomerApiController@customer_mark_favourite_outlet');
        Route::post('get_customer_favourite_outlets', 'Api\CustomerApiController@get_customer_favourite_outlets');
        Route::post('customer_rate_outlet', 'Api\CustomerApiController@customer_rate_outlet');
        Route::get('customerlogout', 'Api\CustomerApiController@logout');

        // Customer Addresses
        Route::post('add_customer_address', 'Api\CustomerApiController@add_customer_address');
        Route::post('get_customer_addresses', 'Api\CustomerApiController@get_customer_addresses');
        Route::post('delete_customer_addresses', 'Api\CustomerApiController@delete_customer_addresses');
        Route::post('update_customer_addresses', 'Api\CustomerApiController@update_customer_addresses');

        // Cart API'S
        Route::post('get_cart_data_by_customer_id', 'Api\CartItemsController@get_cart_data_by_customer_id');
        Route::post('create_temp_order_by_customer_id', 'Api\CartItemsController@create_temp_order_by_customer_id');
        Route::post('order_pre_finalize', 'Api\CartItemsController@order_pre_finalize');
        Route::post('apply_promo_code', 'Api\CartItemsController@apply_promo_code');
        Route::post('remove_offer_from_cart', 'Api\CartItemsController@remove_offer_from_cart');
        Route::post('place_order', 'Api\CartItemsController@place_order');

        Route::post('get_items_in_cart_by_customer_id', 'Api\CartItemsController@get_items_in_cart_by_customer_id');
        Route::post('get_cart_count', 'Api\CartItemsController@get_cart_count');
        Route::post('add_to_cart', 'Api\CartItemsController@add_to_cart');
        Route::post('remove_item_from_cart', 'Api\CartItemsController@remove_item_from_cart');
        Route::post('empty_cart', 'Api\CartItemsController@empty_cart');

        // Get items by outlet ID 
        Route::post('get_items_by_outlet_id_bk_240321', 'Api\OutletApiController@get_items_by_outlet_id');
        //Temp route by suyash
        Route::post('get_items_by_outlet_id_bk', 'Api\OutletApiController@get_items_by_outlet_id_bk');
        Route::post('get_items_by_outlet_id', 'Api\OutletApiController@get_items_by_outlet_id_bk_240321');
        Route::post('search_by_item_name_outlet_id', 'Api\OutletApiController@search_by_item_name_outlet_id');
        Route::post('offer_by_outlet', 'Api\OutletApiController@offer_by_outlet');

        // Get Offers By Merchant Id
        Route::post('get_all_offers_by_geolocation', 'Api\OutletApiController@get_all_offers_by_geolocation');

        Route::get('get_offer_list', 'Api\OffersApiController@get_offer_list');


    });

    // All rider routes with auth token
    Route::group([
        // 'namespace' => 'Rider', 
        // 'prefix' => 'rider/v1', 
        'middleware' => 'auth:rider-api'
    ], function() {

        // define your routes here for the "rider"
        Route::post('rider', 'Api\RiderApiController@rider');
        Route::post('changeriderpassword', 'Api\RiderApiController@changeriderpassword');
        Route::get('riderlogout', 'Api\RiderApiController@logout');
        Route::post('get_orders_by_rider_id', 'Api\OrdersApiController@ordersByRiderID');
        
        Route::post('rider_notification','Api\RiderApiController@rider_notification');
        Route::post('get_order_details', 'Api\OrdersApiController@get_order_details');
        Route::post('test', 'Api\RiderApiController@test');
        Route::post('active_inactive_status', 'Api\RiderApiController@riderActiveInactive');
        Route::post('online_offline_status', 'Api\RiderApiController@riderOnlineOffline');
        Route::post('order_report', 'Api\RiderApiController@riderOrderReport');

    });

    Route::post('orderUpdate','Api\OrdersApiController@orderUpdate');


    // outlet app api
    Route::group(['prefix' => 'outlet'],function(){
        Route::POST('login','Api\Outlet\Auth\LoginController@login');
        Route::POST('logout-outlet','Api\Outlet\Auth\LoginController@logoutOutlet');
        Route::post('checkoutletmobile', 'Api\Outlet\OutletApiController@checkoutletmobile');
        Route::post('outletNewPassword', 'Api\Outlet\OutletApiController@outletNewPassword');
        Route::post('outlet_notification','Api\Outlet\OutletApiController@outlet_notification');
        Route::post('test_outlet_notification','Api\Outlet\OutletApiController@test_notification');
        Route::group(['middleware' => 'auth:outlet-api'],function(){
            Route::POST('logout','Api\Outlet\Auth\LoginController@logout');
            Route::POST('outlet_open_close','Api\Outlet\OutletApiController@outletOpenClose');
            Route::POST('outlet_flag','Api\Outlet\OutletApiController@outletFlag');
            Route::group(['prefix' => 'order'],function(){
                Route::POST('/get_order','Api\Outlet\OutletApiController@getOrder');
                Route::POST('pending','Api\Outlet\OutletApiController@pendingOrder');
                Route::POST('in-progress','Api\Outlet\OutletApiController@inProgress');
                Route::POST('past','Api\Outlet\OutletApiController@pastOrder');
                Route::POST('update_status','Api\Outlet\OutletApiController@updateStatus');
            });
            Route::group(['prefix' => 'rider'],function(){
                Route::POST('all_riders','Api\Outlet\RiderApiController@allRiders');
                Route::POST('assign_rider_order','Api\Outlet\RiderApiController@assign_rider_order');
            });
        });
    });
   
    Route::group(['prefix' => 'test_function'],function(){
        Route::post('test_route_distance','testController@routeDistance');
    });
});