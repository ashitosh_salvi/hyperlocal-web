<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/outlet_logout', 'Auth\LoginController@OutletLogout');

// Merchant Login
Route::get('/merchant_login', 'MerchantController@showLoginForm');
Route::post('merchantlogin', 'MerchantController@merchantlogin')->name('merchantlogin');

// Outlet Routes
Route::get('/outlet_login', 'OutletController@showLoginForm');
Route::post('outletlogin', 'OutletController@outletlogin')->name('outletlogin');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
 Route::post('/admin_crop_outlet_image', 'OutletController@imageCrop')->name('admin_crop_outlet_image');

// Route::get('/reset_pin', function () {
//     return view('merchant.reset_pin');
// });

// Route::get('/manage_user', function () {
//     return view('merchant.manage_user');
// });
// Route::get('/monthly_salary', function () {
//     return view('merchant.monthly_salary');
// }); 
// Route::get('/incentive', function () {
//     return view('merchant.incentive');
// });

// Route::get('/employee_create', function () {
//     return view('employee.employee_create');
// })->name('employee_create');
// Route::get('/employee_edit', function () {
//     return view('employee.employee_edit');
// });
// Route::get('/employee_login', function () {
//     return view('employee.employee_login');
// });
// Route::get('/employee_dashboard', function () {
//     return view('employee.employee_dashboard');
// });
// Route::get('/employee_salary', function () {
//     return view('employee.employee_salary');
// });
// Route::get('/employee_salary_edit', function () {
//     return view('employee.employee_salary_edit');
// });
// Route::get('/employee_advance_payment', function () {
//     return view('employee.employee_advance_payment');
// });


Route::group([
//     'middleware' => 'role:ROLE_MERCHANT',
    'middleware' => 'merchant',
], function() {
    // Merchant Routes
    Route::get('/merchant_dashboard', 'MerchantController@index');
    
    // Employee Routes
    Route::get('/employee_list', 'EmployeeController@index');
    Route::post('/create_employee','EmployeeController@create_employee')->name('create_employee');
    Route::post('/edit_employee','EmployeeController@edit_employee')->name('edit_employee');
    Route::post('/delete_employee','EmployeeController@delete_employee')->name('delete_employee');
    Route::get('/get_employee','EmployeeController@get_employee')->name('get_employee');
    Route::get('/get_employee_list','EmployeeController@get_employee_list')->name('get_employee_list');
    Route::get('/get_employee_roles','EmployeeController@get_employee_roles')->name('get_employee_roles');
    Route::get('/get_employee_role_by_id','EmployeeController@get_employee_role_by_id')->name('get_employee_role_by_id');
    Route::post('/update_employee_status','EmployeeController@update_employee_status')->name('update_employee_status');

    // Outlet Routes
    Route::get('/outlet_list', 'OutletController@outlet_list');
    Route::post('/crop_outlet_image', 'OutletController@imageCrop')->name('crop_outlet_image');
    Route::post('/create_outlet','OutletController@create_outlet')->name('create_outlet');
    Route::post('/edit_outlet','OutletController@edit_outlet')->name('edit_outlet');
    Route::get('/get_outlet_list', 'OutletController@get_outlet_list')->name('get_outlet_list');
    Route::get('/get_outlet','OutletController@get_outlet')->name('get_outlet');
    Route::get('/destroy_outlet_session/{id?}','OutletController@destroyAllSession')->name('destroy_outlet_session');
    Route::get('/destroy_app_outlet_session/{id?}','OutletController@destroyAllAppSession')->name('destroy_app_outlet_session');

    // Offers Routes
    Route::post('/create_offer','OfferController@create_offer')->name('create_offer');
    Route::get('/offer_list', 'OfferController@offer_list');
    Route::get('/get_offer','OfferController@get_offer')->name('get_offer');
    Route::post('/edit_offer','OfferController@edit_offer')->name('edit_offer');
    Route::post('/delete_offer','OfferController@delete_offer')->name('delete_offer');
    Route::get('/get_offer_list','OfferController@get_offer_list')->name('get_offer_list');
    Route::post('/update_offer_status','OfferController@update_offer_status')->name('update_offer_status');
    Route::post('/check_offer_name','OfferController@check_offer_name')->name('check_offer_name');

    // Account Routes
    Route::post('/create_account','AccountController@create_account')->name('create_account');
    Route::get('/account_list', 'AccountController@account_list');
    Route::get('/get_account','AccountController@get_account')->name('get_account');
    Route::post('/edit_account','AccountController@edit_account')->name('edit_account');
    Route::post('/delete_account','AccountController@delete_account')->name('delete_account');
    Route::get('/get_account_list','AccountController@get_account_list')->name('get_account_list');
    Route::post('/check_account_email','AccountController@check_account_email')->name('check_account_email');
    
});

Route::post('/delete_outlet','OutletController@delete_outlet')->name('delete_outlet');
Route::post('/attachement_outlet','OutletController@attachement_outlet')->name('attachement_outlet');
Route::post('/outlet_check_username','OutletController@outlet_check_username')->name('outlet_check_username');

Route::group([
//     'middleware' => 'role:ROLE_SUPERADMIN',
        'middleware' => 'auth'
], function() {

    // Merchant Routes
    Route::get('/merchant_list', 'MerchantController@merchant_list');
    Route::post('/create_merchant','MerchantController@create_merchant')->name('create_merchant');
    Route::post('/edit_merchant','MerchantController@edit_merchant')->name('edit_merchant');
    Route::post('/delete_merchant','MerchantController@delete_merchant')->name('delete_merchant');
    Route::get('/get_merchant','MerchantController@get_merchant')->name('get_merchant');
    Route::post('/check_username','MerchantController@check_username')->name('check_username');
    Route::get('/get_merchant_list','MerchantController@get_merchant_list')->name('get_merchant_list');

    // Super Admin Dashboard
    Route::get('/admin_dashboard', function () {
        return view('superadmin.admin_dashboard');
    });

    // Vendor Routes
    Route::get('/vendor_list', function () {
        return view('vendor.vendor_list');
    });

    // Rider Routes
    Route::get('/rider_list', 'RiderController@rider_list');
    Route::get('/get_rider_list','RiderController@get_rider_list')->name('get_rider_list');
    Route::post('/create_rider','RiderController@create_rider')->name('create_rider');
    Route::get('/get_rider','RiderController@get_rider')->name('get_rider');
    Route::post('/edit_rider','RiderController@edit_rider')->name('edit_rider');
    Route::post('/update_rider_status','RiderController@update_rider_status')->name('update_rider_status');

    // Roles Routes
    Route::post('/create_role','RoleController@create_role')->name('create_role');
    Route::get('/role_list', 'RoleController@role_list');
    Route::get('/get_role','RoleController@get_role')->name('get_role');
    Route::post('/edit_role','RoleController@edit_role')->name('edit_role');
    Route::post('/delete_role','RoleController@delete_role')->name('delete_role');
    Route::get('/get_role_list','RoleController@get_role_list')->name('get_role_list');
    Route::post('/update_role_status','RoleController@update_role_status')->name('update_role_status');

    // admin outlet 
    Route::get('/admin_outlet_list', 'OutletController@admin_outlet_list');
    Route::get('/get_admin_outlet_list', 'OutletController@get_admin_outlet_list')->name('get_admin_outlet_list');
    Route::post('/admin_update_outlet_status','OutletController@admin_update_outlet_status')->name('admin_update_outlet_status');

    // rider report for admin update
    Route::get('/admin_rider_report', 'RiderController@admin_rider_report')->name('admin_rider_report');
    Route::get('/get_admin_rider_report', 'RiderController@get_admin_rider_report')->name('get_admin_rider_report');
    Route::get('/export_get_admin_rider_report/{from_date?}/{to_date?}/{search?}', 'RiderController@export_get_admin_rider_report')->name('export_get_admin_rider_report');

    Route::get('/get_admin_rider_details_repor/{rider_id?}', 'RiderController@get_admin_rider_details_report')->name('get_admin_rider_details_report');
    Route::get('/export_get_admin_rider_details_repor/{from_date?}/{to_date?}/{rider_id?}', 'RiderController@export_get_admin_rider_details_report')->name('export_get_admin_rider_details_report');
    
    
    // rider report for admin update
    Route::get('/admin_merchant_report', 'MerchantController@admin_merchant_report')->name('admin_merchant_report');
    Route::get('/get_admin_merchant_report', 'MerchantController@get_admin_merchant_report')->name('get_admin_merchant_report');
    Route::get('/export_get_admin_merchant_report/{from_date?}/{to_date?}/{search?}', 'MerchantController@export_get_admin_merchant_report')->name('export_get_admin_merchant_report');

    Route::get('/get_admin_merchant_details_report/{merchant_id?}', 'MerchantController@get_admin_merchant_details_report')->name('get_admin_merchant_details_report');
    Route::get('/export_get_admin_merchant_details_report/{from_date?}/{to_date?}/{merchant_id?}', 'MerchantController@export_get_admin_merchant_details_report')->name('export_get_admin_merchant_details_report');
    

    // outlet MIS 
    Route::get('/individual_outlet','OutletReportController@individualOutlet')->name('individual_outlet');
    Route::get('/get_individual_outlet_report','OutletReportController@getIndividualOutletReport')->name('get_individual_outlet_report');
    Route::get('/export_individual_outlet_report/{from_date?}/{to_date?}/{search?}/{outlet_id?}', 'OutletReportController@exportIndividualOutletReport')->name('export_individual_outlet_report');

    Route::get('/outlet_daily_sales','OutletReportController@outletDailySales')->name('outlet_daily_sales');
    Route::get('/get_outlet_daily_sales_report','OutletReportController@getoutletDailySalesReport')->name('get_outlet_daily_sales_report');
    Route::get('/export_outlet_daily_sales_report/{from_date?}/{to_date?}/{search?}/{outlet_id?}', 'OutletReportController@exportOutletDailySalesReport')->name('export_outlet_daily_sales_report');
    
});

Route::group([
//     'middleware' => 'role:ROLE_SUPERADMIN',
        'middleware' => 'outlet'
], function() {      

    // Outlet Dashboard
    Route::get('/outlet_dashboard', 'OutletController@index');

    // Order Routes
    Route::get('/orders_list/{order_id?}', 'OrdersController@index')->name('orders_list');
    Route::get('/get_current_order_list','OrdersController@get_current_order_list')->name('get_current_order_list');
    Route::get('/get_past_order_list','OrdersController@get_past_order_list')->name('get_past_order_list');
    Route::post('/get_order_details', 'OrdersController@getOrderDetails')->name('get_order_details');
    Route::post('/edit_order','OrdersController@editOrderDetails')->name('edit_order');
    Route::post('/delete_order','OrdersController@delete_order')->name('delete_order');

    Route::get('/generate_qr', 'OutletController@generate_qr')->name('generate_qr');
    Route::get('/downloadQR/{qrData}', 'OutletController@downloadQR')->name('downloadQR');
    
    
    
    Route::post('/get_recived_order_id','OrdersController@get_recived_order_id')->name('get_recived_order_id');

    /* PRODUCT CATEGORY ROUTE */
    Route::post('save_category_form', 'CategoryController@store')->name('save_category_form');
    Route::resource('category', 'CategoryController');
    Route::match(['get', 'post'], 'get-category','CategoryController@getCategory')->name('get-category');
    Route::get('/get_category_product_view', 'CategoryController@get_category_product_view')->name('get_category_product_view');
    Route::get('/get_grossery_cat', 'ProductController@get_grossery_cat')->name('get_grossery_cat');

    /* RECIPE CATEGORY ROUTE */
    Route::resource('recipe_category', 'RecipeCategoryController');
    Route::match(['get', 'post'], 'get-recipe-category','RecipeCategoryController@getCategory')->name('get-recipe-category');
    Route::get('/get_recipe_category_product_view', 'RecipeCategoryController@get_recipe_category_product_view')->name('get_recipe_category_product_view');
    Route::post('/get_main_category', 'RecipeCategoryController@get_main_category')->name('get_main_category');
    Route::post('/get_recipe_cat_by_type', 'MenuItemController@get_recipe_cat_by_type')->name('get_recipe_cat_by_type');
    
    // Product Routes
    Route::post('/create_product','ProductController@create_product')->name('create_product');
    Route::get('/product_list', 'ProductController@product_list');
    Route::get('/get_product','ProductController@get_product')->name('get_product');
    Route::post('/edit_product','ProductController@edit_product')->name('edit_product');
    Route::post('/delete_product','ProductController@delete_product')->name('delete_product');
    Route::post('/update_product_status','ProductController@update_product_status')->name('update_product_status');
    Route::get('/get_product_list','ProductController@get_product_list')->name('get_product_list');
    Route::get('/get_grossery_subcat_by_cat_id', 'ProductController@get_grossery_subcat_by_cat_id')->name('get_grossery_subcat_by_cat_id');
    Route::get('/get_grossery_childcat_by_subcat_id', 'ProductController@get_grossery_childcat_by_subcat_id')->name('get_grossery_childcat_by_subcat_id');


    // Menu item Routes
    Route::post('/create_menuitem','MenuItemController@create_menuitem')->name('create_menuitem');
    Route::get('/menuitem_list', 'MenuItemController@menuitem_list');
    Route::get('/menuitem_grid', 'MenuItemController@menuitem_grid');    
    
    Route::get('/get_menuitem','MenuItemController@get_menuitem')->name('get_menuitem');
    Route::post('/edit_menuitem','MenuItemController@edit_menuitem')->name('edit_menuitem');
    Route::post('/delete_menuitem','MenuItemController@delete_menuitem')->name('delete_menuitem');
    Route::get('/get_menuitem_list','MenuItemController@get_menuitem_list')->name('get_menuitem_list');
    Route::get('/get_recipe_subcat_by_cat_id', 'MenuItemController@get_recipe_subcat_by_cat_id')->name('get_recipe_subcat_by_cat_id');
    Route::post('/update_menuitem_status','MenuItemController@update_menuitem_status')->name('update_menuitem_status');
    
});



// Recipe Management
// Route::get('/recipe_management', function () {
//     return view('recipe.recipe_management');
// });
// Route::get('/without_recipe_management', function () {
//     return view('recipe.without_recipe_management');
// });

// // Rider Master Routes

// Route::get('/rider_dashboard', function () {
//     return view('rider.rider_dashboard');
// });
// Route::get('/assigned_order', function () {
//     return view('rider.assigned_order');
// });
// Route::get('/confirmed_order', function () {
//     return view('rider.confirmed_order');
// });
// Route::get('/past_delivered_order', function () {
//     return view('rider.past_delivered_order');
// });
// Route::get('/incentive_earned', function () {
//     return view('rider.incentive_earned');
// });

// State & City Routes

Route::get('/get_all_states', 'StateController@get_all_states')->name('get_all_states');
Route::get('/get_state_by_name', 'StateController@get_state_by_name')->name('get_state_by_name');

Route::get('/get_all_cities_by_state_id', 'CityController@get_all_cities_by_state_id')->name('get_all_cities_by_state_id');
Route::get('/get_city_by_name', 'CityController@get_city_by_name')->name('get_city_by_name');

Route::get('/get_all_outlet_categories', 'MerchantController@get_all_outlet_categories')->name('get_all_outlet_categories');
Route::get('/get_all_merchants', 'MerchantController@get_all_merchants')->name('get_all_merchants');

Route::post('/assign_rider_merchant', 'RiderController@assign_rider_merchant')->name('assign_rider_merchant');

Route::get('/get_all_riders', 'OrdersController@get_all_riders')->name('get_all_riders');
Route::post('/assign_rider_order', 'OrdersController@assign_rider_order')->name('assign_rider_order');
Route::post('/update_order_status', 'OrdersController@update_order_status')->name('update_order_status');

Route::get('/get_all_outlets', 'OutletController@get_all_outlets')->name('get_all_outlets');
Route::post('/assign_employee_outlet', 'EmployeeController@assign_employee_outlet')->name('assign_employee_outlet');

Route::post('/remove_image', 'Controller@remove_image')->name('remove_image');

Route::get('/transfer', 'TransferController@orderTransfer')->name('transfer');
Route::get('/transfer/{id}', 'TransferController@get_orderTransfer')->name('transfer.view');
Route::post('/transfer_amount', 'TransferController@transfer_amount')->name('transfer_amount');
Route::get('/restaurant/{id}','OutletController@restaurant')->name('restorent');

// firebase notifications
Route::get('set_firebase_notification/{token?}','OutletController@setFCMToken')->name('set_firebase_notification');

// test section
Route::get('test_order_report','Api\RiderApiController@riderOrderReport');
Route::get('firebase_test','testController@index');
Route::get('send_firebase','testController@sendNotification');