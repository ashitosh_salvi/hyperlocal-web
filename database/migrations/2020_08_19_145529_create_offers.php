
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('offer_name');
            $table->string('offer_type');
            $table->text('image_url')->nullable();
            $table->string('offer_description')->nullable();
            $table->string('merchant_id');
            $table->text('outlet_id')->nullable();
            $table->string('promo_code')->nullable();
            $table->string('discount_type')->nullable();
            $table->string('discount')->nullable();
            $table->string('amount')->nullable();
            $table->string('expiry_date')->nullable();
            $table->string('max_discount')->nullable();
            $table->integer('status');
            $table->integer('flag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
