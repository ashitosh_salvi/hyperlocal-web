<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('order_id')->unique();
            $table->string('name');
            $table->string('image_url')->nullable();
            $table->string('description')->nullable();
            $table->string('outlet_id');
            $table->string('category_id');
            $table->string('sub_category_id');
            $table->string('section')->nullable();
            $table->string('food_cat')->nullable();
            $table->string('add_info')->nullable();
            $table->string('availability')->nullable();
            $table->string('from_time')->nullable();
            $table->string('to_time')->nullable();
            $table->string('spice_level')->nullable();
            $table->string('kot_group')->nullable();
            $table->string('price')->nullable();
            $table->string('charges_gst')->nullable();
            $table->string('special_instruction')->nullable();
            $table->string('serving_time')->nullable();
            $table->string('serves')->nullable();
            $table->integer('status');
            $table->integer('flag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe');
    }
}
