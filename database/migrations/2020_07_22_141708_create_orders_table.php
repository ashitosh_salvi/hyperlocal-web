<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id')->unique();
            $table->integer('merchant_id'); 
            $table->integer('outlet_id'); 
            $table->string('promocode')->nullable();
            $table->string('offer_type')->nullable();
            $table->string('amount');
            $table->string('delivery_charges');
            $table->string('discount');
            $table->string('total_amount');
            $table->string('tax');
            $table->string('final_amount');
            $table->string('total_saving');
            $table->integer('customer_id');
            $table->integer('customer_addr_id')->nullable();
            $table->integer('rider_id')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('transaction_id')->nullable();
            $table->integer('rating')->nullable();
            $table->string('feedback')->nullable();
            $table->integer('flag')->nullable(); 
            $table->string('confirm_date')->nullable();
            $table->string('out_for_delivery_date')->nullable();
            $table->string('reached_location_date')->nullable();
            $table->string('delivered_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
