
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant_id');
            $table->string('rozorpay_account_id');
            $table->string('name');
            $table->string('email');
            $table->boolean('tnc_accepted')->nullable()->default(false);
            $table->string('business_name');
            $table->string('business_type');
            $table->string('ifsc_code');
            $table->string('beneficiary_name');
            $table->string('account_number');
            $table->string('account_type');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
