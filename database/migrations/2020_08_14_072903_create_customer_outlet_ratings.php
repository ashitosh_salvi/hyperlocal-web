<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerOutletRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_outlet_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('outlet_id');
            $table->integer('rating')->default(0);
            $table->string('rating_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_outlet_ratings');
    }
}
