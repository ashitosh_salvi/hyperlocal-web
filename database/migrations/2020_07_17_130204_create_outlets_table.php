<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlets', function (Blueprint $table) {
            $table->id();
            $table->string('outlet_user_name');
            $table->string('password');
            $table->string('outlet_name')->unique();
            $table->string('outlet_type');
            $table->string('mobile')->nullable();
            $table->integer('merchant_id');
            $table->integer('outlet_category_id');
            $table->text('image_url')->nullable();
            $table->string('gst')->nullable();
            $table->string('fssai_license')->nullable();
            $table->string('shop_license')->nullable();
            $table->string('gst_rate')->nullable();
            $table->string('delivery_charges')->nullable();
            $table->string('is_cod')->nullable();
            $table->text('img_url')->nullable();
            $table->string('outlet_speciality')->nullable();
            $table->string('booking_for_two')->nullable();
            $table->string('outlet_address')->nullable();
            $table->string('outlet_geoaddress')->nullable();
            $table->double('outlet_latitude')->nullable();
            $table->double('outlet_longitude')->nullable();
            $table->time('open_time')->nullable();
            $table->time('close_time')->nullable();
            $table->string('branch')->nullable();
            $table->integer('flag')->nullable();
        

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlets');
    }
}
