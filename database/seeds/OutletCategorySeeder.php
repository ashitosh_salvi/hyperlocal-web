<?php

use Illuminate\Database\Seeder;

class OutletCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outlet_categories')->insert([
            'category_name' => 'Restaurants',
            'priority' => 1,
            'outlet_type' => 'recipe',
            'image_url' => 'outlet_categories/restaurants.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Home Chef',
            'priority' => 2,
            'outlet_type' => 'recipe',
            'image_url' => 'outlet_categories/home_chef.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Farm Fresh',
            'priority' => 3,
            'outlet_type' => 'product',
            'image_url' => 'outlet_categories/farm_fresh.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Essentials',
            'priority' => 4,
            'outlet_type' => 'product',
            'image_url' => 'outlet_categories/essentials.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Indian Sweet & Namkeen',
            'priority' => 5,
            'outlet_type' => 'product',
            'image_url' => 'outlet_categories/indian_sweets.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Hyperstores',
            'priority' => 6,
            'outlet_type' => 'product',
            'image_url' => 'outlet_categories/hyperstores.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Meat & Sea Foods',
            'priority' => 7,
            'outlet_type' => 'product',
            'image_url' => 'outlet_categories/meat_sea_food.jpeg',
        ]);
        DB::table('outlet_categories')->insert([
            'category_name' => 'Dairy Products',
            'priority' => 8,
            'outlet_type' => 'product',
            'image_url' => 'outlet_categories/dairy_products.jpeg',
        ]);
    }
}
