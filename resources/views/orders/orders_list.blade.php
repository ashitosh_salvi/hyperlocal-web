<?php
                    
use Illuminate\Support\Facades\Auth;

// dd(Auth::user()->id);
// exit;

?>
@extends('layouts.applist')

@section('content')
<?php //if ( Auth::guard('outlet') ) { ?>
@include('layouts.outlet_header')
<?php //} else if ( Auth::guard('merchant') ) { ?>
<?php //} else { ?>
<?php //} ?>
<style>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
  border-bottom: 2px solid #5176e0;
  transition: left .2s ease;
  -webkit-transition: left .2s ease;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.active {
    border-bottom: 2px solid #5176e0;
    transition: left .2s ease;
    -webkit-transition: left .2s ease;
}

#order_items_view{
    background: #f1f1f1;
}
#order_items_view .js_add {
    border-bottom: 1px solid #ccc;
    padding: 15px 0;
    margin: 0;
}
#order_items_view .js_add:last-child {
    border-bottom: unset;
}

#recived_order_items_view{
    background: #f1f1f1;
}
#recived_order_items_view .js_add {
    border-bottom: 1px solid #ccc;
    padding: 15px 0;
    margin: 0;
}
#recived_order_items_view .js_add:last-child {
    border-bottom: unset;
}

#other_order_details {
    font-size: 15px;
}
#other_order_details .row {
    border-bottom: 1px solid #ddd;
}
#other_order_details .row:last-child {
    border-bottom: unset;
}
#other_order_details .col-sm-3 {
    text-align: right;
    padding-right: 40px;
}


#recived_other_order_details {
    font-size: 15px;
}
#recived_other_order_details .row {
    border-bottom: 1px solid #ddd;
}
#recived_other_order_details .row:last-child {
    border-bottom: unset;
}
#recived_other_order_details .col-sm-3 {
    text-align: right;
    padding-right: 40px;
}
td {
    text-transform: capitalize;
}

</style>

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
            <!-- page start-->

        <section class="content-header" style="min-height: unset;">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-body">
                        <div class="col-md-6">
                            <h4><i class="fa fa-list"></i> &nbsp; Orders Management</h4>
                        </div>
                        {{-- <div class="col-md-6 text-right">
                            <button class="btn btn-info btn-flat mr-15"> Print</button>
                            <button class="btn btn-info btn-flat mr-15"> Excel</button>
                            <button class="btn btn-info btn-flat mr-15"> PDF</button>
                            <button class="btn btn-info btn-flat mr-15"> txt</button>
                        </div> --}}

                    </div>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box ">

                <div class="tab">
                    <button class="tablinks active" onclick="tabSwitcher(event, 'current_orders')">Current Orders</button>
                    <button class="tablinks" onclick="tabSwitcher(event, 'past_orders')">Past Orders</button>
                </div>

                <div id="current_orders" class="tabcontent" style="display: block;">
                    
                    <div class="box-body">

                        <div class="table-responsive mt-15">
                                
                            <table id="order_table" class="table table-bordered table-striped">

                                <thead>
                                    <tr>
                                        
                                        <th>Order ID</th>
                                        <th>Customer Name</th>
                                        <th>Address</th>
                                        <th>Offer Discount</th>
                                        <th>Total Amount</th>
                                        <th>Final Amount</th>
                                        <th>Status</th>
                                        <th>Special Request</th>
                                        <th>Table No (Dine In)</th>
                                        <th>Payment Mode</th>
                                        <th>Order Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <!-- <tbody>
                                
                                    @foreach( $orders as $order )

                                        <tr>
                                            <td> {{ $order->order_id }}</td>
                                            <td> Sampada</td>
                                            <td> Test Address</td>
                                            <td> {{ $order->total_amount }}</td>
                                            <td> {{ $order->final_amount }}</td>
                                            <td> {{ date('d-m-Y', strtotime($order->created_at)) }}</td>
                                            <td>
                                                <a title="View Order" class="view btn operations" onclick="getOrdersView({{ $order->id }})" data-toggle="modal" data-target="#viewOrderModal-1"> <i class="fa fa-eye"></i></a>
                                                <a title="Edit Order" class="btn operations" data-toggle="modal" onclick="getOrdersEdit({{ $order->id }})" data-target="#editOrderModal-1">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a title="Delete Order" onclick="delete_order({{ $order->id }})" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a> 
                                            </td>
                                        </tr>

                                    @endforeach
                                
                                </tbody> -->
                            </table>
                        </div>
                        <!-- /.table-responsive -->

                    </div>

                </div>

                <div id="past_orders" class="tabcontent">
                    
                    <div class="box-body">

                        <div class="table-responsive mt-15">
                                
                            <table id="past_order_table" class="table table-bordered table-striped">

                                <thead>
                                    <tr>
                                        
                                        <th>Order ID</th>
                                        <th>Total Amount</th>
                                        <th>Final Amount</th>
                                        <th>Order Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <!-- <tbody>

                                    @foreach( $orders as $order )

                                    <tr>
                                        <td> {{ $order->order_id }}</td>
                                        <td> {{ $order->total_amount }}</td>
                                        <td> {{ $order->final_amount }}</td>
                                        <td> {{ date('d-m-Y', strtotime($order->created_at)) }}</td>
                                        <td>
                                            <a title="View Order" class="view btn operations" onclick="getOrdersView({{ $order->id }})" data-toggle="modal" data-target="#viewOrderModal-1"> <i class="fa fa-eye"></i></a>
                                            <a title="Edit Order" class="btn operations" data-toggle="modal" onclick="getOrdersEdit({{ $order->id }})" data-target="#editOrderModal-1">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a title="Delete Order" onclick="delete_order({{ $order->id }})" href="javascript:void()" class="btn operations"><i class="fa fa-trash-o"></i></a> 
                                        </td>
                                    </tr>

                                    @endforeach

                                </tbody> -->

                            </table>

                        </div>

                    </div>

                </div>

        
        
        

            </div>

        </section>

    </div>
</section>

<div class="modal fade" id="viewOrderModal" role="dialog" aria-labelledby="viewOrderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #000;"><span aria-hidden="true">&times;</span></button>
                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-12" style="font-size: 20px;">
                            <label class="_order_id">ORDER ID : HPL2021-0000006</label>
                        </div>
                        
                        <div class="col-sm-12 rider_data" style="font-weight: 600;text-align: end;">
                            
                        </div>

                        <div class="col-sm-12">
                            <label class="control-label" style="font-weight: bold;">Item Details:</label>
                        </div>

                        <div class="col-sm-12" id="order_items_view">
                            
                        </div>
                        <div class="col-sm-12" id="other_order_details">
                            
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editOrderModal" role="dialog" aria-labelledby="editOrderModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editOrderModalLabel">Edit Orders</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Order Form -->
          <form id="editOrder">  
            @csrf
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">Order ID</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="order_id_edit" required disabled>
                            <input type="hidden" name="order_id">
                            <input type="hidden" class="item_count_edit" name="item_count_edit">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Promocode</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="promocode_edit" required disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Amount</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="amount_edit" required disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Delivery Charges</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="delivery_charges_edit" required disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Discount</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="discount_edit" required disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Total Amount</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="total_amount_edit" required disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Final Amount</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="final_amount_edit" required disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Tax</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="tax_edit" required disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                    </div>

                    <div class="row mt-15">
                        
                        <div class="col-sm-2">
                            <label class="control-label">Order Details</label>
                        </div>
                        <div class="col-sm-10"></div>

                    </div>

                    <div class="row mt-15" id="order_items_edit">
                        
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="update_order" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                            <button class="btn btn-sm btn-warning btn-flat mr-50" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>

          </form>

        </div>
        
      </div>
    </div>
</div>

<div class="modal fade" id="getRiderOrderModal"  role="dialog" aria-labelledby="getRiderOrderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="getRiderOrderModalLabel">Assign Rider</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='assignRiderOrder'>  
                        @csrf
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Order ID</label>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" id="order_id"></label>
                            </div>
                        </div>
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Select Rider</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="hidden" name="rider_order_id" id="rider_order_id">
                                <input type="hidden" name="merchant_id" id="merchant_id">
                                <select class="form-control select2" id="riders" name="riders" required>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="assign_rider" type="submit" class="btn btn-sm btn-warning btn-flat mr-50">Assign</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateOrderStatusModal"  role="dialog" aria-labelledby="updateOrderStatusModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="updateOrderStatusModalLabel">Update Order Status</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='updateOrderStatus'>  
                        @csrf
                        <input type="hidden" class="order_update_id" name="order_update_id" >
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Order ID</label>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label set_order_id" ></label>
                            </div>
                        </div>
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Select status</label>
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control order_status_value"  name="status_update" required>
                                    <option value="" > Select Status</option>
                                    <!-- <option value="assigned">Assigned</option>  -->
                                    <option value="accepted"> Accept </option>
                                    <option value="ready"> Ready </option>
                                    <option value="pickedup"> Picked Up </option>
                                    {{-- <option value="reachedlocation"> Reached Location </option> --}}
                                    <option value="delivered"> Delivered </option>
                                    <option value="rejected">Reject</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button  type="submit" class="btn btn-sm btn-warning btn-flat mr-50">Update</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection 

@section('js')

<script src="{{ url('/') }}/jquery/scripting.js?v=2"></script>

<script>

    

    function getOrdersView(id){ 
        $.ajax({
            type: "POST",
            url: "{{ route('get_order_details')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
            },
            success: function (r) {
                console.log(r);
                create_order_view(r);                
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    

    function getOrdersEdit(id){ 
        $.ajax({
            type: "POST",
            url: "{{ route('get_order_details')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
            },
            success: function (r) {
                console.log(r);
                create_order_edit(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_order_view(data) {
        $(".js_add").remove();
        if( data ) {
            
            // $("input[name=order_id_view]").val(data.orders.order_id);
            // $("input[name=promocode_view]").val(data.orders.promocode);
            // $("input[name=amount_view]").val(data.orders.amount);
            // $("input[name=delivery_charges_view]").val(data.orders.delivery_charges);
            // $("input[name=discount_view]").val(data.orders.discount);
            // $("input[name=total_amount_view]").val(data.orders.total_amount);
            // $("input[name=tax_view]").val(data.orders.tax);
            // $("input[name=final_amount_view]").val(data.orders.final_amount);
            $("._order_id").text("ORDER ID : " + data.orders.order_id);
            if(data.rider){
                $(".rider_data").text("Rider Name : " + data.rider.first_name);    
            }else{
                $(".rider_data").text("");
            }
            
            
            let other_html = '<div class="row mt-15">\
                                    <div class="col-sm-9">Total</div><div class="col-sm-3"> ₹ '+ data.orders.amount +'</div>\
                                    <div class="col-sm-9" style="color: #00a300;">Promo / Discount</div><div class="col-sm-3" style="color: #00a300;"> ₹ '+ data.orders.discount +'</div>\
                                    <div class="col-sm-9" style="color: #F00;">Taxes & Charges</div><div class="col-sm-3" style="color: #F00;"> ₹ '+ data.orders.tax +'</div>\
                                </div>\
                                <div class="row mt-15">\
                                    <div class="col-sm-9" style="font-weight: bold;font-size: 18px;">Grand total</div><div class="col-sm-3" style="font-weight: bold;font-size: 18px;"> ₹ '+ data.orders.final_amount +'</div>\
                                    <div class="col-sm-9" style="color: #00a300;">Total Saving</div><div class="col-sm-3" style="color: #00a300;"> ₹ '+ data.orders.total_saving +'</div>\
                                </div>';

            $("#other_order_details").html(other_html);

            for( let i = 0; i < data.order_items.length; i++ ) {
                let order_items_html = '<div class="row mt-15 js_add">\
                                    <div class="col-sm-2">\
                                        <img class="img-responsive" style="height: 75px;width: 75px;" src="'+ data.order_items[i].item_img +'">\
                                    </div>\
                                    <div class="col-sm-8">\
                                        <label class="control-label" style="font-size: 20px; font-weight: 500;">'+ data.order_items[i].item_name +'</label><br/>\
                                        <label class="control-label" style="font-size: 15px;">'+ data.order_items[i].item_qty +'  X  ₹ '+ data.order_items[i].item_price +' </label><br/>\
                                    </div>\
                                    <div class="col-sm-2">\
                                        <label class="control-label"></label><br/>\
                                        <label class="control-label" style="font-size: 15px;font-weight: bold; float: right;">₹ '+ (data.order_items[i].item_qty* data.order_items[i].item_price) +' </label><br/>\
                                    </div>\
                                </div>';

                
                $("#order_items_view").append(order_items_html);
            }
        }
        $('#viewOrderModal').modal('show');
    }

    function create_order_edit(data) {
        $(".js_add").remove();
        if( data ) {
            
            $("input[name=order_id]").val(data.orders.id);
            $("input[name=order_id_edit]").val(data.orders.order_id);
            $("input[name=promocode_edit]").val(data.orders.promocode);
            $("input[name=amount_edit]").val(data.orders.amount);
            $("input[name=delivery_charges_edit]").val(data.orders.delivery_charges);
            $("input[name=discount_edit]").val(data.orders.discount);
            $("input[name=total_amount_edit]").val(data.orders.total_amount);
            $("input[name=tax_edit]").val(data.orders.tax);
            $("input[name=final_amount_edit]").val(data.orders.final_amount);

            for( let i = 0; i < data.order_items.length; i++ ) {
                let order_items_html = '<div class="row mt-15 edit_item_box js_add" style="border: 1px solid #000; padding: 10px;margin: 15px;">\
                                    <div class="col-sm-2">\
                                        // <img class="img-responsive" src="http://52.66.16.21/blog/public/images/test-5173.jpg">\
                                        <img class="img-responsive" src="{{ asset("dist/img/test-5173.jpg") }}">\
                                    </div>\
                                    <div class="col-sm-3">\
                                        <label class="control-label">Item Name</label>\
                                        <input type="text" class="form-control" name="item_name[]" value="'+ data.order_items[i].item_name +'" required>\
                                        <input type="hidden" name="item_id_edit[]" value="'+ data.order_items[i].id +'">\
                                    </div>\
                                    <div class="col-sm-3">\
                                        <label class="control-label">Item ID</label>\
                                        <input type="text" class="form-control" name="item_id[]" value="'+ data.order_items[i].item_id +'" required>\
                                    </div>\
                                    <div class="col-sm-3">\
                                        <label class="control-label">Item Code</label>\
                                        <input type="text" class="form-control" name="item_code[]" value="'+ data.order_items[i].item_code +'" required>\
                                    </div>\
                                    <div class="col-sm-3">\
                                        <label class="control-label">Item Quantity</label>\
                                        <input type="text" class="form-control" name="item_qty[]" value="'+ data.order_items[i].item_qty +'" required>\
                                    </div>\
                                    <div class="col-sm-3">\
                                        <label class="control-label">Item Price</label>\
                                        <input type="text" class="form-control" name="item_price[]" value="'+ data.order_items[i].item_price +'" required>\
                                    </div>\
                                </div>';
                $("#order_items_edit").append(order_items_html);
            }
        }
        $('#editOrderModal').modal('show');
    }

    function delete_order( order_id ) {

        if( confirm('Are sure to delete the record?') ) {
            
            $.ajax({
                type: "POST",
                url: "{{ route('delete_order')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "order_id": order_id
                },
                success: function (r) {
                        
                        $.notify("Order Deleted Successfully", "success");
                        $('#order_table').DataTable().ajax.reload();
                        $('#past_order_table').DataTable().ajax.reload();
                },
                error:function(e){
                    console.log(e);
                }
            });

        }

    }

    function getRiderOrderModal(id, merchant_id, order_id, rider_id) {
        // console.log('done');
        $("#riders").empty();
        $("#getRiderOrderModal").find("#order_id").text(order_id);
        $("#getRiderOrderModal").find("#rider_order_id").val(id);

        $.ajax({
            type: "GET",
            url: "{{ route('get_all_riders')}}",
            data: {
                'merchant_id': merchant_id
            },
            success: function (r) {
                console.log(r);
                $.each( r, function(index, value) {
                    $("#riders").append($('<option>', {value: value.id, text: ( value.first_name + " " + value.last_name ) }));
                });

                $("#getRiderOrderModal").find("#riders").val( JSON.parse( rider_id ) );
                $("#getRiderOrderModal").find("#riders").trigger('change');
                $('#getRiderOrderModal').modal('show');
            },
            error:function(e){
                console.log(e);
            }
        });

        
    }

    //$(document).ready(function(){

        $('#order_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                        "url": "{{ url('get_current_order_list') }}",
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
            "columns": [
                { "data": "order_id" },
                { "data": "customer_name" },
                { "data": "customer_address" },
                { "data": "offer_discount" },
                { "data": "total_amount" },
                { "data": "final_amount" },
                { "data": "status" },
                { "data": "special_instruction" },
                { "data": "table_no" },
                { "data": "payment_mode" },
                { "data": "created_at" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }],
        });

        $("#past_order_table").DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                        "url": "{{ url('get_past_order_list') }}",
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
            "columns": [
                { "data": "order_id" },
                { "data": "total_amount" },
                { "data": "final_amount" },
                { "data": "created_at" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }],
        });

        $("#editOrder").validate({
            submitHandler: function(form) {
                    let formdata = new FormData($('#editOrder')[0]);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        url: "{{ route('edit_order')}}",
                        data: formdata,
                        success: function (r) {
                                $("#editOrderModal").modal('hide');
                                $.notify("Order Updated Successfully", "success");
                                $('#order_table').DataTable().ajax.reload();
                                $('#past_order_table').DataTable().ajax.reload();
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                },
                errorElement: 'span',
                errorClass: 'help-block error',
                rules:
                {
                    order_id_edit: {
                        required: true
                    }
                },
                messages:
                {	

                },
                ignore: []
        });

        $("#editOrder").click(function(){
            $('.item_count_edit').val($(".edit_item_box").length);
        });

        $(".dataTables_filter input").css({ "width": "250px" });
        $(".dataTable").css({ "width": "100%" });

        $(".select2").select2({
            placeholder: 'Select Rider',
            allowClear: true
        });

        // Assign Rider to merchant
        $( "#assignRiderOrder" ).validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#assignRiderOrder')[0]);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('assign_rider_order')}}",
                    data: formdata,
                    success: function (r) {
                            console.log(r);
                            $("#getRiderOrderModal").modal("hide");
                            Swal.fire(
                                'Success!',
                                'Order Assigned.',
                                'success'
                            );
                            $('#order_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            );
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            rules:
            {},
            messages:
            {},
            ignore: []
        });

        $( "#updateOrderStatus" ).validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#updateOrderStatus')[0]);
                $("#updateOrderStatusModal").modal("hide");
                change_order_status(formdata);
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            rules:
            {},
            messages:
            {},
            ignore: []
        });

        

        

    //});


    function tabSwitcher(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    function update_order(id){
        
        $('.order_update_id').val(id);
        $('.set_order_id').text($('.tblord'+id).data('orderid'));
        $('.order_status_value').val($('.tblord'+id).data('status'));
        $('#updateOrderStatusModal').modal('show');
    }

    $("#riders").on("select2:close", function (e) {  
        $(this).valid(); 
    });

    

    


</script>

@endsection
