@extends('layouts.applist')

@section('content')
@include('layouts.header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Incentive Earned List</h4>
                    </div>
                    <div class="col-md-6 text-right">
                       {{--  <a class="btn btn-success" data-toggle="modal" data-target="#addMerchantModal"><i class="fa fa-plus"></i> Add Merchant</a> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadEmployeeModal"><i class="fa fa-plus"></i> Bulk Upload Employee</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="content-header" style="min-height: unset;">
        <div class="box ">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">From Date:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">To Date:</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-sm btn-success btn-flat mr-50" href="#">Search</a>
                        <a class="btn btn-sm btn-danger btn-flat mr-50" href="#">Reset</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Manage Incentive Earned</h3>
                </div>
                
                <div class="box-body">

                    <div class="table-responsive mt-15">

                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    {{-- <th></th> --}}
                                    <th>Order</th>
                                    <th>Payment Amount</th>
                                    <th>Incentive Earned</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php for($i = 0; $i <= 15; $i++) { ?>    
                                <tr>
                                    {{-- <td><input type="checkbox" name="select_merchant" id="select_merchant"></td> --}}
                                    <td>Order <?php echo ($i + 1); ?></td>
                                    <td>750/-</td>
                                    {{-- <td>123 Test Street</td> --}}
                                    <td><span class="btn btn-danger btn-flat btn-xs">Not Paid<span></span></span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    {{-- <div class="row mt-15">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button>
                        </div>
                    </div> --}}

                </div>

            </div>

        </section>
    </div>
</section>


@endsection 


@section('js')

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
  $(function () {
    $("#item_table").DataTable();
  });

  $(document).ready(function(){

        $("#add_outlet").click(function(e){

            // console.log('working');
            
            $(".outlet_details_box:first").clone().addClass("mt-15").appendTo("#outlet_details");

            e.preventDefault();

        });

        $("#edit_add_outlet").click(function(e){

            // console.log('working');
            
            $(".edit_outlet_details_box:first").clone().addClass("mt-15").appendTo("#edit_outlet_details");

            e.preventDefault();

        });

        $("#cancle_merchant").on('click', function(){

            window.history.back();

        });

    });

</script>

@endsection
