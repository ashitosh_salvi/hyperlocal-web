@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Rider Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_rider btn-flat mr-15" data-toggle="modal" data-target="#addRiderModal"><i class="fa fa-plus"></i> Add Rider</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div class="table-responsive mt-15">
                            
                        <table id="rider_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    {{-- <th>State #</th>
                                    <th>City</th> --}}
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="addRiderModal"  role="dialog" aria-labelledby="addRiderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="addRiderModalLabel">Add Rider</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='createRider'>  
                        @csrf
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">First Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control first_name" name="first_name" value="{{ old('first_name') }}" required>
                                @error('first_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Last Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                                @error('last_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                        	<div class="col-sm-2">
                                <label class="control-label">Email *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Contact *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required> 
                                @error('mobile')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Password *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}" required>
                                @error('password')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Residential Address</label>
                            </div>
                            <div class="col-sm-5">
                                <textarea name="address" class="form-control" rows="3">{{ old('address') }}</textarea>
                                @error('address')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Country</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="country" value="INDIA">
                                @error('country')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">State</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="state" name="state" value="{{ old('state') }}">
                                    <option value="">Select State</option>
                                </select>
                                @error('state')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">City</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="city" name="city" value="{{ old('city') }}" >
                                    <option value="">Select City</option>
                                </select>
                                @error('city')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Pin Code</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="pin_code" name="pin_code" value="{{ old('pin_code') }}">
                                @error('pin_code')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Rider Photo</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form- rider_photo" name="image_url">
                                @error('image_url')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                                <div class="gallery"></div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Address Proof</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="file" name="address_proof">
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Photo ID</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form-" name="photo_id_image_url">
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Driving Licence</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="file" name="other_image_url">
                            </div>

                        </div>

                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="create_rider" type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 

                    
                    </form>
               
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="viewRiderModal"  role="dialog" aria-labelledby="viewRiderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="viewRiderModalLabel">Add Rider</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form>  
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">First Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="first_name_view"  disabled>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Last Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="last_name_view"  disabled>
                            </div>
                        </div>

                        <div class="row mt-15">
                        	<div class="col-sm-2">
                                <label class="control-label">Email</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="email" class="form-control" name="email_view"  disabled>
                            </div>
                            
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Contact</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="mobile_view"  disabled>
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Password</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="password_view"  disabled>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Residential Address</label>
                            </div>
                            <div class="col-sm-5">
                                <textarea name="address_view" class="form-control" rows="3" disabled></textarea>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Country</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="country_view" value="INDIA" disabled>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">State</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="state" name="state_view" disabled>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">City</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="city" name="city_view" disabled>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label" disabled>Pin Code</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="pin_code" name="pin_code_view" disabled>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Rider Photo</label>
                                
                            </div>
                            <div class="col-sm-3">
                                <div id="rider_photo_view"></div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Address Prof</label>
                            </div>
                            <div class="col-sm-3" style="margin-top:-12px">
                                <a href="#" download class="image_address_url"><div style="color: red"> <i class="fa fa-file mt-15" style="margin-right: 8px"></i>Uploaded Document</div></a>
                            </div>
                        </div>
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Photo ID</label>
                            </div>
                            <div class="col-sm-3" style="margin-top:-12px">
                                <a href="#" download class="photo_id_image_url"><div style="color: red"> <i class="fa fa-file mt-15" style="margin-right: 8px"></i>Uploaded Document</div></a>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Driving Licence</label>
                            </div>
                            <div class="col-sm-3" style="margin-top:-12px">
                                <a href="#" download class="other_image_url"><div style="color: red"> <i class="fa fa-file mt-15" style="margin-right: 8px"></i>Uploaded Document</div></a>
                            </div>
                        </div>


                    </form>
               
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="editRiderModal"  role="dialog" aria-labelledby="editRiderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="editRiderModalLabel">Add Rider</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='editRider'>  
                        @csrf
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">First Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="hidden" class="form-control" name="rider_id_edit" required>
                                <input type="text" class="form-control" name="first_name_edit" required>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Last Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="last_name_edit" required>
                            </div>
                        </div>

                        <div class="row mt-15">
                        	<div class="col-sm-2">
                                <label class="control-label">Email</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="email" class="form-control" name="email_edit" required>
                            </div>
                            
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Contact</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="mobile_edit" required>
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Password</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="password_edit" disabled required>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Residential Address</label>
                            </div>
                            <div class="col-sm-5">
                                <textarea name="address_edit" class="form-control" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Country</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="country_edit" value="INDIA">
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">State</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="state_edit" name="state_edit">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">City</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="city_edit" name="city_edit">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Pin Code</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="pin_code_edit" name="pin_code_edit">
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Rider Photo</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form-controls image_url_edit" name="image_url_edit">
                                <div id="rider_photo_edit"></div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Address Prof</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="" name="image_address_url_edit">
                                <a href="#" download class="image_address_url"><div style="color: red"> <i class="fa fa-file mt-15" style="margin-right: 8px"></i>Uploaded Document</div></a>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Photo ID</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form-" name="photo_id_image_url_edit">
                                <a href="#" download class="photo_id_image_url"><div style="color: red"> <i class="fa fa-file mt-15" style="margin-right: 8px"></i>Uploaded Document</div></a>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Driving Licence</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" name="other_image_url_edit">
                                <a href="#" download class="other_image_url"><div style="color: red"> <i class="fa fa-file mt-15" style="margin-right: 8px"></i>Uploaded Document</div></a>
                            </div>

                        </div>

                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="update_rider" type="submit" class="btn btn-sm btn-warning btn-flat mr-50">Update</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 

                    
                    </form>
               
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="getRiderMerchantsModal"  role="dialog" aria-labelledby="getRiderMerchantsModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="getRiderMerchantsModalLabel">Assign Merchant</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='assignRiderMerchant'>  
                        @csrf
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Rider Name</label>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" id="rider_name"></label>
                            </div>
                        </div>
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Select Merchant</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="hidden" name="rider_id" id="rider_id">
                                <select class="form-control select2" id="merchant" name="merchant[]" multiple>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="assign_rider" type="submit" class="btn btn-sm btn-warning btn-flat mr-50">Assign</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>    

@endsection

@section('js')

<script>
$(function() {    
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    let img_html = '<div class="img_wrapper">'+
                            '<img src="'+ event.target.result +'" alt="Rider Pic">'+
                            // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                        '</div>';
                    $(placeToInsertImagePreview).empty().append(img_html);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('.rider_photo').on('change', function() {
        // $('div.gallery').empty();
        imagesPreview(this, 'div.gallery');
    });
    $('.image_url_edit').on('change', function() {
        // $('div#rider_photo_edit').empty();
        imagesPreview(this, 'div#rider_photo_edit');
    });
});
function getRider(id){
    
    $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('get_rider')}}",
                data: "id="+id,
                success: function (r) {
                    console.log(r);
                    create_edit(r);
                },
                error:function(e){
                    console.log(e);
                }
            });
}

function getRiderView(id){
    
    $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('get_rider')}}",
                data: "id="+id,
                success: function (r) {
                    console.log(r);
                    create_view(r);
                },
                error:function(e){
                    console.log(e);
                }
            });
}

function create_view(data) { 

    if(data){

        $("input[name=first_name_view]").val(data.rider.first_name);
        $("input[name=last_name_view]").val(data.rider.last_name);
        $("textarea[name=address_view]").val(data.rider.address);
        $("input[name=password_view]").val(data.rider.password);
        $("input[name=state_view]").val(data.rider.state);
        $("input[name=city_view]").val(data.rider.city);
        $("input[name=country_view]").val(data.rider.country);
        $("input[name=pin_code_view]").val(data.rider.pin_code);
        $("input[name=email_view]").val(data.rider.email);
        $("input[name=mobile_view]").val(data.rider.mobile);

        if(data.rider.address_proof_image_url){
            $(".image_address_url").attr('href',"{{URL::asset('/images')}}/"+data.rider.address_proof_image_url).attr('download','download');
        }else{
            $(".image_address_url").attr('href',"#").removeAttr('download');
        }

        if(data.rider.other_image_url){
            $(".other_image_url").attr('download','download').attr('href',"{{URL::asset('/images')}}/"+data.rider.other_image_url);
        }else{
            $(".other_image_url").attr('href',"#").removeAttr('download');
        }

        if(data.rider.photo_id_image_url){
            $(".photo_id_image_url").attr('download','download').attr('href',"{{URL::asset('/images')}}/"+data.rider.photo_id_image_url);
        }else{
            $(".photo_id_image_url").attr('href',"#").removeAttr('download');
        }

        if(data.rider.image_url){
            let rider_img = "{{URL::asset('/images')}}/" + data.rider.image_url;
            let img_html = '<div class="img_wrapper">'+
                                '<img src="'+ rider_img +'"  title="' + data.rider.image_url + '">'+
                                // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
            $("#rider_photo_view").html(img_html);
        }
    }
    
    $('#viewRiderModal').modal('show');

}

function create_edit(data) { 

    if(data){

        $("input[name=rider_id_edit]").val(data.rider.id);
        $("input[name=first_name_edit]").val(data.rider.first_name);
        $("input[name=last_name_edit]").val(data.rider.last_name);
        $("textarea[name=address_edit]").val(data.rider.address);
        $("input[name=password_edit]").val(data.rider.password);
        
        $("input[name=country_edit]").val(data.rider.country);
        $("input[name=pin_code_edit]").val(data.rider.pin_code);
        $("input[name=email_edit]").val(data.rider.email);
        $("input[name=mobile_edit]").val(data.rider.mobile);
        $("input[name=image_address_url_edit]").val('');
        if(data.rider.address_proof_image_url){
            $(".image_address_url").attr('href',"{{URL::asset('/images')}}/"+data.rider.address_proof_image_url);
            $('.image_address_url').attr('download','download')
        }else{
            $(".image_address_url").attr('href',"#");
            $('.image_address_url').removeAttr('download')
        }

        if(data.rider.other_image_url){
            $(".other_image_url").attr('download','download').attr('href',"{{URL::asset('/images')}}/"+data.rider.other_image_url);
        }else{
            $(".other_image_url").attr('href',"#").removeAttr('download');
        }

        if(data.rider.photo_id_image_url){
            $(".photo_id_image_url").attr('download','download').attr('href',"{{URL::asset('/images')}}/"+data.rider.photo_id_image_url);
        }else{
            $(".photo_id_image_url").attr('href',"#").removeAttr('download');
        } 

        if(data.rider.image_url){
            let rider_img = "{{URL::asset('/images')}}/" + data.rider.image_url;
            let img_html = '<div class="img_wrapper">'+
                            '<img src="'+ rider_img +'"  title="' + data.rider.image_url + '">'+
                            //'<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                        '</div>';
            $("#rider_photo_edit").html(img_html);
        }

        $.ajax({
            type: "GET",
            url: "{{ route('get_state_by_name')}}",
            data: {
                "state_name": data.rider.state
            },
            success: function (r) {
                console.log(r.state.length);
                if ( r.state.length > 0 ) {

                    let state_id = r.state[0].state_name;
                    // console.log( typeof( state_id ) + " : " + state_id );
                    $("#state_edit").val( state_id );
                    $('#city_edit').empty();

                    // Get All Cities for selected state
                    $.ajax({
                        type: "GET",
                        url: "{{ route('get_all_cities_by_state_id')}}",
                        data: {
                            "state_id": state_id
                        },
                        success: function (r) {
                            // console.log(r);
                            let cities = r.cities;
                            $('#city_edit').append($('<option>', {value: '', text: 'Select City'}));
                            $.each( cities, function(index, value) {
                                // console.log( index + ' : ' + value.state_name );
                                $('#city_edit').append($('<option>', {value: value.city_name, text: value.city_name}));
                            });

                            // Get Selected city for rider
                            $.ajax({
                                type: "GET",
                                url: "{{ route('get_city_by_name')}}",
                                data: {
                                    "city_name": data.rider.city
                                },
                                success: function (r) {
                                    let city_id = r.city[0].city_name;
                                    $("#city_edit").val( city_id );
                                },
                                error:function(e){
                                    console.log(e);
                                }
                            });
                                
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                }
            },
            error:function(e){
                console.log(e);
            }
        });

        $("#state_edit").on('change', function(){
        
            let state = $(this).val();

            $('#city_edit').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_all_cities_by_state_id')}}",
                data: {
                    "state_id": state
                },
                success: function (r) {
                    // console.log(r);
                    let cities = r.cities;
                    $('#city_edit').append($('<option>', {value: '', text: 'Select City'}));
                    $.each( cities, function(index, value) {
                        // console.log( index + ' : ' + value.state_name );
                        $('#city_edit').append($('<option>', {value: value.city_name, text: value.city_name}));
                    });
                },
                error:function(e){
                    console.log(e);
                }
            });

        });

    }

    $('#editRiderModal').modal('show');

}

function getRiderMerchants(id, name) {
    $("#merchant").empty();
    $("#getRiderMerchantsModal").find("#rider_id").val(id);
    $("#getRiderMerchantsModal").find("#rider_name").text(name);

    $.ajax({
        type: "GET",
        url: "{{ route('get_all_merchants')}}",
        success: function (r) {
            console.log(r);
            // $('#merchant').append($('<option>', {value: '', text: 'Select Merchant'}));
            $.each( r, function(index, value) {
                $("#merchant").append($('<option>', {value: value.id, text: value.merchant_name}));
            });

            let merchant_ids = $("#merchant_list"+id).attr('data-array');
            if ( merchant_ids != "" ) {
                console.log( JSON.parse( merchant_ids ) );
                $("#getRiderMerchantsModal").find(".select2").val( JSON.parse( merchant_ids ) );
                $("#getRiderMerchantsModal").find(".select2").trigger('change');
            }
            
        },
        error:function(e){
            console.log(e);
        }
    });

    $("#getRiderMerchantsModal").modal('show');
}

$(window).load(function(){

    // Assign Rider to merchant
    $( "#assignRiderMerchant" ).validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#assignRiderMerchant')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('assign_rider_merchant')}}",
                data: formdata,
                success: function (r) {
                        console.log(r);
                        $("#getRiderMerchantsModal").modal("hide");
                        Swal.fire(
                            'Success!',
                            'Merchant Mapped Successfully.',
                            'success'
                        );
                        $('#rider_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        );
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {},
        messages:
        {},
        ignore: []
    });
            
    // Update Rider Status

    
    $(document).on('click','input[name="rider_status"]',function(){
        
        let rider_status = $(this).attr("data-attr");
        let message = '';
        let rider_id = $(this).attr("rider-id");

        if ( rider_status == 0 ) {
            rider_status = 1;
            $(this).attr("data-attr", "1");
            message = 'Activated';
        } else {
            rider_status = 0;
            $(this).attr("data-attr", "0");
            message = 'Deactivated';
        }

        $.ajax({
            type: "POST",
            url: "{{ route('update_rider_status')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "rider_id": rider_id,
                "rider_status": rider_status
            },
            success: function (r) {
                    
                $.notify("Rider "+ message +" Successfully", "success");
                    
            },
            error:function(e){
                Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                console.log(e);
            }
        });

    });

});

$(document).ready(function(){

    $('#rider_table').DataTable({
        "language": {
            "emptyTable": "No Record Found"
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
                    "url": "{{ url('get_rider_list') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"}
                },
        "columns": [
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "email" },
            { "data": "mobile" },
            // { "data": "state" },
            // { "data": "city" },
            { "data": "status" },
            { "data": "action" }
        ],
        "aoColumnDefs": [
        {
            bSortable: false,
            aTargets: [ -1 ]
        }],
    });
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 200KB');
    $.validator.addMethod('maxupload', function (value, element, param) {
        var length = ( element.files.length );
        return this.optional( element ) || length <= param;
    }, 'You can add only 5 images');
    $.validator.addMethod(
        "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
    );

        
    $( "#createRider" ).validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#createRider')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('create_rider')}}",
                data: formdata,
                success: function (r) {
                        console.log(r);
                        $("#addRiderModal").modal("hide");
                        Swal.fire(
                                'Success!',
                                'Rider Added Successfully.',
                                'success'
                            );
                        // $.notify("Rider Added Successfully", "success");
                        $('#rider_table').DataTable().ajax.reload();
                        // window.location.reload();
                },
                error:function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR.responseJSON.errors);
                    let err = jqXHR.responseJSON.errors;
                    if(err.mobile){
                        Swal.fire('Error!','The mobile has already been taken.','error')
                    }
                    if(err.email){
                        Swal.fire('Error!','The email has already been taken.','error')
                    }
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
            first_name: {
                required:true,
                regex: /^[a-z]+$/i
            },
            last_name: {
                required:true,
                regex: /^[a-z]+$/i
            },
            mobile: {
                required:true,
                regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
            },
            image_url: {
                extension: 'jpg|jpeg|png|gif',
                filesize : 200000,
                // maxupload: 5,
            }
        },
        messages:
        {	
            first_name: {
                regex: 'Enter only alphabets'
            },
            last_name: {
                regex: 'Enter only alphabets'
            },
            mobile: {
                regex: 'Please add valid mobile number'
            },
            image_url: {
                extension: 'Please select valid image type'
            }
        },
        ignore: []

    });

    $( "#editRider" ).validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editRider')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('edit_rider')}}",
                data: formdata,
                success: function (r) {
                        console.log(r);
                        $("#editRiderModal").modal("hide");
                        Swal.fire(
                                'Success!',
                                'Rider Updated Successfully.',
                                'success'
                            );
                        // $.notify("Rider Edited Successfully", "success");
                        $('#rider_table').DataTable().ajax.reload();
                        // window.location.reload();
                },
                error:function(jqXHR, textStatus, errorThrown){
                    let err = jqXHR.responseJSON.errors;
                    if(err.mobile_edit){
                        Swal.fire('Error!','The mobile has already been taken.','error')
                    }
                    if(err.email_edit){
                        Swal.fire('Error!','The email has already been taken.','error')
                    }
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
            first_name_edit: {
                required:true,
                regex: /^[a-z]+$/i
            },
            last_name_edit: {
                required:true,
                regex: /^[a-z]+$/i
            },
            mobile_edit: {
                required:true,
                regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
            },
            image_url_edit: {
                extension: 'jpg|jpeg|png|gif',
                filesize : 200000,
                // maxupload: 5,
            }
        },
        messages:
        {	
            first_name_edit: {
                regex: 'Enter only alphabets'
            },
            last_name_edit: {
                regex: 'Enter only alphabets'
            },
            mobile_edit: {
                regex: 'Please add valid mobile number'
            },
            image_url_edit: {
                extension: 'Please select valid image type'
            }
        },
        ignore: []

    });

    $('.add_rider').on('click',function(){
        $('#createRider').trigger("reset");
        $("input[name=country]").val('INDIA');
        $("#createRider").find('span.help-block').remove();
        $("#createRider").find('.help-block').removeClass("help-block");
        $("#createRider").find('.error').removeClass("error");
        $("#createRider").find('.has-success').removeClass("has-success");
        $('#createRider').find('.form-control-feedback').remove();
    });

    $.ajax({
        type: "GET",
        url: "{{ route('get_all_states')}}",
        success: function (r) {
            // console.log(r);
            let states = r.states;
            $.each( states, function(index, value) {
                // console.log( index + ' : ' + value.state_name );
                $('#state').append($('<option>', {value: value.state_name, text: value.state_name}));
                $('#state_edit').append($('<option>', {value: value.state_name, text: value.state_name}));
            });
        },
        error:function(e){
            console.log(e);
        }
    });

    $("#state").on('change', function(){
            
        let state = $(this).val();

        $('#city').empty();
        
        $.ajax({
            type: "GET",
            url: "{{ route('get_all_cities_by_state_id')}}",
            data: {
                "state_id": state
            },
            success: function (r) {
                // console.log(r);
                let cities = r.cities;
                $('#city').append($('<option>', {value: '', text: 'Select City'}));
                $.each( cities, function(index, value) {
                    // console.log( index + ' : ' + value.state_name );
                    $('#city').append($('<option>', {value: value.city_name, text: value.city_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });

    });

    $(".dataTables_filter input").css({ "width": "250px" });
    $(".dataTable").css({ "width": "100%" });

    // 
    $(".select2").select2({
        placeholder: 'Select Merchant',
        allowClear: true
    });

});

$('#addRiderModal').on('show.bs.modal', function () {
    $(".first_name").focus();
    $('.img_wrapper').remove();   
});
$('#addRiderModal').on('shown.bs.modal', function () {
    $('.first_name').focus();
})  

</script>

@endsection
