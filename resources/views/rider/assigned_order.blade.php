@extends('layouts.applist')

@section('content')
@include('layouts.header')
<style type="text/css">
    .form-group{
        display: flow-root;
    }
</style>


<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-9">
              <div class="box box-body">
                <div class="col-md-6">
                  <h4><i class="fa fa-pencil"></i> &nbsp; New Assigned Orders</h4>
                </div>
                <div class="col-md-6 text-right">
                  {{-- <a href="<?= base_url('admin/profile/change_pwd'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Change Password</a> --}}
                </div>
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
              <div class="box">
                <div class="box-header with-border">
                    <h4 for="username" class="col-sm-3 control-label">Order #1</h4>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                      <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">Order Id</label>

                        <div class="col-sm-8">
                          <input type="text" name="username" value="" class="form-control" id="username" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstname" class="col-sm-3 control-label">OTP</label>

                        <div class="col-sm-8">
                          <input type="text" name="firstname" value="" class="form-control" id="firstname" placeholder="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">Address</label>

                        <div class="col-sm-8">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3773.898665741199!2d72.83487231466222!3d18.935878987168444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7d1da3991bb6f%3A0x324034dff24e881!2sSouth%20Indian%20Bank!5e0!3m2!1sen!2sin!4v1594193475531!5m2!1sen!2sin" width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                          <input type="text" name="lastname" value="" class="form-control" id="lastname" placeholder="">
                          <p>(Distance in Km for e.g. 2 KM away from restaurant)</p>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="mobile_no" class="col-sm-3 control-label">Order No</label>

                        <div class="col-sm-8">
                          <input type="number" name="mobile_no" value="" class="form-control" id="mobile_no" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="mobile_no" class="col-sm-3 control-label">Time to deliver</label>

                        <div class="col-sm-8">
                          <input type="number" name="mobile_no" value="" class="form-control" id="mobile_no" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        
                        <div class="col-sm-11">
                            <button id="create_merchant" class="btn btn-sm btn-success btn-flat mr-50 pull-right">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50 pull-right" data-dismiss="modal">Cancle</button>
                        </div>
                      </div>
                  </div>
                  <!-- /.box-body -->
              </div>
            </div>
            <div class="col-md-9">
              <div class="box">
                <div class="box-header with-border">
                    <h4 for="username" class="col-sm-3 control-label">Order #2</h4>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                      <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">Order Id</label>

                        <div class="col-sm-8">
                          <input type="text" name="username" value="" class="form-control" id="username" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstname" class="col-sm-3 control-label">OTP</label>

                        <div class="col-sm-8">
                          <input type="text" name="firstname" value="" class="form-control" id="firstname" placeholder="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="lastname" class="col-sm-3 control-label">Address</label>

                        <div class="col-sm-8">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3773.898665741199!2d72.83487231466222!3d18.935878987168444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7d1da3991bb6f%3A0x324034dff24e881!2sSouth%20Indian%20Bank!5e0!3m2!1sen!2sin!4v1594193475531!5m2!1sen!2sin" width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                          <input type="text" name="lastname" value="" class="form-control" id="lastname" placeholder="">
                          <p>(Distance in Km for e.g. 2 KM away from restaurant)</p>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="mobile_no" class="col-sm-3 control-label">Order No</label>

                        <div class="col-sm-8">
                          <input type="number" name="mobile_no" value="" class="form-control" id="mobile_no" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="mobile_no" class="col-sm-3 control-label">Time to deliver</label>

                        <div class="col-sm-8">
                          <input type="number" name="mobile_no" value="" class="form-control" id="mobile_no" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        
                        <div class="col-sm-11">
                            <button id="create_merchant" class="btn btn-sm btn-success btn-flat mr-50 pull-right">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50 pull-right" data-dismiss="modal">Cancle</button>
                        </div>
                      </div>
                  </div>
                  <!-- /.box-body -->
              </div>
            </div>
          </div>  

        </section> 
    </div>
</section>

@endsection 


@section('js')

<script>
    

</script>

@endsection


