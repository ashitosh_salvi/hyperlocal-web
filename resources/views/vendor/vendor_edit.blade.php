@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Edit Vendor</h3>
                </div>
                
                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">User Code</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Password</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Emp Code</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Store Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">GST #</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Shop License</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Outlet Image</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="file" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                            
                        <div class="col-sm-1"></div>

                        <div class="col-sm-10" >

                            <div class="row mt-15">
                                <div class="col-sm-12">
                                    <label class="control-label">Document for PAN Card, Shop License, GST#</label>
                                </div>
                                <div class="col-sm-10">
                                    <label for="files">Upload DOC here</label>
                                    <input type="file" id="files" class="form-control">
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-1"></div>

                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Vendor Address</label>
                        </div>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">State</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Country</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Pin Code</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Contact #</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Point of Contact</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Vendor Type</label>
                        </div>
                        <div class="col-sm-2">
                            <select id="vendor_type" class="form-control">
                                <option>Select from below options</option>
                                <option value="food">Food</option>
                                <option value="liquor">Liquor</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-3">
                            <input style="display: none;" id="other_option" type="text" class="form-control" placeholder="Please mention other vendor type">
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="update_vendor" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                            <button id="cancle_vendor" class="btn btn-sm btn-warning btn-flat mr-50">Cancle</button>
                            <button id="delete_vendor" onclick="return confirm('are you sure to delete?')" class="btn btn-sm btn-danger btn-flat mr-50">Delete</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

            </div>

        </section>

    </div>

</section>

@endsection

@section('js')

<script>
    
    $(document).ready(function(){

        console.log('working');

        $("#vendor_type").on('change', function(){

            // console.log( $(this).val() );
            let selected_option = $(this).val();

            if ( selected_option == "other" ) {
                $("#other_option").show();
            } else {
                $("#other_option").hide();
            }

        });

        $("#cancle_vendor").on('click', function(){

            window.history.back();

        });

    });

</script>

@endsection