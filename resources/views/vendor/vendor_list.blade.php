@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <!-- <h4><i class="fa fa-list"></i> &nbsp; Vendor Management</h4> -->
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#addVendorModal"><i class="fa fa-plus"></i> Add Vendor</a>
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadEmployeeModal"><i class="fa fa-plus"></i> Bulk Upload Employee</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Manage Vendor</h3>
                    <!-- <a href="{{ url('/vendor_create') }}" class="btn btn-success text-right" style="float: right;margin-right: 5px;"><i class="fa fa-plus"></i> Add Vendor</a> -->
                </div>
                
                <div class="box-body">

                    <div class="table-responsive mt-15">

                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Vendor Name</th>
                                    <th>Address</th>
                                    <th>Contact #</th>
                                    <th>Type</th>
                                    <th>GST #</th>
                                    <th>SPOC</th>
                                    <th>Creator</th>
                                    <th>Create Dt.</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php for($i = 0; $i <= 100; $i++) { ?>    
                                <tr>
                                    <td><input type="checkbox" name="select_merchant" id="select_merchant"></td>
                                    <td>Test Vendor</td>
                                    <td>123 Test Street</td>
                                    <td>1234567890</td>
                                    <td>Food</td>
                                    <td>AHFHD765467SG</td>
                                    <td>TEST SPOC</td>
                                    <td>Admin</td>
                                    <td>2nd, May 2020</td>
                                    <td>
                                        <a title="View Vendor" class="view btn btn-sm btn-info" data-toggle="modal" data-target="#viewVendorModal"> <i class="fa fa-eye"></i></a>
                                        <a title="Edit Vendor" class="btn btn-warning btn-sm mr5" data-toggle="modal" data-target="#editVendorModal">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a title="Delete Vendor" href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a> 
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button>
                        </div>
                    </div>

                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="viewVendorModal" tabindex="-1" role="dialog" aria-labelledby="viewVendorModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewVendorModalLabel">View Vendor</h4>
        </div>
        <div class="modal-body">

        <!-- View Vendor Form -->
        <form>  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">User Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Password</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Emp Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">First Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-2 mt-15">
                        <label class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3 mt-15">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Store Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">GST #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Shop License</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Image</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="file" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                        
                    <div class="col-sm-10" >

                        <div class="row mt-15">
                            <div class="col-sm-12">
                                <label class="control-label">Document for PAN Card, Shop License, GST#</label>
                            </div>
                            <div class="col-sm-10">
                                <label for="files">Upload DOC here</label>
                                <input type="file" id="files" class="form-control" disabled>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Vendor Address</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" disabled></textarea>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">State</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Country</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Pin Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Contact #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Point of Contact</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Vendor Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" disabled>
                            <option>Select from below options</option>
                            <option value="food">Food</option>
                            <option value="liquor">Liquor</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3">
                        <input style="display: none;" type="text" class="form-control" placeholder="Please mention other vendor type" disabled>
                    </div>
                </div>

                <!-- <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button id="create_vendor" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button id="cancle_vendor" class="btn btn-sm btn-danger btn-flat mr-50">Cancle</button>
                    </div>
                </div> -->

                <div class="row mt-15">
                    
                </div>

            </div>
        </form>

        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="addVendorModal" tabindex="-1" role="dialog" aria-labelledby="addVendorModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addVendorModalLabel">Add Vendor</h4>
        </div>
        <div class="modal-body">

        <!-- Add Vendor Form -->
        <form>  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">User Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Password</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Emp Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">First Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-2 mt-15">
                        <label class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3 mt-15">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Store Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">GST #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Shop License</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Image</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="file" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                        
                    <div class="col-sm-10" >

                        <div class="row mt-15">
                            <div class="col-sm-12">
                                <label class="control-label">Document for PAN Card, Shop License, GST#</label>
                            </div>
                            <div class="col-sm-10">
                                <label for="files">Upload DOC here</label>
                                <input type="file" id="files" class="form-control">
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Vendor Address</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">State</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Country</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Pin Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Contact #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Point of Contact</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Vendor Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select id="vendor_type" class="form-control">
                            <option>Select from below options</option>
                            <option value="food">Food</option>
                            <option value="liquor">Liquor</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3">
                        <input style="display: none;" id="other_option" type="text" class="form-control" placeholder="Please mention other vendor type">
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button id="create_vendor" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancle</button>
                    </div>
                </div>

                <div class="row mt-15">
                    
                </div>

            </div>
        </form>

        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editVendorModal" tabindex="-1" role="dialog" aria-labelledby="editVendorModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editVendorModalLabel">Edit Vendor</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Vendor Form -->
        <form>  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">User Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Password</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Emp Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">First Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-2 mt-15">
                        <label class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3 mt-15">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Store Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">GST #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Shop License</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Image</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="file" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                        
                    <div class="col-sm-10" >

                        <div class="row mt-15">
                            <div class="col-sm-12">
                                <label class="control-label">Document for PAN Card, Shop License, GST#</label>
                            </div>
                            <div class="col-sm-10">
                                <label for="files">Upload DOC here</label>
                                <input type="file" id="files" class="form-control">
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Vendor Address</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">State</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Country</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Pin Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Contact #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Point of Contact</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Vendor Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select id="edit_vendor_type" class="form-control">
                            <option>Select from below options</option>
                            <option value="food">Food</option>
                            <option value="liquor">Liquor</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3">
                        <input style="display: none;" id="edit_other_option" type="text" class="form-control" placeholder="Please mention other vendor type">
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button id="update_vendor" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                        <button class="btn btn-sm btn-warning btn-flat mr-50" data-dismiss="modal">Cancle</button>
                        <!-- <button onclick="return confirm('are you sure to delete?')" class="btn btn-sm btn-danger btn-flat mr-50">Delete</button> -->
                    </div>
                </div>

            </div>
        </form>

        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="bulkUploadEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="bulkUploadEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="bulkUploadEmployeeModalLabel">Bulk Upload Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload Merchant Form -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label">Select file to upload: </label>
                    </div>
                    <div class="col-sm-10">
                        <input type="file" id="bulk_employee_files" class="form-control">
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>

@endsection 


@section('js')

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });

    $(document).ready(function(){

        $("#vendor_type").on('change', function(){

            // console.log( $(this).val() );
            let selected_option = $(this).val();

            if ( selected_option == "other" ) {
                $("#other_option").show();
            } else {
                $("#other_option").hide();
            }

        });

        $("#edit_vendor_type").on('change', function(){

            // console.log( $(this).val() );
            let selected_option = $(this).val();

            if ( selected_option == "other" ) {
                $("#edit_other_option").show();
            } else {
                $("#edit_other_option").hide();
            }

        });

    });
    
  $(function () {
    $("#item_table").DataTable();
  });

</script>

@endsection


