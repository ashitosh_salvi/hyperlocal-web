@extends('layouts.applist')

@section('content')
@include('layouts.outlet_header')
<style type="text/css">
    .cat-nav .nav a, .nav label {
        display: block;
        padding: 1rem;
        color: #000;
        background: #ffffff;
        box-shadow: 0px 0px 4px 0.5px #a9a9a95e;
        -webkit-transition: all .25s ease-in;
        transition: all .25s ease-in;
    }
    .Delete-Category{
        float: right;
        background: unset;
        margin-right: 5px;
        border: unset;
        text-decoration: underline;
    }
    .Delete-Category.success{
        color: #008d4c;
    }
    .Delete-Category.danger{
        color: #dd4b39;
    }
    .Edit-Category{
        float: right;
        color: #e08e0b;
        background: unset;
        margin-right: 5px;
        border: unset;
        text-decoration: underline;
    }
    .cat-nav .nav a:focus, .cat-nav .nav a:hover, .cat-nav .nav label:focus, .cat-nav .nav label:hover {
        color: #fe5e00;
        /*background: #030303;*/
    }
    .cat-nav .cat-nav .nav label { cursor: pointer; }

    .cat-nav .group-list a, .group-list label {
        padding-left: 2rem;
        background: #ffffff;
        box-shadow: 0px 0px 4px 0.5px #a9a9a95e;
    }
    .cat-nav .group-list a:focus, .cat-nav .group-list a:hover, .cat-nav .group-list label:focus, .cat-nav .group-list label:hover { 
        /*background: #131313; */
    }

    .cat-nav .sub-group-list a, .cat-nav .sub-group-list label {
        padding-left: 4rem;
        background: #ffffff;
        box-shadow: 0px 0px 4px 0.5px #a9a9a95e;
    }
    .cat-nav .sub-group-list a:focus, .cat-nav .sub-group-list a:hover, .cat-nav .sub-group-list label:focus, .cat-nav .sub-group-list label:hover { 
        /*background: #232323; */
    }

    .cat-nav .sub-sub-group-list a, .cat-nav .sub-sub-group-list label {
        padding-left: 6rem;
        background: #ffffff;
        box-shadow: 0px 0px 4px 0.5px #a9a9a95e;
    }
    .cat-nav .sub-sub-group-list a:focus, .cat-nav .sub-sub-group-list a:hover, .cat-nav .sub-sub-group-list label:focus, .cat-nav .sub-sub-group-list label:hover { 
        /*background: #333333; */
    }

    .cat-nav .group-list, .cat-nav .sub-group-list, .cat-nav .sub-sub-group-list {
        height: 100%;
        max-height: 0;
        overflow: hidden;
        -webkit-transition: max-height .5s ease-in-out;
        transition: max-height .5s ease-in-out;
    }
    ul.nav__list {
        margin-bottom: 30px;
    }
    .cat-nav .nav__list input[type=checkbox]:checked + label + ul { /* reset the height when checkbox is checked */
        max-height: 1000px; }
    .cat-nav label > span {
        /*float: right;*/
        padding: 5px;
        -webkit-transition: -webkit-transform .65s ease;
        transition: transform .65s ease;
    }
    .cat-nav .nav__list input[type=checkbox]:checked + label > span {
        -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
    .cat-nav li{
        list-style: none;
    }
    ul#category_list_food, ul#category_list_beverages {
        padding-left: unset;
    }
</style>
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Category List</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-success add_category" id="add"><i class="fa fa-plus"></i> Add Category</button>
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Manage Category</h3>
                </div>
                
                <div class="box-body">

                    <div class="cat-nav">
                        <nav class="nav" role="navigation">
                            <h4><i class="fa fa-list-alt"></i> &nbsp; Foods</h4>
                            <ul class="nav__list" id="category_list_food">
                            
                            </ul>
                            <h4><i class="fa fa-list-alt"></i> &nbsp; Beverages</h4>
                            <ul class="nav__list" id="category_list_beverages">
                            
                            </ul>
                        </nav>
                    </div>
                    
                    <!-- /.table-responsive -->

                    

                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="addSubMainCatModal" tabindex="-1" role="dialog" aria-labelledby="addSubMainCatModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addSubMainCatModalLabel">Add Category</h4>
        </div>
        <div class="modal-body add-cat-body">

        <!-- Add New Merchant Form -->
        <form  method="POST" enctype="multipart/form-data" action="javascript:void(0)" id="SubmitCreateCategoryForm">  
            
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label">Category Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" name="category_name" id="category_name" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Description</label>
                        </div>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3"  name="description" id="description" placeholder="Enter ..." spellcheck="false" style="resize: vertical;"></textarea>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Select Main Category</label>
                        </div>
                        <div class="col-sm-8">
                            <label class="food">
                                <input type="radio" class="minimal-red" name="category_type" value="food" required>
                                Food
                            </label>
                            <label class="beverages">
                                <input type="radio" class="minimal-red" name="category_type" value="beverages" required>
                                Beverages
                            </label>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Select Parent Category</label>
                        </div>
                        <div class="col-sm-8">
                            <select class="form-control" name="parent_category" id="parent_category">
                                <option value=''>Select Parent Category </option>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

        </form>

        </div>

    </div>
    </div>
</div>


<div class="modal fade" id="editSubMainCatModal" tabindex="-1" role="dialog" aria-labelledby="editSubMainCatModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editSubMainCatModalLabel">Edit Category</h4>
        </div>
        <div class="modal-body edit-cat-body">

        <!-- Edit Merchant Form -->
        <form id="SubmitUpdateCategoryForm" method="PUT" enctype="multipart/form-data" action="javascript:void(0)">
                                
            <div class="box-body">

                <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label">Category Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" id="edit_id" class="form-control">
                            <input type="text" name="category_name" id="edit_category_name" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Description</label>
                        </div>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3"  name="description" id="edit_description" placeholder="Enter ..." spellcheck="false" style="resize: vertical;"></textarea>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Select Parent Category</label>
                        </div>
                        <div class="col-sm-8">
                            <select class="form-control" name="parent_category" id="edit_parent_category">
                                
                            </select>
                        </div>
                    </div>
                    

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" type="reset" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

        </form>

        </div>
        
    </div>
    </div>
</div>


@endsection 


@section('js')

<script src="{{ url('/') }}/jquery/scripting.js?v=2"></script>

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
$(function () {
    $("#item_table").DataTable();
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });
});

$(document).ready(function(){
        $('.add_category').on('click',function(){
            $('#SubmitCreateCategoryForm').trigger("reset");
            $("#SubmitCreateCategoryForm").find('span.help-block').remove();
            $("#SubmitCreateCategoryForm").find('.help-block').removeClass("help-block");
            $("#SubmitCreateCategoryForm").find('.error').removeClass("error");
            $("#SubmitCreateCategoryForm").find('.has-success').removeClass("has-success");
            $('#SubmitCreateCategoryForm').find('.form-control-feedback').remove();
            $("input[name=category_type]").iCheck('uncheck');
        });

        //Red color scheme for iCheck
        

        $("#add_outlet").click(function(e){

            // console.log('working');
            
            $(".outlet_details_box:first").clone().addClass("mt-15").appendTo("#outlet_details");

            e.preventDefault();

        });

        $("#edit_add_outlet").click(function(e){

            // console.log('working');
            
            $(".edit_outlet_details_box:first").clone().addClass("mt-15").appendTo("#edit_outlet_details");

            e.preventDefault();

        });

        $("#cancle_merchant").on('click', function(){

            window.history.back();

        });
    
        ///Edit Category
        $("#add").on('click', function(){
            category('1,2','add');
            $('#addSubMainCatModal').modal();
        });
        $(document).on('click','.Edit-Category', function(){
            $('#edit_parent_category').parent().parent().show();
            let id=$(this).data('id');
            $('#edit_id').val(id);
            let category_name=$(this).data('name');
            $('#edit_category_name').val(category_name);
            let parent_category=$(this).data('parent');
            let description=$(this).data('description');
            let priority=$(this).data('priority');
            if(priority==3){
                category('1,2','edit');
                // $('#edit_parent_category').prop('disabled',true);
            }
            if(priority==2){
                category('1','edit');
            }
            if(priority==1){
                $('#edit_parent_category').parent().parent().hide();
                // category('1,2','edit');
            }
            $('#edit_description').val(description);
            setTimeout(() => {
                $('#edit_parent_category').val(parent_category);
            }, 2000);
            $('#editSubMainCatModal').modal();
            // console.log("id:"+id+" category:"+category_name+" parent_category:"+parent_category+" description:"+description);
        });

        $(document).on('click','.Delete-Category', function(){
            
            let id=$(this).data('id');
            let status=$(this).data('status');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({
                url: "recipe_category/"+id,
                method: 'DELETE',
                data:{
                    "status": status,
                },
                success: function(result) {
                    if(result['status'] == true){
                        category('1,2,3','list');
                        Swal.fire(
                            'Success!',
                            result['message'],
                            'success'
                        );
                    }else{
                        Swal.fire(
                            'Warning!',
                            result['message'],
                            'warning'
                        );
                    }
                    
                }
            });
            
        });
        
        
        $('#SubmitUpdateCategoryForm').submit(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id=$('#edit_id').val();
            $.ajax({
                url: "category/"+id,
                method: 'PUT',
                data:{
                    "parent_category": $('#edit_parent_category').val(),
                    "category_name":$('#edit_category_name').val(),
                    "description":$('#edit_description').val(),
                },
                success: function(result) {
                    if(result.status == false){
                        if(result.message == 'Validation Error'){
                            var alert = '<div class="alert alert-warning alert-dismissible category-alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                    '<h4><i class="icon fa fa-warning"></i> Alert!</h4>'+
                                    result.data.error +
                                '</div>';
                            $('.edit-cat-body').prepend(alert).fadeIn('slow');
                            $('#edit_category_name').addClass('help-block');
                            $('#edit_category_name').focus();
                            setTimeout(function(){
                                $('#edit_category_name').removeClass('help-block');
                                $('.category-alert').fadeOut('slow');
                            }, 4000);
                        }
                        else{
                            var alert = '<div class="alert alert-warning alert-dismissible category-alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                    '<h4><i class="icon fa fa-warning"></i> Alert!</h4>'+
                                    result.data.errors['category_name']+
                                '</div>';
                            $('.edit-cat-body').prepend(alert).fadeIn('slow');
                            $('#edit_category_name').addClass('help-block');
                            $('#edit_category_name').focus();
                            setTimeout(function(){
                                $('#edit_category_name').removeClass('help-block');
                                $('.category-alert').fadeOut('slow');
                            }, 4000);
                        }   
                    }
                    if(result.status == true){
                        $('#editSubMainCatModal').modal('hide');
                        Swal.fire(
                            'Success!',
                            'Category Updated Successfully',
                            'success'
                        );
                        // $.notify("Category Updated Successfully", "success");
                        category('1,2,3','list');
                    }
                    
                    
                }
            });
        });
            // Create Category Ajax request.
        $('#SubmitCreateCategoryForm').submit(function(e) {
            e.preventDefault();
        
            var formData = new FormData(this);

            $.ajax({
                url: "{{ route('recipe_category.store') }}",
                method: 'post',
                cache:false,
                contentType: false,
                processData: false,                   
                data:formData,
                success: function(result) {
                    console.log(result);
                    if(result.status == false){
                        if(result.message == 'Validation Error'){
                            var alert = '<div class="alert alert-warning alert-dismissible category-alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                    '<h4><i class="icon fa fa-warning"></i> Alert!</h4>'+
                                    result.data.error +
                                '</div>';
                            $('.add-cat-body').prepend(alert).fadeIn('slow');
                            $('#category_name').addClass('help-block');
                            $('#category_name').focus();
                            setTimeout(function(){
                                $('#category_name').removeClass('help-block');
                                $('.category-alert').fadeOut('slow');
                            }, 4000);
                        }
                        else{
                            var alert = '<div class="alert alert-warning alert-dismissible category-alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                    '<h4><i class="icon fa fa-warning"></i> Alert!</h4>'+
                                    result.data.errors['category_name']+
                                '</div>';
                            $('.add-cat-body').prepend(alert).fadeIn('slow');
                            $('#category_name').addClass('help-block');
                            $('#category_name').focus();
                            setTimeout(function(){
                                $('#category_name').removeClass('help-block');
                                $('.category-alert').fadeOut('slow');
                            }, 4000);
                        }
                        
                    }
                    if(result.status == true){
                        $('#addSubMainCatModal').modal('hide');
                        Swal.fire(
                            'Success!',
                            'Category Added Successfully',
                            'success'
                        );
                        // $.notify("Category Added Successfully", "success");
                        category('1,2,3','list');
                    }            
                }
            });
        });

        // $("#SubmitCreateCategoryForm").click(function(){
        //     $(this).trigger("reset");
        // });

        // OnClick Main Category Load Parent Category
        $('input[value=beverages]').on('ifChecked', function(event){

            console.log($("input[value=beverages]").val());
            
            let category_type = $("input[value=beverages]").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "get_main_category",
                method: 'POST',
                data: {
                    "category_type": category_type
                },
                success: function(result) {
                    console.log(result);
                    $('#parent_category').empty();
                    let html="<option value=''>Select Parent Category </option>";
                    let category = result.category;
                    if(category){
                        $.each(category, function( index, value ) {
                            html +="<option value="+value.id+">"+value.category_name+"</option>";
                        });
                    }
                    $('#parent_category').append(html);
                },
                error: function(e) {
                    console.log(e)
                }
            });

        });
        $('input[value=food]').on('ifChecked', function(event){
          // alert('abcd efg');
          console.log($("input[value=food]").val());

            let category_type = $("input[value=food]").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "get_main_category",
                method: 'POST',
                data: {
                    "category_type": category_type
                },
                success: function(result) {
                    console.log(result);
                    $('#parent_category').empty();
                    let html="<option value=''>Select Parent Category </option>";
                    let category = result.category;
                    if(category){
                        $.each(category, function( index, value ) {
                            html +="<option value="+value.id+">"+value.category_name+"</option>";
                        });
                    }
                    $('#parent_category').append(html);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        });

        // $(".food").on('click', function(){

        // });

        //// fetch category list
    function category(priority,operation){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "get-recipe-category",
                method: 'POST',
                data:{priority:priority},
                success: function(result) {
                    console.log(result);
                    if(operation=="edit"){
                        $('#edit_parent_category').children('option').remove();     
                    }
                    if(operation=="edit"){
                        let html="<option value=''>Select Parent Category </option>";
                        if(result.status){
                            $.each(result.data, function( index, value ) {
                                html +="<option value="+value.id+">"+value.category_name+"</option>";
                            });
                        }
                        $('#edit_parent_category').append(html);     
                    }
                    if(operation=="list"){
                        $('#category_list_food').children().remove();
                        $('#category_list_beverages').children().remove();
                        $.each(result.data, function( index, value ) {
                            if(value.category_type =='food'){
                                var category_list_food = '<li>'+                                    
                                '<input id="group-'+value.id+'" type="checkbox" hidden />'+
                                '<label for="group-'+value.id+'"><span class="fa fa-angle-right"></span>'+value.category_name+
                                '<button type="button" class="Edit-Category" data-id="'+value.id+'" data-priority="'+value.priority+'" data-name="'+value.category_name+'" data-parent="'+value.parent_category+'" data-description="'+value.description+'" >Edit</button>'+
                                '<button type="button" class="Delete-Category '+ ( (value.status == 0) ? 'success' : 'danger' ) +'" data-id="'+value.id+'" data-status="'+value.status+'" >';
                            
                                if(value.status == 0){
                                    category_list_food +='Activate';
                                }else{
                                    category_list_food +='Deactivate';
                                }
                                            
                            
                                category_list_food +='</button>'+
                                    '</button>'+
                                    '</label>'+
                                    '<ul class="group-list">';

                                $.each(result.data, function( index_1, value_1 ) {
                                    if(value_1.parent_category==value.id && value_1.priority==2){
                                            category_list_food +='<li>'+
                                                '<input id="sub-group-'+value_1.id+'" type="checkbox" hidden />'+
                                                '<label for="sub-group-'+value_1.id+'"><span class="fa fa-angle-right"></span>'+value_1.category_name+
                                                '<button type="button" class="Edit-Category" data-id="'+value_1.id+'" data-priority="'+value_1.priority+'" data-name="'+value_1.category_name+'" data-parent="'+value_1.parent_category+'" data-description="'+value_1.description+'" >Edit</button>'+
                                                '<button type="button" class="Delete-Category '+ ( (value_1.status == 0) ? 'success' : 'danger' ) +'" data-id="'+value_1.id+'" data-status="'+value_1.status+'" >';
                                            if(value_1.status == 0){
                                                category_list_food +='Activate';
                                            }else{
                                                category_list_food +='Deactivate';
                                            }
                                                    
                                    
                                            category_list_food +='</button>'+
                                                '</label>'+
                                                '<ul class="sub-group-list">';
                                            $.each(result.data, function( index_2, value_2 ) {
                                                if(value_2.parent_category==value_1.id && value_2.priority==3){
                                                    
                                                        category_list_food +='<li>'+
                                                            '<input id="sub-group-'+value_2.id+'" type="checkbox" hidden />'+
                                                                '<label for="sub-group-'+value_2.id+'">'+value_2.category_name+
                                                                '<button type="button" class="Edit-Category" data-id="'+value_2.id+'" data-priority="'+value_2.priority+'" data-name="'+value_2.category_name+'" data-parent="'+value_2.parent_category+'" data-description="'+value_2.description+'" >Edit</button>'+
                                                                '<button type="button" class="Delete-Category '+ ( (value_2.status == 0) ? 'success' : 'danger' ) +'" data-id="'+value_2.id+'" data-status="'+value_2.status+'" >';
                                                                        
                                                                    if(value_2.status == 0){
                                                                        category_list_food +='Activate';
                                                                    }else{
                                                                        category_list_food +='Deactivate';
                                                                    }
                                                                    
                                                    
                                                                    category_list_food +='</button>'+
                                                                    
                                                                        '</label>'+
                                                                    
                                                                    '</li>';
                                                           
                                                    
                                                }
                                            });            
                                            category_list_food +='</ul>'+
                                                '</li>';
                                        
                                        
                                    }
                                });
                                category_list_food +='</ul>'+
                                '</li>';
                                
                                if(value.priority==1){
                                    $('#category_list_food').append(category_list_food);
                                }    
                            }
                            if(value.category_type == 'beverages'){
                                var category_list_beverages = '<li>'+                                    
                                '<input id="group-'+value.id+'" type="checkbox" hidden />'+
                                '<label for="group-'+value.id+'"><span class="fa fa-angle-right"></span>'+value.category_name+
                                '<button type="button" class="Edit-Category" data-id="'+value.id+'" data-priority="'+value.priority+'" data-name="'+value.category_name+'" data-parent="'+value.parent_category+'" data-description="'+value.description+'" >Edit</button>'+
                                '<button type="button" class="Delete-Category '+ ( (value.status == 0) ? 'success' : 'danger' ) +'" data-id="'+value.id+'" data-status="'+value.status+'" >';
                            
                                if(value.status == 0){
                                    category_list_beverages +='Activate';
                                }else{
                                    category_list_beverages +='Deactivate';
                                }
                                            
                            
                                category_list_beverages +='</button>'+
                                    '</button>'+
                                    '</label>'+
                                    '<ul class="group-list">';

                                $.each(result.data, function( index_1, value_1 ) {
                                    if(value_1.parent_category==value.id && value_1.priority==2){
                                            category_list_beverages +='<li>'+
                                                '<input id="sub-group-'+value_1.id+'" type="checkbox" hidden />'+
                                                '<label for="sub-group-'+value_1.id+'"><span class="fa fa-angle-right"></span>'+value_1.category_name+
                                                '<button type="button" class="Edit-Category" data-id="'+value_1.id+'" data-priority="'+value_1.priority+'" data-name="'+value_1.category_name+'" data-parent="'+value_1.parent_category+'" data-description="'+value_1.description+'" >Edit</button>'+
                                                '<button type="button" class="Delete-Category '+ ( (value_1.status == 0) ? 'success' : 'danger' ) +'" data-id="'+value_1.id+'" data-status="'+value_1.status+'" >';
                                            if(value_1.status == 0){
                                                category_list_beverages +='Activate';
                                            }else{
                                                category_list_beverages +='Deactivate';
                                            }
                                                    
                                    
                                            category_list_beverages +='</button>'+
                                                '</label>'+
                                                '<ul class="sub-group-list">';
                                            $.each(result.data, function( index_2, value_2 ) {
                                                if(value_2.parent_category==value_1.id && value_2.priority==3){
                                                        category_list_beverages +='<li>'+
                                                            '<input id="sub-group-'+value_2.id+'" type="checkbox" hidden />'+
                                                                '<label for="sub-group-'+value_2.id+'">'+value_2.category_name+
                                                                '<button type="button" class="Edit-Category" data-id="'+value_2.id+'" data-priority="'+value_2.priority+'" data-name="'+value_2.category_name+'" data-parent="'+value_2.parent_category+'" data-description="'+value_2.description+'" >Edit</button>'+
                                                                '<button type="button" class="Delete-Category '+ ( (value_2.status == 0) ? 'success' : 'danger' ) +'" data-id="'+value_2.id+'" data-status="'+value_2.status+'" >';
                                                                        
                                                                    if(value_2.status == 0){
                                                                        category_list_beverages +='Activate';
                                                                    }else{
                                                                        category_list_beverages +='Deactivate';
                                                                    }
                                                                    
                                                    
                                                                    category_list_beverages +='</button>'+
                                                                    
                                                                        '</label>'+
                                                                    
                                                                    '</li>';
                                                           
                                                    
                                                }
                                            });            
                                            category_list_beverages +='</ul>'+
                                                '</li>';
                                        
                                        
                                    }
                                });
                                category_list_beverages +='</ul>'+
                                '</li>';
                                
                                if(value.priority==1){
                                    $('#category_list_beverages').append(category_list_beverages);
                                }    
                            }

                            
                        });
                    }
                }
            });
        }
        category('1,2','add');
        category('1,2,3','list');
    });

</script>

@endsection
