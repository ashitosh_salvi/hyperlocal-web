@extends('layouts.applist')

@section('content')
@include('layouts.merchant_header')
<style type="text/css">
    .cat-nav .nav a, .nav label {
        display: block;
        padding: 1rem;
        color: #fff;
        background-color: #151515;
        box-shadow: inset 0 -1px #1d1d1d;
        -webkit-transition: all .25s ease-in;
        transition: all .25s ease-in;
    }
    .cat-nav .nav a:focus, .cat-nav .nav a:hover, .cat-nav .nav label:focus, .cat-nav .nav label:hover {
        color: rgba(255, 255, 255, 0.5);
        background: #030303;
    }
    .cat-nav .cat-nav .nav label { cursor: pointer; }

    .cat-nav .group-list a, .group-list label {
        padding-left: 2rem;
        background: #252525;
        box-shadow: inset 0 -1px #373737;
    }
    .cat-nav .group-list a:focus, .cat-nav .group-list a:hover, .cat-nav .group-list label:focus, .cat-nav .group-list label:hover { background: #131313; }

    .cat-nav .sub-group-list a, .cat-nav .sub-group-list label {
        padding-left: 4rem;
        background: #353535;
        box-shadow: inset 0 -1px #474747;
    }
    .cat-nav .sub-group-list a:focus, .cat-nav .sub-group-list a:hover, .cat-nav .sub-group-list label:focus, .cat-nav .sub-group-list label:hover { background: #232323; }

    .cat-nav .sub-sub-group-list a, .cat-nav .sub-sub-group-list label {
        padding-left: 6rem;
        background: #454545;
        box-shadow: inset 0 -1px #575757;
    }
    .cat-nav .sub-sub-group-list a:focus, .cat-nav .sub-sub-group-list a:hover, .cat-nav .sub-sub-group-list label:focus, .cat-nav .sub-sub-group-list label:hover { background: #333333; }

    .cat-nav .group-list, .cat-nav .sub-group-list, .cat-nav .sub-sub-group-list {
        height: 100%;
        max-height: 0;
        overflow: hidden;
        -webkit-transition: max-height .5s ease-in-out;
        transition: max-height .5s ease-in-out;
    }
    .cat-nav .nav__list input[type=checkbox]:checked + label + ul { /* reset the height when checkbox is checked */
        max-height: 1000px; }
    .cat-nav label > span {
        /*float: right;*/
        padding: 5px;
        -webkit-transition: -webkit-transform .65s ease;
        transition: transform .65s ease;
    }
    .cat-nav .nav__list input[type=checkbox]:checked + label > span {
        -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
    .cat-nav li{
        list-style: none;
    }
</style>
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Category List</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-success" id="add"><i class="fa fa-plus"></i> Add Category</button>
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Manage Category</h3>
                </div>
                
                <div class="box-body">

                    <!-- <div class="row">
                        <div class="col-sm-1">
                            <label class="control-label">Merchant Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">GST Number</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-1">
                            <label class="control-label">Rest Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    
                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <a class="btn btn-sm btn-success btn-flat mr-50" href="#">Search</a>
                        <a class="btn btn-sm btn-danger btn-flat mr-50" href="#">Reset</a>
                        <a class="btn btn-sm btn-warning btn-flat mr-50" href="{{ url('/merchant_create') }}">Add New</a>
                        </div>
                    </div> -->
                    <div class="row cat-nav">
                        <nav class="nav" role="navigation">
                            <ul class="nav__list">
                            @foreach($data as $key=>$value)
                                @if($value->priority==1)
                                <li>

                                    <input id="group-{{$key}}" type="checkbox" hidden />
                                    <label for="group-{{$key}}"><span class="fa fa-angle-right"></span> {{$value->category_name}}
                                    <button style="float: right;color: #f00;" class="Edit-Category" data-id="{{$value->id}}" data-priority="{{$value->priority}}" data-name="{{$value->category_name}}" data-parent="{{$value->parent_category}}" data-description="{{$value->description}}" >edit</button>
                                    <button style="float: right;color: #f00;" class="Delete-Category" data-id="{{$value->id}}" data-id="{{$value->status}}" >
                                    @if($value->status==0)
                                    Active
                                    @else
                                    Deactivate
                                    @endif
                                    </button>
                                                
                                    </label>
                                    <ul class="group-list">
                                       
                                    @foreach($data as $key_1=>$value_1)
                                        @if($value_1->priority==2 && $value->id==$value_1->parent_category)
                                        <li>
                                            <input id="sub-group-{{$key_1}}" type="checkbox" hidden />
                                            <label for="sub-group-{{$key_1}}"><span class="fa fa-angle-right"></span> {{$value_1->category_name}}
                                                <button style="float: right;color: #f00;" class="Edit-Category" data-id="{{$value_1->id}}" data-priority="{{$value_1->priority}}" data-name="{{$value_1->category_name}}" data-parent="{{$value_1->parent_category}}" data-description="{{$value_1->description}}">edit</button>
                                                <button style="float: right;color: #f00;" class="Delete-Category" data-id="{{$value_1->id}}" data-status="{{$value_1->status}}">
                                                @if($value_1->status==0)
                                                    Active
                                                    @else
                                                    Deactivate
                                                    @endif
                                                </button>
                                            </label>
                                            <ul class="sub-group-list">
                                                
                                        @foreach($data as $key_2=>$value_2)
                                        @if($value_2->priority==3 && $value->id==$value_1->parent_category && $value_1->id==$value_2->parent_category )
                                                <li>
                                                    <input id="sub-sub-group-{{$key_2}}" type="checkbox" hidden />
                                                    <label for="sub-sub-group-{{$key_2}}"><span class="fa fa-angle-right"></span> {{$value_2->category_name}}
                                                    <button style="float: right;color: #f00;" class="Edit-Category" data-id="{{$value_2->id}}" data-priority="{{$value_2->priority}}" data-name="{{$value_2->category_name}}" data-parent="{{$value_2->parent_category}}" data-description="{{$value_2->description}}">edit</button>
                                                    <button style="float: right;color: #f00;" class="Delete-Category" data-id="{{$value_2->id}}" data-status="{{$value_2->status}}">
                                                    @if($value_2->status==0)
                                                    Active
                                                    @else
                                                    Deactivate
                                                    @endif
                                                    </button>
                                                   
                                                    </label>
                                                   
                                                </li>
                                        @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                    
                    <div class="table-responsive mt-15">
                        
                        {{-- <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>  
                                <tr>
                                    <td>
                                        <div class="content" data-id="26" data-series="1" data-priority="1" onclick="collapseExpand(this,event)">
                                            <span class="fa fa-caret-down"></span>
                                            <span class="search" data-id="26"> 1. xyz</span>
                                            <span class="pull-right"> 
                                                <button data-id="26" class="btn_add edit" style="border: 0.5px solid #be1623"><i class="fa fa-pencil"></i> Edit</button>&nbsp;&nbsp;&nbsp;
                                                <a data-id="26" data-status="0" style="cursor:pointer;border: 0.5px solid #be1623;    padding: 5px 18px;" class="getStatusId btn_add">
                                                    <i class="fa fa-close"></i> Deactivate
                                                </a>
                                            </span>
                                        </div>
                                        <div id="cat26" data-series="1" style="display: block;">
                                            <div class="childs" data-id="27">
                                                <div colspan="4">
                                                    <div class="content" data-id="27" data-series="1.1" data-priority="2" onclick="collapseExpand(this,event)">
                                                        <span> 1.1. zvcxvcx</span>
                                                        <span class="pull-right icon-m">
                                                            <button data-id="27" class="btn_add edit" style="border: 0.5px solid #be1623">
                                                                <i class="fa fa-pencil"></i> Edit 
                                                            </button>&nbsp;&nbsp;&nbsp;
                                                            <a data-id="27" data-status="0" style="cursor:pointer;border: 0.5px solid #be1623;    padding: 5px 18px;" class="getStatusId btn_add">
                                                                <i class="fa fa-close"></i> Deactivate 
                                                            </a>
                                                        </span>
                                                    </div>
                                                    <div id="cat27" style="display:none;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table> --}}
                    </div>
                    <!-- /.table-responsive -->

                    {{-- <div class="row mt-15">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button>
                        </div>
                    </div> --}}

                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="addSubMainCatModal" tabindex="-1" role="dialog" aria-labelledby="addSubMainCatModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addSubMainCatModalLabel">Add Category</h4>
        </div>
        <div class="modal-body">

        <!-- Add New Merchant Form -->
          <form  method="POST" enctype="multipart/form-data" action="javascript:void(0)" id="SubmitCreateCategoryForm">  
            
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label">Category Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" name="category_name" id="category_name" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Description</label>
                        </div>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3"  name="description" id="description" placeholder="Enter ..." spellcheck="false"></textarea>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Select Parent Category</label>
                        </div>
                        <div class="col-sm-8">
                            <select class="form-control" name="parent_category" id="parent_category">
				                
                            </select>
                        </div>
                    </div>
                    

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="create_merchant" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancle</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

        </div>

      </div>
    </div>
</div>


<div class="modal fade" id="editSubMainCatModal" tabindex="-1" role="dialog" aria-labelledby="editSubMainCatModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editSubMainCatModalLabel">Edit Category</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Merchant Form -->
        <form id="SubmitUpdateCategoryForm" method="PUT" enctype="multipart/form-data" action="javascript:void(0)">
                                
            <div class="box-body">

                <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label">Category Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="hidden" name="id" id="edit_id" class="form-control">
                            <input type="text" name="category_name" id="edit_category_name" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Description</label>
                        </div>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3"  name="description" id="edit_description" placeholder="Enter ..." spellcheck="false"></textarea>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-4">
                            <label class="control-label">Select Parent Category</label>
                        </div>
                        <div class="col-sm-8">
                            <select class="form-control" name="parent_category" id="edit_parent_category">
				                
                            </select>
                        </div>
                    </div>
                    

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="create_merchant" type="submit" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" type="reset" data-dismiss="modal">Cancle</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

        </div>
        
      </div>
    </div>
</div>


@endsection 


@section('js')

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
  $(function () {
    $("#item_table").DataTable();
    /* var html = '<li>'+                                    
                    '<input id="group-20" type="checkbox" hidden />'+
                    '<label for="group-20"><span class="fa fa-angle-right"></span> Cat 1'+
                    '<button style="float: right;color: #f00;" class="Edit-Category" data-id="1" data-priority="1" data-name="cat 1" data-parent="abc" data-description="test data" >edit</button>'+
                    '<button style="float: right;color: #f00;" class="Delete-Category" data-id="1" data-status="0" >'+
                    
                    'Deactivate'+
                    
                    '</button>'+
                                
                    '</label>'+
                    '<ul class="group-list">'+
                       
                        '<li>'+
                            '<input id="sub-group-20" type="checkbox" hidden />'+
                            '<label for="sub-group-20"><span class="fa fa-angle-right"></span> Sub cat1'+
                                '<button style="float: right;color: #f00;" class="Edit-Category" data-id="2" data-priority="2" data-name="sub cat 1" data-parent="1" data-description="test data">edit</button>'+
                                '<button style="float: right;color: #f00;" class="Delete-Category" data-id="2" data-status="0">'+
                                
                                    'Deactivate'+
                
                                '</button>'+
                            '</label>'+
                            '<ul class="sub-group-list">'+
                                
                            '<li>'+
                                    '<input id="sub-sub-group-20" type="checkbox" hidden />'+
                                    '<label for="sub-sub-group-20"><span class="fa fa-angle-right"></span> child cat 1'+
                                    '<button style="float: right;color: #f00;" class="Edit-Category" data-id="3" data-priority="3" data-name="child cat 1" data-parent="2" data-description="test data">edit</button>'+
                                    '<button style="float: right;color: #f00;" class="Delete-Category" data-id="3" data-status="0">'+
                                    
                                        'Active'+
                                    
                                    '</button>'+
                                   
                                    '</label>'+
                                   
                                '</li>'+
                            '</ul>'+
                        '</li>'+
                    '</ul>'+
                '</li>';*/
  });

  $(document).ready(function(){

        $("#add_outlet").click(function(e){

            // console.log('working');
            
            $(".outlet_details_box:first").clone().addClass("mt-15").appendTo("#outlet_details");

            e.preventDefault();

        });

        $("#edit_add_outlet").click(function(e){

            // console.log('working');
            
            $(".edit_outlet_details_box:first").clone().addClass("mt-15").appendTo("#edit_outlet_details");

            e.preventDefault();

        });

        $("#cancle_merchant").on('click', function(){

            window.history.back();

        });
    
        ///Edit Category
        $("#add").on('click', function(){
            category('1,2','add');
            $('#addSubMainCatModal').modal();
        });
        $(".Edit-Category").on('click', function(){
            $('#edit_parent_category').parent().parent().show();
            let id=$(this).data('id');
            $('#edit_id').val(id);
            let category_name=$(this).data('name');
            $('#edit_category_name').val(category_name);
            let parent_category=$(this).data('parent');
            let description=$(this).data('description');
            let priority=$(this).data('priority');
            if(priority==3){
                category('1,2','edit');
                // $('#edit_parent_category').prop('disabled',true);
            }
            if(priority==2){
                category('1','edit');
            }
            if(priority==1){
                $('#edit_parent_category').parent().parent().hide();
                // category('1,2','edit');
            }
            $('#edit_description').val(description);
            setTimeout(() => {
                $('#edit_parent_category').val(parent_category);
            }, 1000);
            $('#editSubMainCatModal').modal();
            // console.log("id:"+id+" category:"+category_name+" parent_category:"+parent_category+" description:"+description);
        });
        
        // Create Category Ajax request.
        $('#SubmitUpdateCategoryForm').submit(function(e) {
            e.preventDefault();
            
        });
        // Create Category Ajax request.
        $('#SubmitUpdateCategoryForm').submit(function(e) {
            e.preventDefault();
           $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id=$('#edit_id').val();
            $.ajax({
                url: "category/"+id,
                method: 'PUT',
                data:{
                    "parent_category": $('#edit_parent_category').val(),
                    "category_name":$('#edit_category_name').val(),
                    "description":$('#edit_description').val(),
                },
                success: function(result) {
                   
                }
            });
        });
            // Create Category Ajax request.
        $('#SubmitCreateCategoryForm').submit(function(e) {
            e.preventDefault();
           
            var formData = new FormData(this);

            $.ajax({
                url: "{{ route('category.store') }}",
                method: 'post',
                cache:false,
                contentType: false,
                processData: false,                   
                data:formData,
                success: function(result) {
                    
                }
            });
        });

        //// fetch category list
       function category(priority,operation){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "get-category",
                method: 'POST',
                data:{priority:priority},
                success: function(result) {
                console.log(result);
                if(operation=="edit"){
                    $('#edit_parent_category').children('option').remove();     
                }else{
                    $('#parent_category').children('option').remove();     
                }
                let html="<option value=''>Select Parent Category </option>";
                if(result.status){
                    $.each(result.data, function( index, value ) {
                        html +="<option value="+value.id+">"+value.category_name+"</option>";
                    });
                }
                if(operation=="add"){
                    $('#parent_category').append(html);
                }else{
                    $('#edit_parent_category').append(html);     
                }
              }
            });
       }
        category('1,2','add');
    });

</script>

@endsection
