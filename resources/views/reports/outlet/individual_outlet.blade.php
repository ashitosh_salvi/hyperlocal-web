@extends('layouts.applist')
@section('content')
    @include('layouts.admin_header')
    <style>
        .pac-container{
            z-index: 9999!important;
        }
        #outlet_table input[type="checkbox"].tgl-ios + label{
            top: -7px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <section id="main-content">
        <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
            <!-- page start-->
            <section class="content-header" style="min-height: unset;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-body">
                            <div class="col-md-6">
                                <h4><i class="fa fa-list"></i> &nbsp; Individual Outlet Report</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="box ">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div style="width: 100%; height: 34px; position: relative;">
                            <button id="export_button" type="button" class="btn btn-primary">Export</button>

                            <div class="form-group" style="width: fit-content; float: right;margin: 0px;">
                                <input type="text" class="form-control" name="datetimes" style="width: 250px !important; float: right; ">
                                <span style="float: right; margin-top: 9px; margin-right: 6px;">Date search : </span>
                                <input type="hidden" id="from_date_input" value="">
                                <input type="hidden" id="to_date_input" value="">
                            </div>

                            <div class="form-group" style=" width: 240px;float: right;margin: 0px;margin-right: 20px;">
                                <select id="selected_outlet" name="selected_outlet" class="form-control" aria-label="Default select example">
                                    @foreach ($outlets as $outlet)
                                        <option value="{{ $outlet->id }}">{{ $outlet->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="table-responsive mt-15">
                            <table id="outlet_table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Order No.</th>
                                        <th>Name</th>
                                        <th>Contact No.</th>
                                        <th>Order Value</th>
                                        <th>Discount</th>
                                        <th>Delivery & Charges</th>
                                        <th>GST Amount</th>
                                        <th>Payment Mode</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection 

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script>
        $(document).ready(function(){
            loadMerchantReport(
                moment().subtract(1, 'months').format("YYYY-MM-DD"),
                moment().format("YYYY-MM-DD"),
                $('#selected_outlet').val()
            );

            $('input[name="datetimes"]').daterangepicker(
                {
                    showDropdowns: true,
                    startDate: moment().subtract(1, 'months'),
                    endDate: moment(),
                    maxDate:moment(),
                    locale: { 
                        format: 'YYYY-MM-DD'
                    }
                }, 
                function(start, end, label) {
                    let start_date      = start.format("YYYY-MM-DD");
                    let end_date        = end.format("YYYY-MM-DD");
                    let selected_outlet = $('#selected_outlet').val();
                    loadMerchantReport(start_date, end_date,selected_outlet);
                }
            );

            $('#export_button').click(function(){
                let from_date       = $('#from_date_input').val();
                let to_date         = $('#to_date_input').val();
                let search          = $("input[type=search]").val();
                let selected_outlet = $('#selected_outlet').val();
                if(search == "")
                {
                    search = 0;
                }
                let url             = "{{ route('export_individual_outlet_report') }}/"+from_date+'/'+to_date+'/'+search+'/'+selected_outlet;
                console.log(url);
                window.location.href = url;
            }); 

            $('#selected_outlet').change(function(){
                let start_date      = $("#from_date_input").val();
                let end_date        = $("#to_date_input").val();
                let selected_outlet = $('#selected_outlet').val();
                loadMerchantReport(start_date, end_date,selected_outlet);
            });
        });

        function loadMerchantReport(from_date, to_date,selected_outlet) {
            $('#from_date_input').val(from_date);
            $('#to_date_input').val(to_date);
            $('#outlet_table').dataTable().fnDestroy();
            $('#outlet_table').DataTable({
                "language": {
                    "emptyTable": "No Record Found"
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                        "url": "{{ route('get_individual_outlet_report') }}",
                        "dataType": "json",
                        "type": "GET",
                        "data":{ 
                            _token: "{{csrf_token()}}",
                            from_date:from_date,
                            to_date:to_date,
                            outlet:selected_outlet,
                        }
                },
                "columns": [
                    { "data": "order_no" },
                    { "data": "name" },
                    { "data": "contact_no" },
                    { "data": "order_value" },
                    { "data": "discount" },
                    { "data": "delivery_charges" },
                    { "data": "gst" },
                    { "data": "payment_mode" },
                    { "data": "status" },
                ],
                "aoColumnDefs": [
                    {
                        bSortable: false,
                        aTargets: [ -1,-2,-3,-4,-5,-6,-7,-8,-9]
                    }
                ],
            });
        }



    </script>
@endsection
