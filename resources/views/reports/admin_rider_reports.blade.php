@extends('layouts.applist')
@section('content')
@include('layouts.admin_header')

<style>
.pac-container{
    z-index: 9999!important;
}
#outlet_table input[type="checkbox"].tgl-ios + label{
    top: -7px;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Rider Report</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div style="width: 100%; height: 34px; position: relative;">
                        <button id="export_button" type="button" class="btn btn-primary">Export</button>
                        <input type="text" class="form-control" name="datetimes" style="width: 250px !important; float: right; ">
                        <span style="float: right; margin-top: 9px; margin-right: 6px;">Date search : </span>
                        <input type="hidden" id="from_date_input" value="">
                        <input type="hidden" id="to_date_input" value="">
                    </div>

                    <div class="table-responsive mt-15">
                            
                        <table id="outlet_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Rider Name</th>
                                    <th>Rider Phone</th>
                                    {{-- <th>Outlet Name</th> --}}
                                    <th>Orders</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
            </div>
        </section>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="viewRiderDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="rider_popup_header" style="display: inline;font-size: 15px;"> Report</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="box-body">
                <button id="export_popup_button" type="button" class="btn btn-primary">Export</button>
                <input type="hidden" name="rider_id" id="rider_popup_id" value="">
                <div class="table-responsive mt-15">
                        
                    <table id="rider_details_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Outlet Name</th>
                                <th>Count of Orders</th>
                                <th>Value of Orders</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>


@endsection 

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
$(document).ready(function(){
    
    loadRiderReport(moment().subtract(1, 'months').format("YYYY-MM-DD"),moment().format("YYYY-MM-DD"));
    $('input[name="datetimes"]').daterangepicker(
        {
            showDropdowns: true,
            startDate: moment().subtract(1, 'months'),
            endDate: moment(),
            maxDate:moment(),
            locale: { 
                format: 'YYYY-MM-DD'
            }
        }, 
        function(start, end, label) {
            let start_date = start.format("YYYY-MM-DD");
            let end_date = end.format("YYYY-MM-DD");
            loadRiderReport(start_date, end_date);
        }
    );

    $('#export_button').click(function(){
        let from_date = $('#from_date_input').val();
        let to_date = $('#to_date_input').val();
        let search = $("input[type=search]").val();
        let url = "{{ route('export_get_admin_rider_report') }}/"+from_date+'/'+to_date+'/'+search;
        window.location.href = url;
    }); 

    $('#export_popup_button').click(function(){
        let from_date   = $('#from_date_input').val();
        let to_date     = $('#to_date_input').val();
        let rider_id    = $('#rider_popup_id').val();
        let url = "{{ route('export_get_admin_rider_details_report') }}/"+from_date+'/'+to_date+'/'+rider_id;
        window.location.href = url;
    });

});

function loadRiderReport(from_date, to_date)
{
    $('#from_date_input').val(from_date);
    $('#to_date_input').val(to_date);
    $('#outlet_table').dataTable().fnDestroy();

    $('#outlet_table').DataTable({
        "language": {
            "emptyTable": "No Record Found"
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
                "url": "{{ route('get_admin_rider_report') }}",
                "dataType": "json",
                "type": "GET",
                "data":{ 
                    _token: "{{csrf_token()}}",
                    from_date:from_date,
                    to_date:to_date
                    }
            },
        "columns": [
            { "data": "rider_name" },
            { "data": "rider_number" },
            { "data": "orders" }
        ],
        "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }
        ],
    });
}

function viewRiderDetails(rider_id,rider_name) 
{   
    $('#rider_popup_id').val(rider_id);
    let from_date = $('#from_date_input').val();
    let to_date = $('#to_date_input').val();
    let url = "{{ route('get_admin_rider_details_report') }}/"+rider_id;

    $('#rider_popup_header').html('Report of '+ rider_name)
    $('#rider_details_table').dataTable().fnDestroy();
    $('#rider_details_table').DataTable({
        "language": {
            "emptyTable": "No Record Found"
        },
        "processing": true,
        "serverSide": true,
        "searching": false,
        "ordering": false,
        "ajax":{
                "url": url,
                "dataType": "json",
                "type": "GET",
                "data":{ 
                    _token: "{{csrf_token()}}",
                    from_date:from_date,
                    to_date:to_date
                    }
            },
        "columns": [
            { "data": "outlet_name" },
            { "data": "count_of_order" },
            { "data": "value_of_order" }
        ],
        "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }
        ],
    });
    $('#viewRiderDetailsModal').modal('show');
}

</script>

@endsection
