<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- @if (isset($_SERVER['HTTPS'])) --}}
        {{-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> --}}
    {{-- @endif --}}

    <title>{{ config('app.name','HyperLocal') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <!-- Datatable style -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">		
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-green.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    
    
    @yield('css')

    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

    <style type="text/css">
        /*.modal-header {
            background: #00bcd4;
        }*/
        @media (min-width: 768px) {
            #addMerchantModal .modal-dialog, #editMerchantModal .modal-dialog, #viewMerchantModal .modal-dialog, #addVendorModal .modal-dialog, #editVendorModal .modal-dialog, #viewVendorModal .modal-dialog, #addEmployeeModal .modal-dialog, #editEmployeeModal .modal-dialog, #addEmployeeModal .modal-dialog, #editEmployeeModal .modal-dialog, #viewEmployeeModal .modal-dialog, #addProductModal .modal-dialog, #editProductModal .modal-dialog, #viewProductModal .modal-dialog, #addMenuItemModal .modal-dialog, #editMenuItemModal .modal-dialog, #viewMenuItemModal .modal-dialog, #viewOrderModal .modal-dialog, #editOrderModal .modal-dialog, #viewOutletModal .modal-dialog, #editOutletModal .modal-dialog, #addOutletModal .modal-dialog, #addRiderModal .modal-dialog,  #viewRiderModal .modal-dialog,  #editRiderModal .modal-dialog, #addOfferModal .modal-dialog,  #viewOfferModal .modal-dialog,  #editOfferModal .modal-dialog {
                width: 1000px;
                margin: 30px auto;
            }
        }
        .select2{
          width: 100% !important;
        }
        .action-group{
          display: flex;
        }
        .img_wrapper{
          max-width: 120px;
          border: dashed 1px #cbcbcb;
          background-color: #FFF;
          -webkit-box-shadow: 0 0px 10px #cbcbcb;
          border-radius: 10px;
          -webkit-border-radius: 10px;
          -moz-border-radius: 10px;
          text-align: center;
          padding: 5px;
          padding-bottom: 3px;
          font-family: Verdana, Geneva, sans-serif;
          font-size: 13px;
          display: inline-block;
          margin-right: 20px;
          margin-bottom: 20px;
          position: relative;
        }
        .img_wrapper img{
          width: 85px;
          height: 85px;
          border: 0px solid;
        }
        .img_wrapper a.remove-btn{
          position: absolute;
          top: 5px;
          background: #FFFFFF;
          padding: 0px 3px;
          right: 5px;
          color: #666666;
          font-weight: bold;
          cursor: pointer;
          display: none;
        }
        .img_wrapper:hover a.remove-btn{
          display: block;
        }
        .dataTables_empty{
          text-align: center;
        }
        .dataTables_filter input{
          width: 250px !important;
        }
        textarea {
            resize: vertical;
        }
        button.close{
          color: #FFF;
          opacity: 1;
        }
        button.close:hover{
          color: #FFF;
          opacity: 1;
        }

    </style>
</head>
<body>
     <div id="app" class="wrapper skin-green sidebar-mini"> 
        @yield('content')
    </div> 
    
    
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- AdminLTE App -->
  <script src="{{ asset('dist/js/app.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>
  <script src="{{ asset('plugins/notify/notify.min.js') }}"></script>
  <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
  <script src="{{ asset('dist/js/customjs.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  @yield('js')
  <script>
    $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
      console.log(helpPage);
      console.log(message);
      if(message){
        window.location.reload();
      }
    };
  </script>
  
</body>
</html>
