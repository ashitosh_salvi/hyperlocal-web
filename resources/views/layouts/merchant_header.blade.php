
<header class="header white-bg skin-green sidebar-mini">
  <header class="main-header">
      <!-- Logo -->
      <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        {{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-h.png" style="width: 100%;"> --}}
        <img src="{{ asset('dist/img/hyper-logo-h.png') }}" style="width: 100%;">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        {{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-h.png" style="width: 100%;"> --}}
        <img src="{{ asset('dist/img/hyper-logo-h.png') }}" style="width: 100%;">
      </span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" >
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
              
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> --}}
              <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">

                  <?php

                    use Illuminate\Support\Facades\Auth;

                    echo Auth::guard('merchant')->user()->user_name;
                    
                ?>

              </span>
              </a>
              <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                  {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}
                  <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                  <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">Sign out</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  </div>
                  {{-- <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">profile</a>
                  </div> --}}
              </li>
              </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
              <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
          </li>
          </ul>
      </div>
      </nav>
  </header>
</header>

<aside class="main-sidebar skin-green sidebar-mini" style="background-color: #222d32;">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- <div class="user-panel">
      <div class="pull-left image">
        <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Your Name</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Superadmin</a>
      </div>
    </div> -->
   
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="treeview">
        <a href="{{ url('/merchant_dashboard') }}">
          <i class="fa fa-user"></i> <span>Merchant Dashboard</span>
          
        </a>
      </li>
      <li class="treeview">
        <a href="{{ url('/employee_list') }}">
          <i class="fa fa-user"></i> <span>Employee Management</span>
          
        </a>
      </li>
      <li class="treeview">
        <a href="{{ url('/outlet_list') }}">
          <i class="fa fa-user"></i> <span>Outlet Management</span>
          
        </a>
      </li>
      <li class="treeview">
        <a href="{{ url('/offer_list') }}">
          <i class="fa fa-user"></i> <span>Offers Management</span>
          
        </a>
      </li>
      <li class="treeview">
        <a href="{{ url('/account_list')}}">
          <i class="fa fa-user"></i> <span>Account Management</span>
          
        </a>
      </li>
    </ul>

  </section>
  <!-- /.sidebar -->
</aside>