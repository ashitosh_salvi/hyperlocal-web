<header class="header white-bg skin-green sidebar-mini">
  <header class="main-header">
      <!-- Logo -->
      <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        {{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-h.png" style="width: 100%;"> --}}
        <img src="{{ asset('dist/img/hyper-logo-h.png') }}" style="width: 100%;">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        {{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-h.png" style="width: 100%;"> --}}
        <img src="{{ asset('dist/img/hyper-logo-h.png') }}" style="width: 100%;">
      </span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" >
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
              
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> --}}
              <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">
                    <?php
                    
                    use Illuminate\Support\Facades\Auth;
                    
                    echo Auth::guard('outlet')->user()->outlet_user_name;

                    ?>
              </span>
              </a>
              <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                  {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}
                  <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                  <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="{{ url('outlet_logout') }}">Sign out</a>
                  </div>
                  {{-- <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">profile</a>
                  </div> --}}
              </li>
              </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
              <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
          </li>
          </ul>
      </div>
      </nav>
  </header>
</header>

<aside class="main-sidebar skin-green sidebar-mini" style="background-color: #222d32;">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- <div class="user-panel">
      <div class="pull-left image">
        <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Your Name</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Outlet Owner</a>
      </div>
    </div> -->
   
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="treeview">
        <a href="{{ url('/outlet_dashboard') }}">
          <i class="fa fa-user"></i> <span>Outlet Dashboard</span>
          
        </a>
      </li>
      
      <?php
      
      $outlet_type = Auth::guard('outlet')->user()->outlet_type; 
      
      if ( $outlet_type == 'recipe' ) {
      ?>

        <!-- <li id="profile" class="treeview">
            <a href="{{ url('/recipe_category') }}">
            <i class="fa fa-user"></i> <span>Recipe Category Management</span>
            
            </a>
        </li> -->

        {{-- <li class="treeview">
            <a href="{{ url('/menuitem_list') }}">
            <i class="fa fa-user"></i> <span>Menu Item Management</span>
            
            </a>
        </li> --}}

         <li class="treeview">
            <a href="{{ url('/menuitem_grid') }}">
            <i class="fa fa-user"></i> <span>Menu Management</span>
            
            </a>
        </li> 

      <?php } else { ?>

        <li id="profile" class="treeview">
            <a href="{{ url('/category') }}">
            <i class="fa fa-user"></i> <span>Category Management</span>
            
            </a>
        </li>

        <li class="treeview">
            <a href="{{ url('/product_list') }}">
            <i class="fa fa-user"></i> <span>Product Management</span>
            
            </a>
        </li>
    
      <?php } ?>

      <li class="treeview">
        <a href="{{ url('/orders_list') }}">
          <i class="fa fa-user"></i> <span>Order Management</span>
          
        </a>
      </li>
      <li class="treeview">
        <a href="{{ url('/generate_qr') }}">
          <i class="fa fa-qrcode"></i> <span>Geneate QR</span>
          
        </a>
      </li>
    </ul>

  </section>
  <!-- /.sidebar -->
</aside>

<div class="modal fade" id="viewRecivedOrderModal" role="dialog" aria-labelledby="viewRecivedOrderModalLabel" style="z-index:99999">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #000;"><span aria-hidden="true">&times;</span></button>
                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-12" style="font-size: 20px;">
                            <label class="recived_order_id">ORDER ID : HPL2021-0000006</label>
                        </div>
                        <input type="hidden" class="recived_id_order" name="data_id_value" value="">

                        <div class="col-sm-12">
                            <label class="control-label" style="font-weight: bold;">Item Details:</label>
                        </div>

                        <div class="col-sm-12" id="recived_order_items_view">
                            
                        </div>
                        <div class="col-sm-12" id="recived_other_order_details">
                            
                        </div>
                        <div class="col-sm-12" style="text-align:center;margin-top:10px">
                            <button class="btn btn-sm btn-success btn-flat mr-50 rbdaccept" >Accept</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50 rbdreject" >Reject</button>
                        </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
    </div>
</div>


<audio id="ringtone" >
  <source src="{{ asset('plugins/audio/Samsung_Galaxy.mp3') }}" type="audio/mp3" muted>
</audio>


<input type="hidden" id="get_recived_order_id"  value="{{ route('get_recived_order_id')}}" >
<input type="hidden" id="outlet_logout_url"  value="{{ url('outlet_logout')}}" >

<input type="hidden" id="get_order_details"  value="{{ route('get_order_details')}}" >

<input type="hidden" id="update_order_status"  value="{{ route('update_order_status')}}" >

<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js"></script>

<script>
    var firebaseConfig = {
        apiKey: "AIzaSyCApVidr6inkYUHO84GicKlLeNDNpMfReU",
        authDomain: "hyperlocal-23aa0.firebaseapp.com",
        projectId: "hyperlocal-23aa0",
        storageBucket: "hyperlocal-23aa0.appspot.com",
        messagingSenderId: "97793726607",
        appId: "1:97793726607:web:6e2c4847b3cbef2bba78fa",
        measurementId: "G-ZL3WBG1ZJC"
    };
    firebase.initializeApp(firebaseConfig);
    const messaging=firebase.messaging();

    function IntitalizeFireBaseMessaging() {
        messaging
            .requestPermission()
            .then(function () {
                console.log("Notification Permission");
                return messaging.getToken();
            })
            .then(function (token) {
                console.log("Token : "+token);
                let url = "{{ url('set_firebase_notification') }}/"+token;
                $.ajax({
                  url:url,
                  type: "GET",
                  success: function(response){
                    // console.log(response);
                  }
                });
            })
            .catch(function (reason) {
                console.log(reason);
            });
    }

    messaging.onMessage(function (payload) {
        console.log(payload);
        const notificationOption={
            body:payload.notification.body,
            icon:payload.notification.icon
        };

        if(Notification.permission==="granted"){
            var notification=new Notification(payload.notification.title,notificationOption);

            notification.onclick=function (ev) {
                ev.preventDefault();
                window.location = payload.notification.click_action;
                // window.open(payload.notification.click_action);
                notification.close();
            }
        }

    });
    messaging.onTokenRefresh(function () {
        messaging.getToken()
            .then(function (newtoken) {
                console.log("New Token : "+ newtoken);
            })
            .catch(function (reason) {
                console.log(reason);
            })
    })
    IntitalizeFireBaseMessaging();
</script>