@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Role Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_role" data-toggle="modal" data-target="#addRoleModal"><i class="fa fa-plus"></i> Add Role</a>
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadProductModal"><i class="fa fa-plus"></i> Bulk Upload</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                </div>
                
                <div class="box-body">


                    <div class="table-responsive mt-15">
                        
                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button>
                        </div>
                    </div>

                </div>

            </div>

        </section>
    </div>
</section>


<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog" aria-labelledby="addRoleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addRoleModalLabel">Add Role</h4>
        </div>
        <div class="modal-body">

        <!-- Add Role Modal -->
        <form id="createRole" >  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Role Name</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" required>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-3">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea class="form-control" rows="3" name="description" required></textarea>
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog" aria-labelledby="editRoleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editRoleModalLabel">Edit Role</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Role Modal -->
        <form id="editRole">  
            <div class="box-body">
                <input type="hidden" name="role_id" value="">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Role Name</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name_ed" required>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-3">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea class="form-control" rows="3" name="description_ed" required></textarea>
                    </div>
                </div>
                

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="viewRoleModal" tabindex="-1" role="dialog" aria-labelledby="viewRoleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewRoleModalLabel">View Role</h4>
        </div>
        <div class="modal-body">

        <!-- View Role Modal -->
        <form id="viewRole">  
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Role Name</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name_view" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-3">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea class="form-control" rows="3" name="description_view" disabled></textarea>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

@endsection 


@section('js')

<script>
    
  $(function () {
    $('#item_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('get_role_list') }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "description" },
                { "data": "status" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
               bSortable: false,
               aTargets: [ -1 ]
            }],
        });
    
    $('.add_role').on('click',function(){
        $('#createRole').trigger("reset");
        $("#createRole").find('.has-error').removeClass("has-error");
        $("#createRole").find('.has-success').removeClass("has-success");
        $('#createRole').find('.form-control-feedback').remove();
    });

    $("body").on("change",".role_status",function(){
            
        let role_status = $(this).data("status");
        let message = '';
        let role_id = $(this).data("id");

        if ( role_status == 0 ) {
            role_status = 1;
            $(this).data("status", "1");
            message = 'Activated';
        } else {
            role_status = 0;
            $(this).data("status", "0");
            message = 'Deactivated';
        }

        $.ajax({
            type: "POST",
            url: "{{ route('update_role_status')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "role_id": role_id,
                "role_status": role_status
            },
            success: function (r) {
                    
                $.notify("Role "+ message +" Successfully", "success");
                    
            },
            error:function(e){
                Swal.fire(
                    'Error!',
                    'Something went wrong.',
                    'error'
                )
                console.log(e);
            }
        });

    });

    $("#createRole").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#createRole')[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('create_role')}}",
                data: formdata,
                success: function (r) {
                        // console.log(r);
                        $('#item_table').DataTable().ajax.reload();
                        $('#addRoleModal').modal('hide');
                        Swal.fire(
                                    'Success!',
                                    'Role Added Successfully.',
                                    'success'
                                );
                        // $.notify("Role added Successfully", "success");
                        // window.location.reload();
                },
                error:function(e){
                    Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
               
        },
        messages:
        {   
            
        },
        ignore: []

    });

    $("#editRole").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editRole')[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('edit_role')}}",
                data: formdata,
                success: function (r) {
                        $('#item_table').DataTable().ajax.reload();
                        $('#editRoleModal').modal('hide');
                        Swal.fire(
                                    'Success!',
                                    'Role Updated Successfully.',
                                    'success'
                                );
                        // $.notify("Product Updated Successfully", "success");
                        // window.location.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
               
        },
        messages:
        {   
            
        },
        ignore: []

    });

  });

    function getRole(id){
        
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_role')}}",
            data: "id="+id,
            success: function (r) {

                create_edit(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_edit(data){
        
        if(data){

            
            $("input[name=name_ed]").val(data.role.name);
            $("textarea[name=description_ed]").val(data.role.description);       
            $("input[name=role_id]").val(data.role.id);
 
            $('#editRoleModal').modal('show'); 

        }
               

    }

    function getRoleView(id){
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_role')}}",
            data: "id="+id,
            success: function (r) {
                create_view(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_view(data){

        if(data){
            
            $("input[name=name_view]").val(data.role.name);
            $("textarea[name=description_view]").val(data.role.description);       
            $("input[name=role_id]").val(data.role.id);

            $('#viewRoleModal').modal('show'); 
        }
    }

    function delete_role( role_id ) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('delete_role')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "role_id": role_id
                    },
                    success: function (r) {
                            console.log(r);
                            Swal.fire(
                                        'Success!',
                                        'Role Deleted Successfully.',
                                        'success'
                                    );
                            // $.notify("Role Deleted Successfully", "success");
                            $('#item_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            }
        });
  
    }


</script>

@endsection


