@extends('layouts.applist')

@section('content')
@include('layouts.merchant_header')
<style>
.dropdown-submenu {
  position: relative;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -1px;
}
/*#offer_type-error{
    display: none !important;
}*/
</style>
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Offer Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_offer btn-flat mr-15" data-toggle="modal" data-target="#addOfferModal"><i class="fa fa-plus"></i> Add Offer</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadOfferModal"><i class="fa fa-plus"></i> Bulk Upload</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                   

                    <div class="table-responsive mt-15">

                        <table id="offer_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Offer Name</th>
                                    <th>Offer Type</th>
                                    <th>Promo Code</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>

            </div>

        </section>
    </div>
</section>



<div class="modal fade" id="addOfferModal" role="dialog" aria-labelledby="addOfferModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addOfferModalLabel">Add Offer</h4>
        </div>
        <div class="modal-body">

        <!-- Add Offer Modal -->
        <form id="createOffer" >  
            <div class="box-body">
                @csrf
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Offer Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="offer_name" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Offer Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="offer_type" id="offer_type" required>
                            <option value="">Select Offer Type</option>
                            <option value="delivery_based">Delivery Based</option>
                            <option value="promo_code_based">Promo Code Based</option>
                        </select>
                        {{-- <span id="offer_type-error_clone" class="help-block error"></span> --}}
                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea name="offer_description" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Select Outlet</label>
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control select2" id="outlet_id" name="outlet_id[]" multiple>
                            <option value="">Select Outlet</option>
                            @foreach($outlet_list as $key => $outlet_val)
                                <option value="{{ $outlet_val['id'] }}">{{ $outlet_val['outlet_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mt-15 promo_box" style="display:none;">
                    <div class="col-sm-2">
                        <label class="control-label">Promo Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="promo_code">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Discount Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="discount_type" id="discount_type">
                            <option value="">Select discount Type</option>
                            <option value="percentage">Percentage(%)</option>
                            <option value="flat_amount">Flat Amount</option>
                        </select>
                    </div>
                    
                </div>
                

                <div class="row mt-15 offer_value_box" style="display:none;">
                    <div class="col-sm-2">
                        <label class="control-label disc_percent">Discount (%)</label>
                        <label class="control-label disc_flat" style="display: none;">Discount (₹)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="discount" min="0" max="100">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2" >
                        <label class="control-label">Min Amount</label>
                    </div>
                    <div class="col-sm-3" >
                        <input type="number" class="form-control" name="amount" value="0" min="0" max="999999999">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Expiry Date</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="expiry_date" class="form-control pull-right" id="expiry_date">
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="max_discount_box" style="display: none">
                        <div class="col-sm-2">
                            <label class="control-label">Max Discount (₹)</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="number" class="form-control"  value="0" name="max_discount" max="999999999">
                        </div>
                    </div>
                </div>

                <div class="row mt-15" style="margin-bottom: 15px;">
                    <div class="col-sm-5">
                        <label for="fileslx">Upload Offer Image here</label>
                        <input type="file" id="offer_image" class="form-control offer_doc" name="image_url">
                        
                    </div>
                </div>

                <div class="gallery">

                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editOfferModal" role="dialog" aria-labelledby="editOfferModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editOfferModalLabel">Edit Offer</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Offer Modal -->
        <form id="editOffer" >  
            <div class="box-body">
                @csrf
                <input type="hidden" name="offer_id" value="">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Offer Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="offer_name_ed" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Offer Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="offer_type_ed" id="offer_type_ed" required>
                            <option value="">Select Offer Type</option>
                            <option value="delivery_based">Delivery Based</option>
                            <option value="promo_code_based">Promo Code Based</option>
                        </select>
                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea name="offer_description_ed" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Select Outlet</label>
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control select2" id="outlet_id_ed" name="outlet_id_ed[]" multiple>
                            <option value="">Select Category</option>
                            @foreach($outlet_list as $key => $outlet_val)
                                <option value="{{ $outlet_val['id'] }}">{{ $outlet_val['outlet_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mt-15 promo_box" style="display:none;">
                    <div class="col-sm-2">
                        <label class="control-label">Promo Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="promo_code_ed">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Discount Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="discount_type_ed" id="discount_type_ed">
                            <option value="">Select discount Type</option>
                            <option value="percentage">Percentage(%)</option>
                            <option value="flat_amount">Flat Amount</option>
                        </select>
                    </div>
                </div>
                

                <div class="row mt-15 offer_value_box" style="display:none;">
                    <div class="col-sm-2">
                        <label class="control-label">Discount (%)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="discount_ed" min="0" max="100">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Min Amount</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="amount_ed" min="0" max="999999999">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Expiry Date</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="expiry_date_ed" class="form-control pull-right" id="expiry_date_ed">
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="max_discount_box" style="display: none">
                        <div class="col-sm-2">
                            <label class="control-label">Max Discount (₹)</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" name="max_discount_ed" min="0" max="999999999">
                        </div>
                    </div>

                </div>

                <div class="row mt-15" style="margin-bottom: 15px;">
                    <div class="col-sm-5">
                        <label for="fileslx">Upload Offer Image here</label>
                        <input type="file" id="offer_image_ed" class="form-control offer_doc" name="image_url_ed">
                        
                    </div>
                </div>

                <div class="box_of_image">

                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="viewOfferModal" role="dialog" aria-labelledby="viewOfferModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewOfferModalLabel">View Offer</h4>
        </div>
        <div class="modal-body">

        <!-- View Offer Modal -->
        <form >  
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Offer Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="offer_name_view" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Offer Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="offer_type_view" id="offer_type_view" required>
                            <option value="">Select Offer Type</option>
                            <option value="delivery_based">Delivery Based</option>
                            <option value="promo_code_based">Promo Code Based</option>
                            <option value="product_based">Product Based</option>
                        </select>
                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea name="offer_description_view" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Select Outlet</label>
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control select2" id="outlet_id_view" name="outlet_id_view[]" multiple>
                            <option value="">Select Category</option>
                            @foreach($outlet_list as $key => $outlet_val)
                                <option value="{{ $outlet_val['id'] }}">{{ $outlet_val['outlet_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mt-15 promo_box" style="display:none;">
                    <div class="col-sm-2">
                        <label class="control-label">Promo Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="promo_code_view">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Discount Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="discount_type_view" id="discount_type_view">
                            <option value="">Select discount Type</option>
                            <option value="percentage">Percentage(%)</option>
                            <option value="flat_amount">Flat Amount</option>
                        </select>
                    </div>
                </div>
                

                <div class="row mt-15 offer_value_box" style="display:none;">
                    <div class="col-sm-2">
                        <label class="control-label">Discount (%)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="discount_view" min="0" max="100">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Amount</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="amount_view" min="0" max="999999999" >
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Expiry Date</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="expiry_date_view" class="form-control pull-right" id="expiry_date_view">
                        </div>
                    </div>
                </div>

                <div class="row mt-15" style="margin-bottom: 15px;">
                    <div class="col-sm-5">
                        <label for="fileslx">Upload Offer Image here</label>
                        <input type="file" id="offer_image_view" class="form-control offer_doc" name="image_url_view">
                        
                    </div>
                </div>

                <div class="box_of_image_view">
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

@endsection 


@section('js')

<script>
    $(function() {
        $('#expiry_date').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            // minDate: 0,
            startDate: '-0m'
        });
        $('#expiry_date_ed').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            // minDate: 0,
            startDate: '-0m'
        });
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        let img_html = '<div class="img_wrapper">'+
                                '<img src="'+ event.target.result +'" alt="Offer Image">'+
                                // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
                        $(placeToInsertImagePreview).append(img_html);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#offer_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                        "url": "{{ url('get_offer_list') }}",
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
            "columns": [
                { "data": "id" },
                { "data": "offer_name" },
                { "data": "offer_type" },
                { "data": "promo_code" },
                { "data": "status" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }],
        });

        $("body").on('change','#offer_image', function() {
            $('.gallery').children().remove();
            imagesPreview(this, '.gallery');
        });
        $("body").on('change','#offer_image_ed', function() {
            $('.box_of_image').children().remove();
            imagesPreview(this, '.box_of_image');
        });

       
        
        
        
    });

    $.validator.addMethod(
        "check_offer",
            function(value, element) {
                
                let offer_name = value;
                let offer_id = $('input[name=offer_id]').val();
                let isSuccess = false;
                
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "{{ route('check_offer_name')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "offer_name": offer_name,
                        "offer_id": offer_id,

                    },
                    success: function (r) {
                        console.log(r);

                        if ( r.message == 'Offer already Exist' ) {
                            isSuccess = false;
                        } else {
                            isSuccess = true;
                        }

                    },
                    error:function(e){
                        console.log(e);
                    }
                });

                return isSuccess;
            }
    );

    jQuery.validator.addMethod("noSpace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    $("#editOffer").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editOffer')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('edit_offer')}}",
                data: formdata,
                success: function (r) {
                        // console.log(r);
                        $('#editOfferModal').modal('hide');
                        Swal.fire(
                            'Success!',
                            'Offer Updated Successfully.',
                            'success'
                        );
                        // $.notify("Offer Updated Successfully", "success");
                        $('#offer_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
            offer_name_ed: {
                required: true,
                check_offer: true,
            },
        },
        messages:
        {   
            offer_name_ed: {
                required: "Offer name is required",
                check_offer: "Offer Already Exists",
            },
        },
        ignore: []

    });


    function getOffer(id){
        $("#category_id_ed").val(null).trigger("change"); 
        $("#sub_category_id_ed").html($('<option>', {value: '', text: 'Select Sub Category'})).val(null).trigger("change"); 
        $("#child_category_id_ed").html($('<option>', {value: '', text: 'Select Child Category'})).val(null).trigger("change"); 
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_offer')}}",
            data: "id="+id,
            success: function (r) {
                // console.log(r);
                create_edit(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_edit(data){
        
        if(data){

            
            $("input[name=offer_id]").val(data.offer.id);

            $("input[name=offer_name_ed]").val(data.offer.offer_name);
            $("#offer_type_ed").val( data.offer.offer_type ).trigger("change");
            $("#discount_type_ed").val( data.offer.discount_type ).trigger("change");
            $("textarea[name=offer_description_ed]").val(data.offer.offer_description);
            $("input[name=discount_ed]").val(data.offer.discount);
            $("input[name=amount_ed]").val(data.offer.amount);
            $("input[name=expiry_date_ed]").val(data.offer.expiry_date);
            $("input[name=promo_code_ed]").val(data.offer.promo_code);  
            $("input[name=max_discount_ed]").val(data.offer.max_discount);
            $("select#outlet_id_ed").val(JSON.parse( data.offer.outlet_id )).trigger("change");
            
            let offer_img = "{{URL::asset('/images')}}/" +  data.offer.image_url;
            let img_html = '';
            if(data.offer.image_url != ''){
                img_html = '<div class="img_wrapper">'+
                            '<img src="'+ offer_img +'" alt="Offer Image">'+
                            '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                        '</div>'; 
            }
                       
            $('.box_of_image').html(img_html);    

        }
        $('#editOfferModal').modal('show');        

    }

    function getOfferView(id){
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_offer')}}",
            data: "id="+id,
            success: function (r) {
                // console.log(r);
                create_view(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_view(data){

        if(data){

            $("input[name=offer_name_view]").val(data.offer.offer_name);
            $("#offer_type_view").val( data.offer.offer_type ).trigger('change');
            $("#discount_type_view").val( data.offer.discount_type ).trigger('change');
            $("textarea[name=offer_description_view]").val(data.offer.offer_description);
            $("input[name=discount_view]").val(data.offer.discount);
            $("input[name=amount_view]").val(data.offer.amount);
            $("input[name=expiry_date_view]").val(data.offer.expiry_date);
            $("input[name=promo_code_view]").val(data.offer.promo_code);
            $("select#outlet_id_view").val(JSON.parse( data.offer.outlet_id )).trigger('change');
            
            let offer_img = "{{URL::asset('/images')}}/" +  data.offer.image_url;
            let img_html = '';
            if(data.offer.image_url != ''){
                img_html = '<div class="img_wrapper">'+
                                '<img src="'+ offer_img +'" alt="Offer Image">'+
                                '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';  
            }      
            $('.box_of_image_view').html(img_html);    

            }
        $('#viewOfferModal').modal('show');

    }

    $(function() {
        $(".select2").select2();
        
        $("select#offer_type, select#offer_type_ed, select#offer_type_view").on('change', function(){
            
            let offer_type = $(this).val();
            if(offer_type == 'delivery_based'){
                $('.promo_box, .offer_value_box, .max_discount_box').hide();
                $('input[name=promo_code], input[name=discount], input[name=amount], input[name=max_discount]').val('');
                $('input[name=promo_code_ed], input[name=discount_ed], input[name=amount_ed],input[name=max_discount_ed]').val('');
                $('input[name=promo_code_view], input[name=discount_view], input[name=amount_view]').val('');
                $('select[name=discount_type], select[name=discount_type_ed], select[name=discount_type_view]').val(null).trigger("change");
            }
            else if(offer_type == 'promo_code_based'){
                $('.promo_box, .offer_value_box, .max_discount_box').show();
                $("select[name=discount_type], select[name=discount_type_ed], select[name=discount_type_view]").val('percentage').trigger("change");
            }
            else{
                $('.promo_box, .offer_value_box','.max_discount_box').hide();
                $('input[name=promo_code], input[name=discount], input[name=amount], input[name=max_discount]').val('');
                $('input[name=promo_code_ed], input[name=discount_ed], input[name=amount_ed],input[name=max_discount_ed]').val('');
                $('input[name=promo_code_view], input[name=discount_view], input[name=amount_view]').val('');
                $('input[name=discount_type], input[name=discount_type_ed], input[name=discount_type_view]').val(null).trigger("change");
            }

        });
        $("select#discount_type, select#discount_type_ed, select#discount_type_view").on('change', function(){
            let disc_type = $(this).val();
            if(disc_type == 'percentage'){
                $('.disc_percent').show();
                $('.disc_flat').hide();
            }
            else if(disc_type == 'flat_amount'){
                $('.disc_percent').hide();
                $('.disc_flat').show();
            }
        });


    });

    $(document).ready(function(){

        $('.add_offer').on('click',function(){
            $('#createOffer').trigger("reset");
            $("#createOffer").find('span.help-block').remove();
            $("#createOffer").find('.help-block').removeClass("help-block");
            $("#createOffer").find('.error').removeClass("error");
            $("#createOffer").find('.has-success').removeClass("has-success");
            $('#createOffer').find('.form-control-feedback').remove();
            $('.gallery').children().remove();
            $("#offer_type").val(null).trigger("change"); 
             
        });
        

        let createOfferValidator = $("#createOffer").validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#createOffer')[0]);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('create_offer')}}",
                    data: formdata,
                    success: function (r) {
                            // console.log(r);
                            $('#addOfferModal').modal('hide');
                            Swal.fire(
                                'Success!',
                                'Offer Added Successfully.',
                                'success'
                            );
                            // $.notify("Offer added Successfully", "success");
                            $('#offer_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            
            rules:
            {
                offer_name: {
                    required: true,
                    check_offer: true,
                },
            },
            messages:
            {	
                offer_name: {
                    required: "Offer name is required",
                    check_offer: "Offer Already Exists",
                },
            },
            ignore: []

        });

        $.ajax({
            type: "GET",
            url: "{{ route('get_grossery_cat')}}",
            success: function (r) {
                // console.log(r);
                let category = r.category;
                $.each( category, function(index, value) {
                        $("#category_id").append($('<option>', {value: value.id, text: value.category_name}));
                        $("#category_id_ed").append($('<option>', {value: value.id, text: value.category_name}));
                        $("#category_id_view").append($('<option>', {value: value.id, text: value.category_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });

        $(".dataTables_filter input").css({ "width": "250px" });

    });

    $(window).load(function(){
            
        // Update Offer Status

        $(document).on('click','input[name="offer_status"]',function(){
                        
            let offer_status = $(this).data("status");
            let message = '';
            let offer_id = $(this).data("id");
    
            if ( offer_status == 0 ) {
                offer_status = 1;
                $(this).data("status", "1");
                message = 'Activated';
            } else {
                offer_status = 0;
                $(this).data("status", "0");
                message = 'Deactivated';
            }
    
            $.ajax({
                type: "POST",
                url: "{{ route('update_offer_status')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "offer_id": offer_id,
                    "offer_status": offer_status
                },
                success: function (r) {
                        
                        $.notify("Offer "+ message +" Successfully", "success");
                        // $('#offer_table').DataTable().ajax.reload();
                        // $('#past_order_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
    
        });

    });


function delete_offer( offer_id ) {

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{ route('delete_offer')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "offer_id": offer_id
                },
                success: function (r) {
                    Swal.fire(
                        'Success!',
                        'Offer Deleted Successfully.',
                        'success'
                    );
                    // $.notify("Offer Deleted Successfully", "success");
                    $('#offer_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
        }
    });

}

</script>

@endsection


