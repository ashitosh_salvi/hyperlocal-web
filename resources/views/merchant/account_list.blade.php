@extends('layouts.applist')

@section('content')
@include('layouts.merchant_header')
<style>
.dropdown-submenu {
  position: relative;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -1px;
}
/*#offer_type-error{
    display: none !important;
}*/
</style>
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Account Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_account btn-flat mr-15" data-toggle="modal" data-target="#addAccountModal"><i class="fa fa-plus"></i> Add Account</a>
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                   

                    <div class="table-responsive mt-15">

                        <table id="account_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Razorpay Account Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Account Number</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>

            </div>

        </section>
    </div>
</section>



<div class="modal fade" id="addAccountModal" role="dialog" aria-labelledby="addAccountModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addAccountModalLabel">Add Account</h4>
        </div>
        <div class="modal-body">

        <!-- Add Account Modal -->
        <form id="createAccount" >  
            <div class="box-body">
                @csrf
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" name="email" required>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Business Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="business_name" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Business Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="business_type" required>
                            <option value="">---select---</option>
                            <option value="llp">LLP</option>
                            <option value="ngo">Ngo</option>
                            <option value="individual">Individual</option>
                            <option value="partnership">Partnership</option>
                            <option value="proprietorship">Proprietorship</option>
                            <option value="public_limited">Public Limited</option>
                            <option value="private_limited">Private Limited</option>
                            <option value="trust">Trust</option>
                            <option value="society">Society</option>
                            <option value="not_yet_registered">Not Yet Registered </option>
                            <option value="educational_institutes">Educational Institutes</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">IFSC Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="ifsc_code" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Beneficiary Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="beneficiary_name" required>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Account Number</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="account_number" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Account Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="account_type" required>
                            <option value="">---select---</option>
                            <option value="current">Current</option>
                            <option value="savings">Savings</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-12" align="center">
                        <input type="checkbox" name="tnc_accepted" id="tnc_accepted" value="1">
                        &nbsp;&nbsp;Please accept the terms and conditions
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="viewAccountModal" role="dialog" aria-labelledby="viewAccountModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewAccountModalLabel">View Account</h4>
        </div>
        <div class="modal-body">

        <!-- View Account Modal -->
        <form >  
            <div class="box-body">
            <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="name" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" name="email" disabled>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Business Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="business_name" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Business Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="business_type" id="business_type" disabled>
                            <option value="">---select---</option>
                            <option value="llp">LLP</option>
                            <option value="ngo">Ngo</option>
                            <option value="individual">Individual</option>
                            <option value="partnership">Partnership</option>
                            <option value="proprietorship">Proprietorship</option>
                            <option value="public_limited">Public Limited</option>
                            <option value="private_limited">Private Limited</option>
                            <option value="trust">Trust</option>
                            <option value="society">Society</option>
                            <option value="not_yet_registered">Not Yet Registered </option>
                            <option value="educational_institutes">Educational Institutes</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">IFSC Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="ifsc_code" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Beneficiary Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="beneficiary_name" disabled>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Account Number</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="account_number" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Account Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="account_type" disabled>
                            <option value="">---select---</option>
                            <option value="current">Current</option>
                            <option value="savings">Savings</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-12" align="center">
                        <input type="checkbox" name="tnc_accepted" id="tnc_accepted" value="1" disabled>
                        &nbsp;&nbsp;Terms and conditions have been accepted or not
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

@endsection 


@section('js')

<script>
    $(function() {
        $('#account_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                        "url": "{{ url('get_account_list') }}",
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
            "columns": [
                { "data": "id" },
                { "data": "rozorpay_account_id" },
                { "data": "name" },
                { "data": "email" },
                { "data": "account_number" },
                { "data": "status" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }],
        });        
        
    });

    $.validator.addMethod(
        "check_account",
            function(value, element) {
                
                let email = value;
                let account_id = $('input[name=account_id]').val();
                let isSuccess = false;
                
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "{{ route('check_account_email')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "email": email,
                        "account_id": account_id,

                    },
                    success: function (r) {
                        console.log(r);

                        if ( r.message == 'Account email already Exist' ) {
                            isSuccess = false;
                        } else {
                            isSuccess = true;
                        }

                    },
                    error:function(e){
                        console.log(e);
                    }
                });

                return isSuccess;
            }
    );

    jQuery.validator.addMethod("noSpace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    function getAccountView(id){
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_account')}}",
            data: "id="+id,
            success: function (r) {
                // console.log(r);
                create_view(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_view(data){
        if(data){
            $("input[name=name]").val(data.account.name);
            $("input[name=email]").val(data.account.email);
            $("input[name=business_name]").val(data.account.business_name);
            $("input[name=ifsc_code]").val(data.account.ifsc_code);
            $("input[name=beneficiary_name]").val(data.account.beneficiary_name);
            $("input[name=account_number]").val(data.account.account_number);
            $("input[name=account_type]").val(data.account.account_type);
            $("select#business_type").val(data.account.business_type);  
            if(data.account.tnc_accepted=='1'){
                $("#tnc_accepted").prop("checked", true);
            }
        }
        $('#viewAccountModal').modal('show');

    }

    $(function() {
        $(".select2").select2();

    });

    $(document).ready(function(){

        $('.add_account').on('click',function(){
            $('#createAccount').trigger("reset");
            $("#createAccount").find('span.help-block').remove();
            $("#createAccount").find('.help-block').removeClass("help-block");
            $("#createAccount").find('.error').removeClass("error");
            $("#createAccount").find('.has-success').removeClass("has-success");
            $('#createAccount').find('.form-control-feedback').remove();             
        });
        

        let createAccountValidator = $("#createAccount").validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#createAccount')[0]);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('create_account')}}",
                    data: formdata,
                    success: function (r) {
                            // console.log(r);
                            $('#addAccountModal').modal('hide');
                            Swal.fire(
                                'Success!',
                                'Account Added Successfully.',
                                'success'
                            );
                            // $.notify("Account added Successfully", "success");
                            $('#account_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            
            rules:
            {
                email: {
                    required: true,
                    check_account: true,
                },
            },
            messages:
            {	
                email: {
                    required: "Account email is required",
                    check_account: "Account email already exists",
                },
            },
            ignore: []

        });

        $(".dataTables_filter input").css({ "width": "250px" });

    });

    $(window).load(function(){
            
        // Update Account Status

        $(document).on('click','input[name="offer_status"]',function(){
                        
            let offer_status = $(this).data("status");
            let message = '';
            let account_id = $(this).data("id");
    
            if ( offer_status == 0 ) {
                offer_status = 1;
                $(this).data("status", "1");
                message = 'Activated';
            } else {
                offer_status = 0;
                $(this).data("status", "0");
                message = 'Deactivated';
            }
    
            $.ajax({
                type: "POST",
                url: "{{ route('update_offer_status')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "account_id": account_id,
                    "offer_status": offer_status
                },
                success: function (r) {
                        
                        $.notify("Offer "+ message +" Successfully", "success");
                        // $('#account_table').DataTable().ajax.reload();
                        // $('#past_order_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
    
        });

    });


function delete_account( account_id ) {

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "{{ route('delete_account')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "account_id": account_id
                },
                success: function (r) {
                    Swal.fire(
                        'Success!',
                        'Account Deleted Successfully.',
                        'success'
                    );
                    // $.notify("Account Deleted Successfully", "success");
                    $('#account_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
        }
    });

}

</script>

@endsection


