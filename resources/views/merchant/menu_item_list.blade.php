@extends('layouts.applist')

@section('content')
@include('layouts.outlet_header')
<style type="text/css">
    #item_table tr td:first {
        max-width: 200px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    
</style>
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Menu Item Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_menuitem btn-flat mr-15" data-toggle="modal" data-target="#addMenuItemModal"><i class="fa fa-plus"></i> Add Menu Item</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadProductModal"><i class="fa fa-plus"></i> Bulk Upload</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    {{-- <h3 class="box-title">Manage Menu Item</h3> --}}
                    <!-- <a href="{{ url('/product_create') }}" class="btn btn-success text-right" style="float: right;margin-right: 5px;"><i class="fa fa-plus"></i> Add Menu Item</a> -->
                </div>
                
                <div class="box-body">

                    <!-- <div class="row">
                        <div class="col-sm-1">
                            <label class="control-label">User Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-1">
                            <label class="control-label">Status</label>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option>Authorised</option>
                                <option>Unauthorised</option>
                                <option>Deleted</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-1">
                            <label class="control-label">Role</label>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <a class="btn btn-sm btn-success btn-flat mr-50" href="#">Search</a>
                        <a class="btn btn-sm btn-danger btn-flat mr-50" href="#">Reset</a>
                        <a class="btn btn-sm btn-warning btn-flat mr-50" href="{{ url('/product_create') }}" >Add New</a>
                        </div>
                    </div> -->

                    <div class="table-responsive mt-15">
                        
                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    {{-- <th></th> --}}
                                    <th>Menu Item Name</th>
                                    <th>Section</th>
                                    <th>Category</th>
                                    <th>Spice Level</th>
                                    <th>From </th>
                                    <th>To</th>
                                    <th>price</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    <div class="row mt-15">
                        <div class="col-sm-12">
                        {{-- <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button> --}}
                        </div>
                    </div>

                </div>

            </div>

        </section>
    </div>
</section>


<div class="modal fade" id="addMenuItemModal" role="dialog" aria-labelledby="addMenuItemModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addMenuItemModalLabel">Add Menu Item</h4>
        </div>
        <div class="modal-body">

        <!-- Add Menu Item Modal -->
        <form id="createMenuItem" >  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Menu Item Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Menu Item Image</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="file" class="form-control" name="image_url_old" id="fileinput" data-maxheight="260" data-maxwidth="260">
                        <input type="hidden" name="image_url" class="resize_url" value="">
                    </div>
                </div>

                

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Item Description</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="description"></textarea>
                    </div>
                    <div class="col-sm-1"></div> 
                    <div class="col-sm-4">
                        <div class="show_img">
                        </div>
                        
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Category Type</label>
                    </div>
                    <div class="col-sm-4">
                        <label class="food">
                            <input type="radio" class="minimal-red" name="category_type" value="food" required>
                            Food
                        </label>
                        <label class="beverages">
                            <input type="radio" class="minimal-red" name="category_type" value="beverages" required>
                            Beverages
                        </label>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3">

                        <select class="form-control select2" name="category_id" id="category_id" required>
                            <option value="">Select Category</option>
                            {{-- @foreach($main_category as $key => $main_cat_val)
                                <option value="{{ $main_cat_val['id'] }}">{{ $main_cat_val['category_name'] }}</option>
                            @endforeach --}}
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Sub Category</label>
                        
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="sub_category_id">
                            <option value="0">Select Sub Category</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Section</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="section" required>
                            <option value="">Select Section</option>
                            
                            @foreach($food_section as $key => $s_value)
                                <option value="{{ $s_value['id'] }}">{{ $s_value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Additional Info</label>
                    </div>
                    <div class="col-sm-3">
                        <label>
                          <input type="checkbox" class="minimal-red" name="add_info[]" value="jain_food">
                          Jain Food
                        </label>
                        <label>
                          <input type="checkbox" class="minimal-red" name="add_info[]" value="rest_spl">
                          Restaurant Spl*
                        </label>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Food Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="food_cat" required>
                            <option value="">Select food Category</option>

                            @foreach($food_cat as $key => $f_value)
                                <option value="{{ $f_value['id'] }}">{{ $f_value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Availability</label>
                    </div>
                    <div class="col-sm-10">
                        {{-- <label>
                          <input type="checkbox" class="minimal-red" name="availability[]" value="dine_in" disabled="disabled">
                          Dine-In
                        </label> --}}
                        <label>
                          <input type="checkbox" class="minimal-red" name="availability[]" value="home_delivery" >
                          Home Delivery
                        </label>
                        {{-- <label>
                          <input type="checkbox" class="minimal-red" name="availability[]" value="take_away" disabled="disabled">
                          Take Away
                        </label>
                        <label>
                          <input type="checkbox" class="minimal-red" name="availability[]" value="3rd_party_order" disabled="disabled">
                          3rd Party Orders
                        </label> --}}
                    </div>
                    
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Available Time</label>
                    </div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="from_time">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> From
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="to_time">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> To
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Spice Level:</label>
                    </div>
                    <div class="col-sm-4">
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level" value="non_spicy">
                          Non Spicy
                        </label>
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level" value="medium">
                          Medium
                        </label>
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level" value="spicy">
                          Spicy
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">KOT group</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="kot_group">
                    </div>
                    
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="price" min="1" max="9999999999999" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Charges(GST/VAT)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="charges_gst">
                    </div>
                </div>

                {{-- <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Special Instructions:</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="special_instruction"></textarea>
                    </div>
                </div> --}}
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Serving Time:</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" name="serving_time">

                            <div class="input-group-addon">
                              Mins
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Serves:</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="serves">
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editMenuItemModal" role="dialog" aria-labelledby="editMenuItemModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editMenuItemModalLabel">Edit Menu Item</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Menu Item Modal -->
        <form id="editMenuItem">  
            <div class="box-body">
                <input type="hidden" name="menuitem_id" value="">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Menu Item Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="name_ed" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Menu Item Image</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="file" class="form-control" id="image_url_edit" name="image_url_ed_old">
                        <input type="hidden" class="resize_url_edit"  name="image_url_ed" value="">
                    </div>
                </div>

                

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Item Description</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="description_ed" required></textarea>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-4">
                        <div class="show_img_ed">
                        </div>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Category Type</label>
                    </div>
                    <div class="col-sm-4">
                        <label class="food">
                            <input type="radio" class="minimal-red" name="category_type_ed" value="food"  required>
                            Food
                        </label>
                        <label class="beverages">
                            <input type="radio" class="minimal-red" name="category_type_ed" value="beverages" required>
                            Beverages
                        </label>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="category_id_ed" required>
                            <option value="">Select Category</option>
                            
                            @foreach($main_category as $key => $main_cat_val)
                                <option value="{{ $main_cat_val['id'] }}">{{ $main_cat_val['category_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Sub Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="sub_category_id_ed">
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Section</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="section_ed" required>
                            <option value="">Select Section</option>
                            
                            @foreach($food_section as $key => $s_value)
                                <option value="{{ $s_value['id'] }}">{{ $s_value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Additional Info</label>
                    </div>
                    <div class="col-sm-3">
                        <label>
                            <input type="checkbox" class="minimal-red add_info_ed" name="add_info_ed[]" value="jain_food">
                            Jain Food
                        </label>
                        <label>
                            <input type="checkbox" class="minimal-red add_info_ed" name="add_info_ed[]" value="rest_spl">
                            Restaurant Spl*
                        </label>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Food Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="food_cat_ed" required>
                            <option value="">Select food Category</option>
                            
                            @foreach($food_cat as $key => $f_value)
                                <option value="{{ $f_value['id'] }}">{{ $f_value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Availability</label>
                    </div>
                    <div class="col-sm-10">
                        {{-- <label>
                          <input type="checkbox" class="minimal-red availability_ed" name="availability_ed[]" value="dine_in" disabled="disabled">
                          Dine-In
                        </label> --}}
                        <label>
                          <input type="checkbox" class="minimal-red availability_ed" name="availability_ed[]" value="home_delivery" >
                          Home Delivery
                        </label>
                        {{-- <label>
                          <input type="checkbox" class="minimal-red availability_ed" name="availability_ed[]" value="take_away" disabled="disabled">
                          Take Away
                        </label>
                        <label>
                          <input type="checkbox" class="minimal-red availability_ed" name="availability_ed[]" value="3rd_party_order" disabled="disabled">
                          3rd Party Orders
                        </label> --}}
                    </div>
                    
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Available Time</label>
                    </div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="from_time_ed">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> From
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="to_time_ed">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> To
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Spice Level:</label>
                    </div>
                    <div class="col-sm-4">
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level_ed" value="non_spicy">
                          Non Spicy
                        </label>
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level_ed" value="medium">
                          Medium
                        </label>
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level_ed" value="spicy">
                          Spicy
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">KOT group</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="kot_group_ed">
                    </div>
                    
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="price_ed" min="1" max="999999999999" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Charges(GST/VAT)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="charges_gst_ed">
                    </div>
                </div>

                {{-- <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Special Instructions:</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="special_instruction_ed"></textarea>
                    </div>
                </div> --}}
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Serving Time:</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" name="serving_time_ed">

                            <div class="input-group-addon">
                              Mins
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Serves:</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="serves_ed">
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="viewMenuItemModal" role="dialog" aria-labelledby="viewMenuItemModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewMenuItemModalLabel">View Menu Item</h4>
        </div>
        <div class="modal-body">

        <!-- View Menu Item Modal -->
        <form id="viewMenuItem">  
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Menu Item Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="name_view" disabled required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Menu Item Image</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="file" class="form-control" name="image_url_view" disabled>
                        
                    </div>
                    
                </div>

                

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Item Description</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="description_view" disabled required></textarea>
                    </div>
                    <div class="col-sm-1">
                    </div>
                    <div class="col-sm-4">
                        <div class="show_img_view">
                        </div>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3">
                        <input class="form-control" name="category_id_view" disabled >
                            
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Sub Category</label>
                    </div>
                    <div class="col-sm-3">
                        <input class="form-control" name="sub_category_id_view" disabled >
                          
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Section</label>
                    </div>
                    <div class="col-sm-3">
                        <input class="form-control" name="section_view" disabled required>
                    
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Additional Info</label>
                    </div>
                    <div class="col-sm-3">
                        <label>
                          <input type="checkbox" class="minimal-red add_info_view" disabled name="add_info_ed[]" value="jain_food">
                          Jain Food
                        </label>
                        <label>
                          <input type="checkbox" class="minimal-red add_info_view" disabled name="add_info_ed[]" value="rest_spl">
                          Restaurant Spl*
                        </label>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Food Category</label>
                    </div>
                    <div class="col-sm-3">
                        <input class="form-control" name="food_cat_view" disabled required>
                            
                        
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Availability</label>
                    </div>
                    <div class="col-sm-10">
                        {{-- <label>
                          <input type="checkbox" class="minimal-red availability_view" disabled name="availability_ed[]" value="dine_in" disabled="disabled">
                          Dine-In
                        </label> --}}
                        <label>
                          <input type="checkbox" class="minimal-red availability_view" disabled name="availability_ed[]" value="home_delivery"  >
                          Home Delivery
                        </label>
                        {{-- <label>
                          <input type="checkbox" class="minimal-red availability_view" disabled name="availability_ed[]" value="take_away" disabled="disabled">
                          Take Away
                        </label>
                        <label>
                          <input type="checkbox" class="minimal-red availability_view" disabled name="availability_ed[]" value="3rd_party_order" disabled="disabled">
                          3rd Party Orders
                        </label> --}}
                    </div>
                    
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Available Time</label>
                    </div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="from_time_view" disabled>

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> From
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="to_time_view" disabled>

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> To
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Spice Level:</label>
                    </div>
                    <div class="col-sm-4">
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level_view" disabled value="non_spicy">
                          Non Spicy
                        </label>
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level_view" disabled value="medium">
                          Medium
                        </label>
                        <label>
                          <input type="radio" class="minimal-red" name="spice_level_view" disabled value="spicy">
                          Spicy
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <label class="control-label">KOT group</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="kot_group_view" disabled>
                    </div>
                    
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="price_view" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Charges(GST/VAT)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="charges_gst_view" disabled>
                    </div>
                </div>

                {{-- <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Special Instructions:</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="special_instruction_view" disabled></textarea>
                    </div>
                </div> --}}
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Serving Time:</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" name="serving_time_view" disabled>

                            <div class="input-group-addon">
                              Mins
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Serves:</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="serves_view" disabled>
                    </div>
                </div>

                <div class="row mt-15 text-center">
                
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div id="preview"></div>

<form id="form"></form>

@endsection 


@section('js')


<script src="{{ url('/') }}/jquery/scripting.js?v=2"></script>


<script>
    
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    let img_html = '<div class="img_wrapper">'+
                            '<img src="'+ event.target.result +'" alt=" Image">'+
                            // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                        '</div>';
                    $(placeToInsertImagePreview).append(img_html);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $("body").on('change','input[name=image_url]', function() {
        $('.show_img').children().remove();
        imagesPreview(this, '.show_img');
    });
    $("body").on('change','input[name=image_url_ed]', function() {
        $('.show_img_ed').children().remove();
        imagesPreview(this, '.show_img_ed');
    });
      
  $(function () {

    $(".select2").select2();
    $('#item_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('get_menuitem_list') }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "name" },
                { "data": "section" },
                { "data": "category_name" },
                { "data": "spice_level" },
                { "data": "from" },
                { "data": "to" },
                { "data": "price" },
                { "data": "status" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
               bSortable: false,
               aTargets: [ -1 ]
            }],
        });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });

    $('.add_menuitem').on('click',function(){
        $('#createMenuItem').trigger("reset");
        $('#createMenuItem').find('.form-control-feedback').remove();
        create_Menu_Item.resetForm();
        $('.img_wrapper').remove();
    });

    $('input[value=beverages]').on('ifChecked', function(event){

        console.log($("input[value=beverages]").val());
        
        let category_type = $("input[value=beverages]").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "get_recipe_cat_by_type",
            method: 'POST',
            data: {
                "category_type": category_type
            },
            success: function(result) {
                console.log(result);
                $('select[name=category_id]').empty();
                $('select[name=category_id_ed]').empty();
                $('select[name=sub_category_id]').empty();
                $('select[name=sub_category_id_ed]').empty();
                let html="<option value=''>Select Category </option>";
                let category = result.category;
                if(category){
                    $.each(category, function( index, value ) {
                        html +="<option value="+value.id+">"+value.category_name+"</option>";
                    });
                }
                $('select[name=category_id]').append(html);
                $('select[name=category_id_ed]').append(html);
            },
            error: function(e) {
                console.log(e)
            }
        });

    });
    $('input[value=food]').on('ifChecked', function(event){
      // alert('abcd efg');
      console.log($("input[value=food]").val());

        let category_type = $("input[value=food]").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "get_recipe_cat_by_type",
            method: 'POST',
            data: {
                "category_type": category_type
            },
            success: function(result) {
                console.log(result);
                $('select[name=category_id]').empty();
                $('select[name=category_id_ed]').empty();
                $('select[name=sub_category_id]').empty();
                $('select[name=sub_category_id_ed]').empty();
                let html="<option value=''>Select Parent Category </option>";
                let category = result.category;
                if(category){
                    $.each(category, function( index, value ) {
                        html +="<option value="+value.id+">"+value.category_name+"</option>";
                    });
                }
                $('select[name=category_id]').append(html);
                $('select[name=category_id_ed]').append(html);
            },
            error: function(e) {
                console.log(e)
            }
        });
    });

    $(document).on('click','input[name="menuitem_status"]',function(){
                        
            let menuitem_status = $(this).attr("data-attr");
            let message = '';
            let menuitem_id = $(this).attr("menuitem-id");
    
            if ( menuitem_status == 0 ) {
                menuitem_status = 1;
                $(this).attr("data-attr", "1");
                message = 'Activated';
            } else {
                menuitem_status = 0;
                $(this).attr("data-attr", "0");
                message = 'Deactivated';
            }
    
            $.ajax({
                type: "POST",
                url: "{{ route('update_menuitem_status')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "menuitem_id": menuitem_id,
                    "menuitem_status": menuitem_status
                },
                success: function (r) {
                        
                        $.notify("Menu Item "+ message +" Successfully", "success");
                        $('#item_table').DataTable().ajax.reload();
                        // $('#past_order_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
    
        });

    $("select[name=category_id]").on('change', function(){
            
        let cat_id = $(this).val();

        $('select[name=sub_category_id]').empty();
        
        $.ajax({
            type: "GET",
            url: "{{ route('get_recipe_subcat_by_cat_id')}}",
            data: {
                "cat_id": cat_id
            },
            success: function (r) {
                // console.log(r);
                let sub_category = r.sub_category;
                $('select[name=sub_category_id]').append($('<option>', {value: '', text: 'Select Sub Category'}));
                if(sub_category.length > 0){
                    $('select[name=sub_category_id]').attr('required',true); 
                    $.each( sub_category, function(index, value) {
                        // console.log( index + ' : ' + value.state_name );
                        $('select[name=sub_category_id]').append($('<option>', {value: value.id, text: value.category_name}));
                    });
                }
                else {
                    $('select[name=sub_category_id]').removeAttr('required'); 
                }
            },
            error:function(e){
                console.log(e);
            }
        });

    });
    $("select[name=category_id_ed]").on('change', function(){
            
        let cat_id = $(this).val();

        $('select[name=sub_category_id_ed]').empty();
        
        $.ajax({
            type: "GET",
            url: "{{ route('get_recipe_subcat_by_cat_id')}}",
            data: {
                "cat_id": cat_id
            },
            success: function (r) {
                // console.log(r);
                let sub_category = r.sub_category;

                $('select[name=sub_category_id_ed]').append($('<option>', {value: '0', text: 'Select Sub Category'}));
                if(sub_category.length > 0){
                    $('select[name=sub_category_id_ed]').attr('required',true);
                    $.each( sub_category, function(index, value) {
                        // console.log( index + ' : ' + value.state_name );
                        $('select[name=sub_category_id_ed]').append($('<option>', {value: value.id, text: value.category_name}));
                    });
                }
                else{
                    $('select[name=sub_category_id_ed]').removeAttr('required');
                }
            },
            error:function(e){
                console.log(e);
            }
        });

    });

    let create_Menu_Item = $("#createMenuItem").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#createMenuItem')[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('create_menuitem')}}",
                data: formdata,
                success: function (r) {
                    // console.log(r);
                    $('#item_table').DataTable().ajax.reload();
                    $('#addMenuItemModal').modal('hide');
                     $.notify("Menu Item Added Successfully", "success");
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: 'success',
                    //     text: 'Menu Item added Successfully',
                    // })
                    window.location.reload();
                },
                error:function(e){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    })
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
               
        },
        messages:
        {   
            category_type: ' '
        },
        ignore: []

    });

    $("#editMenuItem").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editMenuItem')[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('edit_menuitem')}}",
                data: formdata,
                success: function (r) {
                    $('#item_table').DataTable().ajax.reload();
                    $('#editMenuItemModal').modal('hide');
                    Swal.fire({
                        icon: 'success',
                        title: 'success',
                        text: 'Menu Item Updated Successfully',
                    })
                    // $.notify("Product Updated Successfully", "success");
                    // window.location.reload();
                },
                error:function(e){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    })
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
               
        },
        messages:
        {   
            
        },
        ignore: []

    });

  });

    function getMenuItem(id){
        
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_menuitem')}}",
            data: "id="+id,
            success: function (r) {

                create_edit(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_edit(data){
        
        $("input.add_info_ed").iCheck('uncheck');
        $("input.availability_ed").iCheck('uncheck');
        $("input.spice_level_ed").iCheck('uncheck');

        if(data){

            var add_info = JSON.parse(data.menuitem.add_info);
            var availability = JSON.parse(data.menuitem.availability);

            $("input[name=name_ed]").val(data.menuitem.name);
            $("textarea[name=description_ed]").val(data.menuitem.description);       
            $("input[name=category_type_ed][value=" + data.menuitem.category_type + "]").iCheck('check');

            $.each(add_info, function (index, info_value) {
                $("input.add_info_ed[value=" + info_value + "]").iCheck('check');
            })
            $.each(availability, function (index, ava_value) {
                $("input.availability_ed[value=" + ava_value + "]").iCheck('check');
            })
           
            if(data.menuitem.category_id != 0){
                setTimeout(function(){
                    $("select[name=category_id_ed]").val(data.menuitem.category_id).trigger('change');
                }, 1000);
            }
            if(data.menuitem.sub_category_id != 0){
                setTimeout(function(){
                    $("select[name=sub_category_id_ed]").val(data.menuitem.sub_category_id).trigger('change');
                }, 2000);
            }
            
            $("select[name=section_ed]").val(data.menuitem.section).trigger('change');
            $("select[name=food_cat_ed]").val(data.menuitem.food_cat).trigger('change');
            $("input[name=from_time_ed]").val(data.menuitem.from_time);
            $("input[name=to_time_ed]").val(data.menuitem.to_time);

            $("input[name=spice_level_ed][value=" + data.menuitem.spice_level + "]").iCheck('check');
            $("input[name=kot_group_ed]").val(data.menuitem.kot_group);
            $("input[name=price_ed]").val(data.menuitem.price);
            $("input[name=charges_gst_ed]").val(data.menuitem.charges_gst);
            // $("textarea[name=special_instruction_ed]").val(data.menuitem.special_instruction);
            $("input[name=serving_time_ed]").val(data.menuitem.serving_time);
            $("input[name=serves_ed]").val(data.menuitem.serves);
            $("input[name=menuitem_id]").val(data.menuitem.id);

            let out_resp_img = data.menuitem.image_url;
            
            let menuitem_img = "{{URL::asset('/images')}}/" + out_resp_img;
             
            let img_html = '<div class="img_wrapper">'+
                                '<img src="'+ menuitem_img +'" alt="Menuitem Image">'+
                                '<a class="remove-btn" title="Click here to remove" onclick="remove_image(\'recipes\', \'image_url\', \''+data.menuitem.id+'\', \''+out_resp_img+'\', \'img_wrapper\');">X</a>'+
                            '</div>';
            
            $('.show_img_ed').html(img_html);    
            $('#editMenuItemModal').modal('show');

        }
               

    }

    function getMenuItemView(id){
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_menuitem')}}",
            data: "id="+id,
            success: function (r) {
                create_view(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_view(data){

        $("input.add_info_view").iCheck('uncheck');
        $("input.availability_view").iCheck('uncheck');
        $("input.spice_level_view").iCheck('uncheck');

        if(data){
            var add_info = JSON.parse(data.menuitem.add_info);
            var availability = JSON.parse(data.menuitem.availability);

            $("input[name=name_view]").val(data.menuitem.name);
            $("textarea[name=description_view]").val(data.menuitem.description);       

            $.each(add_info, function (index, info_value) {
                $("input.add_info_view[value=" + info_value + "]").iCheck('check');
            })
            $.each(availability, function (index, ava_value) {
                $("input.availability_view[value=" + ava_value + "]").iCheck('check');
            })

            $("input[name=category_id_view]").val(data.menuitem.category_name);
            
            $("input[name=section_view]").val(data.menuitem.food_section_name);
            $("input[name=food_cat_view]").val(data.menuitem.food_cat_name);
            $("input[name=from_time_view]").val(data.menuitem.from_time);
            $("input[name=to_time_view]").val(data.menuitem.to_time);
            $("input[name=spice_level_view][value=" + data.menuitem.spice_level + "]").iCheck('check');
            $("input[name=kot_group_view]").val(data.menuitem.kot_group);
            $("input[name=price_view]").val(data.menuitem.price);
            $("input[name=charges_gst_view]").val(data.menuitem.charges_gst);
            // $("textarea[name=special_instruction_view]").val(data.menuitem.special_instruction);
            $("input[name=serving_time_view]").val(data.menuitem.serving_time);
            $("input[name=serves_view]").val(data.menuitem.serves);
            $("input[name=menuitem_id]").val(data.menuitem.id);

            if(data.sub)
                $("input[name=sub_category_id_view]").val(data.sub.category_name);
            else
            $("input[name=sub_category_id_view]").val('');
            let out_resp_img = data.menuitem.image_url;
            let menuitem_img = "{{URL::asset('/images')}}/" + out_resp_img;

            let img_html = '<div class="img_wrapper">'+
                                '<img src="'+ menuitem_img +'" alt="Menuitem Image">'+
                                '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
            
            $('.show_img_view').html(img_html);    
            $('#viewMenuItemModal').modal('show'); 
        }
    }

    function delete_menuitem( menuitem_id ) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            // cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('delete_menuitem')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "menuitem_id": menuitem_id
                    },
                    success: function (r) {
                            console.log(r);
                            Swal.fire(
                                'Deleted!',
                                'Menu Item Deleted Successfully.',
                                'success'
                            )
                            // $.notify("Menu Item Deleted Successfully", "success");
                            $('#item_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            }
        });
        
    }

    function remove_image(table_name, column_name, row_id, image_name, img_div) {
        $.ajax({
            type: "POST",
            url: "{{ route('remove_image')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "table_name": table_name,
                "column_name": column_name,
                "row_id": row_id,
                "image_name": image_name
            },
            success: function (r) {
                    console.log(r);
                    Swal.fire('Success!','Image Deleted Successfully.','success');
                    $("."+img_div).remove();
            },
            error:function(e){
                Swal.fire('Error!','Something went wrong.','error');
                console.log(e);
            }
        });
    }

    $("#category_id").on("select2:close", function (e) {  
        $(this).valid(); 
    });
    $("select[name=food_cat]").on("select2:close", function (e) {  
        $(this).valid(); 
    });
    $("select[name=section]").on("select2:close", function (e) {  
        $(this).valid(); 
    });


    

    var fileinput = document.getElementById('fileinput');
    var max_width = fileinput.getAttribute('data-maxwidth');
    var max_height = fileinput.getAttribute('data-maxheight');

    var preview = document.getElementById('preview');

    var form = document.getElementById('form');
    var global_base64;

    function processfile(file) {
    
        if( !( /image/i ).test( file.type ) )
            {
                alert( "File "+ file.name +" is not an image." );
                return false;
            }

        // read the files
        var reader = new FileReader();
        reader.readAsArrayBuffer(file);
        
        reader.onload = function (event) {
        // blob stuff
        var blob = new Blob([event.target.result]); // create blob...
        window.URL = window.URL || window.webkitURL;
        var blobURL = window.URL.createObjectURL(blob); // and get it's URL
        
        // helper Image object
        var image = new Image();
        image.src = blobURL;
        //preview.appendChild(image); // preview commented out, I am using the canvas instead
        image.onload = function() {
            // have to wait till it's loaded
            var resized = resizeMe(image); // send it to canvas
            var newinput = document.createElement("input");
            newinput.type = 'hidden';
            newinput.name = 'images[]';
            newinput.value = resized; // put result from canvas into new hidden input
            form.appendChild(newinput);
            //$('.resize_url').val(resized);
            window.global_base64 = resized;
            
        }
        };
    }

    function readfiles(files) {
    
        // remove the existing canvases and hidden inputs if user re-selects new pics
        var existinginputs = document.getElementsByName('images[]');
        var existingcanvases = document.getElementsByTagName('canvas');
        while (existinginputs.length > 0) { // it's a live list so removing the first element each time
        // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
        form.removeChild(existinginputs[0]);
        preview.removeChild(existingcanvases[0]);
        } 
        for (var i = 0; i < files.length; i++) {
          processfile(files[i]); // process each file at once
        }
        //fileinput.value = ""; //remove the original files from fileinput
        
        // TODO remove the previous hidden inputs if user selects other files
    }
    // === RESIZE ====

    function resizeMe(img) {
    
        var canvas = document.createElement('canvas');

        var width = img.width;
        var height = img.height;

        // calculate the width and height, constraining the proportions
        if (width > height) {
            if (width > max_width) {
            //height *= max_width / width;
            height = Math.round(height *= max_width / width);
            width = max_width;
            }
        } else {
            if (height > max_height) {
            //width *= max_height / height;
            width = Math.round(width *= max_height / height);
            height = max_height;
            }
        }
        
        // resize the canvas and draw the image data into it
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        
        preview.appendChild(canvas); // do the actual resized preview
        
        return canvas.toDataURL("image/jpeg",0.8); // get the data from canvas as 70% JPG (can be also PNG, etc.)

    }

    // this is where it starts. event triggered when user selects files
    fileinput.onchange = function(){
        if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
            alert('The File APIs are not fully supported in this browser.');
            return false;
        }
        $('.show_img').children().remove();
        imagesPreview(this, '.show_img');
        readfiles(fileinput.files);
        setTimeout(function(){
            $('.resize_url').val(window.global_base64);
        },3000);
        
    }

    var fileinputedit = document.getElementById('image_url_edit');
    
    fileinputedit.onchange = function(){
        if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
            alert('The File APIs are not fully supported in this browser.');
            return false;
        }
        $('.show_img_ed').children().remove();
        imagesPreview(this, '.show_img_ed');
        readfiles(fileinputedit.files);
        setTimeout(function(){
            $('.resize_url_edit').val(window.global_base64);
        },3000);
        
    }
    


</script>

@endsection


