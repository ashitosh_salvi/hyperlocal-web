@extends('layouts.applist')

@section('content')

<div id="login-form"  >
    
    <div class="container">

        <div class="row">
            
            <div class="col-md-4 col-md-offset-4 text-center">

                <div class="form-box" >
                    <div class="login-title">
                        {{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-v.png" style="width: 100%;"> --}}
                        <img src="{{ asset('dist/img/hyper-logo-v.png') }}" style="width: 100%;">
                    </div>
                    <div class="caption">
                        <h4>Sign in to start your session</h4>
                    </div>

                    <div class="input-group login-form">
                        <div class="login-div">
        
                            <form method="POST" action="{{ route('outletlogin') }}">
                                @csrf

                                    <input id="outlet_user_name" type="text" class="form-control @error('outlet_user_name') is-invalid @enderror" name="outlet_user_name" value="{{ old('outlet_user_name') }}" required autocomplete="outlet_user_name" autofocus placeholder="Username">

                                    @error('outlet_user_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <input type="submit" class="form-control" name="submit" id="submit" value="{{ __('Login') }}">

                                     @error('outlet_already_login')
                                        <span class="invalid-feedback" role="alert" style="color: red;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                            </form>

                        </div>
                        <div class="otp-div" style="display: none;">
                            <input type="submit" class="form-control" value="Proceed">
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
	
</script>

@endsection

