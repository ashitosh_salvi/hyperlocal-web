@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Main Category List</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#addMainCatModal"><i class="fa fa-plus"></i> Add Main Category</a>
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Manage Main Category</h3>
                </div>
                
                <div class="box-body">

                    <!-- <div class="row">
                        <div class="col-sm-1">
                            <label class="control-label">Merchant Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">GST Number</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-1">
                            <label class="control-label">Rest Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    
                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <a class="btn btn-sm btn-success btn-flat mr-50" href="#">Search</a>
                        <a class="btn btn-sm btn-danger btn-flat mr-50" href="#">Reset</a>
                        <a class="btn btn-sm btn-warning btn-flat mr-50" href="{{ url('/merchant_create') }}">Add New</a>
                        </div>
                    </div> -->

                    <div class="table-responsive mt-15">

                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Category Name</th>
                                    <th>Creator</th>
                                    <th>Create Dt.</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php for($i = 0; $i <= 17; $i++) { ?>    
                                <tr>
                                    <td><input type="checkbox" name="select_merchant" id="select_merchant"></td>
                                    <td>Cat <?php echo ($i+1); ?></td>
                                    <td>Admin</td>
                                    <td>2nd, May 2020</td>
                                    <td>
                                        <a title="Edit Merchant" class="btn btn-warning btn-sm mr5" data-toggle="modal" data-target="#editMainCatModal">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a title="Delete Merchant" href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    {{-- <div class="row mt-15">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button>
                        </div>
                    </div> --}}

                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="addMainCatModal" tabindex="-1" role="dialog" aria-labelledby="addMainCatModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addMainCatModalLabel">Add Main Category</h4>
        </div>
        <div class="modal-body">

        <!-- Add New Merchant Form -->
          <form>  
            
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label">Category Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control">
                        </div>
                    </div>



                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="create_merchant" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancle</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

        </div>

      </div>
    </div>
</div>


<div class="modal fade" id="editMainCatModal" tabindex="-1" role="dialog" aria-labelledby="editMainCatModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editMainCatModalLabel">Edit Category</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Merchant Form -->
          <form>  
            
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label">Category Name</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="create_merchant" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancle</button>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

        </div>
        
      </div>
    </div>
</div>

<div class="modal fade" id="bulkUploadEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="bulkUploadEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="bulkUploadEmployeeModalLabel">Bulk Upload Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload Merchant Form -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label">Select file to upload: </label>
                    </div>
                    <div class="col-sm-10">
                        <input type="file" id="bulk_employee_files" class="form-control">
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>

<div class="modal fade" id="offboardOutletModal" tabindex="-1" role="dialog" aria-labelledby="offboardOutletModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="offboardOutletModalLabel">Offboarding Outlet</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload Merchant Form -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Selcted Merchant / Vendor</label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-3">
                        <label class="control-label">Select Outlet</label>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control">
                            <option value="">Select outlet from list</option>
                        </select>
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>


@endsection 


@section('js')

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
  $(function () {
    $("#item_table").DataTable();
  });

  $(document).ready(function(){

        $("#add_outlet").click(function(e){

            // console.log('working');
            
            $(".outlet_details_box:first").clone().addClass("mt-15").appendTo("#outlet_details");

            e.preventDefault();

        });

        $("#edit_add_outlet").click(function(e){

            // console.log('working');
            
            $(".edit_outlet_details_box:first").clone().addClass("mt-15").appendTo("#edit_outlet_details");

            e.preventDefault();

        });

        $("#cancle_merchant").on('click', function(){

            window.history.back();

        });

    });

</script>

@endsection
