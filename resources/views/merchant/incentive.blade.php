@extends('layouts.applist')

@section('content')
@include('layouts.header')



<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">


            <div class="box">
            
                <div class="box-body">
                    <div class="row" >
                        <div class="col-sm-2">
                            <label class="control-label">Month</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Date Range</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Employee Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat ">Search</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">Incentive Earned</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="box-body">
                            <div class="table-responsive">
                            <table class="table no-margin">
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Menu Item</th>
                                <th>Qty</th>
                                <th>% Incentives per M.L</th>
                                <th>Total Incentive Amount </th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                <td><a href="#">OR9842</a></td>
                                <td>Call of Duty IV</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><a href="#">OR1848</a></td>
                                <td>Samsung Smart TV</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><a href="#">OR7429</a></td>
                                <td>iPhone 6 Plus</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                                  
                            </tbody>
                            </table>
                            </div>
                            <!-- /.table-responsive -->
                              
                        </div>
                    </div>
                    
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="control-label">Total Incentives :</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col" style="margin-top: 25px"> 
                            <button class="btn btn-sm btn-info btn-flat ">Approve</button>
                            <button class="btn btn-sm btn-info btn-flat ">Modify</button>
                            <button class="btn btn-sm btn-info btn-flat ">Cancel</button>
                        </div>
                    
                </div>
            </div>

        </section>
    </div>
</section>

@endsection 


@section('js')

<script>
    

</script>

@endsection


