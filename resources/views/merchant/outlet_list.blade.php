@extends('layouts.applist')

@section('content')
@include('layouts.merchant_header')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
<script src="https://unpkg.com/dropzone"></script>
<script src="https://unpkg.com/cropperjs"></script>
<style>
    .pac-container{
        z-index: 9999!important;
    }

    .image_area {
        position: relative;
    }

    img {
        display: block;
        max-width: 100%;
    }

    .preview, .ed_preview {
        overflow: hidden;
        width: 160px; 
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg{
        max-width: 1000px !important;
    }

    .overlay {
        position: absolute;
        bottom: 10px;
        left: 0;
        right: 0;
        background-color: rgba(255, 255, 255, 0.5);
        overflow: hidden;
        height: 0;
        transition: .5s ease;
        width: 100%;
    }

    .image_area:hover .overlay {
        height: 50%;
        cursor: pointer;
    }

    .text {
        color: #333;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }
    #attachment_doc_html{
        font-size: 16px;
        font-weight: 600;
    }

    #attachment_doc_html .modal-title{
        font-size: 24px;
    }

    #attachment_doc_html div{
        padding: 10px;
    }
    #attachment_doc_html a{
        color: blue;
    }
</style>

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Outlet Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success btn-flat mr-15 add_outlet" data-toggle="modal" data-target="#addOutletModal"><i class="fa fa-plus"></i> Add Outlet</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div class="table-responsive mt-15">
                            
                        <table id="outlet_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Outlet Name</th>
                                    <th>Outlet User Name</th>
                                    <th>Outlet Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
            </div>
        </section>

    </div>
</section>

<div class="modal fade" id="addOutletModal" tabindex="-1" role="dialog" aria-labelledby="addOutletModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="addOutletModalLabel">Add Outlet</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id="createOutlet">  
                        @csrf
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet User Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="outlet_user_name" name="outlet_user_name">
                                <span id="custom_error" class="help-block error custom_error" style="display: none"></span>

                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Password</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="outlet_password">
                            </div>
                        </div>
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_name">
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">GST #</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="gst">
                                
                            </div>
                        </div>

                        <div class="row mt-15 mb-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Speciality</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_speciality">
                            </div>
                            {{-- <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Booking Rate</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two">
                            </div> --}}

                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="">
                                    <input type="radio" class="flag_active minimal-red" name="flag" value="0" >
                                    Active
                                </label>
                                <label class="">
                                    <input type="radio" class="flag_inactive minimal-red" name="flag" value="1" >
                                    Inactive
                                </label>
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">FSSAI License</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="fssai_license">
                                
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Shop License</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="shop_license">
                                
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">GST Rate (%)</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="gst_rate">
                                
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Taxes/Delivery Charges</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="delivery_charges">
                                
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Available Time</label>
                            </div>
                            <div class="col-sm-3 bootstrap-timepicker">
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="open_time">
        
                                    <div class="input-group-addon">
                                      <i class="fa fa-clock-o"></i> Open
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Available Time</label>
                            </div>
                            <div class="col-sm-3 bootstrap-timepicker">
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="close_time">
        
                                    <div class="input-group-addon">
                                      <i class="fa fa-clock-o"></i> Close
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Type</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control outlet_type_select_add" name="outlet_type">
                                    <option value="">Select Type</option>
                                    <option value="product">Grocery</option>
                                    <option value="recipe">Restaurant</option>
                                </select>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Category Type</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control outlet_category_type_add" name="outlet_category_type" required>
                                </select>
                            </div>
                            </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Geo Address</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="geo_address" name="geo_address" class="controls form-control">
                                <div id="map"></div>
                                <input type="hidden" id="geo_cname" name="city2" />
                                <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                                <input type="hidden" name="address_longitude" id="address-longitude" value="0" />
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Image</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form-control" name="image_url" id="outlet_image_upload" required>
                                <input type="hidden" class="form-control" name="outlet_image_name" id="outlet_image_name">
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Mobile No.</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" maxlength="10" class="form-control" name="outlet_mobile" required>
                            </div>
                        </div>
                        

                        <div class="row mt-15">
                            <div class="col-sm-8">
                                <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>
                            </div>
                            <div class="col-sm-4">
                                <label>
                                  <input type="checkbox" class="minimal-red is_cod" name="is_cod" value="1">
                                  is COD?
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <label for="fileslx">Upload DOC here</label>
                                <input type="file" class="form-control outlet_doc outlet_doc_upload" name="document[]" multiple>
                                <div id="outlet_doc_add"></div>
                            </div>
                        </div>

                        <div class="row mt-15 mb-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Address</label>
                            </div>
                            <div class="col-sm-4">
                                <textarea name="outlet_address" class="form-control" rows="3" required></textarea>
                                
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Branch</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="branch">
                                
                            </div>
                        </div>

                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="create_outlet" type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewOutletModal" tabindex="-1" role="dialog" aria-labelledby="viewOutletModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="viewOutletModalLabel">View Outlet</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <!-- <form id="createOutlet">   -->
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet User Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_user_name_view" disabled>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Password</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="outlet_password_view" disabled>
                            </div>
                        </div>
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_name_view" disabled>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">GST #</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="gst_view" disabled>
                                
                            </div>
                        </div>

                        <div class="row mt-15 mb-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Speciality</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_speciality_view" disabled>
                            </div>
                            {{-- <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Booking Rate</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two_view" disabled>
                            </div> --}}

                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="">
                                    <input type="radio" class="flag_active minimal-red" name="flag_view" value="0" disabled>
                                    Active
                                </label>
                                <label class="">
                                    <input type="radio" class="flag_inactive minimal-red" name="flag_view" value="1" disabled>
                                    Inactive
                                </label>
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">FSSAI License</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="fssai_license_view" disabled>
                                
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Shop License</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="shop_license_view" disabled>
                                
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">GST Rate (%)</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="gst_rate_view" disabled>
                                
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Taxes/Delivery Charges</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="delivery_charges_view" disabled>
                                
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Available Time</label>
                            </div>
                            <div class="col-sm-3 bootstrap-timepicker">
                                <div class="input-group">
                                    <input type="text" class="form-control " name="open_time_view" disabled>
        
                                    <div class="input-group-addon">
                                      <i class="fa fa-clock-o"></i> Open
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Available Time</label>
                            </div>
                            <div class="col-sm-3 bootstrap-timepicker">
                                <div class="input-group">
                                    <input type="text" class="form-control " name="close_time_view" disabled>
        
                                    <div class="input-group-addon">
                                      <i class="fa fa-clock-o"></i> Close
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Type</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="outlet_type_view" disabled>
                                    <option value="">Select Type</option>
                                    <option value="product">Grocery</option>
                                    <option value="recipe">Restaurant</option>
                                </select>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Category Type</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control outlet_category_type" name="outlet_category_type_view" disabled>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Geo Address</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="geo_address_view" class="controls form-control" disabled>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Image</label>
                            </div>
                            <div class="col-sm-3">
                                <div id="view_rest_img"></div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Mobile No.</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_mobile_view" disabled>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-8">
                                <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>
                            </div>
                            <div class="col-sm-4">
                                <label>
                                  <input type="checkbox" class="minimal-red is_cod_view" disabled name="is_cod_view" value="1">
                                  is COD?
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <!-- <label for="fileslx">Upload DOC here</label> -->
                                <!-- <input type="file" class="form-control outlet_doc" name="documentv[]" disabled multiple> -->
                                <div id="outlet_doc_view_preview"></div>
                            </div>
                        </div>

                        <div class="row mt-15 mb-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Address</label>
                            </div>
                            <div class="col-sm-4">
                                <textarea name="outlet_address" class="form-control" rows="3" disabled></textarea>
                                
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Branch</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="branch_view" disabled>
                                
                            </div>
                        </div>

                    <!-- </form> -->
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editOutletModal" tabindex="-1" role="dialog" aria-labelledby="editOutletModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="editOutletModalLabel">Edit Outlet</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id="editOutlet">  
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet User Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="hidden" class="form-control" name="outlet_id_edit">
                                <input type="text" class="form-control" name="outlet_user_name_edit">
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Password</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="outlet_password_edit">
                            </div>
                        </div>
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Name</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_name_edit">
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">GST #</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="gst_edit">
                                
                            </div>
                        </div>

                        <div class="row mt-15 mb-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Speciality</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_speciality_ed">
                            </div>
                            {{-- <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Booking Rate</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two_ed">
                            </div> --}}

                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-4">
                                <label class="">
                                    <input type="radio" class="flag_active minimal-red" name="flag_edit" value="0" required>
                                    Active
                                </label>
                                <label class="">
                                    <input type="radio" class="flag_inactive minimal-red" name="flag_edit" value="1" required>
                                    Inactive
                                </label>
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">FSSAI License</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="fssai_license_edit">
                                
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Shop License</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="shop_license_edit">
                                
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">GST Rate (%)</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="gst_rate_edit">
                                
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Taxes/Delivery Charges</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="delivery_charges_edit">
                                
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Available Time</label>
                            </div>
                            <div class="col-sm-3 bootstrap-timepicker">
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="open_time_edit">
        
                                    <div class="input-group-addon">
                                      <i class="fa fa-clock-o"></i> Open
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Available Time</label>
                            </div>
                            <div class="col-sm-3 bootstrap-timepicker">
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="close_time_edit">
        
                                    <div class="input-group-addon">
                                      <i class="fa fa-clock-o"></i> Close
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Type</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control outlet_type_select_edit" name="outlet_type_edit">
                                    <option value="">Select Type</option>
                                    <option value="product">Grocery</option>
                                    <option value="recipe">Restaurant</option>
                                </select>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Category Type</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control outlet_category_type_edit" name="outlet_category_type_edit" required>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Geo Address</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="geo_address_edit" name="geo_address_edit" class="controls form-control">
                                <div id="map"></div>
                                <input type="hidden" id="geo_cname_edit" name="city2" />
                                <input type="hidden" name="address_latitude_edit" id="address-latitude_edit" value="0" />
                                <input type="hidden" name="address_longitude_edit" id="address-longitude_edit" value="0" />
                            </div>
                        </div>
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Image</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form-control edit_image" name="ed_rest_img" id="ed_rest_img_input" >
                                <input type="hidden" class="form-control" name="outlet_image_name" id="ed_outlet_image_name">
                                <div id="ed_rest_img"></div>
                            </div>
                            <div class="col-sm-1">
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Mobile No.</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="outlet_mobile_edit" maxlength="10" required>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-8">
                                <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>
                            </div>
                            <div class="col-sm-4">
                                <label>
                                  <input type="checkbox" class="minimal-red is_cod_edit" name="is_cod_edit" value="1">
                                  is COD?
                                </label>
                            </div>
                            <div class="col-sm-10">
                                <label for="fileslx">Upload DOC here</label>
                                <input type="file" class="form-control outlet_doc outlet_doc_edit_upload" name="documented[]" multiple>
                                <div id="outlet_doc_edit_preview">
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15 mb-15">
                            <div class="col-sm-2">
                                <label class="control-label">Outlet Address</label>
                            </div>
                            <div class="col-sm-4">
                                <textarea name="outlet_address_edit" class="form-control" rows="3" required></textarea>
                                
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Branch</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="branch_edit">
                                
                            </div>
                        </div>

                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="edit_outlet" type="submit" class="btn btn-sm btn-warning btn-flat mr-50">Update</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="image_crop_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="" id="sample_image" />
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop" class="btn btn-primary">Crop</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ed_image_crop_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="" id="ed_sample_image" />
                        </div>
                        <div class="col-md-4">
                            <div class="ed_preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="ed_crop" class="btn btn-primary">Crop</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="attachement_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="display: inline-block; font-size: 19px;">Outlet attachment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12" id="attachment_doc_html">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBb4GVJI3MIpjcwZwG72m6HxDa_Cv1qWNM&libraries=places&callback=initAutocomplete"
		 async defer></script>

<script type="text/javascript">
    function initAutocomplete() {
        // var options = {
        // types: ['(countries)'],
        // componentRestrictions: {country: "in"}
        // };
        var input = document.getElementById('geo_address');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.setComponentRestrictions(
                {'country': ['in']});
        
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var place_name = place.name;


        document.getElementById('geo_cname').value = place_name ;
        document.getElementById('address-latitude').value = latitude;
        document.getElementById('address-longitude').value =longitude;
        });


        var input1 = document.getElementById('geo_address_edit');
        var autocomplete1 = new google.maps.places.Autocomplete(input1);
        autocomplete1.setComponentRestrictions(
                {'country': ['in']});
        
        google.maps.event.addListener(autocomplete1, 'place_changed', function () {
            var place1 = autocomplete1.getPlace();
            var latitude1 = place1.geometry.location.lat();
            var longitude1 = place1.geometry.location.lng();
            var place_name1 = place1.name;

            document.getElementById('geo_cname_edit').value = place_name1 ;
            document.getElementById('address-latitude_edit').value = latitude1;
            document.getElementById('address-longitude_edit').value =longitude1;
        }); 

    }
</script>

@endsection 

@section('js')
<script>
    $(document).ready(function(){

        var $modal = $('#image_crop_modal');

        var image = document.getElementById('sample_image');

        var cropper;
        window.cropedimage = 0;

        $('#outlet_image_upload').change(function(event){
            var files = event.target.files;

            var done = function(url){
                image.src = url;
                $modal.modal('show');
                window.cropedimage = 0;
            };

            if(files && files.length > 0)
            {
                reader = new FileReader();
                reader.onload = function(event)
                {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 2,
                viewMode: 3,
                preview:'.preview'
            });
        }).on('hidden.bs.modal', function(){
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function(){
            canvas = cropper.getCroppedCanvas({
                width:600,
                height:300
            });

            canvas.toBlob(function(blob){
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function(){
                    var base64data = reader.result;
                    $.ajax({
                        url:'{{ route('crop_outlet_image') }}',
                        method:'POST',
                        data:{_token: CSRF_TOKEN, image:base64data},
                        success:function(data)
                        {
                            $modal.modal('hide');
                            window.cropedimage = 1;
                            $('#outlet_image_name').val(data.image_name);
                            $('html').css('overflow','hidden');
                            $('#addOutletModal').css('overflow-x','hidden');
                            $('#addOutletModal').css('overflow-y','auto');
                        }
                    });
                };
            });
        });

        $('#image_crop_modal').on('hidden.bs.modal', function () {
            if (window.cropedimage == 0) {
                $('#outlet_image_name').val('');
                $('#outlet_image_upload').val('');
            }
            $('html').css('overflow','hidden');
            $('#addOutletModal').css('overflow-x','hidden');
            $('#addOutletModal').css('overflow-y','auto');
        })
        
    });
</script>

<script>
    $(document).ready(function(){

        var $modal = $('#ed_image_crop_modal');

        var image = document.getElementById('ed_sample_image');

        var cropper;

        $('#ed_rest_img_input').change(function(event){
            var files = event.target.files;

            var done = function(url){
                image.src = url;
                $modal.modal('show');
            };

            if(files && files.length > 0)
            {
                reader = new FileReader();
                reader.onload = function(event)
                {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 2,
                viewMode: 3,
                preview:'.ed_preview'
            });
        }).on('hidden.bs.modal', function(){
            cropper.destroy();
            cropper = null;
        });

        $('#ed_crop').click(function(){
            canvas = cropper.getCroppedCanvas({
                width:600,
                height:300
            });

            canvas.toBlob(function(blob){
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function(){
                    var base64data = reader.result;
                    $.ajax({
                        url:'{{ route('crop_outlet_image') }}',
                        method:'POST',
                        data:{_token: CSRF_TOKEN, image:base64data},
                        success:function(data)
                        {
                            $modal.modal('hide');
                            $('#ed_outlet_image_name').val(data.image_name);
                            let rest_img = "{{URL::asset('/images')}}/" + data.image_name;
                            $("#ed_rest_img").html('');
                            let outlet_img_html = '<div class="img_wrapper" style="max-width: none;">'+
                                                    '<img src="'+ rest_img +'" alt="Outlet Image" style="width: 200px;height: 100px;">'+
                                                    //'<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                                '</div>';
                            $("#ed_rest_img").append(outlet_img_html);
                            $('html').css('overflow','hidden');
                            $('#editOutletModal').css('overflow-x','hidden');
                            $('#editOutletModal').css('overflow-y','auto');
                        }
                    });
                };
            });
        });
        
    });
</script>

<script>

let fileTypes = ['jpg', 'jpeg', 'png']; // array for valid image preview

$(function() {
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
            isSuccess = fileTypes.indexOf(extension) > -1;
            var filesAmount = input.files.length;
            if(isSuccess){
                for (i = 0; i < filesAmount; i++) {

                    var reader = new FileReader();
                    reader.onload = function(event) {
                        img_html = '<div class="img_wrapper">'+
                                '<img src="'+ event.target.result +'" alt="Outlet Image" >'+
                                // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
                        $(placeToInsertImagePreview).append(img_html);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
    };
    $('.outlet_doc_add_upload').on('change', function() {
        $('#outlet_doc_add_preview').children().remove();
        imagesPreview(this, '#outlet_doc_add_preview');
    });
    $('.outlet_doc_edit_upload').on('change', function() {
        $('#outlet_doc_edit_preview').children().remove();
        imagesPreview(this, '#outlet_doc_edit_preview');
    });

    $('.edit_image').on('change',function(){
        $('#ed_rest_img').children('.img_wrapper').remove();
        imagesPreview(this, '#ed_rest_img');
    });
    
});
$(document).ready(function(){
    $('.add_outlet').on('click',function(){

        $('input[name=outlet_id_edit]').val('');
        $('#createOutlet').trigger("reset");
        $("#createOutlet").find('span.help-block').remove();
        $("#createOutlet").find('.help-block').removeClass("help-block");
        $("#createOutlet").find('.error').removeClass("error");
        $("#createOutlet").find('.has-success').removeClass("has-success");
        $('#createOutlet').find('.form-control-feedback').remove();
    });
    $('#outlet_table').DataTable({
        "language": {
            "emptyTable": "No Record Found"
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
                "url": "{{ url('get_outlet_list') }}",
                "dataType": "json",
                "type": "GET",
                "data":{ _token: "{{csrf_token()}}"}
            },
        "columns": [
            { "data": "outlet_name" },
            { "data": "outlet_user_name" },
            { "data": "outlet_type" },
            { "data": "action" }
        ],
        "aoColumnDefs": [
        {
        bSortable: false,
        aTargets: [ -1 ]
        }],
    });

    $(".timepicker").timepicker({
        showInputs: false
    });

    $.validator.addMethod(
        "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
    );

    $.validator.addMethod(
        "check_outlet_user_name",
            function(value, element) {
                
                let outlet_user_name = value;

                let outlet_id = $('input[name=outlet_id_edit]').val();
                console.log( outlet_user_name );
                console.log( typeof(outlet_user_name) );
                let isSuccess = false;
                
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "{{ route('outlet_check_username')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "outlet_user_name": outlet_user_name,
                        "outlet_id": outlet_id,

                    },
                    success: function (r) {
                        console.log(r);
                        let outlet_user_name_message = r.message;

                        console.log( outlet_user_name_message );

                        if ( outlet_user_name_message == 'User already Exist' ) {
                            // $("#outlet_user_name").parent().find("#custom_error").show().text('User already Exist');
                            isSuccess = false;
                        } else {
                            isSuccess = true;
                        }

                    },
                    error:function(e){
                        console.log(e);
                    }
                });

                return isSuccess;
            }
    );

    $.validator.addMethod('maxupload', function (value, element, param) {
        var length = ( element.files.length );
        return this.optional( element ) || length <= 10;
    }, 'You can add only maximum 10 images');
    $.validator.addMethod(
        "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
    );

    $.validator.addMethod('filesize', function (value, element, arg) {
        
        if (element.files[0] == undefined) {
            return true;
        }
        console.log(element.files[0].size/1024/1024);
        if(element.files[0].size/1024/1024<=arg){
            return true;
        }else{
            return false;
        }

    });

    $( "#createOutlet" ).validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#createOutlet')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('create_outlet')}}",
                data: formdata,
                success: function (r) {
                        console.log(r);
                        $("#addOutletModal").modal("hide");
                        $.notify("Outlet Added Successfully", "success");
                        $('#outlet_table').DataTable().ajax.reload();
                        // window.location.reload();
                },
                error:function(e){
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
            outlet_user_name: {
                required: true,
                check_outlet_user_name: true,
            },
            outlet_password: {
                required: true,
            },
            outlet_name: {
                required: true,
            },
            outlet_type: {
                required: true,
            },
            'document[]': {
                required:false,
                extension: 'jpg|jpeg|png|pdf',
                maxupload: 10,
                filesize: 5,
            },
            outlet_mobile:{
                required:true,
                regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
            }
        },
        messages:
        {	
            outlet_user_name: {
                required: 'Outlet user name is required',
                check_outlet_user_name: 'User already Exist.'
            },
            outlet_password: {
                required: 'Outlet password is required',
            },
            outlet_name: {
                required: 'Outlet name is required',
            },
            outlet_type: {
                required: 'Outlet type is required',
            },
            outlet_mobile:{
                regex:'Invalid Mobile No',
            },
            'document[]':{
                filesize:" file size must be less than 5 Mb.",
            }
        },
        ignore: []

    });

    $( "#editOutlet" ).validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editOutlet')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('edit_outlet')}}",
                data: formdata,
                success: function (r) {
                        console.log(r);
                        $("#editOutletModal").modal("hide");
                        $.notify("Outlet Edited Successfully", "success");
                        // $('#outlet_table').DataTable().ajax.reload();
                        // window.location.reload();
                },
                error:function(e){
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
            outlet_user_name_edit: {
                required: true,
                check_outlet_user_name: true,
            },
            outlet_password_edit: {
                required: true,
            },
            outlet_name_edit: {
                required: true,
            },
            outlet_type_edit: {
                required: true,
            },
            'documented[]': {
                required:false,
                extension: 'jpg|jpeg|png|pdf',
                maxupload: 10,
                filesize: 5,
            },
            outlet_mobile_edit: {
                required:true,
                regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
            }
        },
        messages:
        {	
            outlet_user_name_edit: {
                required: 'Outlet user name is required',
                check_outlet_user_name: 'User already Exist.'
            },
            outlet_password_edit: {
                required: 'Outlet password is required',
            },
            outlet_name_edit: {
                required: 'Outlet name is required',
            },
            outlet_type_edit: {
                required: 'Outlet type is required',
            },
            outlet_mobile_edit: {
                regex: 'Invalid Mobile No',
            },
            'documented[]':{
                filesize:" file size must be less than 5 Mb.",
            }
        },
        ignore: [],

    });

    // $("#createOutlet").click(function(){
    //     $(this).trigger('reset');
    // });
    $(".dataTables_filter input").css({ "width": "250px" });

    $('.outlet_category_type_add').append($('<option>', {value: '', text: 'Select Outlet Category'}));
            
    $.ajax({
        type: "GET",
        url: "{{ route('get_all_outlet_categories')}}",
        success: function (r) {
            
            $('.outlet_category_type').append($('<option>', {value: '', text: 'Select Outlet Category'}));
            $.each( r, function(index, value) {
                $(".outlet_category_type").append($('<option>', {value: value.id, text: value.category_name}));
            });
        },
        error:function(e){
            console.log(e);
        }
    });    

});

function getOutletView(id){
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                url: "{{ route('get_outlet')}}",
                data: "id="+id,
                success: function (r) {
                    console.log(r);
                    create_view(r);
                },
                error:function(e){
                    console.log(e);
                }
            });
}

function create_view(data) { 

    $("div.view_js_add").remove();
    $("div.img_wrapper").remove();
    $("input.is_cod_view").iCheck('uncheck');
    if(data){

        $("input[name=outlet_user_name_view]").val(data.outlet.outlet_user_name);
        $("input[name=outlet_password_view]").val(data.outlet.password);
        $("input[name=outlet_name_view]").val(data.outlet.outlet_name);
        $("input[name=gst_view]").val(data.outlet.gst);
        $("input[name=fssai_license_view]").val(data.outlet.fssai_license);
        $("input[name=shop_license_view]").val(data.outlet.shop_license);
        $("input[name=gst_rate_view]").val(data.outlet.gst_rate);
        $("input[name=delivery_charges_view]").val(data.outlet.delivery_charges);
        $("select[name=outlet_type_view]").val(data.outlet.outlet_type);
        $("textarea[name=outlet_address]").val(data.outlet.outlet_address);
        $("input[name=branch_view]").val(data.outlet.branch);
        $("input[name=outlet_mobile_view]").val(data.outlet.mobile);
        $("input[name=open_time_view]").val(data.outlet.open_time);
        $("input[name=close_time_view]").val(data.outlet.close_time);
        $("input[name=geo_address_view]").val(data.outlet.outlet_geoaddress);
        
        if(data.outlet.is_cod == 1){
            $("input.is_cod_view").iCheck('check');
        }
        $("select[name=outlet_category_type_view]").val(data.outlet.outlet_category_id);
        $("input[name=outlet_speciality_view]").val(data.outlet.outlet_speciality);
        // $("input[name=booking_for_two_view]").val(data.outlet.booking_for_two);

        if(data.outlet.flag == 0){
            $("input.flag_active").iCheck('check');
        }else{
            $("input.flag_inactive").iCheck('check');
        }

        if ( data.outlet.image_url ) {

            let decode_img = JSON.parse( data.outlet.image_url );
            let rest_img = "{{URL::asset('/images')}}/" + decode_img;
            let outlet_img_html = '<div class="img_wrapper" style="max-width: none;">'+
                                    '<img src="'+ rest_img +'" alt="Outlet Image" style="width: 200px;height: 100px;">'+
                                    //'<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                '</div>';
            $("#view_rest_img").append(outlet_img_html);
        }

        if ( data.outlet['img_url'] ) {

            let out_resp_img = JSON.parse( data.outlet['img_url'] );

            if ( out_resp_img.length > 0 ) {
                let img_html = '<div class="row mt-15 view_js_add">';
                for( let j = 0; j < out_resp_img.length; j++ ) {

                    let outlet_img = "{{URL::asset('/images')}}/" + out_resp_img[j];
                    img_html += '<div class="img_wrapper">'+
                                    '<img src="'+ outlet_img +'" alt="Outlet Image" title="' + out_resp_img[j] + '">'+
                                    // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                '</div>';
                    
                }
                img_html += '</div>';
                $("#outlet_doc_view_preview").append(img_html);
            }

        }

    }
    $('#viewOutletModal').modal('show');    

}

function getOutlet(id){
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                url: "{{ route('get_outlet')}}",
                data: "id="+id,
                success: function (r) {
                    console.log(r);
                    create_edit(r);
                },
                error:function(e){
                    console.log(e);
                }
            });
}

function signout_outlet(id)
{   
    let url = "{{ url('destroy_outlet_session') }}/"+id;
    $.ajax({
            type: "GET",
            url: url,
            success: function (r) {
                $.notify("Signout all Outlet Successfully", "success");
            },
            error:function(e){
                $.notify("Signout all Outlet not possible, please try again", "error");
            }
        });
}

function signout_app_outlet(id)
{   
    let url = "{{ url('destroy_app_outlet_session') }}/"+id;
    $.ajax({
            type: "GET",
            url: url,
            success: function (r) {
                $.notify("Signout all Outlet Successfully", "success");
            },
            error:function(e){
                $.notify("Signout all Outlet not possible, please try again", "error");
            }
        });
}

function create_edit(data) { 
    
    $("div.edit_js_add").remove();
    $("div.img_wrapper").remove();
    $("input.is_cod_edit").iCheck('uncheck');
    if(data){
        $("input[name=outlet_id_edit]").val(data.outlet.id);
        $("input[name=outlet_user_name_edit]").val(data.outlet.outlet_user_name);
        $("input[name=outlet_user_name_edit]").attr('data-attr', data.outlet.id);
        $("input[name=outlet_password_edit]").val(data.outlet.password);
        $("input[name=outlet_name_edit]").val(data.outlet.outlet_name);
        $("input[name=gst_edit]").val(data.outlet.gst);
        $("input[name=fssai_license_edit]").val(data.outlet.fssai_license);
        $("input[name=shop_license_edit]").val(data.outlet.shop_license);
        $("input[name=gst_rate_edit]").val(data.outlet.gst_rate);
        $("input[name=delivery_charges_edit]").val(data.outlet.delivery_charges);
        $("select[name=outlet_type_edit]").val(data.outlet.outlet_type);
        $("textarea[name=outlet_address_edit]").val(data.outlet.outlet_address);
        $("input[name=branch_edit]").val(data.outlet.branch);
        $("input[name=outlet_mobile_edit]").val(data.outlet.mobile);
        $("input[name=open_time_edit]").val(data.outlet.open_time);
        $("input[name=close_time_edit]").val(data.outlet.close_time);
        $("input[name=geo_address_edit]").val(data.outlet.outlet_geoaddress);

        if(data.outlet.is_cod == 1){
            $("input.is_cod_edit").iCheck('check');
        }

        if(data.outlet.flag == 0){
            $("input.flag_active").iCheck('check');
        }else{
            $("input.flag_inactive").iCheck('check');
        }

        //$("select[name=outlet_category_type_edit]").val(data.outlet.outlet_category_id);
        $("input[name=outlet_speciality_ed]").val(data.outlet.outlet_speciality);
        // $("input[name=booking_for_two_ed]").val(data.outlet.booking_for_two);

        if ( data.outlet.image_url ) {

            let decode_img = JSON.parse( data.outlet.image_url );
            let rest_img = "{{URL::asset('/images')}}/" + decode_img;
            let outlet_img_html = '<div class="img_wrapper" style="max-width: none;">'+
                                    '<img src="'+ rest_img +'" alt="Outlet Image" style="width: 200px;height: 100px;">'+
                                    //'<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                '</div>';
            $("#ed_rest_img").append(outlet_img_html);
        }

        if ( data.outlet['img_url'] ) {

            let out_resp_img = JSON.parse( data.outlet['img_url'] );

            if ( out_resp_img.length > 0 ) {
                let img_html = '<div class="row mt-15 edit_js_add">';
                for( let j = 0; j < out_resp_img.length; j++ ) {

                    let outlet_img = "{{URL::asset('/images')}}/" + out_resp_img[j];

                    // img_html += '<div class="img_wrapper">'+
                    //                 '<img src="'+ outlet_img +'" alt="Outlet Image" title="' + out_resp_img[j] + '">'+
                    //                 '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                    //             '</div>';
                    let rmdiv = 'doct'+j;
                    img_html += '<div id="'+rmdiv+'" style="height: 20px;display:inline-block;margin-bottom:10px;margin-left:20px;padding-left: 10px;width: 167px;background: #F0F0F0;">'+
                                '<a downlad href="' + outlet_img + '">Uploaded Document </a>'+
                                '<a title="Click here to remove" onclick="remove_image(\'outlets\', \'img_url\', \''+data.outlet.id+'\', \''+out_resp_img[j]+'\', \''+rmdiv+'\');" '+ 
                                'style="margin-left: 12px;cursor: pointer;color: red;font-weight: 900;">X</a>'+
                                '</div>';
                                  
                }
                img_html += '</div>';
                $("#outlet_doc_edit_preview").append(img_html);
            }

        }

        $.ajax({
        type: "GET",
        data:{
            outlet_type:data.outlet.outlet_type
        },
        url: "{{ route('get_all_outlet_categories')}}",
        success: function (r) {
            //console.log(r);
            $(".outlet_category_type_edit").html('');
            $('.outlet_category_type_edit').append($('<option>', {value: '', text: 'Select Outlet Category'}));
            $.each( r, function(index, value) {
                $(".outlet_category_type_edit").append($('<option>', {value: value.id, text: value.category_name}));
            });
            $(".outlet_category_type_edit").val(data.outlet.outlet_category_id)
        },
        error:function(e){
            console.log(e);
        }
    });

    }
    $('#editOutletModal').modal('show');

}

function delete_outlet( outlet_id ) {

    console.log( outlet_id );

    if( confirm('Are sure to delete the record?') ) {
        
        $.ajax({
            type: "POST",
            url: "{{ route('delete_outlet')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "outlet_id": outlet_id
            },
            success: function (r) {
                    console.log(r);
                        $.notify("Outlet Deleted Successfully", "success");
                        $('#outlet_table').DataTable().ajax.reload();
                        // window.location.reload();
            },
            error:function(e){
                console.log(e);
            }
        });

    } else {

    }

}

function attachment_outlet( outlet_id ) {
    $.ajax({
        type: "POST",
        url: "{{ route('attachement_outlet')}}",
        data: {
            "_token": "{{ csrf_token() }}",
            "outlet_id": outlet_id
        },
        success: function (response) {
                let attachement = response.attachements;
                let html = '';
                console.log(attachement.length);
                for (let i = 0; i < attachement.length; i++) {
                    let count = i +1;
                    let rmdiv = 'document_popup_id_'+count;
                    const element = attachement[i];
                    let rest_img = "{{URL::asset('/images')}}/" + element;
                    html += '<div style="padding: 10px; font-size: 16px;" id="'+rmdiv+'"><a href="'+rest_img+'" target="_blank"> <span> <i class="fa fa-paperclip" aria-hidden="true"></i> </span> '+ element +'</a> <span style="float: right;"> <a href="'+rest_img+'" target="_blank"><i class="fa fa-download" aria-hidden="true" style=" margin-right: 9px; cursor: pointer; color: blue;"></i></a> <a href="javascript:void(0)" onclick="remove_image(\'outlets\', \'img_url\', \''+outlet_id+'\', \''+element+'\', \''+rmdiv+'\');"> <i class="fa fa-trash" aria-hidden="true" style="color: red; cursor: pointer;"></i> </a> </span></div>';
                }

                if (attachement.length == 0) {
                    html = '<p style="text-align: center; color: red; margin: 10px 0px">No document found</p>';
                }
                $('#attachment_doc_html').html(html);
                $('#attachement_modal').modal('show');
        },
        error:function(e){
            console.log(e);
        }
    });
}

$(document).on('change','.outlet_type_select_edit',function(){
    $(this).closest('.row').find('.outlet_category_type_edit').html('');
    let box = $(this).closest('.row').find('.outlet_category_type_edit');
    let type =  $(this).val();
    if(!$(this).val())
        type = 'test';
    $.ajax({
        type: "GET",
        data:{ 
            outlet_type:type
        },
        url: "{{ route('get_all_outlet_categories')}}",
        success: function (r) {
            box.append($('<option>', {value: '', text: 'Select Outlet Category'}));
            $.each( r, function(index, value) {
                box.append($('<option>', {value: value.id, text: value.category_name}));
            });
        },
        error:function(e){
            console.log(e);
        }
    });
});

$(document).on('change','.outlet_type_select_add',function(){
    $(this).closest('.row').find('.outlet_category_type_add').html('');
    let box = $(this).closest('.row').find('.outlet_category_type_add');
    let type =  $(this).val();
    if(!$(this).val())
        type = 'test';
    $.ajax({
        type: "GET",
        data:{ 
            outlet_type:type
        },
        url: "{{ route('get_all_outlet_categories')}}",
        success: function (r) {
            box.append($('<option>', {value: '', text: 'Select Outlet Category'}));
            $.each( r, function(index, value) {
                box.append($('<option>', {value: value.id, text: value.category_name}));
            });
        },
        error:function(e){
            console.log(e);
        }
    });
});



$('#addOutletModal').on("scroll", function() {      
  $("#geo_address").blur(); 
});
$('#editOutletModal').on("scroll", function() {      
  $("#geo_address_edit").blur();
});


function remove_image(table_name, column_name, row_id, image_name, img_div_id) {
    $.ajax({
        type: "POST",
        url: "{{ route('remove_image')}}",
        data: {
            "_token": "{{ csrf_token() }}",
            "table_name": table_name,
            "column_name": column_name,
            "row_id": row_id,
            "image_name": image_name
        },
        success: function (r) {
                console.log(r);
                Swal.fire(
                    'Success!',
                    'Image Deleted Successfully.',
                    'success'
                );
                // $('.modal').modal('hide');
                $("#"+img_div_id).remove();
        },
        error:function(e){
            Swal.fire(
                    'Error!',
                    'Something went wrong.',
                    'error'
                );
            console.log(e);
        }
    });
}
 

</script>

@endsection
