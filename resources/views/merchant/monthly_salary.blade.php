@extends('layouts.applist')

@section('content')
@include('layouts.header')



<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">


            <div class="box ">
            
                <div class="box-body">
                    <div class="row" >
                        <div class="col-sm-2">
                            <label class="control-label">Month</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Employee Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat ">Calculate Salary</button>
                        <button class="btn btn-sm btn-info btn-flat ">Reset</button>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">Working Days</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Loss of Pay Days</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header"><h3 class="box-title">Employee</h3></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Employee Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Department</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Education details</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Designation</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="box ">                
                        <div class="box-header"><h3 class="box-title">Earnings : </h3></div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">Basic</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> HRA </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> DA</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">PF </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Other allowances </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Total Earnings</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">                
                        <div class="box-header"><h3 class="box-title">Deduction : </h3></div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">PF</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> Professional Tax </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> Any Fund</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Income Tax </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Total Deductions </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                              
                        </div>
                    </div>
                </div>

            </div>

            <div class="box">
                <div class="box-body">
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Net salary</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Amount in words</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        
                    </div>
                    <div class="col" style="margin-top: 30px">
                        <button class="btn btn-info "> Approve</button> 
                        <button class="btn btn-info "> Modify</button> 
                        <button class="btn btn-info "> Cancle</button>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>


@endsection 


@section('js')

<script>
    

</script>

@endsection


