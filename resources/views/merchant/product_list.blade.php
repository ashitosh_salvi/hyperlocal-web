@extends('layouts.applist')

@section('content')
@include('layouts.outlet_header')
<style>
.dropdown-submenu {
  position: relative;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -1px;
}
#product_list tr td:first {
    max-width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
</style>
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Product Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_product btn-flat mr-15" data-toggle="modal" data-target="#addProductModal"><i class="fa fa-plus"></i> Add Product</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadProductModal"><i class="fa fa-plus"></i> Bulk Upload</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                   

                    <div class="table-responsive mt-15">

                        <table id="product_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Description</th>
                                    <th>Brand</th>
                                    <th>Price</th>
                                    <th>Manufacturer</th>
                                    {{-- <th>Discount (%)</th> --}}
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>

            </div>

        </section>
    </div>
</section>



<div class="modal fade" id="addProductModal" role="dialog" aria-labelledby="addProductModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addProductModalLabel">Add Product</h4>
        </div>
        <div class="modal-body">

        <!-- Add Product Modal -->
        <form id="createProduct" >  
            <div class="box-body">
                @csrf
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Product Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="product_name" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="category_id" id="category_id" required>
                            <option value="">Select Category</option>
                        </select>
                    </div>

                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Sub Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="sub_category_id" id="sub_category_id">
                            <option value="">Select Sub Category</option>
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Child Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="child_category_id" id="child_category_id">
                            <option value="">Select Child Category</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-3">
                        <textarea name="description" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Manufacturer</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="manufacturer">
                    </div>
                </div>


                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Brand</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="brand">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Product SKU / Unit</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="sku_unit">
                    </div>
                </div>
                

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="price" min="0" max="999999999" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Special Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="special_price" min="0" max="999999999" required>
                    </div>
                </div>

                <div class="row mt-15">
                    {{-- <div class="col-sm-2">
                        <label class="control-label">Quantity</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="quantity" min="0" max="999999999" required>
                    </div>
                    <div class="col-sm-1"></div> --}}
                    {{-- <div class="col-sm-2">
                        <label class="control-label">Discount (%)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="discount" min="0" max="100">
                    </div> --}}
                </div>

                <div class="row mt-15" style="margin-bottom: 15px;">
                    <div class="col-sm-5">
                        <label for="fileslx">Upload Product Image here</label>
                        <input type="file" id="product_image" class="form-control outlet_doc" name="images[]" multiple="" required>
                        
                    </div>
                </div>

                <div class="gallery">

                </div>

                {{-- <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Role</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option value="">Select from below User Group Profile</option>
                            <option value="manager">Manager</option>
                            <option value="accountant">Accountant</option>
                            <option value="rider">Rider</option>
                        </select>
                    </div>
                </div> --}}

                

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50 create_btn">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editProductModal" role="dialog" aria-labelledby="editProductModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editProductModalLabel">Edit Product</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Product Modal -->
        <form id="editProduct" >  
            <div class="box-body">
                @csrf
                <input type="hidden" name="product_id" value="">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Product Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="product_name_ed" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" id="category_id_ed" name="category_id_ed" required>
                            <option value="">Select Category</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Sub Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" id="sub_category_id_ed" name="sub_category_id_ed">
                            <option value="">Select Sub Category</option>
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Child Category</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select2" id="child_category_id_ed" name="child_category_id_ed">
                            <option value="">Select Child Category</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-3">
                        <textarea name="description_ed" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Manufacturer</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="manufacturer_ed">
                    </div>
                </div>


                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Brand</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="brand_ed">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Product SKU / Unit</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="sku_unit_ed">
                    </div>
                </div>
                

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="price_ed" min="0" max="999999999" required>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Special Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="special_price_ed" min="0" max="999999999" required>
                    </div>
                </div>

                <div class="row mt-15">
                    {{-- <div class="col-sm-2">
                        <label class="control-label">Quantity</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="quantity_ed" min="0" max="999999999" required>
                    </div>
                    <div class="col-sm-1"></div> --}}
                    {{-- <div class="col-sm-2">
                        <label class="control-label">Discount (%)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="discount_ed" min="0" max="100">
                    </div> --}}
                </div>

                <div class="row mt-15" style="margin-bottom: 15px;">
                    <div class="col-sm-5">
                        <label for="fileslx">Upload Product Image here</label>
                        <input type="file" id="product_image_edit" class="form-control" name="images_edit[]" multiple="" >
                        
                    </div>
                </div>

                <div class="box_of_image">

                </div>

                {{-- <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Role</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option value="">Select from below User Group Profile</option>
                            <option value="manager">Manager</option>
                            <option value="accountant">Accountant</option>
                            <option value="rider">Rider</option>
                        </select>
                    </div>
                </div> --}}

                

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="viewProductModal" role="dialog" aria-labelledby="viewProductModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewProductModalLabel">View Product</h4>
        </div>
        <div class="modal-body">

        <!-- View Product Modal -->
        <form >  
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">Product Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="product_name_view" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3">
                        <!-- <input type="number" class="form-control" id="category_id_view" name="category_id_view" disabled> -->
                        <select class="form-control" id="category_id_view" name="category_id_view" required disabled>
                            <option value="">Select Category</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Description</label>
                    </div>
                    <div class="col-sm-3">
                        <textarea name="description_view" disabled class="form-control" rows="3"></textarea>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Manufacturer</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="manufacturer_view" disabled>
                    </div>
                </div>


                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Brand</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="brand_view" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Product SKU / Unit</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="sku_unit_view" disabled>
                    </div>
                </div>
                

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="price_view" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Special Price</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="special_price_view" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    {{-- <div class="col-sm-2">
                        <label class="control-label">Quantity</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="quantity_view" disabled>
                    </div>
                    <div class="col-sm-1"></div> --}}
                    {{-- <div class="col-sm-2">
                        <label class="control-label">Discount (%)</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="discount_view" disabled>
                    </div> --}}
                </div>

                <div class="box_of_image_view">
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

@endsection 


@section('js')


<script src="{{ url('/') }}/jquery/scripting.js?v=2"></script>


<script>
    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        let img_html = '<div class="img_wrapper">'+
                                '<img src="'+ event.target.result +'" alt="Product Image">'+
                                // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
                        $(placeToInsertImagePreview).append(img_html);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $("body").on('change','#product_image', function() {
            $('.gallery').children().remove();
            imagesPreview(this, '.gallery');
        });
        
        
        
    });

    jQuery.validator.addMethod("noSpace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    let editProductForm = $("#editProduct").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editProduct')[0]);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('edit_product')}}",
                data: formdata,
                success: function (r) {
                        console.log(r);
                        $('#editProductModal').modal('hide');
                        Swal.fire(
                            'Success!',
                            'Product Updated Successfully.',
                            'success'
                        );
                        // $.notify("Product Updated Successfully", "success");
                        $('#product_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
               
        },
        messages:
        {	
            
        },
        ignore: [],
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2')) {     
                error.insertAfter(element.next('span'));  // select2
            } else {                                      
                error.insertAfter(element);               // default
            }
        }

    });


    function getProduct(id){
        $("#category_id_ed").val(null).trigger("change"); 
        $("#sub_category_id_ed").html($('<option>', {value: '', text: 'Select Sub Category'})).val(null).trigger("change"); 
        $("#child_category_id_ed").html($('<option>', {value: '', text: 'Select Child Category'})).val(null).trigger("change"); 
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_product')}}",
            data: "id="+id,
            success: function (r) {
                console.log(r);
                create_edit(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_edit(data){
        
        if(data){

            $("input[name=brand_ed]").val(data.product.brand);
            // $("input[name=category_id_ed]").val(data.product.category_id);
            $("#category_id_ed").val( data.product.category_id ).trigger('change');
            
            if(data.product.sub_category_id != 0){
                setTimeout(function(){
                    $("#sub_category_id_ed").val( data.product.sub_category_id ).trigger('change');
                    
                }, 1500);
            }
            if(data.product.child_category_id != 0){
                setTimeout(function(){
                    $("#child_category_id_ed").val( data.product.child_category_id ).trigger('change'); 
                }, 3000);
            }
            
            
            
            $("textarea[name=description_ed]").val(data.product.description);
            // $("input[name=discount_ed]").val(data.product.discount);
            $("input[name=manufacturer_ed]").val(data.product.manufacturer);
            $("input[name=price_ed]").val(data.product.price);
            $("input[name=product_name_ed]").val(data.product.product_name);
            // $("input[name=quantity_ed]").val(data.product.quantity);
            $("input[name=sku_unit_ed]").val(data.product.sku_unit);
            $("input[name=special_price_ed]").val(data.product.special_price);
            $("input[name=product_id]").val(data.product.id);

            let images = data.product.image_url.split(",");
            let out_resp_img = data.product.image_url.split(",");
            let img_html = '<div class="">';
                for( let j = 0; j < out_resp_img.length; j++ ) {
                    let prod_img = "{{URL::asset('/images')}}/" + out_resp_img[j];
                    img_html += '<div class="img_wrapper">'+
                                            '<img src="'+ prod_img +'" alt="Outlet Image">'+
                                            '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                        '</div>';
                }
                img_html += '</div>';   
            $('.box_of_image').html(img_html);    

        }
        editProductForm.resetForm();
        $('#editProductModal').modal('show');        

    }

    function getProductView(id){
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_product')}}",
            data: "id="+id,
            success: function (r) {
                console.log(r);
                create_view(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    }

    function create_view(data){

        if(data){

            $("input[name=brand_view]").val(data.product.brand);
            $("#category_id_view").val( data.product.category_id );
            $("textarea[name=description_view]").val(data.product.description);
            // $("input[name=discount_view]").val(data.product.discount);
            $("input[name=manufacturer_view]").val(data.product.manufacturer);
            $("input[name=price_view]").val(data.product.price);
            $("input[name=product_name_view]").val(data.product.product_name);
            // $("input[name=quantity_view]").val(data.product.quantity);
            $("input[name=sku_unit_view]").val(data.product.sku_unit);
            $("input[name=special_price_view]").val(data.product.special_price);
            //$("input[name=product_id]").val(data.product.id);

            let images = data.product.image_url.split(",");
            let out_resp_img = data.product.image_url.split(",");
            let img_html = '<div class="row mt-15">';
                for( let j = 0; j < out_resp_img.length; j++ ) {
                    img_html += '<div class="col-sm-2">';
                    let outlet_img = "{{URL::asset('/images')}}/" + out_resp_img[j];
                    img_html += '<img class="img-responsive" src="'+ outlet_img +'">'
                    img_html += '</div>';
                }
                img_html += '</div>';   
            $('.box_of_image_view').html(img_html);    

            }
        $('#viewProductModal').modal('show');

    }

    $(function() {
        $(".select2").select2();
        
        $('#product_image_edit').on('change', function() {
            $('.box_of_image').children().remove();
            imagesPreview(this, '.box_of_image');
        });

        $("select[name=category_id]").on('change', function(){
            
            let cat_id = $(this).val();

            $('select[name=sub_category_id]').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_grossery_subcat_by_cat_id')}}",
                data: {
                    "cat_id": cat_id
                },
                success: function (r) {

                    let sub_category = r.sub_category;
                    $('select[name=sub_category_id]').append($('<option>', {value: '', text: 'Select Sub Category'}));
                    if(sub_category.length > 0){
                        $('select[name=sub_category_id]').attr('required',true);                        
                        $.each( sub_category, function(index, value) {

                            $('select[name=sub_category_id]').append($('<option>', {value: value.id, text: value.category_name}));
                        });
                    }
                    else {
                        $('select[name=sub_category_id]').removeAttr('required'); 
                    }
                },
                error:function(e){
                    console.log(e);
                }
            });

        });

        $("#category_id_ed").on('change', function(){
            
            let cat_id = $(this).val();

            $('#sub_category_id_ed').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_grossery_subcat_by_cat_id')}}",
                data: {
                    "cat_id": cat_id
                },
                success: function (r) {
                    // console.log(r);
                    let sub_category = r.sub_category;

                    $('#sub_category_id_ed').append($('<option>', {value: '', text: 'Select Sub Category'}));
                    if(sub_category.length > 0){
                        $('#sub_category_id_ed').attr('required',true); 
                        $.each( sub_category, function(index, value) {

                            $('#sub_category_id_ed').append($('<option>', {value: value.id, text: value.category_name}));
                        });
                    }
                    else {
                        $('#sub_category_id_ed').removeAttr('required'); 
                    }
                },
                error:function(e){
                    console.log(e);
                }
            });

        });

        $("select[name=sub_category_id]").on('change', function(){
            
            let cat_id = $(this).val();

            $('select[name=child_category_id]').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_grossery_childcat_by_subcat_id')}}",
                data: {
                    "cat_id": cat_id
                },
                success: function (r) {

                    let child_category = r.child_category;
                    $('select[name=child_category_id]').append($('<option>', {value: '', text: 'Select Child Category'}));
                    if(child_category.length > 0){
                        $('select[name=child_category_id]').attr('required',true);
                        $.each( child_category, function(index, value) {

                            $('select[name=child_category_id]').append($('<option>', {value: value.id, text: value.category_name}));
                        });
                    }
                    else {
                        $('select[name=child_category_id]').removeAttr('required');
                    }
                },
                error:function(e){
                    console.log(e);
                }
            });

        });

        $("#sub_category_id_ed").on('change', function(){
            
            let cat_id = $(this).val();
            $('#child_category_id_ed').empty();
            
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_grossery_childcat_by_subcat_id')}}",
                data: {
                    "cat_id": cat_id
                },
                success: function (r) {

                    let child_category = r.child_category;

                    $('#child_category_id_ed').append($('<option>', {value: '', text: 'Select Child Category'}));
                    if(child_category.length > 0){
                        $('#child_category_id_ed').attr('required',true);
                        $.each( child_category, function(index, value) {
                            $('#child_category_id_ed').append($('<option>', {value: value.id, text: value.category_name}));
                        });
                    }
                    else {
                        $('#child_category_id_ed').removeAttr('required');
                    }
                },
                error:function(e){
                    console.log(e);
                }
            });

        });
    });

    $(document).ready(function(){

        $('.add_product').on('click',function(){
            $('#createProduct').trigger("reset");
            $("#createProduct").find('span.help-block').remove();
            $("#createProduct").find('.help-block').removeClass("help-block");
            $("#createProduct").find('.error').removeClass("error");
            $("#createProduct").find('.has-success').removeClass("has-success");
            $('#createProduct').find('.form-control-feedback').remove();
            $('.gallery').children().remove();
            $("#category_id").val(null).trigger("change"); 
            $("#sub_category_id").html($('<option>', {value: '', text: 'Select Sub Category'})).val(null).trigger("change"); 
            $("#child_category_id").html($('<option>', {value: '', text: 'Select Child Category'})).val(null).trigger("change"); 
        });
        $('#product_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
            "processing": true,
            "serverSide": true,
            "ajax":{
                        "url": "{{ url('get_product_list') }}",
                        "dataType": "json",
                        "type": "GET",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
            "columns": [
                { "data": "product_name" },
                { "data": "description" },
                { "data": "brand" },
                { "data": "price" },
                { "data": "manufacturer" },
                // { "data": "discount" },
                { "data": "status" },
                { "data": "action" }
            ],
            "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }],
        });

        // $('.create_btn').on('click',function(){
        //     $(this).attr({disabled: true});
        //     $("#createProduct").validate();
        // });
        
        let createProductValidator = $("#createProduct").validate({
            
            submitHandler: function(form) {
                $('.create_btn').attr({disabled: true});          
                                
                let formdata = new FormData($('#createProduct')[0]);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('create_product')}}",
                    data: formdata,
                    success: function (r) {
                            console.log(r);
                            $('#addProductModal').modal('hide');
                            $('.create_btn').removeAttr('disabled');
                            Swal.fire(
                                'Success!',
                                'Product Added Successfully.',
                                'success'
                            );
                            // $.notify("Product added Successfully", "success");
                            $('#product_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            rules:
            {
                
            },
            messages:
            {	
                
            },
            ignore: [],
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) { 
                    error.insertAfter(element.parent());      // radio/checkbox?
                } else if (element.hasClass('select2')) {     
                    error.insertAfter(element.next('span'));  // select2
                } else {                                      
                    error.insertAfter(element);               // default
                }
            }

        });

        $.ajax({
            type: "GET",
            url: "{{ route('get_grossery_cat')}}",
            success: function (r) {
                console.log(r);
                let category = r.category;
                $.each( category, function(index, value) {
                        $("#category_id").append($('<option>', {value: value.id, text: value.category_name}));
                        $("#category_id_ed").append($('<option>', {value: value.id, text: value.category_name}));
                        $("#category_id_view").append($('<option>', {value: value.id, text: value.category_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });

        $(".dataTables_filter input").css({ "width": "250px" });

    });

    $(window).load(function(){
            
        // Update Product Status

        $(document).on('click','input[name="product_status"]',function(){
                        
            let product_status = $(this).attr("data-attr");
            let message = '';
            let product_id = $(this).attr("product-id");
    
            if ( product_status == 0 ) {
                product_status = 1;
                $(this).attr("data-attr", "1");
                message = 'Activated';
            } else {
                product_status = 0;
                $(this).attr("data-attr", "0");
                message = 'Deactivated';
            }
    
            $.ajax({
                type: "POST",
                url: "{{ route('update_product_status')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "product_id": product_id,
                    "product_status": product_status
                },
                success: function (r) {
                        
                        $.notify("Product "+ message +" Successfully", "success");
                        $('#product_table').DataTable().ajax.reload();
                        // $('#past_order_table').DataTable().ajax.reload();
                },
                error:function(e){
                    Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                    console.log(e);
                }
            });
    
        });

    });


    function delete_product( product_id ) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('delete_product')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "product_id": product_id
                    },
                    success: function (r) {
                        Swal.fire(
                            'Success!',
                            'Product Deleted Successfully.',
                            'success'
                        );
                        // $.notify("Product Deleted Successfully", "success");
                        $('#product_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            }
        });

    }

    $("#category_id").on("select2:close", function (e) {  
            $(this).valid();
    });
    $("#category_id_ed").on("select2:close", function (e) {  
            $(this).valid();
    });


</script>

@endsection


