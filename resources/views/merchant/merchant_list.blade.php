@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
<link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
<script src="https://unpkg.com/dropzone"></script>
<script src="https://unpkg.com/cropperjs"></script>
<style>
    .remove_outlet_details_box, .edit_remove_outlet_details_box {
        position: absolute;
        right: 71px;
        top: -12px;
        border: 1px solid #000;
        padding: 2px 6px;
        background-color: red;
        border-radius: 50%;
        z-index: 1;
        cursor: pointer;
    }
    .outlet_label_count {
        font-weight: bolder;
        font-size: 15px;
    }
</style>
<style>
    .pac-container{
        z-index: 9999!important;
    }

    .image_area {
        position: relative;
    }

    img {
        /* display: block; */
        max-width: 100%;
    }

    .preview, .ed_preview {
        overflow: hidden;
        width: 160px; 
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg{
        max-width: 1000px !important;
    }

    .overlay {
        position: absolute;
        bottom: 10px;
        left: 0;
        right: 0;
        background-color: rgba(255, 255, 255, 0.5);
        overflow: hidden;
        height: 0;
        transition: .5s ease;
        width: 100%;
    }

    .image_area:hover .overlay {
        height: 50%;
        cursor: pointer;
    }

    .text {
        color: #333;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }
    #attachment_doc_html{
        font-size: 16px;
        font-weight: 600;
    }

    #attachment_doc_html .modal-title{
        font-size: 24px;
    }

    #attachment_doc_html div{
        padding: 10px;
    }
    #attachment_doc_html a{
        color: blue;
    }
</style>

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Merchant Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success btn-flat add_merchant mr-15" data-toggle="modal" data-target="#addMerchantModal"><i class="fa fa-plus"></i> Add Merchant</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadEmployeeModal"><i class="fa fa-plus"></i> Bulk Upload Employee</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                </div>
                <div class="box-body">

                   
                    <div class="table-responsive mt-15">
                            
                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Merchant Name</th>
                                    <th>User Name</th>
                                    <th>Owner Name</th>
                                    <th>Email</th>
                                    <th>city</th>
                                    <th>State</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    
                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="addMerchantModal" tabindex="-1" role="dialog" aria-labelledby="addMerchantModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="addMerchantModalLabel">Add Merchant</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id="createMerchant">  
                        <input type="hidden" value="1" class="outlet_count" name="outlet_count">
                        <input type="hidden" value="1" class="no_outlet" name="no_outlet">
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="control-label">User Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="user_name" name="user_name" required>
                                <span id="custom_error" class="help-block error custom_error" style="display: none"></span>
                                @csrf
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Password *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                                @error('password')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">First Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required>
                                @error('first_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Last Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                                @error('last_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Merchant Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="merchant_name" value="{{ old('merchant_name') }}" required>
                                @error('merchant_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Residential Address *</label>
                            </div>
                            <div class="col-sm-5">
                                <textarea name="address" required class="form-control" rows="3">{{ old('address') }}</textarea>
                                @error('address')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Country</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="country" required value="INDIA">
                                @error('country')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">State *</label>
                            </div>
                            <div class="col-sm-3">
                                <!-- <input type="text" class="form-control" name="state" required value="{{ old('state') }}"> -->
                                <select class="form-control" id="state" name="state" required value="{{ old('state') }}">
                                    <option value="">Select State</option>
                                </select>
                                @error('state')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">City *</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="city" name="city" value="{{ old('city') }}" required >
                                    <option value="">Select City</option>
                                </select>
                                @error('city')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Pin Code *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="pin_code" value="{{ old('pin_code') }}" required>
                                @error('pin_code')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Email *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Contact *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="contact" value="{{ old('contact') }}" required> 
                                @error('contact')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-10">
                                <label class="control-label">Outlet Details</label>
                            </div>
                            
                        </div>

                        <div id="outlet_details" class="row mt-15">
                            
                        </div>

                        <div class="row mt-15 text-center">
                            <div style="float: right">
                                <button id="add_outlet" class="btn btn-sm btn-info btn-flat" style="margin-right: 68px;">Add Outlet</button>
                            </div>
                        </div>

                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="create_merchant" type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 
                    
                    </form>

                    <div id="outlet_details_box" class="row" style="display: none;position: relative;">
                        <div id="remove_outlet_details_box"><i class="fa fa-remove"></i></div>
                        <div class="col-sm-1"></div>

                        <div class="col-sm-10" style="border: 1px solid #c3c3c3;">
                            <div class="mt-15">
                                <label class="outlet_label_count">#Outlet 1:</label>
                            </div>
                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet User Name *</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="outlet_user_name[]" required>
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Password *</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="password" class="form-control" name="outlet_password[]" required>
                                </div>
                            </div>
                            
                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Name *</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="outlet_name[]">
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">GST #</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="gst[]">
                                    
                                </div>
                            </div>

                            <div class="row mt-15 mb-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Speciality</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="outlet_speciality[]">
                                    
                                </div>
                                {{-- <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <label class="control-label">Booking Rate</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two[]">
                                    
                                </div> --}}

                                <div class="col-sm-2">
                                    <label class="control-label">Status</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" name="flag[]" required>
                                        <option value="0">Active</option>
                                        <option value="1">InActive</option>
                                    </select>
                                </div>

                            </div>

                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">FSSAI License</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="fssai_license[]">
                                    
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">Shop License</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="shop_license[]">
                                    
                                </div>
                            </div>

                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">GST Rate (%)</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="gst_rate[]">
                                    
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">Taxes/Delivery Charges</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="delivery_charges[]">
                                    
                                </div>
                            </div>

                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Available Time</label>
                                </div>
                                <div class="col-sm-3 bootstrap-timepicker">
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker" autocomplete="off" name="open_time[]">
                                        <div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i> Open
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2">
                                    <label class="control-label">Available Time</label>
                                </div>
                                <div class="col-sm-3 bootstrap-timepicker">
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker" autocomplete="off" name="close_time[]">
                                            <div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i> Close
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Type *</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control outlet_type_select_add" name="outlet_type[]" required>
                                        <option value="">Select Type</option>
                                        <option value="product">Grocery</option>
                                        <option value="recipe">Restaurant</option>
                                    </select>
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Category Type *</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control outlet_category_type_add" name="outlet_category_type[]" required>
                                    </select>
                                </div>
                                <!-- <div class="col-sm-2">
                                    <label class="control-label">Geo Address</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" id="address-input" name="address_address" class="form-control map-input">
                                    <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                                    <input type="hidden" name="address_longitude" id="address-longitude" value="0" />
                                </div> -->
                            </div>

                            <!-- <div class="row-mt-15">
                                <div id="address-map-container" style="width:100%;height:400px; display: none;">
                                    <div style="width: 100%; height: 100%" id="address-map"></div>
                                </div>
                            </div> -->

                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Image *</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="file" class="form-control add_rest_img outlet_image_upload" count_attr="0"  name="image_url0" required>
                                    <input type="hidden" class="form-control outlet_image_name" name="outlet_image_name_0">
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">Mobile No. *</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="outlet_mobile[]" required>
                                </div>
                            </div>

                            <div class="row mt-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Geo Address</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text"name="geo_address_add[]" class="controls form-control geo_address_add " id="geo_address_add0">
                                    <div class="map" id="map0"></div>
                                    <input type="hidden" class="geo_city" id="geo_city0" name="geo_city[]" />
                                    <input type="hidden" class="address_latitude_add" name="address_latitude_add[]" id="address_latitude_add0" value="0" />
                                    <input type="hidden" class="address_longitude_add" name="address_longitude_add[]" id="address_longitude_add0" value="0" />
                                </div>
                            </div>

                            <div class="row mt-15">
                                <div class="col-sm-8">
                                    <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>
                                </div>
                                
                                <div class="col-sm-10">
                                    <label for="fileslx">Upload doc here</label>
                                    <input type="file" class="form-control outlet_doc" name="document0[]" multiple>
                                    
                                </div>
                            </div>

                            <div class="row mt-15 mb-15">
                                <div class="col-sm-2">
                                    <label class="control-label">Outlet Address *</label>
                                </div>
                                <div class="col-sm-4">
                                    <textarea name="outlet_address[]" class="form-control" rows="3" required></textarea>
                                    
                                </div>
                                {{-- <div class="col-sm-1"></div> --}}
                                <div class="col-sm-2">
                                    <label class="control-label">Branch</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="branch[]">
                                    
                                </div>
                            </div>

                        </div>
                    {{-- <div class="col-sm-1"></div> --}}
                </div>   
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal fade" id="viewMerchantModal" tabindex="-1" role="dialog" aria-labelledby="viewMerchantModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewMerchantModalLabel">View Merchant</h4>
        </div>
        <div class="modal-body">

        <!-- View Merchant Form -->
          <form>  
            
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">User Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="user_name_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Password</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="password" class="form-control" name="password_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="first_name_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Merchant Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="merchant_name_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Residential Address</label>
                        </div>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="3" name="address_view" disabled></textarea>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Country</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="country_view" value="INDIA" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">State</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="state_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                    <div class="col-sm-2">
                            <label class="control-label">City</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="city_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Pin Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="pin_code_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Contact</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="contact_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                        <div class="col-sm-2">
                            <label class="control-label">Outlet Details</label>
                        </div>
                        <div class="col-sm-10"></div>

                    </div>

                    <div id="view_outlet_details" class="row mt-15">
                        
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-10">
                        </div>
                        <div class="col-sm-2">
                            <!-- <button id="edit_add_outlet" class="btn btn-sm btn-info btn-flat mr-50">Add Outlet</button> -->
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <!-- <button id="create_merchant" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                            <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button> -->
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

        </div>
        
      </div>
    </div>
</div>

<div class="modal fade " id="editMerchantModal" tabindex="-1" role="dialog" aria-labelledby="editMerchantModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editMerchantModalLabel">Edit Merchant</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Merchant Form -->
          <form id="editMerchant">  
            
            <div class="box-body">

                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">User Name</label>
                        </div>
                        @csrf
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="user_name_ed" required>
                            <input type="hidden" name="merchant_id_edit">
                            <input type="hidden" class="outlet_count_edit" name="outlet_count_edit">
                            <input type="hidden" value="1" class="no_outlet_edit" name="no_outlet_edit">
                            
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Password</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="password" class="form-control" name="password_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="first_name_ed" required>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Merchant Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="merchant_name_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Residential Address</label>
                        </div>
                        <div class="col-sm-5">
                            <textarea class="form-control" name="address_ed" rows="3" required></textarea>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Country</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="country_ed" value="INDIA" required>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">State</label>
                        </div>
                        <div class="col-sm-3">
                            <!-- <input type="text" class="form-control" name="state_ed" required> -->
                            <select class="form-control" id="state_ed" name="state_ed" required >
                                <option value="">Select State</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">City</label>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="city_ed" name="city_ed" required >
                                <option value="">Select City</option>
                            </select>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Pin Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="pin_code_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Contact</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="contact_ed" required>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                        <div class="col-sm-2">
                            <label class="control-label">Outlet Details</label>
                        </div>
                        <div class="col-sm-10"></div>

                    </div>

                    

                    <div id="edit_outlet_details" class="row mt-15">
                        
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-10">
                        </div>
                        <div class="col-sm-2">
                            <button id="edit_add_outlet" class="btn btn-sm btn-info btn-flat" style="margin-right: 68px;">Add Outlet</button>
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="update_merchant" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                            <button class="btn btn-sm btn-warning btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            <!-- <button onclick="return confirm('are you sure to delete?')" class="btn btn-sm btn-danger btn-flat mr-50">Delete</button> -->
                        </div>
                    </div>

                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

          <div id="edit_outlet_details_box" class="row mt-15" style="display: none; position: relative;">
            <div id="edit_remove_outlet_details_box"><i class="fa fa-remove"></i></div>
            <div class="col-sm-1"></div>
            <div class="col-sm-10" style="border: 1px solid #c3c3c3;">
                <div class="mt-15">
                    <label class="outlet_label_count">#Outlet 1:</label>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Outlet User Name</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control outlet_control" name="outlet_user_name_ed[]" required
                        >
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Password</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="password" class="form-control outlet_control" name="outlet_password_ed[]" required>
                    </div>
                </div>
                
                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Name</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control outlet_control" name="outlet_name_ed[]" required>
                        <input type="hidden" name="outlet_id[]">
                        <input type="hidden" name="merchant_id_ed[]">
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">GST #</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="gst_ed[]">
                    </div>
                </div>

                <div class="row mt-15 mb-15">
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Speciality</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="outlet_speciality_ed[]">
                        
                    </div>
                    {{-- 
                    <div class="col-sm-2">
                        <label class="control-label">Booking Rate</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two_ed[]">
                        
                    </div> --}}
                    
                    <div class="col-sm-2">
                        <label class="control-label">Status</label>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control" name="flag_ed[]" required>
                            <option value="0">Active</option>
                            <option value="1">InActive</option>
                        </select>
                    </div>

                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">FSSAI License</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="fssai_license_ed[]">
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">Shop License</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="shop_license_ed[]">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">GST Rate (%)</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="gst_rate_ed[]">
                        
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">Taxes/Delivery Charges</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="delivery_charges_ed[]">
                        
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Available Time</label>
                    </div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" autocomplete="off" name="open_time_ed[]">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> Open
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Available Time</label>
                    </div>
                    <div class="col-sm-3 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" autocomplete="off" name="close_time_ed[]">
                                <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> Close
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Type</label>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control outlet_type_select outlet_control" name="outlet_type_ed[]" required>
                            <option value="">Select Type</option>
                            <option value="product">Grocery</option>
                            <option value="recipe">Restaurant</option>
                        </select>
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Category Type</label>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control outlet_category_type outlet_control" name="outlet_category_type_ed[]" required>
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Image</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="file" class="form-control ed_rest_img outlet_control" name="ed_rest_img0" required>
                        <div id="ed_rest_img"></div>
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">Mobile No.</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="outlet_mobile_ed[]" required>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Geo Address</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text"name="ed_geo_address_add[]" class="controls form-control ed_geo_address_add " id="ed_geo_address_add0">
                        <div class="map" id="map1"></div>
                        <input type="hidden" class="ed_geo_city" id="ed_geo_city0" name="ed_geo_city[]" />
                        <input type="hidden" class="ed_address_latitude_add" name="ed_address_latitude_add[]" id="ed_address_latitude_add0" value="0" />
                        <input type="hidden" class="ed_address_longitude_add" name="ed_address_longitude_add[]" id="ed_address_longitude_add0" value="0" />
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-12">
                        <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>
                    </div>
                    <div class="col-sm-10">
                        <label for="fileskx">Upload doc here</label>
                        <input type="file" id="fileskx" class="form-control ed_outlet_doc" name="documented0[]" multiple>
                    </div>
                </div>

                <div class="row mt-15 mb-15">
                    <div class="col-sm-2">
                        <label class="control-label">Outlet Address</label>
                    </div>
                    <div class="col-sm-4">
                        <textarea name="outlet_address_ed[]" class="form-control" rows="3" required></textarea>
                    </div>
                    
                    <div class="col-sm-2">
                        <label class="control-label">Branch</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="branch_ed[]">
                    </div>
                </div>

            </div>
            
        </div>

        </div>
        
      </div>
    </div>
</div>

<div class="modal fade" id="bulkUploadEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="bulkUploadEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="bulkUploadEmployeeModalLabel">Bulk Upload Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload Merchant Form -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label">Select file to upload: </label>
                    </div>
                    <div class="col-sm-10">
                        <input type="file" id="bulk_employee_files" class="form-control">
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>

<div class="modal fade" id="offboardOutletModal" tabindex="-1" role="dialog" aria-labelledby="offboardOutletModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="offboardOutletModalLabel">Offboarding Outlet</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload Merchant Form -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Selcted Merchant / Vendor</label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-3">
                        <label class="control-label">Select Outlet</label>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control">
                            <option value="">Select outlet from list</option>
                        </select>
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>

<div class="modal fade" id="image_crop_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="" id="sample_image" />
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop" class="btn btn-primary">Crop</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


@endsection 


@section('js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBb4GVJI3MIpjcwZwG72m6HxDa_Cv1qWNM&libraries=places&callback=initAutocomplete"
		 async defer></script>

<script type="text/javascript">
    function initAutocomplete() {
        var input = document.getElementById('geo_address_add0');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.setComponentRestrictions(
                {'country': ['in']}
        );
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            console.log(place);
            
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('geo_city0').value = place_name ;
            document.getElementById('address_latitude_add0').value = latitude;
            document.getElementById('address_longitude_add0').value =longitude;
        });

        var input = document.getElementById('geo_address_add1');
        var autocomplete1 = new google.maps.places.Autocomplete(input);
        autocomplete1.setComponentRestrictions(
                {'country': ['in']});
        
        google.maps.event.addListener(autocomplete1, 'place_changed', function () {
            var place = autocomplete1.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;


            document.getElementById('geo_city1').value = place_name ;
            document.getElementById('address_latitude_add1').value = latitude;
            document.getElementById('address_longitude_add1').value =longitude;
        });

        var input = document.getElementById('geo_address_add2');
        var autocomplete2 = new google.maps.places.Autocomplete(input);
        autocomplete2.setComponentRestrictions(
                {'country': ['in']});
        
        google.maps.event.addListener(autocomplete2, 'place_changed', function () {
            var place = autocomplete2.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;


            document.getElementById('geo_city2').value = place_name ;
            document.getElementById('address_latitude_add2').value = latitude;
            document.getElementById('address_longitude_add2').value =longitude;
        });

        var input = document.getElementById('geo_address_add3');
        var autocomplete3 = new google.maps.places.Autocomplete(input);
        autocomplete3.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete3, 'place_changed', function () {
            var place = autocomplete3.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;


            document.getElementById('geo_city3').value = place_name ;
            document.getElementById('address_latitude_add3').value = latitude;
            document.getElementById('address_longitude_add3').value =longitude;
        });

        var input = document.getElementById('geo_address_add4');
        var autocomplete4 = new google.maps.places.Autocomplete(input);
        autocomplete4.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete4, 'place_changed', function () {
            var place = autocomplete4.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;

            document.getElementById('geo_city4').value = place_name ;
            document.getElementById('address_latitude_add4').value = latitude;
            document.getElementById('address_longitude_add4').value =longitude;
        });

        var input = document.getElementById('geo_address_add5');
        var autocomplete5 = new google.maps.places.Autocomplete(input);
        autocomplete5.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete5, 'place_changed', function () {
            var place = autocomplete5.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('geo_city5').value = place_name ;
            document.getElementById('address_latitude_add5').value = latitude;
            document.getElementById('address_longitude_add5').value =longitude;
        });

        var input = document.getElementById('geo_address_add6');
        var autocomplete6 = new google.maps.places.Autocomplete(input);
        autocomplete6.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete6, 'place_changed', function () {
            var place = autocomplete6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('geo_city6').value = place_name ;
            document.getElementById('address_latitude_add6').value = latitude;
            document.getElementById('address_longitude_add6').value =longitude;
        });
    }

    function initAutocompleteEd() {
        var input = document.getElementById('ed_geo_address_add0');
        var autocomplete_ed1 = new google.maps.places.Autocomplete(input);
        autocomplete_ed1.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed1, 'place_changed', function () {
            var place = autocomplete_ed1.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city0').value = place_name ;
            document.getElementById('ed_address_latitude_add0').value = latitude;
            document.getElementById('ed_address_longitude_add0').value =longitude;
        });

        var input = document.getElementById('ed_geo_address_add1');
        var autocomplete_ed2 = new google.maps.places.Autocomplete(input);
        autocomplete_ed2.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed2, 'place_changed', function () {
            var place = autocomplete_ed2.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city1').value = place_name ;
            document.getElementById('ed_address_latitude_add1').value = latitude;
            document.getElementById('ed_address_longitude_add1').value =longitude;
        });

        var input = document.getElementById('ed_geo_address_add2');
        var autocomplete_ed3 = new google.maps.places.Autocomplete(input);
        autocomplete_ed3.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed3, 'place_changed', function () {
            var place = autocomplete_ed3.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city2').value = place_name ;
            document.getElementById('ed_address_latitude_add2').value = latitude;
            document.getElementById('ed_address_longitude_add2').value =longitude;
        });

        var input = document.getElementById('ed_geo_address_add3');
        var autocomplete_ed4 = new google.maps.places.Autocomplete(input);
        autocomplete_ed4.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed4, 'place_changed', function () {
            var place = autocomplete_ed4.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city3').value = place_name ;
            document.getElementById('ed_address_latitude_add3').value = latitude;
            document.getElementById('ed_address_longitude_add3').value =longitude;
        });

        var input = document.getElementById('ed_geo_address_add4');
        var autocomplete_ed5 = new google.maps.places.Autocomplete(input);
        autocomplete_ed5.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed5, 'place_changed', function () {
            var place = autocomplete_ed5.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city4').value = place_name ;
            document.getElementById('ed_address_latitude_add4').value = latitude;
            document.getElementById('ed_address_longitude_add4').value =longitude;
        });

        var input = document.getElementById('ed_geo_address_add5');
        var autocomplete_ed6 = new google.maps.places.Autocomplete(input);
        autocomplete_ed6.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed6, 'place_changed', function () {
            var place = autocomplete_ed6.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city5').value = place_name ;
            document.getElementById('ed_address_latitude_add5').value = latitude;
            document.getElementById('ed_address_longitude_add5').value =longitude;
        });

        var input = document.getElementById('ed_geo_address_add6');
        var autocomplete_ed7 = new google.maps.places.Autocomplete(input);
        autocomplete_ed7.setComponentRestrictions(
                {'country': ['in']});
        google.maps.event.addListener(autocomplete_ed7, 'place_changed', function () {
            var place = autocomplete_ed7.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var place_name = place.name;
            document.getElementById('ed_geo_city6').value = place_name ;
            document.getElementById('ed_address_latitude_add6').value = latitude;
            document.getElementById('ed_address_longitude_add6').value =longitude;
        });

    }

    
</script>
<script src="{{ asset('dist/js/mapInput.js') }}"></script>
<script>
    $(document).ready(function(){
        
        var $modal = $('#image_crop_modal');

        var image = document.getElementById('sample_image');

        var cropper;
        window.cropedimage = 0;
        $("body").on('change','.add_rest_img', function(event) {
            window.inputCount = $(this).attr('count_attr');
            var files = event.target.files;

            var done = function(url){
                image.src = url;
                $modal.modal('show');
                window.cropedimage = 0;
            };

            if(files && files.length > 0)
            {
                reader = new FileReader();
                reader.onload = function(event)
                {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 2,
                viewMode: 3,
                preview:'.preview'
            });
        }).on('hidden.bs.modal', function(){
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function(){
            canvas = cropper.getCroppedCanvas({
                width:600,
                height:300
            });

            canvas.toBlob(function(blob){
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function(){
                    var base64data = reader.result;
                    $.ajax({
                        url:'{{ route('admin_crop_outlet_image') }}',
                        method:'POST',
                        data:{_token: CSRF_TOKEN, image:base64data},
                        success:function(data)
                        {
                            $modal.modal('hide');
                            window.cropedimage = 1;
                            $('input[name=outlet_image_name_'+window.inputCount+']').html();
                            $('input[name=outlet_image_name_'+window.inputCount+']').val(data.image_name);
                            // $('#outlet_image_name').val(data.image_name);
                            $('html').css('overflow','hidden');
                            $('#addMerchantModal').css('overflow-x','hidden');
                            $('#addMerchantModal').css('overflow-y','auto');
                        }
                    });
                };
            });
        });

        $('#image_crop_modal').on('hidden.bs.modal', function () {
            // if (window.cropedimage == 0) {
            //     $('#outlet_image_name').val('');
            //     $('#outlet_image_upload').val('');
            // }
            $('html').css('overflow','hidden');
            $('#addMerchantModal').css('overflow-x','hidden');
            $('#addMerchantModal').css('overflow-y','auto');
        })

        $('#addMerchantModal').on('hidden.bs.modal', function () {
            $('html').css('overflow-y','auto');
            $('html').css('overflow-x','hidden');
        })
        
    });
</script>
<script>
    
    // console.log( "{{ config('GOOGLE_MAPS_API_KEY', '') }}" );

    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            if (input.files) {
                $(input).parent().children('.img_wrapper').remove();
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        img_html = '<div class="img_wrapper">'+
                                '<img src="'+ event.target.result +'" alt="Outlet Image">'+
                                // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
                        $(input).parent().append(img_html);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $("body").on('change','.outlet_doc', function() {
            // $('#outlet_doc_add_preview').children().remove();
            imagesPreview(this, '#demo');
        });
        // $("body").on('change','.add_rest_img', function() {
        //     debugger;
        //     // $('#outlet_doc_add_preview').children().remove();
        //     imagesPreview(this, '#demo');
        // });
        $("body").on('change','.ed_rest_img', function() {
            // $('#outlet_doc_add_preview').children().remove();
            imagesPreview(this, '#demo');
        });
        $("body").on('change','.view_rest_img', function() {
            // $('#outlet_doc_add_preview').children().remove();
            imagesPreview(this, '#demo');
        });
        
        
    });

    function getMerchant(id){
        
        $.ajax({
                    type: "GET",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('get_merchant')}}",
                    data: "id="+id,
                    success: function (r) {
                        console.log(r);
                        create_edit(r);
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
    }

    function getMerchantView(id){
        
        $.ajax({
                    type: "GET",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('get_merchant')}}",
                    data: "id="+id,
                    success: function (r) {
                        console.log(r);
                        create_view(r);
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
    }

    function create_view(data) { 

        $("div.view_js_add").remove();

        if(data){

            $("input[name=user_name_view]").val(data.merchant.user_name);
            $("input[name=merchant_name_view]").val(data.merchant.merchant_name);
            $("input[name=first_name_view]").val(data.merchant.first_name);
            $("input[name=last_name_view]").val(data.merchant.last_name);
            $("textarea[name=address_view]").val(data.merchant.address);
            $("input[name=password_view]").val(data.merchant.password);
            $("input[name=state_view]").val(data.merchant.state);
            $("input[name=city_view]").val(data.merchant.city);
            $("input[name=country_view]").val(data.merchant.country);
            $("input[name=pin_code_view]").val(data.merchant.pin_code);
            $("input[name=email_view]").val(data.merchant.email);
            $("input[name=contact_view]").val(data.merchant.contact);

            console.log(data.outlet.length);

            if ( data.outlet.length > 0 ) {    
                for( let i = 0; i < data.outlet.length; i++ ) {
                    let decode_img , outlet_img_html = '';
                    if(data.outlet[i].image_url){

                        decode_img = JSON.parse( data.outlet[i].image_url );

                        let rest_img = "{{URL::asset('/images')}}/" + decode_img;
                    
                        outlet_img_html = '<div class="img_wrapper">'+
                                            '<img src="'+ rest_img +'" alt=" ">'+
                                            // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                        '</div>';
                    }

                    // console.log( out_resp_img );
                    let img_html = '<div class="row mt-15" style="padding: 0 15px;">';
                    
                    if ( data.outlet[i]['img_url']  ) {

                        let out_resp_img = JSON.parse( data.outlet[i]['img_url'] );

                        if ( out_resp_img.length > 0 ) {
                            for( let j = 0; j < out_resp_img.length; j++ ) {
                                
                                let outlet_img = "{{URL::asset('/images')}}/" + out_resp_img[j];
                                img_html += '<div class="img_wrapper">'+
                                    '<img src="'+ outlet_img +'" alt=" ">'+
                                    // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                '</div>';
                            }
                        }

                    }
                    img_html += '</div>';
                    let html = '<div class="row view_outlet_details_box mt-15 view_js_add">\
                                <div class="col-sm-1"></div>\
                                <div class="col-sm-10" style="border: 1px solid #c3c3c3;">\
                                    <div class="mt-15">\
                                        <label class="outlet_label_count">#Outlet 1:</label>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet User Name</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="outlet_user_name_view[]" value="' + data.outlet[i]['outlet_user_name'] + '" required disabled>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Password</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="password" class="form-control" name="outlet_password_view[]" value="' + data.outlet[i]['password'] + '" disabled>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Name</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="outlet_name_view[]" value="' + data.outlet[i]['outlet_name'] + '" required disabled>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">GST #</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="gst_view[]" value="' + ( (data.outlet[i]['gst']) == null ? "" : data.outlet[i]['gst'] ) + '" disabled>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15 mb-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Speciality</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="outlet_speciality_view[]" value="' + ( (data.outlet[i]['outlet_speciality'] == null ) ? "" : data.outlet[i]['outlet_speciality'] ) + '" disabled>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Status</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <select class="form-control" name="flag_view[]" disabled>\
                                                <option value="1" '+(data.outlet[i]['flag'] == 1 ? "selected" : " ")+' >InActive</option>\
                                                <option value="0" '+(data.outlet[i]['flag'] == 0 ? "selected" : " ")+' >Active</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">FSSAI License</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="fssai_license_view[]" value="' + ( (data.outlet[i]['fssai_license']) == null ? "" : data.outlet[i]['fssai_license'] ) + '" disabled>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Shop License</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="shop_license_view[]" value="' + ( (data.outlet[i]['shop_license']) == null ? "" : data.outlet[i]['shop_license'] ) + '" disabled>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">GST Rate</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="gst_rate_view[]" value="' + ( (data.outlet[i]['gst_rate']) == null ? "" : data.outlet[i]['gst_rate'] ) + '" disabled>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Taxes/Delivery Charges</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="delivery_charges_view[]" value="' + ( (data.outlet[i]['delivery_charges']) == null ? "" : data.outlet[i]['delivery_charges'] ) + '" disabled>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Available Time</label>\
                                        </div>\
                                        <div class="col-sm-3 bootstrap-timepicker">\
                                            <div class="input-group">\
                                                <input type="text" class="form-control timepicker" autocomplete="off" disabled value="' + ( (data.outlet[i]['open_time']) == null ? "" : data.outlet[i]['open_time'] ) + '" name="open_time_view[]">\
                                                <div class="input-group-addon">\
                                                <i class="fa fa-clock-o"></i> Open\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-1"></div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Available Time</label>\
                                        </div>\
                                        <div class="col-sm-3 bootstrap-timepicker">\
                                            <div class="input-group">\
                                                <input type="text" class="form-control timepicker" autocomplete="off" disabled value="' + ( (data.outlet[i]['close_time']) == null ? "" : data.outlet[i]['close_time'] ) + '"  name="close_time_view[]">\
                                                    <div class="input-group-addon">\
                                                <i class="fa fa-clock-o"></i> Close\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Type</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <select class="form-control" name="outlet_type_view[]" disabled>\
                                                <option value="">Select Type</option>\
                                                <option value="product" ' + ( ( data.outlet[i]['outlet_type'] == "product" ) ? "selected" : "" ) + '>Grocery</option>\
                                                <option value="recipe" ' + ( ( data.outlet[i]['outlet_type'] == "recipe" ) ? "selected" : "" ) + '>Restaurant</option>\
                                            </select>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Category Type</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <select class="form-control outlet_category_type" id="outlet_category_type_view'+ i +'" disabled>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Image</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            '+ outlet_img_html +'\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Mobile No.</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="outlet_mobile_view[]" value="' + ( (data.outlet[i]['mobile']) == null ? "" : data.outlet[i]['mobile'] ) + '" disabled>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Geo Address</label>\
                                        </div>\
                                        <div class="col-sm-9">\
                                            <input type="text" class="controls form-control ed_geo_address_add " value="'+data.outlet[i]['outlet_geoaddress']+'" disabled>\
                                            <div class="map" id="map'+ i +'"></div>\
                                            <input type="hidden" class="ed_geo_city" />\
                                            <input type="hidden" class="ed_address_latitude_add"   value="'+data.outlet[i]['outlet_latitude']+'" />\
                                            <input type="hidden" class="ed_address_longitude_add"   value="'+data.outlet[i]['outlet_longitude']+'" />\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-12">\
                                            <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>\
                                        </div>\
                                        <div class="col-sm-10">\
                                            <label>Upload doc here</label>\
                                            <input type="file" class="form-control view_outlet_doc" name="documentview'+ i +'[]" multiple disabled>\
                                        </div>\
                                    </div>'+ img_html +'<div class="row mt-15 mb-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Address</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <textarea name="outlet_address_view[]" class="form-control" rows="3" disabled>' + ( (data.outlet[i]['outlet_address']) == null ? "" : data.outlet[i]['outlet_address'] ) + '</textarea>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Branch</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="branch_view[]" value="' + ( (data.outlet[i]['branch']) == null ? "" : data.outlet[i]['branch'] ) + '" disabled>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';

                            // ---------booking rate field---------

                            // <div class="col-sm-1"></div>\
                            //             <div class="col-sm-2">\
                            //                 <label class="control-label">Booking Rate</label>\
                            //             </div>\
                            //             <div class="col-sm-3">\
                            //                 <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two_view[]" value="' + ( (data.outlet[i]['booking_for_two'] == null ) ? "" : data.outlet[i]['booking_for_two'] ) + '" disabled>\
                            //             </div>\

                            // ---------booking rate field end---------

                    $("#view_outlet_details").addClass("mt-15").append(html);
                    $.ajax({
                        type: "GET",
                        url: "{{ route('get_all_outlet_categories')}}",
                        success: function (r) {
                            console.log(r);
                            $('#outlet_category_type_view' + i).append($('<option>', {value: '', text: 'Select Outlet Category'}));
                            $.each( r, function(index, value) {
                                $("#outlet_category_type_view" + i).append($('<option>', {value: value.id, text: value.category_name}));
                            });
                            $("#outlet_category_type_view" + i).val(data.outlet[i].outlet_category_id);
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                }

            }
        }
        $('#viewMerchantModal').modal('show');

    }

    function create_edit(data){

        // To remove other outlet boxes
        $("div.edit_outlet_details_box").remove();

        if(data){

            $("input[name=user_name_ed]").val(data.merchant.user_name);
            $("input[name=merchant_name_ed]").val(data.merchant.merchant_name);
            $("input[name=merchant_id_edit]").val(data.merchant.id);
            $("input[name=first_name_ed]").val(data.merchant.first_name);
            $("input[name=last_name_ed]").val(data.merchant.last_name);
            $("textarea[name=address_ed]").val(data.merchant.address);
            $("input[name=password_ed]").val(data.merchant.password);
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_state_by_name')}}",
                data: {
                    "state_name": data.merchant.state
                },
                success: function (r) {
                    let state_id = r.state[0].state_name;
                    // console.log( typeof( state_id ) + " : " + state_id );
                    $("#state_ed").val( state_id );
                    $('#city_ed').empty();

                    // Get All Cities for selected state
                    $.ajax({
                        type: "GET",
                        url: "{{ route('get_all_cities_by_state_id')}}",
                        data: {
                            "state_id": state_id
                        },
                        success: function (r) {
                            // console.log(r);
                            let cities = r.cities;
                            $('#city_ed').append($('<option>', {value: '', text: 'Select City'}));
                            $.each( cities, function(index, value) {
                                // console.log( index + ' : ' + value.state_name );
                                $('#city_ed').append($('<option>', {value: value.city_name, text: value.city_name}));
                            });

                            // Get Selected city for merchant
                            $.ajax({
                                type: "GET",
                                url: "{{ route('get_city_by_name')}}",
                                data: {
                                    "city_name": data.merchant.city
                                },
                                success: function (r) {
                                    let city_id = r.city[0].city_name;
                                    $("#city_ed").val( city_id );
                                },
                                error:function(e){
                                    console.log(e);
                                }
                            });
                                
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                    
                },
                error:function(e){
                    console.log(e);
                }
            });

            $("#state_ed").on('change', function(){
            
                let state = $(this).val();

                $('#city_ed').empty();
                
                $.ajax({
                    type: "GET",
                    url: "{{ route('get_all_cities_by_state_id')}}",
                    data: {
                        "state_id": state
                    },
                    success: function (r) {
                        // console.log(r);
                        let cities = r.cities;
                        $('#city_ed').append($('<option>', {value: '', text: 'Select City'}));
                        $.each( cities, function(index, value) {
                            // console.log( index + ' : ' + value.state_name );
                            $('#city_ed').append($('<option>', {value: value.city_name, text: value.city_name}));
                        });
                    },
                    error:function(e){
                        console.log(e);
                    }
                });

            });

            $("input[name=country_ed]").val(data.merchant.country);
            $("input[name=pin_code_ed]").val(data.merchant.pin_code);
            $("input[name=email_ed]").val(data.merchant.email);
            $("input[name=contact_ed]").val(data.merchant.contact);

            let div_id = "editem";
            
            if ( data.outlet.length > 0 ) {    
                for( let i = 0; i < data.outlet.length; i++ ) {

                    let decode_img , outlet_img_html = '';
                    let ttp = data.outlet[i]['outlet_type'];
                    let fpp = data.outlet[i].outlet_category_id;

                    $.ajax({
                        type: "GET",
                        data:{
                            outlet_type:ttp
                        },
                        url: "{{ route('get_all_outlet_categories')}}",
                        success: function (r) {
                            $('#outlet_category_type' + i).append($('<option>', {value: '', text: 'Select Outlet Category'}));
                            $.each( r, function(index, value) {
                                $("#outlet_category_type" + i).append($('<option>', {value: value.id, text: value.category_name})); 
                            });
                            $("#outlet_category_type" + i).val(fpp);
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                    

                    if(data.outlet[i].image_url){

                        decode_img = JSON.parse( data.outlet[i].image_url );
                        let rest_img = "{{URL::asset('/images')}}/" + decode_img;
                        let rest_img_div_id = "rest" + i;
                        outlet_img_html = '<div id="'+rest_img_div_id+'" class="img_wrapper">'+
                                            '<img src="'+ rest_img +'" alt="Outlet Image">'+
                                            '<a class="remove-btn" title="Click here to remove" onclick="remove_image(\'outlets\', \'image_url\', \''+data.outlet[i].id+'\', \''+decode_img+'\', \''+rest_img_div_id+'\');">X</a>'+
                                        '</div>';
                    }
                    
                    // console.log( out_resp_img );
                    let img_html = '<div class="row mt-15" style="padding: 0 15px;">';

                    if ( data.outlet[i]['img_url'] ) {

                        let out_resp_img = JSON.parse( data.outlet[i]['img_url'] );

                        if ( out_resp_img.length > 0 ) {
                            for( let j = 0; j < out_resp_img.length; j++ ) {
                                
                                let outlet_img = "{{URL::asset('/images')}}/" + out_resp_img[j];
                                let img_div_id = "other" + i;
                                
                                img_html += '<div id="'+img_div_id+'" class="img_wrapper">'+
                                    '<img src="'+ outlet_img +'" alt="Outlet Image">'+
                                    '<a class="remove-btn" title="Click here to remove" onclick="remove_image(\'outlets\', \'img_url\', \''+data.outlet[i].id+'\', \''+out_resp_img[j]+'\', \''+img_div_id+'\');">X</a>'+
                                '</div>';
                            }
                        }

                    }
                    
                    img_html += '</div>';
                    let html = '<div id="'+ div_id + data.outlet[i]['id'] +'" class="row edit_outlet_details_box mt-15 js_add" style="position:relative;">\
                                <div class="edit_remove_outlet_details_box" onclick="delete_outlet('+ data.outlet[i]['id'] +')"><i class="fa fa-remove"></i></div>\
                                <div class="col-sm-1"></div>\
                                <div class="col-sm-10" style="border: 1px solid #c3c3c3;">\
                                    <div class="mt-15">\
                                        <label class="outlet_label_count">#Outlet '+ (i+1) +':</label>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet User Name</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control " style="cursor:not-allowed" name="outlet_user_name_ed[]" data-id="'+ data.outlet[i]['id'] +'" value="' + data.outlet[i]['outlet_user_name'] + '"  readonly>\
                                            <input type="hidden" name="outlet_id[]" value="' + data.outlet[i]['id'] + '">\
                                            <input type="hidden" name="dummy[]" value="gsdssd">\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Password</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="password" class="form-control " name="outlet_password_ed[]" value="' + data.outlet[i]['password'] + '" required readonly>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Name</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control " name="outlet_name_ed[]" value="' + data.outlet[i]['outlet_name'] + '" required>\
                                            <input type="hidden" name="merchant_id_ed[]" value="' + data.merchant.id + '">\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">GST #</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="gst_ed[]" value="' + ( (data.outlet[i]['gst'] == null ) ? "" : data.outlet[i]['gst'] ) + '">\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15 mb-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Speciality</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control " name="outlet_speciality_ed[]" value="' + ( (data.outlet[i]['outlet_speciality'] == null ) ? "" : data.outlet[i]['outlet_speciality'] ) + '">\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Status</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <select class="form-control" name="flag_ed[]" required>\
                                                <option value="0" '+(data.outlet[i]['flag'] == 0 ? "selected" : " ")+' >Active</option>\
                                                <option value="1" '+(data.outlet[i]['flag'] == 1 ? "selected" : " ")+' >InActive</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">FSSAI License</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="fssai_license_ed[]" value="' + ( (data.outlet[i]['fssai_license']) == null ? "" : data.outlet[i]['fssai_license'] ) + '">\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Shop License</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="shop_license_ed[]" value="' + ( (data.outlet[i]['shop_license']) == null ? "" : data.outlet[i]['shop_license'] ) + '">\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">GST Rate</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="gst_rate_ed[]" value="' + ( (data.outlet[i]['gst_rate']) == null ? "" : data.outlet[i]['gst_rate'] ) + '" >\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Taxes/Delivery Charges</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="delivery_charges_ed[]" value="' + ( (data.outlet[i]['delivery_charges']) == null ? "" : data.outlet[i]['delivery_charges'] ) + '" >\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Available Time</label>\
                                        </div>\
                                        <div class="col-sm-3 bootstrap-timepicker">\
                                            <div class="input-group">\
                                                <input type="text" class="form-control timepicker" autocomplete="off" value="' + ( (data.outlet[i]['open_time']) == null ? " " : data.outlet[i]['open_time'] ) + '"  name="open_time_ed[]">\
                                                <div class="input-group-addon">\
                                                <i class="fa fa-clock-o"></i> Open\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-1"></div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Available Time</label>\
                                        </div>\
                                        <div class="col-sm-3 bootstrap-timepicker">\
                                            <div class="input-group">\
                                                <input type="text" class="form-control timepicker" autocomplete="off" value="' + ( (data.outlet[i]['close_time']) == null ? " " : data.outlet[i]['close_time'] ) + '"  name="close_time_ed[]">\
                                                    <div class="input-group-addon">\
                                                <i class="fa fa-clock-o"></i> Close\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Type</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <select class="form-control outlet_type_select" name="outlet_type_ed[]" required>\
                                                <option value="">Select Type</option>\
                                                <option value="product" ' + ( ( data.outlet[i]['outlet_type'] == "product" ) ? "selected" : "" ) + '>Grocery</option>\
                                                <option value="recipe" ' + ( ( data.outlet[i]['outlet_type'] == "recipe" ) ? "selected" : "" ) + '>Restaurant</option>\
                                            </select>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Category Type</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <select class="form-control outlet_category_type" id="outlet_category_type'+ i +'" name="outlet_category_type_ed[]">\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Image</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="file" class="form-control ed_rest_img" name="ed_rest_img'+ i +'">\
                                            '+ outlet_img_html +'\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Mobile No.</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="outlet_mobile_ed[]" value="' + ( (data.outlet[i]['mobile']) == null ? "" : data.outlet[i]['mobile'] ) + '" required>\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Geo Address</label>\
                                        </div>\
                                        <div class="col-sm-9">\
                                            <input type="text"name="ed_geo_address_add[]" class="controls form-control ed_geo_address_add " id="ed_geo_address_add'+ i +'" value="'+data.outlet[i]['outlet_geoaddress']+'">\
                                            <div class="map" id="map'+ i +'"></div>\
                                            <input type="hidden" class="ed_geo_city" id="ed_geo_city'+ i +'" name="ed_geo_city[]" />\
                                            <input type="hidden" class="ed_address_latitude_add" name="ed_address_latitude_add[]" id="ed_address_latitude_add'+ i +'" value="'+data.outlet[i]['outlet_latitude']+'" />\
                                            <input type="hidden" class="ed_address_longitude_add" name="ed_address_longitude_add[]" id="ed_address_longitude_add'+ i +'" value="'+data.outlet[i]['outlet_longitude']+'" />\
                                        </div>\
                                    </div>\
                                    <div class="row mt-15">\
                                        <div class="col-sm-12">\
                                            <label class="control-label">Document for FSSAI license, Shop License, GST#, PAN Card</label>\
                                        </div>\
                                        <div class="col-sm-10">\
                                            <label >Upload doc here</label>\
                                            <input type="file"  class="form-control ed_outlet_doc" name="documented'+ i +'[]" multiple>\
                                        </div>\
                                    </div>'+ img_html +'<div class="row mt-15 mb-15">\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Outlet Address</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <textarea name="outlet_address_ed[]" class="form-control" rows="3" required>' + ( (data.outlet[i]['outlet_address']) == null ? "" : data.outlet[i]['outlet_address'] ) + '</textarea>\
                                        </div>\
                                        <div class="col-sm-2">\
                                            <label class="control-label">Branch</label>\
                                        </div>\
                                        <div class="col-sm-4">\
                                            <input type="text" class="form-control" name="branch_ed[]" value="' + ( (data.outlet[i]['branch']) == null ? "" : data.outlet[i]['branch'] ) + '">\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>';

                            // <div class="col-sm-1"></div>\
                            //             <div class="col-sm-2">\
                            //                 <label class="control-label">Booking Rate</label>\
                            //             </div>\
                            //             <div class="col-sm-3">\
                            //                 <input type="number" class="form-control" min="0" max="100000000" name="booking_for_two_ed[]" value="' + ( (data.outlet[i]['booking_for_two'] == null ) ? "" : data.outlet[i]['booking_for_two'] ) + '">\
                            //             </div>\
                        $("#edit_outlet_details").addClass("mt-15").append(html);
                        initAutocompleteEd();

                }
                // $(".edit_outlet_details_box:first").clone().addClass("mt-15").appendTo("#edit_outlet_details");

            }
        }
        
        $(".timepicker").click(function(){
            $(this).timepicker({
                showInputs: false
            });
        });

        $('#editMerchantModal').modal('show');
    }

    function delete_merchant( merchant_id ) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('delete_merchant')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "merchant_id": merchant_id
                    },
                    success: function (r) {
                            console.log(r);
                            Swal.fire(
                                'Success!',
                                'Merchant Deleted Successfully.',
                                'success'
                            )
                            // $.notify("Merchant Deleted Successfully", "success");
                            $('#item_table').DataTable().ajax.reload();
                            // window.location.reload();
                    },
                    error:function(e){
                        Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            )
                        console.log(e);
                    }
                });
            }
        });

    }
    
    function delete_outlet( outlet_id ) {

        let div_id = "#editem"+outlet_id;
        console.log( div_id );
        console.log( $(this).parent().attr('id') );
        if($('.edit_outlet_details_box ').length == 1 ){
            $.notify("Outlet cannot be deleteted", "warning");
            return;
        }

        if( confirm('Are sure to delete the outlet?') ) {
            
            $.ajax({
                type: "POST",
                url: "{{ route('delete_outlet')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "outlet_id": outlet_id
                },
                success: function (r) {
                    // console.log(r);
                    $(div_id).remove();
                    $.notify("Outlet Deleted Successfully", "success");
                    // $("#" + div_id).html();
                },
                error:function(e){
                    console.log(e);
                }
            });

        } else {

        }

    }

    function remove_image(table_name, column_name, row_id, image_name, img_div_id) {
        $.ajax({
            type: "POST",
            url: "{{ route('remove_image')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "table_name": table_name,
                "column_name": column_name,
                "row_id": row_id,
                "image_name": image_name
            },
            success: function (r) {
                    console.log(r);
                    Swal.fire(
                        'Success!',
                        'Image Deleted Successfully.',
                        'success'
                    );
                    // $('.modal').modal('hide');
                    $("#"+img_div_id).remove();
            },
            error:function(e){
                Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    );
                console.log(e);
            }
        });
    }

  $(document).ready(function(){
      
    $('.add_merchant').on('click',function(){
        $('#createMerchant').trigger("reset");
        $("input[name=country]").val('INDIA');
        $("#createMerchant").find('span.help-block').remove();
        $("#createMerchant").find('.help-block').removeClass("help-block");
        $("#createMerchant").find('.error').removeClass("error");
        $("#createMerchant").find('.has-success').removeClass("has-success");
        $('#createMerchant').find('.form-control-feedback').remove();
        $('input[name=merchant_id_edit]').val('');
    });
    $('#item_table').DataTable({
        "language": {
            "emptyTable": "No Record Found"
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
                    "url": "{{ url('get_merchant_list') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"}
                },
        "columns": [
            { "data": "merchant_name" },
            { "data": "user_name" },
            { "data": "first_name" },
            { "data": "email" },
            { "data": "city" },
            { "data": "state" },
            { "data": "action" }
        ],
        "aoColumnDefs": [
        {
            bSortable: false,
            aTargets: [ -1 ]
        }],
    });
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size/1024/1024 <= param)
    }, 'File size must be less than 5 MB');
    
    $.validator.addMethod('maxupload', function (value, element, param) {
        var length = ( element.files.length );
        return this.optional( element ) || length <= param;
    }, 'You can add only 5 images');
    $.validator.addMethod(
        "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
    );

        $.validator.addMethod(
            "check_user_name",
                function(value, element) {
                    
                    let user_name = value;
                    let merchant_id = $('input[name=merchant_id_edit]').val();
                    // console.log( user_name );
                    let isSuccess = false;
                    
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "{{ route('check_username')}}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "merchant_id": merchant_id,
                            "user_name": user_name
                        },
                        success: function (r) {
                            // console.log(r);
                            let user_name_message = r.message;

                            console.log( user_name_message );

                            if ( user_name_message == 'User already Exist' ) {
                                // $("#user_name").parent().find("#custom_error").show().text('User already Exist');
                                isSuccess = false;
                            } else {
                                isSuccess = true;
                            }

                        },
                        error:function(e){
                            console.log(e);
                        }
                    });

                    return isSuccess;
                }
        );

        $.validator.addMethod(
            "check_outlet_user_name",
                function(value, element) {
                    
                    let outlet_user_name = value;
                    
                    console.log( $(element).data('id'));
                    let isSuccess = false;
                    let outlet_id = $(element).data('id');
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "{{ route('outlet_check_username')}}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "outlet_user_name": outlet_user_name,
                            "outlet_id": outlet_id
                        },
                        success: function (r) {
                            console.log(r);
                            let outlet_user_name_message = r.message;

                            console.log( outlet_user_name_message );

                            if ( outlet_user_name_message == 'User already Exist' ) {
                                
                                isSuccess = false;
                            } else {
                                isSuccess = true;
                            }

                        },
                        error:function(e){
                            console.log(e);
                        }
                    });

                    return isSuccess;
                }
        );

        $.validator.addMethod(
            "field_required",
                function(value, element) {
                    let isSuccess = false;
                        if ( value != null ) {
                            isSuccess = true;
                        }
                    return isSuccess;
                }
        );

        $( "#createMerchant" ).validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#createMerchant')[0]);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('create_merchant')}}",
                    data: formdata,
                    success: function (r) {
                            console.log(r);
                            $("#addMerchantModal").modal("hide");
                            Swal.fire(
                                'Success!',
                                'Merchant Added Successfully.',
                                'success'
                            )
                            $('#item_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            )
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            rules:
            {
                user_name: {
                    required: true,
                    check_user_name: true,
                },
                password: {
                    required: true,
                    minlength: 6
                },
                first_name: {
                    required:true,
                    regex: /^[a-z]+$/i
                },
                last_name: {
                    required:true,
                    regex: /^[a-z]+$/i
                },
                pin_code: {
                    required:true,
                    regex: /^[0-9]{1,6}$/g
                },
                contact: {
                    required:true,
                    regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
                },
                'outlet_user_name[]': {
                    required:true,
                    check_outlet_user_name: true
                },
                'outlet_password[]': {
                    required:true,
                    minlength: 6
                },
                'outlet_name[]': {
                    required:true
                },
                'outlet_type[]': {
                    required:true
                },
                'outlet_category_type[]': {
                    required:true
                },
                // 'image_url[]': {
                //     required:true,
                //     extension: 'jpg|jpeg|png|gif',
                //     maxupload: 5,
                //     filesize:200000,
                // },
                'document0[]': {
                    required:false,
                    extension: 'jpg|jpeg|png|gif|pdf',
                    maxupload: 5,
                    filesize:5,
                },
                'document1[]': {
                    required:false,
                    extension: 'jpg|jpeg|png|gif|pdf',
                    maxupload: 5,
                    filesize:5,
                },
                'document2[]': {
                    required:false,
                    extension: 'jpg|jpeg|png|gif|pdf',
                    maxupload: 5,
                    filesize:5,
                },
                'document3[]': {
                    required:false,
                    extension: 'jpg|jpeg|png|gif|pdf',
                    maxupload: 5,
                    filesize:5,
                },
                'document4[]': {
                    required:false,
                    extension: 'jpg|jpeg|png|gif|pdf',
                    maxupload: 5,
                    filesize:5,
                },
                'document5[]': {
                    required:false,
                    extension: 'jpg|jpeg|png|gif|pdf',
                    maxupload: 5,
                    filesize:5,
                }

            },
            messages:
            {	
                user_name: {
                    required: 'User Name is required',
                    check_user_name: 'User already Exist.'
                },
                first_name: {
                    regex: 'Enter only alphabets'
                },
                last_name: {
                    regex: 'Enter only alphabets'
                },
                pin_code: {
                    regex: 'Please enter correct pin code'
                },
                contact: {
                    regex: 'Please add valid mobile number'
                },
                'outlet_user_name[]': {
                    required: 'Please enter outlet username',
                    check_outlet_user_name: 'User already Exist.'
                },
                'outlet_password[]': {
                    required: 'Please enter outlet password'
                },
                'outlet_name[]': {
                    required: 'Please enter outlet name'
                },
                'outlet_type[]': {
                    required: 'Please select outlet type'
                },
                'outlet_category_type[]': {
                    required: 'Please select outlet category'
                },
                // 'image_url[]': {
                //     extension: 'Please select valid image type'
                // }
            },
            ignore: []

        });

        $("#outlet_details_box").clone().show().removeAttr('id').attr('id', 'item0' ).addClass("outlet_details_box").addClass("mt-15").appendTo("#outlet_details");
        $(".outlet_details_box").find("#remove_outlet_details_box").addClass("remove_outlet_details_box").removeAttr('id');

        $("#add_outlet").click(function(e){

            // console.log('worksddasdasdg');
            let outlet_doc_name = $('#outlet_details').find('.outlet_doc:last').attr('name');
            let div_id = "item";

            if ( outlet_doc_name == undefined ) {
                // console.log('true');
                div_id += "0";
                $("#outlet_details_box").clone().show().removeAttr('id').attr('id', div_id ).addClass("outlet_details_box").addClass("mt-15").appendTo("#outlet_details");
                $('.outlet_count').val("1");
                $('#outlet_details').find('.outlet_doc:last').attr('name', 'document0[]');
                $('#outlet_details').find('.add_rest_img:last').attr('name', 'image_url0');
                $('#outlet_details').find('.add_rest_img:last').attr('count_attr', '0');
                $('#outlet_details').find('.outlet_image_name:last').attr('name', 'outlet_image_name_0');

                // $('#outlet_details').find('.geo_address_add:last').attr('id', 'geo_address_add0');
                // $('#outlet_details').find('.geo_city:last').attr('id', 'geo_city0');
                // $('#outlet_details').find('.address_latitude_add:last').attr('id', 'address_latitude_add0');
                // $('#outlet_details').find('.address_longitude_add:last').attr('id', 'address_longitude_add0');
            } else {

                let outlet_doc_number = parseInt(outlet_doc_name.slice(outlet_doc_name.length - 3).charAt(0));
                div_id += ( outlet_doc_number + 1 );
                $("#outlet_details_box").clone().show().removeAttr('id').attr('id', div_id ).addClass("outlet_details_box").addClass("mt-15").appendTo("#outlet_details");
                $("#" + div_id).find('.outlet_label_count').text('#Outlet ' + ( outlet_doc_number + 2 ) + ':');
                $('.outlet_count').val($(".outlet_details_box").length);

                $('#outlet_details').find('.outlet_doc:last').attr('name', 'document' + ( outlet_doc_number + 1 ) + '[]');
                $('#outlet_details').find('.add_rest_img:last').attr('name', 'image_url' + ( outlet_doc_number + 1 ) );
                $('#outlet_details').find('.add_rest_img:last').attr('count_attr', outlet_doc_number + 1);
                $('#outlet_details').find('.outlet_image_name:last').attr('name', 'outlet_image_name_' + ( outlet_doc_number + 1 ) );

                $('#outlet_details').find('.geo_address_add:last').attr('id', 'geo_address_add' + ( outlet_doc_number + 1 ));
                $('#outlet_details').find('.geo_city:last').attr('id', 'geo_city' + ( outlet_doc_number + 1 ));
                $('#outlet_details').find('.address_latitude_add:last').attr('id', 'address_latitude_add' + ( outlet_doc_number + 1 ));
                $('#outlet_details').find('.address_longitude_add:last').attr('id', 'address_longitude_add' + ( outlet_doc_number + 1 ));

                initAutocomplete();

            } 

            $(".outlet_details_box").find("#remove_outlet_details_box").addClass("remove_outlet_details_box").removeAttr('id');

            // $('#addMerchantModal').animate({scrollTop: $("#" + div_id).offset().top});
            $('#addMerchantModal').animate({
                scrollTop: $("#" + div_id).offset().top - $('#addMerchantModal').offset().top + $('#addMerchantModal').scrollTop()
            });
            e.preventDefault();

            $("div.remove_outlet_details_box").on('click', function(e){
                // console.log( $(this).parent().html() );
                if( confirm('Are sure to delete the outlet?') ) {
                    $(this).parent().remove();
                    let no_outlet = $("#createMerchant").find('.remove_outlet_details_box').length;
                    if ( no_outlet == 0 ) {
                        $("#createMerchant").find('.no_outlet').val('0');
                    }
                    // console.log('true');
                }
                e.preventDefault();
            });
            // $('#create_merchant').focus();
            $(".timepicker").click(function(){
                $(this).timepicker({
                    showInputs: false
                });
            });
            
        });

        $("div.remove_outlet_details_box").on('click', function(e){
            // console.log( $(this).parent().html() );
            if( confirm('Are sure to delete the outlet?') ) {
                $(this).parent().remove();
                let no_outlet = $("#createMerchant").find('.remove_outlet_details_box').length;
                if ( no_outlet == 0 ) {
                    $("#createMerchant").find('.no_outlet').val('0');
                }
                // console.log('true');
            }
            e.preventDefault();
        });

        $("#edit_add_outlet").click(function(e){
            
            // console.log('working');
            let outlet_doc_name = $('#edit_outlet_details').find('.ed_outlet_doc:last').attr('name');
            let div_id = "editem";
            
            if ( outlet_doc_name == undefined ) {
                div_id += "0";
                console.log('Add outlet');
                $("#edit_outlet_details_box").clone().show().removeAttr("id").attr('id', div_id ).addClass("edit_outlet_details_box").appendTo("#edit_outlet_details");
                let abc = $("#edit_outlet_details > .edit_outlet_details_box").size();
                
                $("#" + div_id).find('.outlet_label_count').text('#Outlet ' + abc + ':');

                $('.outlet_count_edit').val("1");
                $('#edit_outlet_details').find('.ed_outlet_doc:last').attr('name', 'documented0[]');
                $('#edit_outlet_details').find('.ed_rest_img:last').attr('name', 'ed_rest_img0');

                // $('#edit_outlet_details').find('.ed_geo_address_add:last').attr('id', 'ed_geo_address_add0');
                // $('#edit_outlet_details').find('.ed_geo_city:last').attr('id', 'ed_geo_city0');
                // $('#edit_outlet_details').find('.ed_address_latitude_add:last').attr('id', 'ed_address_latitude_add0');
                // $('#edit_outlet_details').find('.ed_address_longitude_add:last').attr('id', 'ed_address_longitude_add0');
            } else {

                let outlet_doc_number = parseInt(outlet_doc_name.slice(outlet_doc_name.length - 3).charAt(0));
                div_id += ( outlet_doc_number + 1 );
                $("#edit_outlet_details_box").clone().show().removeAttr("id").attr('id', div_id ).addClass("edit_outlet_details_box").appendTo("#edit_outlet_details");
                let abc = $("#edit_outlet_details > .edit_outlet_details_box").size();
                
                $("#" + div_id).find('.outlet_label_count').text('#Outlet ' + abc + ':');
                $('.outlet_count_edit').val($(".edit_outlet_details_box").length);
                $('#edit_outlet_details').find('.ed_outlet_doc:last').attr('name', 'documented' + ( outlet_doc_number + 1 ) + '[]');
                $('#edit_outlet_details').find('.ed_rest_img:last').attr('name', 'ed_rest_img' + ( outlet_doc_number + 1 ) );

                $('#edit_outlet_details').find('.ed_geo_address_add:last').attr('id', 'ed_geo_address_add' + ( outlet_doc_number + 1 ));
                $('#edit_outlet_details').find('.ed_geo_city:last').attr('id', 'ed_geo_city' + ( outlet_doc_number + 1 ));
                $('#edit_outlet_details').find('.ed_address_latitude_add:last').attr('id', 'ed_address_latitude_add' + ( outlet_doc_number + 1 ));
                $('#edit_outlet_details').find('.ed_address_longitude_add:last').attr('id', 'ed_address_longitude_add' + ( outlet_doc_number + 1 ));

                initAutocompleteEd();
                
            }
            $(".edit_outlet_details_box").find("#edit_remove_outlet_details_box").addClass("edit_remove_outlet_details_box").removeAttr('id');

            // $('#editMerchantModal').animate({ scrollTop: $("#" + div_id).offset().top }, 500);
            $('#editMerchantModal').animate({
                scrollTop: $("#" + div_id).offset().top - $('#editMerchantModal').offset().top + $('#editMerchantModal').scrollTop()
            });
            e.preventDefault();

            $("div.edit_remove_outlet_details_box").on('click', function(e){
                
                if( confirm('Are sure to delete the outlet?') ) {
                    $(this).parent().remove();
                    let no_outlet_edit = $("#editMerchant").find('.edit_remove_outlet_details_box').length;
                    if ( no_outlet_edit == 0 ) {
                        $("#editMerchant").find('.no_outlet_edit').val('0');
                    }   
                }
                e.preventDefault();
            });

            $(".timepicker").click(function(){
                $(this).timepicker({
                    showInputs: false
                });
            })

        });

        $(window).load(function() {

            

        });

        $("#cancle_merchant").on('click', function(){

            window.history.back();

        });

        $.ajax({
            type: "GET",
            url: "{{ route('get_all_states')}}",
            success: function (r) {
                // console.log(r);
                let states = r.states;
                $.each( states, function(index, value) {
                    // console.log( index + ' : ' + value.state_name );
                    $('#state').append($('<option>', {value: value.state_name, text: value.state_name}));
                    $('#state_ed').append($('<option>', {value: value.state_name, text: value.state_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });

        $("#state").on('change', function(){
            
            let state = $(this).val();

            $('#city').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_all_cities_by_state_id')}}",
                data: {
                    "state_id": state
                },
                success: function (r) {
                    // console.log(r);
                    let cities = r.cities;
                    $('#city').append($('<option>', {value: '', text: 'Select City'}));
                    $.each( cities, function(index, value) {
                        // console.log( index + ' : ' + value.state_name );
                        $('#city').append($('<option>', {value: value.city_name, text: value.city_name}));
                    });
                },
                error:function(e){
                    console.log(e);
                }
            });

        });
        
        $.ajax({
            type: "GET",
            url: "{{ route('get_all_outlet_categories')}}",
            success: function (r) {
                console.log(r);
                $('.outlet_category_type').append($('<option>', {value: '', text: 'Select Outlet Category'}));
                $.each( r, function(index, value) {
                    $(".outlet_category_type").append($('<option>', {value: value.id, text: value.category_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });

        $("#editMerchant").validate({
            submitHandler: function(form) {
                    let formdata = new FormData($('#editMerchant')[0]);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        url: "{{ route('edit_merchant')}}",
                        data: formdata,
                        success: function (r) {
                                console.log(r);
                                $("#editMerchantModal").modal("hide");
                                Swal.fire(
                                    'Success!',
                                    'Merchant Updated Successfully.',
                                    'success'
                                )
                                // $.notify("Merchant Updated Successfully", "success");
                                //$('#item_table').DataTable().ajax.reload();
                                window.location.reload();
                        },
                        error:function(e){
                            Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            )
                            console.log(e);
                        }
                    });
                },
                errorElement: 'span',
                errorClass: 'help-block error',
                rules:
                {
                    user_name_ed: {
                        required: true,
                        check_user_name: true,
                    },
                    password_ed: {
                        required: true,
                        minlength: 6
                    },
                    first_name_ed: {
                        required:true,
                        regex: /^[a-z]+$/i
                    },
                    last_name_ed: {
                        required:true,
                        regex: /^[a-z]+$/i
                    },
                    pin_code_ed: {
                        required:true,
                        regex: /^[0-9]{1,6}$/g
                    },
                    contact_ed: {
                        required:true,
                        regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
                    },
                    // ed_rest_img: {
                    //     required:false,
                    //     extension: 'jpg|jpeg|png|gif',
                    //     maxupload: 5,
                    //     filesize:200000,
                    // },
                    'outlet_user_name_ed[]': {
                        required:true,
                        check_outlet_user_name: true
                    },
                    'outlet_password_ed[]': {
                        required:true,
                        minlength: 6
                    },
                    'outlet_name_ed[]': {
                        required:true
                    },
                    'outlet_type_ed[]': {
                        required:true
                    },
                    'outlet_category_type_ed[]': {
                        required:true
                    },
                    'documented0[]': {
                        required:false,
                        extension: 'jpg|jpeg|png|gif|pdf',
                        maxupload: 5,
                        filesize:5,
                    },
                    'documented1[]': {
                        required:false,
                        extension: 'jpg|jpeg|png|gif|pdf',
                        maxupload: 5,
                        filesize:5,
                    },
                    'documented2[]': {
                        required:false,
                        extension: 'jpg|jpeg|png|gif|pdf',
                        maxupload: 5,
                        filesize:5,
                    },
                    'documented3[]': {
                        required:false,
                        extension: 'jpg|jpeg|png|gif|pdf',
                        maxupload: 5,
                        filesize:5,
                    },
                    'documented4[]': {
                        required:false,
                        extension: 'jpg|jpeg|png|gif|pdf',
                        maxupload: 5,
                        filesize:5,
                    },
                    'documented5[]': {
                        required:false,
                        extension: 'jpg|jpeg|png|gif|pdf',
                        maxupload: 5,
                        filesize:5,
                    }
                    
                },
                messages:
                {	
                    user_name_ed: {
                        required: 'User Name is required',
                        check_user_name: 'User already Exist.'
                    },
                    first_name_ed: {
                        regex: 'Enter only alphabets'
                    },
                    last_name_ed: {
                        regex: 'Enter only alphabets'
                    },
                    pin_code_ed: {
                        regex: 'Please enter correct pin code'
                    },
                    contact_ed: {
                        regex: 'Please add valid mobile number'
                    },
                    // ed_rest_img: {
                    //     extension: 'Please select valid image type'
                    // },
                    'outlet_user_name_ed[]': {
                        required: 'Please enter outlet username',
                        check_outlet_user_name: 'User already Exist.'
                    },
                    'outlet_password_ed[]': {
                        required: 'Please enter outlet password'
                    },
                    'outlet_name[]_ed': {
                        required: 'Please enter outlet name'
                    },
                    'outlet_type_ed[]': {
                        required: 'Please select outlet type'
                    },
                    'outlet_category_type_ed[]': {
                        required: 'Please select outlet category'
                    },
                },
                ignore: [],
        });

        $('#update_merchant').click(function(){

            $('.outlet_control').each(function() {
                    $(this).valid();
            });
        });
        

        $(".dataTables_filter input").css({ "width": "250px" });

        $('#editMerchant').click(function(){
            $('.outlet_count_edit').val($(".edit_outlet_details_box").length);
        });

        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });

        $(".timepicker").click(function(){
            $(this).timepicker({
                showInputs: false
            });
        });

    });


    $(document).on('change','.outlet_type_select',function(){
        $(this).closest('.row').find('.outlet_category_type').html('');
        let box = $(this).closest('.row').find('.outlet_category_type');
        let type =  $(this).val();
        $.ajax({
            type: "GET",
            data:{ 
                outlet_type:type
            },
            url: "{{ route('get_all_outlet_categories')}}",
            success: function (r) {
                box.append($('<option>', {value: '', text: 'Select Outlet Category'}));
                $.each( r, function(index, value) {
                    box.append($('<option>', {value: value.id, text: value.category_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });
    });

    $(document).on('change','.outlet_type_select_add',function(){
        $(this).closest('.row').find('.outlet_category_type_add').html('');
        let box = $(this).closest('.row').find('.outlet_category_type_add');
        let type =  $(this).val();
        $.ajax({
            type: "GET",
            data:{ 
                outlet_type:type
            },
            url: "{{ route('get_all_outlet_categories')}}",
            success: function (r) {
                box.append($('<option>', {value: '', text: 'Select Outlet Category'}));
                $.each( r, function(index, value) {
                    box.append($('<option>', {value: value.id, text: value.category_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });
    });

    $('#addMerchantModal').on('shown.bs.modal', function () {
        $('#user_name').focus();
    });

    

</script>
@endsection
