@extends('layouts.applist')

@section('content')
@include('layouts.outlet_header')

<style>
    html, body {
        height: auto !important;
        min-height: 100%;
    }

    #app
    {
        min-height: 100vh;
    }

    .tgl_checkbox {
        display: none;
    }

    .box_actions
    {
        float: right;
    }

    .time_box
    {
        width: 86px !important;
    }

    ul {
        padding: 0;
        border: 0;
        font-size: 100%;
      }

    header, menu, nav, section { display: block }
    ol, ul { list-style: none }
    a { text-decoration: none; }

    
    .nav_grid a, .nav_grid label {
      display: block;
     padding: .85rem;
      color: #000;
        background-color: #ffffff;
        box-shadow: inset 0 -1px #ffffff;
      -webkit-transition: all .25s ease-in;
      transition: all .25s ease-in;
    }

    .nav a:focus, .nav a:hover, .nav label:focus, .nav label:hover {
       /* color: rgba(255, 255, 255, 0.9); */
  color: #f66806;
        background: #ffffff;
    }

    .nav label { cursor: pointer; }


    .group-list a, .group-list label {
      padding-left: 2rem;
        background: #ffffff;
        box-shadow: inset 0 -1px #ffffff;
    }

    .group-list a:focus, .group-list a:hover, .group-list label:focus, .group-list label:hover { background: #ffffff; }

    /**
     * Styling second level list items
     */

    .sub-group-list a, .sub-group-list label {
        padding-left: 4rem;
        background: #ffffff;
        box-shadow: inset 0 -1px #ffffff;
        text-decoration: none;
    }
    .sub-group-list li , .sub-sub-group-list li{
      border-bottom: 1px solid #dddddd;
    }
    .sub-group-list a:focus, .sub-group-list a:hover, .sub-group-list label:focus, .sub-group-list label:hover { background: #ffffff; }

    /**
     * Styling second level list items
     */

    .sub-sub-group-list a, .sub-sub-group-list label {
      padding-left: 4rem;
        background: #ffffff;
        box-shadow: inset 0 -1px #ffffff;
        text-decoration: none;
    }
    .sub-group-list input[type="checkbox"].tgl-ios + label:before{
        width: 39px;
        height: 18px;
    }
    .sub-group-list input[type="checkbox"].tgl-ios + label:after{
        width: 18px;
        height: 18px;
    }
    .sub-group-list input[type="checkbox"].tgl-ios:checked + label:after{
        margin-left: 20px;
    }

    .sub-sub-group-list a:focus, .sub-sub-group-list a:hover, .sub-sub-group-list label:focus, .sub-sub-group-list label:hover { background: #ffffff; }

    /**
     * Hide nested lists
     */

    .group-list, .sub-group-list, .sub-sub-group-list {
      /*height: 100%;*/
      max-height: 0;
      overflow: hidden;
      -webkit-transition: max-height .5s ease-in-out;
      transition: max-height .5s ease-in-out;
    }

    .nav__list input[type=checkbox]:checked + label + ul { 
    max-height: 10000px; }

    


    /**
     * Rotating chevron icon
     */

    label > span {
      float: left;
      margin-top: 3px;
      margin-right: 10px;
      -webkit-transition: -webkit-transform .65s ease;
      transition: transform .65s ease;
    }

    .nav__list input[type=checkbox]:checked + label > span {
      -webkit-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
      transform: rotate(90deg);
    }

    /* radio button and checkbox style */
    input[type=radio],
    input.radio {
    margin:5px,0,0,0 !important;
      float: left;
      clear: none;
    }
    input[type=checkbox],
    input.checkbox {
      float: left;
      clear: none;
    }
    .img_wrapper{
    max-width: 302px !important;
    margin-top: -140px;
    }
    .img_wrapper img{
        width:220px !important;
        height:116px !important;
    }
    .w{
        width:132% !important;
    }
    .mt{
        margin-top:5px;
    }
    .box-body1 {
    padding-left: 0px;
    padding-bottom: 20px;
    padding-top: 20px;
    padding-right: 0px;
    }
    .modal-body1 {
    position: relative;
    padding: 0px;
}
</style>
<style>
  .content-wrapper,.right-side,.main-footer{
    -webkit-transition:-webkit-transform .3s ease-in-out,margin .3s ease-in-out;
    -moz-transition:-moz-transform .3s ease-in-out,margin .3s ease-in-out;
    -o-transition:-o-transform .3s ease-in-out,margin .3s ease-in-out;
    transition:transform .3s ease-in-out,margin .3s ease-in-out;
    margin-left:230px;
    z-index:820;
  }

  .layout-top-nav .content-wrapper,.layout-top-nav .right-side,.layout-top-nav .main-footer{
    margin-left:0
  }

  .content-wrapper,.right-side{
    min-height:100%;
    background-color:#ecf0f5;
    z-index:800;
  }
  .content-wrapper,.right-side,.main-footer{
   /* margin-left:0 !important;*/
    min-height:0 !important;
    -webkit-transform:translate(0, 0) !important;
    -ms-transform:translate(0, 0) !important;
    -o-transform:translate(0, 0) !important;
    transform:translate(0, 0) !important
  }
  .fixed .content-wrapper,.fixed .right-side{
    padding-top:0 !important
  }

  .fixed .content-wrapper,.fixed .right-side{
    padding-top:50px
  }
  body.hold-transition .content-wrapper,body.hold-transition .right-side,body.hold-transition .main-footer,body.hold-transition .main-sidebar,body.hold-transition .left-side,body.hold-transition .main-header .navbar,body.hold-transition .main-header .logo{
    -webkit-transition:none;
    -o-transition:none;
    transition:none
  }
  @media (min-width:768px){
    .sidebar-mini.sidebar-collapse .content-wrapper,.sidebar-mini.sidebar-collapse .right-side,.sidebar-mini.sidebar-collapse .main-footer{
    margin-left:50px !important;
    z-index:840
  }
  }
  @media (min-width:768px){
    .control-sidebar-open .content-wrapper,.control-sidebar-open .right-side,.control-sidebar-open .main-footer{
    margin-right:230px}

  }

  @media (max-width:767px){
    .fixed .content-wrapper,.fixed .right-side{
    padding-top:100px}
  }

  @media (max-width:767px){
    .content-wrapper,.right-side,.main-footer{
    margin-left:0}

      @media (max-width:767px){
        .sidebar-open .content-wrapper,.sidebar-open .right-side,.sidebar-open .main-footer{
        -webkit-transform:translate(230px, 0);
        -ms-transform:translate(230px, 0);
        -o-transform:translate(230px, 0);
        transform:translate(230px, 0)}
        
    }
  }

</style>

<section id="main-content">
    
   <div class="container-fluid content-wrapper">

     <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12" style = "padding-left:0px;">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Menu Management</h4>
                    </div>
                    <div class="col-md-6 text-right" style = "padding:3px;">
                        <a class="btn btn-success add_menuitem btn-flat mr-15" id="add_menuitem"><i class="fa fa-plus"></i> Add Menu Item</a> 
                        <a class="btn btn-success add_subcategory btn-flat mr-15" id="add_subcategory"><i class="fa fa-plus"></i> Add Category</a>                            
                    </div>

                </div>
            </div>
        </div>
    </section>

    <div class="row">
      <div class="col-md-6">
      
<header role="banner">
  <nav class="nav nav_grid" role="navigation">
    
    <?php
        if(isset($category_data) && $category_data)
        {   
            foreach ($category_data as $cat_type_key => $cat_type_value) {               

                $cat_type = ucfirst($cat_type_value['cat_type']);
                $parent_categories = $cat_type_value['parent_categories'];
                ?>
                    <ul class="nav__list">
                        <li>
                            <input id="group-{{$cat_type}}" type="checkbox" hidden />
                            <label for="group-{{$cat_type}}"><span class="fa fa-angle-right"></span> {{$cat_type}}</label>

                                <?php
                                if(isset($parent_categories) && $parent_categories!='')
                                {
                                    ?>
                                    <ul class="group-list">

                                        <?php
                                        foreach ($parent_categories as $parent_cat_key => $parent_cat_value) 
                                        { 
                                            $category_id = $parent_cat_value['category_id'];
                                            $category_name = ucfirst($parent_cat_value['category_name']);
                                            $parent_menus = $parent_cat_value['parent_menus'];
                                            $sub_category = $parent_cat_value['sub_category'];
                                            ?>
                                
                                            <li>
                                                <input id="sub-group-{{$cat_type}}-{{$category_id}}" type="checkbox" hidden />
                                                <label for="sub-group-{{$cat_type}}-{{$category_id}}"><span class="fa fa-angle-right"></span> {{$category_name}}</label>

                                                <ul class="sub-group-list">
                                                <?php
                                                if(isset($parent_menus) && $parent_menus)
                                                {   
                                                    ?>
                                                    
                                                    <?php
                                                    foreach ($parent_menus as $parent_menu_key => $parent_menu_value) 
                                                    { 
                                                            $id = $parent_menu_value->id;
                                                            $name = $parent_menu_value->name;
                                                            $status = $parent_menu_value->status;
                                                        ?>
                                                        
                                                        <li>
                                                            <a href="#"> <span onclick="getMenuItem('{{$id}}')">{{$name}} </span>

                                                                <span>

                                                                    @if($status == 1)

                                                                        <input class="tgl tgl-ios tgl_checkbox box_actions" id="cb_{{$id}}" name="menuitem_status" type="checkbox" data-attr="{{$status}}" checked="" menuitem-id="{{$id}}">
                                                                        <label class="tgl-btn box_actions" for="cb_{{$id}}" style="margin-top:-3px;"></label>

                                                                    @else

                                                                        <input class="tgl tgl-ios tgl_checkbox box_actions" id="cb_{{$id}}" name="menuitem_status" type="checkbox" data-attr="{{$status}}" menuitem-id="{{$id}}">
                                                                        <label class="tgl-btn box_actions" for="cb_{{$id}}" style="margin-top:-3px;"></label>

                                                                    @endif

                                                                    
                                                                </span> 
                                                            
                                                                <span  data-attr="{{$id}}" onclick="delete_menuitem('{{$id}}')" class="btn operations box_actions" style="margin-top:-12px;"><i class="fa fa-trash-o"></i>
                                                                </span> 

                                                            </a>                                 
                                                        </li> 

                                                        <?php
                                                    }                                            
                                                    ?>

                                                    <?php
                                                }                                            
                                                ?>   

                                                        <?php
                                                        if(isset($sub_category) && $sub_category!='')
                                                        {
                                                            ?>
                                                            <ul class="group-list-{{$cat_type}}-{{$category_id}}">
                                                                
                                                            <?php
                                                            foreach ($sub_category as $sub_cat_key => $sub_cat_value) 
                                                            { 
                                                                $sub_category_id = $sub_cat_value['sub_category_id'];
                                                                $category_name = $sub_cat_value['category_name'];
                                                                $sub_menus = $sub_cat_value['sub_menus'];
                                                                ?>
                                                            
                                                                <li style="margin-left: 10px;">

                                                                    <input id="sub-direct-{{$cat_type}}-{{$category_id}}-{{$sub_category_id}}" type="checkbox" hidden />
                                                                    <label for="sub-direct-{{$cat_type}}-{{$category_id}}-{{$sub_category_id}}"><span class="fa fa-angle-right"></span>{{$category_name}}</label>
                                                                    
                                                                    <?php
                                                                    if(isset($sub_menus) && $sub_menus)
                                                                    {   
                                                                    ?>

                                                                        <ul class="sub-group-list">

                                                                        <?php
                                                                        foreach ($sub_menus as $sub_menu_key => $sub_menu_value) 
                                                                        { 
                                                                                $id = $sub_menu_value->id;
                                                                                $name = $sub_menu_value->name;
                                                                                $status = $sub_menu_value->status;
                                                                        ?>

                                                                            <li>
                                                                                <a href="#"> <span onclick="getMenuItem('{{$id}}')">{{$name}} </span>

                                                                                    <span>

                                                                                        @if($status == 1)

                                                                                            <input class="tgl tgl-ios tgl_checkbox box_actions" id="cb_{{$id}}" name="menuitem_status" type="checkbox" data-attr="{{$status}}" checked="" menuitem-id="{{$id}}">
                                                                                            <label class="tgl-btn box_actions" for="cb_{{$id}}" style="margin-top:-3px;"></label>

                                                                                        @else

                                                                                            <input class="tgl tgl-ios tgl_checkbox box_actions" id="cb_{{$id}}" name="menuitem_status" type="checkbox" data-attr="{{$status}}" menuitem-id="{{$id}}">
                                                                                            <label class="tgl-btn box_actions" for="cb_{{$id}}" style="margin-top:-3px;"></label>

                                                                                        @endif

                                                                                        
                                                                                    </span> 

                                                                                    <span  data-attr="{{$id}}" onclick="delete_menuitem('{{$id}}')" class="btn operations box_actions" style="margin-top:-12px;"><i class="fa fa-trash-o"></i>
                                                                                    </span> 

                                                                                </a>                                 
                                                                            </li> 

                                                                        <?php
                                                                        }                                            
                                                                        ?>

                                                                        </ul>

                                                                    <?php
                                                                    }                                            
                                                                    ?>

                                                                </li>

                                                                <?php
                                                            }                                            
                                                            ?>

                                                            </ul>

                                                            <?php
                                                        }                                            
                                                        ?>

                                                    </ul>
                                            </li> 

                                        <?php
                                        }                                
                                        ?>                             

                                    </ul>

                                    <?php
                                }                                
                                ?>
                        </li>
                    </ul>
                <?php
            }
        }

    ?>
  </nav>
</header>

<div class="modal-body1 add-cat-body">

    <!-- Add New Merchant Form -->
    <form method="POST" enctype="multipart/form-data" action="javascript:void(0)" id="SubmitCreateCategoryForm" class="form_add_subcategory" style="display: none;">  
          
        <div class="box-body1">
              <h4 class="modal-title" id="addSubMainCatModalLabel"><b>Add Category</b></h4>

                <div class="row mt">
                      <div class="col-sm-4">
                          <label class="control-label">Category Name *</label>
                      </div>
                      <div class="col-sm-8">
                          <input type="text" name="category_name" id="category_name" class="form-control" required="required">
                      </div>
                </div>
                <div class="row mt">
                      <div class="col-sm-4">
                          <label class="control-label">Description</label>
                      </div>
                      <div class="col-sm-8">
                          <textarea class="form-control" rows="3" name="description" id="description" placeholder="Enter ..." spellcheck="false" style="resize: vertical;"></textarea>
                      </div>
                  </div>
                  <div class="row mt">
                      <div class="col-sm-4">
                          <label class="control-label">Select Main Category *</label>
                      </div>
                      <div class="col-sm-8">
                          <label class="food">
                              <input type="radio" name="category_type" value="food" id="val_food" required="required">&nbsp;&nbsp;Food                              
                          </label>
                          <label class="beverages">
                              <input type="radio" name="category_type" value="beverages" id="val_beverages" required="required">&nbsp;&nbsp;Beverages
                          </label>
                      </div>
                  </div>
                  <div class="row mt">
                      <div class="col-sm-4">
                          <label class="control-label">Select Parent Category </label>
                      </div>
                      <div class="col-sm-8" style="padding-bottom: 5px;">
                          <select class="form-control" name="parent_category" id="parent_category">
                              <option value="">Select Parent Category </option>
                          </select>
                      </div>
                  </div>

                  <div class="row mt text-center">
                      <div class="col-sm-12">
                          <button type="submit" class="btn btn-sm btn-success btn-flat mr-50" id="btn_submit_category_form">Create</button>
                          <span class="btn btn-sm btn-danger btn-flat mr-50" id="btn_cancle_category_form" data-dismiss="modal">Cancel</span>
                      </div>
                  </div>

                  <div class="row mt-3">
                      
                  </div>

              </div>
      </form>

  </div>

</div>

<div class="col-md-6" style="margin-top: -4px;">
    <div class="modal-body" style="padding-top:0px">

          <!-- Edit Menu Item Modal -->
         <form id="editMenuItem" novalidate="novalidate" style="display: none;">
          <div class="box-body" style="padding-top:0px !important;">
              <input type="hidden" name="menuitem_id" id="menuitem_id" value="0">
              <input type="hidden" name="form_action" id="form_action" value="create">
              <div class="row">
                  <div class="form-group col-sm-5">
                    <label class="control-label">Menu Item Name *</label>
                      <input type="text" class="form-control w" name="name_ed" id="name_ed" required="">
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="form-group col-sm-8">
                      <label class="control-label">Menu Item Image </label>
                      <input type="file" class="form-control" name="image_url_ed" id="image_url_ed" style="width: 230px;">
                      <p id="image_error_msg" style="color: red;display:none;">Select image (type png, jpg, jpeg)</p>
                  </div>
              </div>
              
              <div class="row">
                  <div class="form-group col-sm-5">
                      <label class="control-label">Item Description</label>
                      <textarea class="form-control" style = "height:71px; width: 476px;" name="description_ed" id="description_ed"  aria-invalid="false"></textarea>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="form-group col-sm-5" style="margin-top: -87px;">
                      <div class="show_img_ed"></div>
                  </div>
              </div>
              <div class="row" style="padding-bottom:8px;">
                  <div class="col-sm-4" style = "padding-left:15px;">
                      <label class="control-label">Category Type *</label>
                  </div>
                  <div class="col-sm-0" style = "padding-left:10px;">
                      <label class="food">
                        <input type="radio" name="category_type_ed" value="food" id="val_food_ed"/>
                        &nbsp;&nbsp;Food
                    </label>
                      <label class="beverages">
                          <input type="radio" name="category_type_ed" value="beverages" id="val_beverages_ed" />&nbsp;&nbsp;Beverages
                      </label>
                  </div>
              </div>
              <div class="row">
                  <div class="form-group col-sm-5">
                      <label class="control-label">Category *</label>
                     <select class="form-control w" name="category_id_ed" id="category_id_ed" required>
                            <option value="">Select Category</option>
                        </select>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="form-group col-sm-5">
                      <label class="control-label">Sub Category</label>
                    <select class="form-control w select2-accessible" name="sub_category_id_ed" id="sub_category_id_ed" tabindex="-1" aria-hidden="true">
                    <option value="">Select Sub Category</option>
                    </select>
                  </div>
              </div>

              <div class="row">
                  <div class="form-group col-sm-5">
                      <label class="control-label">Section *</label>
                     <select class="form-control w" name="section_ed" id="section_ed" required>
                            <option value="">Select Section</option>
                            @foreach($food_section as $key => $s_value)
                                <option value="{{ $s_value['id'] }}">{{ $s_value['name'] }}</option>
                            @endforeach
                        </select>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="form-group col-sm-5">
                      <label class="control-label">Additional Info:</label>
                    
                        <div>
                            <label for="jai_food">
                              <input type="checkbox" class="add_info_ed" name="add_info_ed[]" value="jain_food">&nbsp;&nbsp;Jain Food
                            </label>
                            <br>
                            <label for="resta_spl">
                              <input type="checkbox" class="add_info_ed" name="add_info_ed[]" value="rest_spl">&nbsp;&nbsp;Restaurant Special
                            </label>
                        </div>
                  </div>
              </div>              

              <div class="row">
                  <div class="form-group col-sm-5">
                      <label class="control-label">Food Category *</label>
                    <select class="form-control w" name="food_cat_ed"  id="food_cat_ed" required>
                            <option value="">Select food Category</option>
                            @foreach($food_cat as $key => $f_value)
                                <option value="{{ $f_value['id'] }}">{{ $f_value['name'] }}</option>
                            @endforeach
                        </select>
                  </div>
              </div>
              <div class="row" style = "padding-bottom:10px;">
                  <div class="col-sm-2 col-lg-2 col-md-4">
                      <label class="control-label" style="padding-left:1px;">Availability</label>
                  </div>
                  <div class="col-sm-10 col-lg-10 col-md-8">
                    <label for="home_delivery">
                      <input type="checkbox" name="availability_ed[]" value="home_delivery" class="availability_ed" id="home_delivery">&nbsp;&nbsp;Home Delivery
                    </label>
                  </div>
              </div>
              <div class="row" style = "padding-bottom:9px;">
                    <div class="col-md-2">
                        <label class="control-label" style="padding-left:1px;">Available Time</label>
                    </div>
                    <div class="col-md-9">
                    <div class="row">
                    <div class="col-md-5 bootstrap-timepicker">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker time_box" name="from_time_ed">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i> From
                            </div>
                        </div>
                    </div>
                    <div class="col-md-0"></div>
                    <div class="col-md-5 bootstrap-timepicker" wid>
                        <div class="input-group" style="margin-left: 10px;">
                            <input type="text" class="form-control timepicker time_box" name="to_time_ed">
                            <div class="input-group-addon" style="padding:0 26px 0 18px;">
                              <i class="fa fa-clock-o"></i> To  
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
              </div>
              <div class="row">
                  <div class="col-sm-2" style="padding-left:16px;">
                      <label class="control-label">Spice Level:</label>
                  </div>
                  <div class="col-sm-4">
                    <label class="">
                        <input type="radio" class="" name="spice_level_ed" value="non_spicy">
                        &nbsp;&nbsp;Non Spicy
                    </label>
                    <br>
                    <label class="">
                        <input type="radio" class="" name="spice_level_ed" value="medium">
                        &nbsp;&nbsp;Medium
                    </label>&nbsp;&nbsp;
                    <br>
                    <label class="">
                        <input type="radio" class="" name="spice_level_ed" value="spicy">
                        &nbsp;&nbsp;Spicy
                    </label>
                  </div>
                  <div class="form-group col-sm-5">
                      <label class="control-label">KOT group</label>
                      <input type="text" class="form-control w" name="kot_group_ed" id="kot_group_ed">
                  </div>
              </div>
              <div class="row">
                  <div class="form-group col-sm-5">
                      <label class="control-label">Price *</label>
                      <input type="number" class="form-control w" name="price_ed" id="price_ed" min="1" max="999999999999" required="">
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="form-group col-sm-5">
                      <label class="control-label">Charges(GST/VAT)</label>
                      <input type="text" class="form-control w" name="charges_gst_ed" id="charges_gst_ed">
                  </div>
              </div>
              <div class="row">
                  <div class="form-group col-sm-5">
                      <label class="control-label">Serving Time:</label>
                      <div class="input-group w">
                          <input type="text" class="form-control" name="serving_time_ed" id="serving_time_ed">
                          <div class="input-group-addon">
                            Mins
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="form-group col-sm-5">
                      <label class="control-label">Serves:</label>
                      <input type="text" class="form-control w" name="serves_ed" id="serves_ed">
                  </div>
              </div>
              <div class="row text-center" style="padding-left: 69px;">
                  <div class="col-sm-12">
                      <button type="submit" class="btn btn-sm btn-success btn-flat mr-50" id="menu_form_action">Create</button>
                      <span class="btn btn-sm btn-danger btn-flat mr-50" id="btn_cancle_menu_form" data-dismiss="modal">Cancel</span>
                  </div>
              </div>
          </div>
        </form>

    </div>
</div>

  </div>
</div>

</section>
@endsection 


@section('js')
<script>

    $('.timepicker').timepicker({
        showInputs: false
    });

    $('#btn_cancle_category_form').on('click', function(event){
        
        $('#SubmitCreateCategoryForm')[0].reset();
    });

    $('#btn_cancle_menu_form').on('click', function(event){

            $("input[name=menuitem_id]").val(0);
            $("input[name=form_action]").val("create");

            $("#menu_form_action").html("Create");
           
            $('#editMenuItem')[0].reset();
            $('#sub_category_id_ed').html("");
            $('.show_img_ed').html("");

            // $('#image_url_ed').attr("required","required");
        });


    $('#add_menuitem').on('click', function(event){
        
        $("#editMenuItem").show();

        $("input[name=menuitem_id]").val(0);
        $("input[name=form_action]").val("create");

        $("#menu_form_action").html("Create");

        $('#editMenuItem')[0].reset();
        $('#sub_category_id_ed').html("");
        $('.show_img_ed').html("");

        // $('#image_url_ed').attr("required","required");
    });


    $('#add_subcategory').on('click', function(event){
        $("#SubmitCreateCategoryForm").show();
        $('#category_name').focus();        
    });


    function getMenuItem(id){
            
        $.ajax({
            type: "GET",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            url: "{{ route('get_menuitem')}}",
            data: "id="+id,
            success: function (r) {

                create_edit(r);
            },
            error:function(e){
                console.log(e);
            }
        });
    } 

    function create_edit(data){
        
        $("#editMenuItem").show();

        $("input.add_info_ed").iCheck('uncheck');
        $("input.availability_ed").iCheck('uncheck');
        $("input.spice_level_ed").iCheck('uncheck'); 

        $("input[name=name_ed]").focus();
        $("#menu_form_action").html("Update");

        // $('#image_url_ed').removeAttr("required");

        $("#name_ed").removeClass("help-block");
        $("#name_ed").removeClass("error");
        $("#name_ed").removeAttr("aria-describedby");
        
        $("#image_url_ed").removeClass("help-block");
        $("#image_url_ed").removeClass("error");
        $("#image_url_ed").removeAttr("aria-describedby");
        
        // $("#description_ed").removeClass("help-block");
        // $("#description_ed").removeClass("error");
        // $("#description_ed").removeAttr("aria-describedby");
        
        $("#category_id_ed").removeClass("help-block");
        $("#category_id_ed").removeClass("error");
        $("#category_id_ed").removeAttr("aria-describedby");
        
        $("#section_ed").removeClass("help-block");
        $("#section_ed").removeClass("error");
        $("#section_ed").removeAttr("aria-describedby");
        
        $("#food_cat_ed").removeClass("help-block");
        $("#food_cat_ed").removeClass("error");
        $("#food_cat_ed").removeAttr("aria-describedby");
        
        $("#price_ed").removeClass("help-block");
        $("#price_ed").removeClass("error");        
        $("#price_ed").removeAttr("aria-describedby");        

        $("#name_ed-error").remove();
        $("#image_url_ed-error").remove();
        // $("#description_ed-error").remove();
        $("#category_id_ed-error").remove();
        $("#section_ed-error").remove();
        $("#food_cat_ed-error").remove();
        $("#price_ed-error").remove();
        
        if(data){
            var add_info = JSON.parse(data.menuitem.add_info);
            var availability = JSON.parse(data.menuitem.availability);

            $("input[name=name_ed]").val(data.menuitem.name);
            $("textarea[name=description_ed]").val(data.menuitem.description_ed);       
            $("input[name=category_type_ed][value=" + data.menuitem.category_type + "]").iCheck('check');
            
            if(data.menuitem.category_type == 'food'){
                $('input[value=food]').trigger( "click" );
            }
            if(data.menuitem.category_type == 'beverages'){
                $('input[value=beverages]').trigger( "click" );
            }

            $.each(add_info, function (index, info_value) {
                $("input.add_info_ed[value=" + info_value + "]").iCheck('check');
            })
            $.each(availability, function (index, ava_value) {
                $("input.availability_ed[value=" + ava_value + "]").iCheck('check');
            })
           
            if(data.menuitem.category_id != 0){
                setTimeout(function(){
                    $("select[name=category_id_ed]").val(data.menuitem.category_id).trigger('change');
                }, 1000);
            }
            if(data.menuitem.sub_category_id != 0){
                setTimeout(function(){
                    $("select[name=sub_category_id_ed]").val(data.menuitem.sub_category_id).trigger('change');
                }, 2000);
            }
            
            $("select[name=section_ed]").val(data.menuitem.section).trigger('change');
            $("select[name=food_cat_ed]").val(data.menuitem.food_cat).trigger('change');

            $("input[name=from_time_ed]").val(data.menuitem.from_time);
            $("input[name=to_time_ed]").val(data.menuitem.to_time);
            
            $("input[name=spice_level_ed][value=" + data.menuitem.spice_level + "]").iCheck('check');
            $("input[name=kot_group_ed]").val(data.menuitem.kot_group);
            $("input[name=price_ed]").val(data.menuitem.price);
            $("input[name=charges_gst_ed]").val(data.menuitem.charges_gst);
            
            $("input[name=serving_time_ed]").val(data.menuitem.serving_time);
            $("input[name=serves_ed]").val(data.menuitem.serves);
            
            $("input[name=menuitem_id]").val(data.menuitem.id);
            $("input[name=form_action]").val("update");
           
            out_resp_img = data.menuitem.image_url;   

            if(out_resp_img == null || out_resp_img == 'null' || out_resp_img == '')
            { 
                img_html = '<div class="img_wrapper"><img src="" alt="Menuitem Image"></div>';
            }
            else
            {
                menuitem_img = "{{URL::asset('/images')}}/" + out_resp_img;
                 
                img_html = '<div class="img_wrapper">'+
                                '<img src="'+ menuitem_img +'" alt="Menuitem Image">'+
                                '<a class="remove-btn" title="Click here to remove" onclick="remove_image(\'recipes\', \'image_url\', \''+data.menuitem.id+'\', \''+out_resp_img+'\', \'img_wrapper\');">X</a>'+
                            '</div>';
            }
           
            $('.show_img_ed').html(img_html);   
        } 
    }

    function remove_image(table_name, column_name, row_id, image_name, img_div) {
            $.ajax({
                type: "POST",
                url: "{{ route('remove_image')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "table_name": table_name,
                    "column_name": column_name,
                    "row_id": row_id,
                    "image_name": image_name
                },
                success: function (r) {
                        console.log(r);
                        Swal.fire('Success!','Image Deleted Successfully.','success');
                        $("."+img_div).remove();
                },
                error:function(e){
                    Swal.fire('Error!','Something went wrong.','error');
                    console.log(e);
                }
            });
    }

    function delete_menuitem( menuitem_id ) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            // confirmButtonColor: '#3085d6',
            // cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('delete_menuitem')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "menuitem_id": menuitem_id
                    },
                    success: function (r) {
                            
                            Swal.fire({
                                title: 'Deleted',
                                text: "Menu Item Deleted Successfully.",
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                window.location.reload();
                            });

                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            }
        });    
    }

    var imagesPreview = function(input, placeToInsertImagePreview) {
                if (input.files) {
                    var filesAmount = input.files.length;
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            let extension = ['png', 'jpg','jpeg'];
                            let base_64 = event.target.result.toLowerCase();
                            let flag = 0;
                            console.log(base_64.indexOf("png"));
                            console.log(base_64.indexOf("jpg"));
                            console.log(base_64.indexOf("jpeg"));
                            if (base_64.indexOf("data:image/png") !== -1) {
                                flag = 1;
                            }
                            if (base_64.indexOf("data:image/jpg") !== -1) {
                                flag = 1;
                            }
                            if (base_64.indexOf("data:image/jpeg") !== -1 ) {
                                flag = 1;
                            }
                            if (flag == 1) {
                                let img_html = '<div class="img_wrapper">'+
                                    '<img src="'+ event.target.result +'" alt=" Image">'+
                                    // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                '</div>';
                                $(placeToInsertImagePreview).append(img_html);
                            }
                            else
                            {
                                $('#image_url_ed').val('');
                                $('#image_error_msg').css('display','block');
                                setTimeout(function(){
                                    $('#image_error_msg').css('display','none');
                                },2000)
                                
                            }
                        }
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            };

    $("#editMenuItem").validate({
        submitHandler: function(form) {
            let formdata = new FormData($('#editMenuItem')[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }); 

            menuitem_id = $("#menuitem_id").val(); //alert(menuitem_id);
            form_action = $("#form_action").val(); //alert(form_action);

            if(menuitem_id ==0 && form_action == "create")
            {
                $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                url: "{{ route('create_menuitem')}}",
                data: formdata,
                success: function (r) {
                        Swal.fire({
                            title: 'success',
                            text: "Menu Item Added Successfully",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            window.location.reload();
                        });
                },
                error:function(e){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    })
                    console.log(e);
                }
            });
            }

            if(menuitem_id >0 && form_action == "update")
            {
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('edit_menuitem')}}",
                    data: formdata,
                    success: function (r) {                       
                        
                        Swal.fire({
                            title: 'success',
                            text: "Menu Item Updated Successfully",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            window.location.reload();
                        });
                       
                    },
                    error:function(e){
                        
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        });

                        console.log(e);
                    }
                });
            }
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        rules:
        {
               
        },
        messages:
        {   
            
        },
        ignore: []

    });

    $("body").on('change','input[name=image_url_ed]', function() {
        $('.show_img_ed').children().remove();
        imagesPreview(this, '.show_img_ed');
    });

    $(document).on('click','input[name="menuitem_status"]',function(){
                            
        let menuitem_status = $(this).attr("data-attr");
        let message = '';
        let menuitem_id = $(this).attr("menuitem-id");

        if ( menuitem_status == 0 ) {
            menuitem_status = 1;
            $(this).attr("data-attr", "1");
            message = 'Activated';
        } else {
            menuitem_status = 0;
            $(this).attr("data-attr", "0");
            message = 'Deactivated';
        }

        $.ajax({
            type: "POST",
            url: "{{ route('update_menuitem_status')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "menuitem_id": menuitem_id,
                "menuitem_status": menuitem_status
            },
            success: function (r) {
                    
                    $.notify("Menu Item "+ message +" Successfully", "success");
                    $('#item_table').DataTable().ajax.reload();
                    // $('#past_order_table').DataTable().ajax.reload();
            },
            error:function(e){
                Swal.fire(
                    'Error!',
                    'Something went wrong.',
                    'error'
                )
                console.log(e);
            }
        });
    });

    $('#val_food').on('change', function(event){
      // alert('abcd efg');
      console.log($("input[value=food]").val());

        let category_type = $("input[value=food]").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "get_recipe_cat_by_type",
            method: 'POST',
            data: {
                "category_type": category_type
            },
            success: function(result) {
                console.log(result);
                $('select[name=category_id_ed]').empty();               
                let html="<option value=''>Select Parent Category </option>";
                let category = result.category;
                if(category){
                    $.each(category, function( index, value ) {
                        html +="<option value="+value.id+">"+value.category_name+"</option>";
                    });
                }
                $('select[name=category_id_ed]').append(html);
            },
            error: function(e) {
                console.log(e)
            }
        });
    });

    $('#val_beverages').on('change', function(event){

            console.log($("input[value=beverages]").val());
            
            let category_type = $("input[value=beverages]").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "get_recipe_cat_by_type",
                method: 'POST',
                data: {
                    "category_type": category_type
                },
                success: function(result) {
                    console.log(result);
                    $('select[name=category_id]').empty();               
                    let html="<option value=''>Select Parent Category </option>";
                    let category = result.category;
                    if(category){
                        $.each(category, function( index, value ) {
                            html +="<option value="+value.id+">"+value.category_name+"</option>";
                        });
                    }
                    $('select[name=category_id]').append(html);
                },
                error: function(e) {
                    console.log(e)
                }
            });
    });

    $('#val_food_ed').on('change', function(event){
      // alert('abcd efg');
      console.log($("input[value=food]").val());

        let category_type = $("input[value=food]").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "get_recipe_cat_by_type",
            method: 'POST',
            data: {
                "category_type": category_type
            },
            success: function(result) {
                console.log(result);
                $('select[name=category_id_ed]').empty();
                $('select[name=sub_category_id_ed]').empty();
                let html="<option value=''>Select Category </option>";
                let category = result.category;
                if(category){
                    $.each(category, function( index, value ) {
                        html +="<option value="+value.id+">"+value.category_name+"</option>";
                    });
                }
                $('select[name=category_id_ed]').append(html);
            },
            error: function(e) {
                console.log(e)
            }
        });
    });

    $('#val_beverages_ed').on('change', function(event){

            console.log($("input[value=beverages]").val());
            
            let category_type = $("input[value=beverages]").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "get_recipe_cat_by_type",
                method: 'POST',
                data: {
                    "category_type": category_type
                },
                success: function(result) {
                    console.log(result);
                    $('select[name=category_id_ed]').empty();
                    $('select[name=sub_category_id_ed]').empty();
                    let html="<option value=''>Select Category </option>";
                    let category = result.category;
                    if(category){
                        $.each(category, function( index, value ) {
                            html +="<option value="+value.id+">"+value.category_name+"</option>";
                        });
                    }
                    $('select[name=category_id_ed]').append(html);
                },
                error: function(e) {
                    console.log(e)
                }
            });
    });

    $("select[name=category_id_ed]").on('change', function(){
                
            let cat_id = $(this).val();

            $('select[name=sub_category_id_ed]').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_recipe_subcat_by_cat_id')}}",
                data: {
                    "cat_id": cat_id
                },
                success: function (r) {
                    // console.log(r);
                    let sub_category = r.sub_category;

                    $('select[name=sub_category_id_ed]').append($('<option>', {value: '0', text: 'Select Sub Category'}));
                    if(sub_category.length > 0){
                        //$('select[name=sub_category_id_ed]').attr('required',true);
                        $.each( sub_category, function(index, value) {
                            // console.log( index + ' : ' + value.state_name );
                            $('select[name=sub_category_id_ed]').append($('<option>', {value: value.id, text: value.category_name}));
                        });
                    }
                    else{
                        $('select[name=sub_category_id_ed]').removeAttr('required');
                    }
                },
                error:function(e){
                    console.log(e);
                }
            });
    });

    $('input[type=radio][name=category_type]').on('change', function() {

        let category_type = $(this).val();

        $.ajax({
                url: "get_main_category",
                method: 'POST',
                data: {
                    "category_type": category_type,
                },
                headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                      },
                success: function(result) {
                       $('#parent_category').empty();
                        let html="<option value=''>Select Parent Category </option>";
                        let category = result.category;
                        if(category){
                            $.each(category, function( index, value ) {
                                html +="<option value="+value.id+">"+value.category_name+"</option>";
                            });
                        }
                        $('#parent_category').append(html);
                    },
                error: function(e) {
                    console.log(e)
                }
        });
    });
    

    $("#btn_submit_category_form").click(function(){
        var category_name = $('#category_name').val();
        var category_type = $('input[name="category_type"]:checked').val();
        let status = 1;
        if (category_name == '' || category_type == undefined  ) {
            status = 0;
        }

        if(status == 1)
        {
            $.ajax({
                url: "save_category_form",
                method: 'POST',
                data: $('#SubmitCreateCategoryForm').serialize(),
                headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                      },
                success: function(result) {

                    location.reload();
                },                   
                error: function(e) {
                    console.log(e)
                }
            });
        }
           
    });

</script>
@endsection

