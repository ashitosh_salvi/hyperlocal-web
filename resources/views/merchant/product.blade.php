@extends('layouts.applist')

@section('content')
@extends('layouts.header')



<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content-header" style="min-height: unset;">
        	<div class="row">
	            <div class="col-md-12">
	                <div class="box box-body">
	                    <div class="col-md-6">
	                        <h4><i class="fa fa-list"></i> &nbsp; Item List</h4>
	                    </div>
	                    <div class="col-md-6 text-right">
	                        <a class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Add New Item</a>
	                    </div>

	                </div>
	            </div>
            </div>
        </section>
        <section class="content">


<!-- TABLE: LATEST ORDERS -->

{{-- <div class="" style="margin-bottom: 60px">
    <button class="btn btn-sm btn-info btn-flat pull-right"  data-toggle="modal" data-target="#exampleModal">Add Item</button>
</div> --}}
<div class="box box-info">
    <div class="box-header with-border">
    <h3 class="box-title">Latest Items</h3>
    
    <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="item_table" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="#">OR9842</a></td>
                <td>Item abc</td>
                <td>120/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="23" id="cb_23" type="checkbox" checked="">
                    <label class="tgl-btn" for="cb_23"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <tr>
                <td><a href="#">OR1848</a></td>
                <td>Item xyz</td>
                <td>250/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="24" id="cb_24" type="checkbox" checked="">
                    <label class="tgl-btn" for="cb_24"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>
                {{-- <td><span class="label label-success">Active</span></td> --}}
                {{-- <td>Edit / Delete</td> --}}
            </tr>
            <tr>

                <td><a href="#">OR7429</a></td>
                <td>Item pqrs</td>
                <td>60/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="25" id="cb_25" type="checkbox" checked="">
                    <label class="tgl-btn" for="cb_25"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>

            </tr>
            <tr>
                <td><a href="#">OR7429</a></td>
                <td>lmr</td>
                <td>499/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="26" id="cb_26" type="checkbox" checked="">
                    <label class="tgl-btn" for="cb_26"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <tr>
                <td><a href="#">OR1848</a></td>
                <td>Item rst</td>
                <td>310/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="27" id="cb_27" type="checkbox" >
                    <label class="tgl-btn" for="cb_27"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <tr>
                <td><a href="#">OR7429</a></td>
                <td>Item spd</td>
                <td>185/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="28" id="cb_28" type="checkbox" checked="">
                    <label class="tgl-btn" for="cb_28"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <tr>
                <td><a href="#">OR9842</a></td>
                <td>Item lpl</td>
                <td>134/-</td>
                <td><input class="tgl tgl-ios tgl_checkbox" data-id="29" id="cb_29" type="checkbox">
                    <label class="tgl-btn" for="cb_29"></label>
                </td>
                <td>
                	<a title="View" class="view btn btn-sm btn-info"> <i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-sm mr5">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            </tbody>
            </table>
        </div>
    <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
    {{-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> --}}
    </div>
    <!-- /.box-footer -->
</div>
    <!-- /.box -->

</section>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Add Item</h4>
        </div>
        <div class="modal-body">
          <form>  
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="price" class="control-label">Price</label>
                        <input type="text" class="form-control" id="price">
                    </div>
                    <div class="form-group">
                        <label for="quantity" class="control-label">Quantity</label>
                        <input type="text" class="form-control" id="quantity">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">code</label>
                        <input type="text" class="form-control" id="message-text">
                    </div>           
                    <div class="form-group">
                        <label for="discount" class="control-label">Discount</label>
                        <input type="text" class="form-control" id="discount">
                    </div>
                    <div class="form-group">
                        <label  class="control-label">Type</label>
                        <select class="form-control">
                            <option>Select Type</option>
                            <option>VEG</option>
                            <option>NON-VEG</option>
                        </select>
                    </div>
                    
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="gallery-photo-add" class="control-label">Images</label>
                        <input type="file" class="form-control" multiple="" id="gallery-photo-add">
                    </div>
                    <div class="gallery" style="display: inline-block">
                    	{{-- <img src="http://52.66.16.21/blog/public/dist/img/default-img.png" style="width: 100%;padding: 10px;" class="user-image" alt="User Image"> --}}
                    	<img src="{{ asset('dist/img/default-img.png') }}" style="width: 100%;padding: 10px;" class="user-image" alt="User Image">
                    </div>      
                </div>
                
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>

@endsection 


@section('js')

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                	if(i==0){
	                    var reader = new FileReader();

	                    reader.onload = function(event) {
	                			$($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
	                    }

	                    reader.readAsDataURL(input.files[i]);
	                }else{
	                	var reader = new FileReader();

	                    reader.onload = function(event) {
	                			$($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
	                    }

	                    reader.readAsDataURL(input.files[i]);
	                }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
        	$('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
  $(function () {
    $("#item_table").DataTable();
  });

</script>

@endsection


