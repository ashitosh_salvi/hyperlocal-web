@extends('layouts.applist')

@section('content')
@include('layouts.outlet_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Outlet QR</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <!-- <a class="btn btn-success add_menuitem btn-flat mr-15" data-toggle="modal" data-target="#addMenuItemModal"><i class="fa fa-plus"></i> Generate New QR</a> -->
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadProductModal"><i class="fa fa-plus"></i> Bulk Upload</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    {{-- <h3 class="box-title">Manage Menu Item</h3> --}}
                    <!-- <a href="{{ url('/product_create') }}" class="btn btn-success text-right" style="float: right;margin-right: 5px;"><i class="fa fa-plus"></i> Add Menu Item</a> -->
                </div>
                
                <div class="box-body">

                    <div class="row mt-15">
                        <div class="col-sm-12">
                            @if(isset($qrData))
                        	{!! QrCode::size(250)->generate($_SERVER['SERVER_NAME']."/hladmin/restaurant/" . $qrData); !!}
                            <br><br><a href="{{route('downloadQR', ['qrData' => $qrData])}}" class="btn btn-success btn-flat">Download</a>
                            @endif

                        	@if(!empty($message))
                        	
                        		<p>{{ $message }}</p>
                        		
                        	@endif
                        </div>
                    </div>

                </div>

            </div>

        </section>
    
    </div>
</section>

@endsection 


@section('js')

<script src="{{ url('/') }}/jquery/scripting.js?v=2"></script>

@endsection

