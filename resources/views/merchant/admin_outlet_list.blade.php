@extends('layouts.applist')
@section('content')
@include('layouts.admin_header')

<style>
.pac-container{
    z-index: 9999!important;
}
#outlet_table input[type="checkbox"].tgl-ios + label{
    top: -7px;
}
</style>

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Outlet Management</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div class="table-responsive mt-15">
                            
                        <table id="outlet_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th>Outlet Name</th>
                                    <th>Outlet User Name</th>
                                    <th>Outlet Type</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
            </div>
        </section>

    </div>
</section>


@endsection 

@section('js')

<script>
$(document).ready(function(){
    $('#outlet_table').DataTable({
        "language": {
            "emptyTable": "No Record Found"
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
                "url": "{{ url('get_admin_outlet_list') }}",
                "dataType": "json",
                "type": "GET",
                "data":{ _token: "{{csrf_token()}}"}
            },
        "columns": [
            { "data": "outlet_name" },
            { "data": "outlet_user_name" },
            { "data": "outlet_type" },
            { "data": "action" }
        ],
        "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [ -1 ]
            }
        ],
    });

    $(document).on('click','input[name="outlet_status"]',function(){
        
        let outlet_status = $(this).attr("data-attr");
        let message = '';
        let outlet_id = $(this).attr("outlet-id");

        if ( outlet_status == 0 ) {
            outlet_status = 1;
            $(this).attr("data-attr", "1");
            message = 'Deactivated';
        } else {
            outlet_status = 0;
            $(this).attr("data-attr", "0");
            message = 'Activated';
        }

        $.ajax({
            type: "POST",
            url: "{{ route('admin_update_outlet_status')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "outlet_id": outlet_id,
                "outlet_status": outlet_status
            },
            success: function (r) {
                    
                $.notify("Outlet "+ message +" Successfully", "success");
                    
            },
            error:function(e){
                Swal.fire(
                        'Error!',
                        'Something went wrong.',
                        'error'
                    )
                console.log(e);
            }
        });

    });

});

</script>

@endsection
