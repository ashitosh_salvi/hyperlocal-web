@extends('layouts.applist')

@section('content')


<div id="login-form" style="height: 100vh;
background: url(https://ak.picdn.net/shutterstock/videos/20709541/thumb/1.jpg);
background-size: cover;" >
<div class="container">
    <div class="row">
        
        <div class="col-md-4 col-md-offset-4 text-center">
            <div class="login-title">
                <h3><span>RESET PIN</span></h3>
            </div>
            
            <div class="form-box" style="margin: 0 auto;">
                    <div class="input-group login-form">
                        <div class="login-div">
                            <input type="text" name="username" id="name" class="form-control" placeholder="Login ID" >
                            <input type="submit" name="submit" id="submit" class="form-control login-btn" value="GET OTP">
                        </div>
                        <div class="otp-div" style="display: none;">
                            <input type="text" name="password" id="password" class="form-control" placeholder="ENTER OTP" >
                            <input type="submit" name="submit" id="submit" class="form-control otp-btn" value="SUBMIT">
                        </div>
                        <div class="repin-div" style="display: none;">
                            <input type="text" name="password" id="password" class="form-control" placeholder="NEW PIN" >
                            <input type="text" name="password" id="password" class="form-control" placeholder="RE-ENTER PIN" >
                        
                            <div class="row">
                              <div class="col-xs-6">
                                <input type="submit" name="submit" id="submit" class="form-control" value="SET PIN">
                              </div>
                              <div class="col-xs-6">
                                <input type="submit" name="submit" id="submit" class="form-control" value="Cancel">
                              </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.login-btn').on('click',function(){
            $('.login-div').hide();
            $('.otp-div').show();
        });
        $('.otp-btn').on('click',function(){
            $('.otp-div').hide();
            $('.repin-div').show();
        });
    });
</script>
@endsection
