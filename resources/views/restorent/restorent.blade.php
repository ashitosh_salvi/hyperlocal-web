@extends('layouts.applist')
<style type="text/css">
	#main-content{
        margin: 0 auto;
        max-width: 100%;
        width: 768px;
        background: #fff;
	}
	.logo img{
		width: 200px;
	    text-align: center;
	    margin: 0 auto;
	    display: block;
	}
	.logo{
		display: block;
		margin-bottom: 15px;
	}
	.outlet-basic{
		border: 1px solid #ddd;
	    border-radius: 15px;
	    overflow: hidden;
	}
	.outlet-basic .rest-img{
		width: 100%;
    	height: 150px;
	}
	.rest-data{
		padding: 0 10px;
	}
	.rest-data h4{
		font-weight: 600;
	    color: #000;
	    font-size: 16px;
	    margin-bottom: 5px;
	}
	.rest-data span{
		color: #5f5f5f;
    	font-size: 13px;
    	padding-right: 15px;
	}
	.rest-data p{
		color: #5f5f5f;
	    font-size: 13px;
	}
	.rest-data span i {
	    font-size: 18px;
	    color: #fe5e00;
	}
	.rest-data p span{
	    color: #fe5e00;
	    font-size: 13px;
	    font-weight: 600;
	}
	.offers div{
	    display: inline-block;
	    padding: 6px 14px;
	    background: #fe5e00;
	    color: #fff;
        margin: 0 2px;
	    font-size: 14px;
	}
	.offers {
		display: block;
	    padding: 15px 0;
	}
	.hp_app_link{
		font-size: 14px;
		text-align: center;
	}
	.skin-green .sidebar-menu>li:hover>a, .skin-green .sidebar-menu>li.active>a{
		color: #fe5e00 !important;
	    background: #fff !important;
	    border-left-color: #fff !important;
	}
	.skin-green .sidebar a{
		color: #000000 !important;
	}
	.skin-green .sidebar-menu>li>.treeview-menu{
		background: #ffffff !important;
	}
	.skin-green .treeview-menu>li>a{
		color: #000000 !important;
	}
	.skin-green .treeview-menu>li.active>a, .skin-green .treeview-menu>li>a:hover{
		color: #fe5e00 !important;
	}
	.sidebar-menu .treeview-menu{
		padding-left: 15px !important;
	}
	li.treeview.active {
	    border-top: 1px solid #bababa !important;
	    border-bottom: 1px solid #bababa !important;
	}
	.sidebar-menu .treeview-menu>li img{
		height: 15px;
		padding-right: 10px;
	}
	.sidebar-menu .treeview-menu>li p{
		display: inline;
	    padding-left: 2px;
	    font-weight: 600;
	    white-space: initial !important;
	}
	.sidebar-menu .treeview-menu>li p span{
		padding-left: 30px;
	    font-size: 13px;
	    color: #fe5e00;
	}
	span.fa.fa-star.checked {
	    color: #fe5e00;
	}
	span.fa.fa-star {
	    color: #5f5f5f;
	}

	.sidebar-menu li>a>.fa-angle-left, .sidebar-menu li>a>.pull-right-container>.fa-angle-left{
		transform: rotate(-90deg);
	}

	.sidebar-menu li.active>a>.pull-right-container>.fa-angle-left {
		transform: rotate(90deg) !important;
	}

</style>
@section('content')
@if(!empty($outlet) && !empty($list))
<section id="main-content">
    <div class="" style="min-height: 785px; padding: 15px;">
    	<!-- page start-->
	    <a href="" class="logo">
	    	{{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-h.png"> --}}
	    	<img src="{{ asset('dist/img/hyper-logo-h.png') }}">
	    </a>
    	<section class="outlet-basic">
    		<div class="rest-img" style="background-image: url({{ asset('dist/img/demorestorent.jpg') }});">
    			<img style="display: none;" src="{{ asset('dist/img/demorestorent.jpg') }}">
    		</div>
    		<div class="rest-data">
    			<h4>{{$outlet->outlet_name}}</h4>
    			<div>
    				<span><i class="fa fa-map-marker" aria-hidden="true"></i> 29 km</span>
    				<span><i class="fa fa-clock-o" aria-hidden="true"></i> 50 Min {{$outlet->outlet_geoaddress}}</span>
    			</div>
    			<p><span>Open now - </span> {{$outlet->open_time}} - {{$outlet->close_time}}</p>
    			<p>
    				<span>Rating - {{$list['data']['user_rating']}}</span>
    				<span class="fa fa-star {{$list['data']['user_rating'] == 1.0 || $list['data']['user_rating'] == 2.0 || $list['data']['user_rating'] == 3.0 || $list['data']['user_rating'] == 4.0 || $list['data']['user_rating'] == 5.0 ? 'checked' : ''}}"></span>

					<span class="fa fa-star {{$list['data']['user_rating'] == 2.0 || $list['data']['user_rating'] == 3.0 || $list['data']['user_rating'] == 4.0 || $list['data']['user_rating'] == 5.0 ? 'checked' : ''}}"></span>

					<span class="fa fa-star {{$list['data']['user_rating'] == 3.0 || $list['data']['user_rating'] == 4.0 || $list['data']['user_rating'] == 5.0 ? 'checked' : ''}}"></span>

					<span class="fa fa-star {{$list['data']['user_rating'] == 4.0 || $list['data']['user_rating'] == 5.0 ? 'checked' : ''}}"></span>

					<span class="fa fa-star {{$list['data']['user_rating'] == 5.0 ? 'checked' : ''}}"></span>
    			</p>
    		</div>
    	</section>
    	<section class="offers">
    		<a href=""><p class="hp_app_link">Click here to download HyperLocal app..</p></a>
    		<div>FLAT 50%</div>
    		<div>FLAT 33%</div>
    		<div>FLAT 80%</div>
    	</section>
    	<section class="products">
    		
			<div class="sidebar">
				<ul class="sidebar-menu">
			        @foreach($list['data']['category'] as $category)
					<li class="treeview active">
			            <a href="#">
			              <span>{{$category['category_name']}}</span>
			              <span class="pull-right-container">
			                <i class="fa fa-angle-left pull-right"></i>
			              </span>
			            </a>
						@if(!empty($category['sub_category']))
			            <ul class="treeview-menu">
							@foreach($category['sub_category'] as $subCategory)
							<li class="treeview active">
								<a href="#">
									</i><span>{{$subCategory->category_name}}</span>
									<span class="pull-right-container">
						                <i class="fa fa-angle-left pull-right"></i>
						            </span>
								</a>
								@if(!empty($subCategory->items))
								<ul class="treeview-menu">
									@foreach($subCategory->items as $item)
									<li class="">
										<a href="">
											<img src="{{ asset('dist/img/veg.png') }}">
											<p>{{$item->name}}</br><span>₹ {{$item->price}}</span></p>
										</a>
									</li>
									@endforeach
							    </ul>
								@endif
							</li>
							@endforeach

							@foreach($category['main_category_items'] as $item)
							<li class="treeview">
								<a href="#">
									<img src="{{ asset('dist/img/veg.png') }}">
									<p>{{$item['name']}}</br><span>₹ {{$item['price']}}</span></p>
								</a>
							</li>
							@endforeach
					    </ul>
						@endif
				  	</li>
					@endforeach
				</ul>
			</div>
    	</section>
    </div>
</section>
@else
<section id="main-content">
    <div class="" style="padding: 15px;">
    	<!-- page start-->
	    <a href="" class="logo">
	    	{{-- <img src="http://52.66.16.21/blog/public/dist/img/hyper-logo-h.png"> --}}
	    	<img src="{{ asset('dist/img/hyper-logo-h.png') }}">
	    </a>
	    <p style="text-align: center; font-weight: 700; font-size: 20px;">Outlet is inactive.</p>
    </div>
</section>
@endif

@endsection 


@section('js')

<script src="{{ url('/') }}/jquery/scripting.js?v=2"></script>

@endsection

