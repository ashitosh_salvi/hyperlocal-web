@extends('layouts.applist')

@section('content')
@include('layouts.header')

<style>
    span.error {
        color: #ff0000;
    }
</style>

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Employee List</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#addEmployeeModal"><i class="fa fa-plus"></i> Add Employee</a>
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadEmployeeModal"><i class="fa fa-plus"></i> Bulk Upload</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
        <!-- Main content -->
        <section class="content">


            <div class="box ">

                <div class="box-header">
                    <h3 class="box-title">Manage Employee</h3>
                    <!-- <a href="{{ url('/employee_create') }}" class="btn btn-success text-right" style="float: right;margin-right: 5px;"><i class="fa fa-plus"></i> Add Employee</a> -->
                </div>
                
                <div class="box-body">

                    <!-- <div class="row">
                        <div class="col-sm-1">
                            <label class="control-label">User Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-1">
                            <label class="control-label">Status</label>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option>Authorised</option>
                                <option>Unauthorised</option>
                                <option>Deleted</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-15">
                        <div class="col-sm-1">
                            <label class="control-label">Role</label>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <a class="btn btn-sm btn-success btn-flat mr-50" href="#">Search</a>
                        <a class="btn btn-sm btn-danger btn-flat mr-50" href="#">Reset</a>
                        <a class="btn btn-sm btn-warning btn-flat mr-50" href="{{ url('/employee_create') }}" >Add New</a>
                        </div>
                    </div> -->

                    <div class="table-responsive mt-15">

                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    <th></th>
                                    <th>User Code</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Creator</th>
                                    <th>Created date</th>
                                    <th>Modifier</th>
                                    <th>Modified date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php for($i = 0; $i <= 30; $i++) { ?>    
                                <tr>
                                    <td><?=$i; ?></td>
                                    <td>SR12345</td>
                                    <td>Abc</td>
                                    <td>xyz</td>
                                    <td>red</td>
                                    <td>20-03-20</td>
                                    <td>XY1234</td>
                                    <td>24-03-20</td>
                                    <td>
                                        <input class="tgl tgl-ios tgl_checkbox" data-id="23" id="cb_23" type="checkbox" checked="">
                                        <label class="tgl-btn" for="cb_23"></label>
                                    </td>
                                    <td>
                                        <a title="View Employee" class="view btn btn-sm btn-info" data-toggle="modal" data-target="#viewEmployeeModal"> <i class="fa fa-eye"></i></a>
                                        <a title="Edit Employee" class="btn btn-warning btn-sm mr5" data-toggle="modal" data-target="#editEmployeeModal">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a title="Delete Employee" href="#" onclick="return confirm('are you sure to delete?')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a> 
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    <div class="row mt-15">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Print</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> Excel</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> PDF</button>
                        <button class="btn btn-sm btn-info btn-flat mr-50"> txt</button>
                        </div>
                    </div>
                    
                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="bulkUploadEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="bulkUploadEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="bulkUploadEmployeeModalLabel">Bulk Upload Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload Merchant Form -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label">Select file to upload: </label>
                    </div>
                    <div class="col-sm-10">
                        <input type="file" id="bulk_employee_files" class="form-control">
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>

<div class="modal fade" id="addEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="addEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="addEmployeeModalLabel">Add Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Add Employee Modal -->
        <form action="{{ route('create_employee') }}">  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">User ID</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="user_id" class="form-control">
                        @error('user_id')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                    
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label" >Employee ID</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="employee_id">
                        @error('employee_id')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">First Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text"  class="form-control" name="f_name">
                        @error('f_name')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="l_name">
                        @error('l_name')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Residential Address</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" name="address"></textarea>
                        @error('address')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">State</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="state">
                        @error('state')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Country</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="country">
                        @error('country')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Pin Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="pin_code">
                        @error('pin_code')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" name="email">
                        @error('email')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Contact #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="contact">
                        @error('contact')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Role</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="role">
                            <option value="">Select from below User Group Profile</option>
                            <option value="manager">Manager</option>
                            <option value="accountant">Accountant</option>
                            <option value="rider">Rider</option>
                        </select>
                        @error('role')
                            <span class="error">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Default PIN</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="pin">
                        @error('pin')
                            <span class="error">{{ $message }}</span>
                            
                        @enderror
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button id="create_employee" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancle</button>
                    </div>
                </div>
                <input type="hidden" name="emp_creation" value="true">
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="editEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editEmployeeModalLabel">Edit Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Edit Employee Modal -->
        <form>  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">User ID</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Employee ID</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">First Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Residential Address</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">State</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Country</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Pin Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Contact #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Role</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option value="">Select from below User Group Profile</option>
                            <option value="manager">Manager</option>
                            <option value="accountant">Accountant</option>
                            <option value="rider">Rider</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Default PIN</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button id="update_employee" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                        <button class="btn btn-sm btn-warning btn-flat mr-50" data-dismiss="modal">Cancle</button>
                    </div>
                </div>
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="viewEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="viewEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewEmployeeModalLabel">View Employee</h4>
        </div>
        <div class="modal-body">

        <!-- View Employee Modal -->
        <form>  
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-2">
                        <label class="control-label">User ID</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Employee ID</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">First Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Residential Address</label>
                    </div>
                    <div class="col-sm-5">
                        <textarea class="form-control" rows="3" disabled></textarea>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">State</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Country</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Pin Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Contact #</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Role</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" disabled>
                            <option value="">Select from below User Group Profile</option>
                            <option value="manager">Manager</option>
                            <option value="accountant">Accountant</option>
                            <option value="rider">Rider</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-15">
                    <div class="col-sm-2">
                        <label class="control-label">Default PIN</label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" disabled>
                    </div>
                </div>

                <!-- <div class="row mt-15 text-center">
                    <div class="col-sm-12">
                        <button id="create_employee" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                        <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancle</button>
                    </div>
                </div> -->
                
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

@endsection 


@section('js')

<script>

    @if($errors->any())
        $('#addEmployeeModal').modal('show');
    @endif



    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
  $(function () {
    $("#item_table").DataTable();
  });



</script>

@endsection


