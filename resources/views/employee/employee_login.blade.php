@extends('layouts.applist')

@section('content')


<div id="login-form" style="height: 100vh;
background: url(https://ak.picdn.net/shutterstock/videos/20709541/thumb/1.jpg);
background-size: cover;" >
<div class="container">
    <div class="row">
        
        <div class="col-md-4 col-md-offset-4 text-center">
            <div class="login-title">
                <h3><span>Employee Login</span></h3>
            </div>
            
            <div class="form-box" style="margin: 0 auto;">
                <div class="caption">
                    <h4>Sign in to start your session</h4>
                </div>
                    <div class="input-group login-form">
                        <input type="text" name="username" id="name" class="form-control" placeholder="Login ID" >
                        <input type="password" name="password" id="password" class="form-control" placeholder="PIN" >
                        <p></p>
                        <div class="row">
                          <div class="col-xs-4">
                            <input type="submit" name="submit" id="submit" class="form-control" value="Login">
                          </div>
                          <div class="col-xs-4">
                            <input type="submit" name="submit" id="submit" class="form-control" value="Cancel">
                          </div>
                          <div class="col-xs-4">
                            <input type="submit" name="submit" id="submit" class="form-control" value="Reset">
                          </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection
