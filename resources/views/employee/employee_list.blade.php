@extends('layouts.applist')
<style type="text/css">
    .gallery img{
        height: 90px;
    }
</style>
@section('content')
@include('layouts.merchant_header')
<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content-header" style="min-height: unset;">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-body">
                    <div class="col-md-6">
                        <h4><i class="fa fa-list"></i> &nbsp; Employee Management</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-success add_employee btn-flat mr-15" data-toggle="modal" data-target="#addEmployeeModal"><i class="fa fa-plus"></i> Add Employee</a>
                        {{-- <button class="btn btn-info btn-flat mr-15"> Print</button>
                        <button class="btn btn-info btn-flat mr-15"> Excel</button>
                        <button class="btn btn-info btn-flat mr-15"> PDF</button>
                        <button class="btn btn-info btn-flat mr-15"> txt</button> --}}
                        {{-- <a class="btn btn-success" data-toggle="modal" data-target="#bulkUploadEmployeeModal"><i class="fa fa-plus"></i> Bulk Upload Employee</a> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>

        <!-- Main content -->
        <section class="content">


            <div class="box ">
                <div class="box-header">
                </div>
                <div class="box-body">

                    <div class="table-responsive mt-15">
                            
                        <table id="item_table" class="table table-bordered table-striped">

                            <thead>
                                <tr>
                                    
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>State #</th>
                                    <th>City</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>

            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="addEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="addEmployeeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="addEmployeeModalLabel">Add Employee</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='createEmployee'>  
                        
                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">First Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control first_name" name="first_name" value="{{ old('first_name') }}" required>
                                @error('first_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Last Name *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                                @error('last_name')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Residential Address *</label>
                            </div>
                            <div class="col-sm-5">
                                <textarea name="address" required class="form-control" rows="3">{{ old('address') }}</textarea>
                                @error('address')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Country</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="country" required value="INDIA">
                                @error('country')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">State *</label>
                            </div>
                            <div class="col-sm-3">
                                <!-- <input type="text" class="form-control" name="state" required value="{{ old('state') }}"> -->
                                <select class="form-control" id="state" name="state" required value="{{ old('state') }}">
                                    <option value="">Select State</option>
                                </select>
                                @error('state')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">City *</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="city" name="city" value="{{ old('city') }}" required >
                                    <option value="">Select City</option>
                                </select>
                                @error('city')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Pin Code *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="pin_code" name="pin_code" value="{{ old('pin_code') }}" required>
                                @error('pin_code')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                        	<div class="col-sm-2">
                                <label class="control-label">Email *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Contact *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="contact" value="{{ old('contact') }}" required> 
                                @error('contact')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Role *</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="role" name="role" required>
                                    
                                </select>
                                @error('role')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <label class="control-label">Default PIN *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="emp_pin" required>
                                @error('emp_pin')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-sm-2">
                                <label class="control-label">Employee Photo *</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="file" class="form--control emp_photo" name="image_url" required>
                                @error('image_url')
                                    <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-sm-3 gallery"></div>
                        </div>

                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="create_employee" type="submit" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 

                    
                    </form>
               
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal fade" id="viewEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="viewEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="viewEmployeeModalLabel">View Employee</h4>
        </div>
        <div class="modal-body">

        <!-- View EmployeeForm -->
          <form>  
            
            <div class="box-body">

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="first_name_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Residential Address</label>
                        </div>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="3" name="address_view" disabled></textarea>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Country</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="country_view" value="INDIA" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">State</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="state_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">City</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="city_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Pin Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="pin_code_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Contact</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="contact_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Role</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="role_view" disabled>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Default PIN</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="emp_pin_view" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Employee Photo</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="file" class="form-control" disabled>
                        </div>
                        <div class="col-sm-3">
                            <div id="view_emp_img"></div>
                            
                        </div>
                    </div>
                    
                    <div class="row mt-15">
                        
                    </div>

                </div>

          </form>

        </div>
        
      </div>
    </div>
</div>

<div class="modal fade" id="editEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="editEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="editEmployeeModalLabel">Edit Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Edit EmployeeForm -->
          <form id="editEmployee">  
            
            <div class="box-body">

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="first_name_ed" required>
                            <input type="hidden" name="employee_id_edit">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Residential Address</label>
                        </div>
                        <div class="col-sm-5">
                            <textarea class="form-control" name="address_ed" rows="3" required></textarea>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Country</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="country_ed" value="INDIA" required>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">State</label>
                        </div>
                        <div class="col-sm-3">
                            <!-- <input type="text" class="form-control" name="state_ed" required> -->
                            <select class="form-control" id="state_ed" name="state_ed" required >
                                <option value="">Select State</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">City</label>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="city_ed" name="city_ed" required >
                                <option value="">Select City</option>
                            </select>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Pin Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="pin_code_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="email_ed" required>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Contact</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="contact_ed" required>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Role</label>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="role_ed" name="role_ed" required>
                                
                            </select>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Default PIN</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="emp_pin_ed" disabled>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-2">
                            <label class="control-label">Employee Photo</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="file" class="form--control edit_emp_photo" name="ed_emp_img">
                        </div>
                        <div class="col-sm-3">
                            <div id="ed_emp_img"></div>
                            {{-- <img class="img-responsive" id="ed_emp_img" alt="Employee Image" src=""> --}}
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="update_employee" class="btn btn-sm btn-success btn-flat mr-50">Update</button>
                            <button class="btn btn-sm btn-warning btn-flat mr-50" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    <div class="row mt-15">
                        
                    </div>
                </div>
          </form>
        </div>
        
      </div>
    </div>
</div>

<div class="modal fade" id="bulkUploadEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="bulkUploadEmployeeModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="bulkUploadEmployeeModalLabel">Bulk Upload Employee</h4>
        </div>
        <div class="modal-body">

        <!-- Bulk Upload EmployeeForm -->
        <form>  

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label">Select file to upload: </label>
                    </div>
                    <div class="col-sm-10">
                        <input type="file" id="bulk_employee_files" class="form-control">
                    </div>
                </div>
            </div>

        </form>

        </div>

    </div>
    </div>
</div>

<div class="modal fade" id="getEmployeeOutletsModal"  role="dialog" aria-labelledby="getEmployeeOutletsModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="getEmployeeOutletsModalLabel">Assign Outlet</h4>
            </div>
            <div class="modal-body">
                
                <div class="box-body">
                    <form id='assignEmployeeOutlet'>  
                        @csrf
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Employee Name</label>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" id="employee_name"></label>
                            </div>
                        </div>
                        <div class="row mt-15">
                            <div class="col-sm-6">
                                <label class="control-label">Select Outlet</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="hidden" name="employee_id" id="employee_id">
                                <select class="form-control select2" id="outlet" name="outlet[]" multiple>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-15 text-center">
                            <div class="col-sm-12">
                                <button id="assign_rider" type="submit" class="btn btn-sm btn-warning btn-flat mr-50">Assign</button>
                                <button class="btn btn-sm btn-danger btn-flat mr-50" data-dismiss="modal">Cancel</button>
                            </div>
                        </div> 
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection 


@section('js')


<script>

    let fileTypes = ['jpg', 'jpeg', 'png']; // array for valid image preview

    $(function() {    
    // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            if (input.files) {
                var filesAmount = input.files.length;
                var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
                
                if(isSuccess){
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            let img_html = '<div class="img_wrapper">'+
                                    '<img src="'+ event.target.result +'" alt="Employee Pic">'+
                                    // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                                '</div>';
                            $(placeToInsertImagePreview).append(img_html);
                        }
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }
        };

        $('.emp_photo').on('change', function() {
            $('.img_wrapper').remove();
            imagesPreview(this, 'div.gallery');
        });

        $('.edit_emp_photo').on('change', function() {
            $('.img_wrapper').remove();
            imagesPreview(this, '#ed_emp_img');
        });

    });
    
    function getEmployee(id){
        $('#editEmployee').trigger("reset");
        $("#editEmployee").find('.has-error').removeClass("has-error");
        $("#editEmployee").find('.has-success').removeClass("has-success");
        $('#editEmployee').find('.form-control-feedback').remove();
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        $.ajax({
                    type: "GET",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('get_employee')}}",
                    data: "id="+id,
                    success: function (r) {
                        console.log(r);
                        create_edit(r);
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
    }

    function getEmployeeView(id){
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        $.ajax({
                    type: "GET",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('get_employee')}}",
                    data: "id="+id,
                    success: function (r) {
                        console.log(r);
                        create_view(r);
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
    }

    function create_view(data) { 

        $("div.view_js_add").remove();

        if(data){

            $("input[name=user_name_view]").val(data.employee.user_name);
            $("input[name=first_name_view]").val(data.employee.first_name);
            $("input[name=last_name_view]").val(data.employee.last_name);
            $("textarea[name=address_view]").val(data.employee.address);
            $("input[name=password_view]").val(data.employee.password);
            $("input[name=state_view]").val(data.employee.state);
            $("input[name=city_view]").val(data.employee.city);
            $("input[name=country_view]").val(data.employee.country);
            $("input[name=pin_code_view]").val(data.employee.pin_code);
            $("input[name=email_view]").val(data.employee.email);
            $("input[name=contact_view]").val(data.employee.contact);
            $("input[name=emp_pin_view]").val(data.employee.pin);
            $.ajax({
                type: "GET",
                url: "{{ route('get_employee_role_by_id')}}",
                data: {
                    'role_id': data.employee.role
                },
                success: function (r) {
                    // console.log(r);
                    $("input[name=role_view]").val(r);
                },
                error:function(e){
                    console.log(e);
                }
            });

            let emp_img = "{{URL::asset('/images')}}/" + data.employee.image_url;
            let img_html = '<div class="img_wrapper">'+
                            '<img src="'+ emp_img +'" alt="alt="Emp Image" title="' + data.employee.image_url + '">'+
                            // '<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                        '</div>';
            $("#view_emp_img").html(img_html);



        }
        $('#viewEmployeeModal').modal('show');

    }

    function create_edit(data){

        // To remove other outlet boxes
        $("div.js_add").remove();

        if(data){

            $("input[name=user_name_ed]").val(data.employee.user_name);
            $("input[name=employee_id_edit]").val(data.employee.id);
            $("input[name=first_name_ed]").val(data.employee.first_name);
            $("input[name=last_name_ed]").val(data.employee.last_name);
            $("textarea[name=address_ed]").val(data.employee.address);
            $("input[name=password_ed]").val(data.employee.password);
            
            // $("input[name=state_ed]").val(data.employee.state);
            $.ajax({
                type: "GET",
                url: "{{ route('get_state_by_name')}}",
                data: {
                    "state_name": data.employee.state
                },
                success: function (r) {
                    let state_id = r.state[0].state_name;
                    // console.log( typeof( state_id ) + " : " + state_id );
                    $("#state_ed").val( state_id );
                    $('#city_ed').empty();

                    // Get All Cities for selected state
                    $.ajax({
                        type: "GET",
                        url: "{{ route('get_all_cities_by_state_id')}}",
                        data: {
                            "state_id": state_id
                        },
                        success: function (r) {
                            // console.log(r);
                            let cities = r.cities;
                            $('#city_ed').append($('<option>', {value: '', text: 'Select City'}));
                            $.each( cities, function(index, value) {
                                // console.log( index + ' : ' + value.state_name );
                                $('#city_ed').append($('<option>', {value: value.city_name, text: value.city_name}));
                            });

                            // Get Selected city for employee
                            $.ajax({
                                type: "GET",
                                url: "{{ route('get_city_by_name')}}",
                                data: {
                                    "city_name": data.employee.city
                                },
                                success: function (r) {
                                    let city_id = r.city[0].city_name;
                                    $("#city_ed").val( city_id );
                                },
                                error:function(e){
                                    console.log(e);
                                }
                            });
                                
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                    
                },
                error:function(e){
                    console.log(e);
                }
            });

            $("#state_ed").on('change', function(){
            
                let state = $(this).val();

                $('#city_ed').empty();
                
                $.ajax({
                    type: "GET",
                    url: "{{ route('get_all_cities_by_state_id')}}",
                    data: {
                        "state_id": state
                    },
                    success: function (r) {
                        // console.log(r);
                        let cities = r.cities;
                        $('#city_ed').append($('<option>', {value: '', text: 'Select City'}));
                        $.each( cities, function(index, value) {
                            // console.log( index + ' : ' + value.state_name );
                            $('#city_ed').append($('<option>', {value: value.city_name, text: value.city_name}));
                        });
                    },
                    error:function(e){
                        console.log(e);
                    }
                });

            });

            $("#role_ed").val(data.employee.role);

            $("input[name=country_ed]").val(data.employee.country);
            $("input[name=pin_code_ed]").val(data.employee.pin_code);
            $("input[name=email_ed]").val(data.employee.email);
            $("input[name=contact_ed]").val(data.employee.contact);
            $("input[name=emp_pin_ed]").val(data.employee.pin);
            
            if(data.employee.image_url){
                let emp_img = "{{URL::asset('/images')}}/" + data.employee.image_url;
                let img_html = '<div class="img_wrapper">'+
                                '<img src="'+ emp_img +'"  title="' + data.employee.image_url + '">'+
                                //'<a class="remove-btn" title="Click here to remove" onclick="remove_image(2);">X</a>'+
                            '</div>';
                $("#ed_emp_img").html(img_html);
                // $("#ed_emp_img").attr("src", emp_img);
            }
        }

        $('#editEmployeeModal').modal('show');
    }

    function delete_employee( employee_id ) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('delete_employee')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "employee_id": employee_id
                    },
                    success: function (r) {
                        console.log(r);
                        Swal.fire(
                                'Success!',
                                'Employee Deleted Successfully.',
                                'success'
                            );
                        // $.notify("Employee Deleted Successfully", "success");
                        $('#item_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            )
                        console.log(e);
                    }
                });
            }
        });

        if( confirm('Are sure to delete the record?') ) {
            

        }

    }    

    function getEmployeeOutlet(id, name) {
        $("#outlet").empty();
        $("#getEmployeeOutletsModal").find("#employee_id").val(id);
        $("#getEmployeeOutletsModal").find("#employee_name").text(name);

        $.ajax({
            type: "GET",
            url: "{{ route('get_all_outlets')}}",
            success: function (r) {
                console.log(r);
                // $('#outlet').append($('<option>', {value: '', text: 'Select Outlet'}));
                $.each( r, function(index, value) {
                    $("#outlet").append($('<option>', {value: value.id, text: value.outlet_name}));
                });

                let outlet_ids = $("#outlet_list"+id).attr('data-array');
                if ( outlet_ids != "" ) {
                    console.log( JSON.parse( outlet_ids ) );
                    $("#getEmployeeOutletsModal").find(".select2").val( JSON.parse( outlet_ids ) );
                    $("#getEmployeeOutletsModal").find(".select2").trigger('change');
                }
                
            },
            error:function(e){
                console.log(e);
            }
        });

        $("#getEmployeeOutletsModal").modal('show');
    }

    $(window).load(function(){
            
            // Update Employee Status
    
            $('input[name="employee_status"]').click(function(){
                    
                    // console.log( $(this).attr("data-attr") );
                    
                    let employee_status = $(this).attr("data-attr");
                    let message = '';
                    let employee_id = $(this).attr("employee-id");
            
                    if ( employee_status == 0 ) {
                        employee_status = 1;
                        $(this).attr("data-attr", "1");
                        message = 'Activated';
                    } else {
                        employee_status = 0;
                        $(this).attr("data-attr", "0");
                        message = 'Deactivated';
                    }
            
                    $.ajax({
                        type: "POST",
                        url: "{{ route('update_employee_status')}}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "employee_id": employee_id,
                            "employee_status": employee_status
                        },
                        success: function (r) {
                                
                            $.notify("Employee "+ message +" Successfully", "success");
                                
                        },
                        error:function(e){
                            Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            )
                            console.log(e);
                        }
                    });
            
                });
    
        });

  $(document).ready(function(){

        
        $('.add_employee').on('click',function(){
            $('#createEmployee').trigger("reset");
            $("input[name=country]").val('INDIA');
            $("#createEmployee").find('span.help-block').remove();
            $("#createEmployee").find('.help-block').removeClass("help-block");
            $("#createEmployee").find('.error').removeClass("error");
            $("#createEmployee").find('.has-success').removeClass("has-success");
            $('#createEmployee').find('.form-control-feedback').remove();
            $('.img_wrapper').remove();
        });

		$('#item_table').DataTable({
            "language": {
                "emptyTable": "No Record Found"
            },
		    "processing": true,
		    "serverSide": true,
		    "ajax":{
		             "url": "{{ url('get_employee_list') }}",
		             "dataType": "json",
		             "type": "GET",
		             "data":{ _token: "{{csrf_token()}}"}
		           },
		    "columns": [
		        { "data": "first_name" },
		        { "data": "last_name" },
		        { "data": "email" },
		        { "data": "role" },
		        { "data": "state" },
		        { "data": "city" },
		        { "data": "status" },
		        { "data": "action" }
		    ],
		    "aoColumnDefs": [
		    {
		       bSortable: false,
		       aTargets: [ -1 ]
		    }],
		});

        $.ajax({
            type: "GET",
            url: "{{ route('get_all_states')}}",
            success: function (r) {
                // console.log(r);
                let states = r.states;
                $.each( states, function(index, value) {
                    // console.log( index + ' : ' + value.state_name );
                    $('#state').append($('<option>', {value: value.state_name, text: value.state_name}));
                    $('#state_ed').append($('<option>', {value: value.state_name, text: value.state_name}));
                });
            },
            error:function(e){
                console.log(e);
            }
        });

        $("#state").on('change', function(){
            
            let state = $(this).val();

            $('#city').empty();
            
            $.ajax({
                type: "GET",
                url: "{{ route('get_all_cities_by_state_id')}}",
                data: {
                    "state_id": state
                },
                success: function (r) {
                    // console.log(r);
                    let cities = r.cities;
                    $('#city').append($('<option>', {value: '', text: 'Select City'}));
                    $.each( cities, function(index, value) {
                        // console.log( index + ' : ' + value.state_name );
                        $('#city').append($('<option>', {value: value.city_name, text: value.city_name}));
                    });
                },
                error:function(e){
                    console.log(e);
                }
            });

        });

        $("#createEmployee").validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#createEmployee')[0]);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('create_employee')}}",
                    data: formdata,
                    success: function (r) {
                            console.log(r);
                            $('#addEmployeeModal').modal('hide');
                            Swal.fire(
                                        'Success!',
                                        'Employee Added Successfully.',
                                        'success'
                                    );
                            // $.notify("Employee Added Successfully", "success");
                            $('#item_table').DataTable().ajax.reload();
                            // window.location.reload();
                    },
                    error:function(e){
                        Swal.fire(
                            'Error!',
                            'Something went wrong.',
                            'error'
                        )
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            rules:
            {
                first_name: {
                    required:true,
                    regex: /^[a-z]+$/i
                },
                last_name: {
                    required:true,
                    regex: /^[a-z]+$/i
                },
                pin_code: {
                    required:true,
                    regex: /^[0-9]{1,6}$/g
                },
                contact: {
                    required:true,
                    regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
                },
                emp_pin: {
                    required:true,
                    regex: /^[0-9]{1,6}$/g
                },
                image_url: {
                    required:true,
                    extension: 'jpg|jpeg|png|gif',
                    filesize:200000,
                }
            },
            messages:
            {	
                first_name: {
                    regex: 'Enter only alphabets'
                },
                last_name: {
                    regex: 'Enter only alphabets'
                },
                pin_code: {
                    regex: 'Please enter correct pin code'
                },
                contact: {
                    regex: 'Please add valid mobile number'
                },
                emp_pin: {
                    regex: 'Please enter valid PIN'
                }
            },
            ignore: []

        });


        $("#cancle_employee").on('click', function(){

            window.history.back();

        });

        $.validator.addMethod(
            "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please check your input."
        );
        
        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than 200KB');

        $("#editEmployee").validate({
            submitHandler: function(form) {
                    let formdata = new FormData($('#editEmployee')[0]);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        url: "{{ route('edit_employee')}}",
                        data: formdata,
                        success: function (r) {
                                console.log(r);
                                $('#editEmployeeModal').modal('hide');
                                Swal.fire(
                                        'Success!',
                                        'Employee Updated Successfully.',
                                        'success'
                                    );
                                // $.notify("Employee Updated Successfully", "success");
                                $('#item_table').DataTable().ajax.reload();
                        },
                        error:function(e){
                            Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            )
                            console.log(e);
                        }
                    });
                },
                errorElement: 'span',
                errorClass: 'help-block error',
                rules:
                {
                    // email_ed: {
                    //     regex: '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
                    // },
                    first_name_ed: {
                        required:true,
                        regex: /^[a-z]+$/i
                    },
                    last_name_ed: {
                        required:true,
                        regex: /^[a-z]+$/i
                    },
                    pin_code_ed: {
                        required:true,
                        regex: /^[0-9]{1,6}$/g
                    },
                    contact_ed: {
                        required:true,
                        regex: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/g
                    },
                    emp_pin_ed: {
                        required:true,
                        regex: /^[0-9]{1,6}$/g
                    },
                    image_url: {
                        required:true,
                        extension: 'jpg|jpeg|png|gif',
                        filesize:200000,
                    }   
                },
                messages:
                {	
                    // email_ed: {
                    //     regex: 'Please enter valid email address'
                    // },
                    first_name_ed: {
                        regex: 'Enter only alphabets'
                    },
                    last_name_ed: {
                        regex: 'Enter only alphabets'
                    },
                    pin_code_ed: {
                        regex: 'Please enter correct pin code'
                    },
                    contact_ed: {
                        regex: 'Please add valid mobile number'
                    },
                    emp_pin_ed: {
                        regex: 'Please enter valid PIN'
                    }
                },
                ignore: []
        });

        $.ajax({
            type: "GET",
            url: "{{ route('get_employee_roles')}}",
            success: function (r) {
                // console.log(r);
                let roles = r.employee_roles;
                $('#role').append($('<option>', {value: "", text: "Select Role"}));
                $.each( roles, function(index, value) {
                    // console.log( index + ' : ' + value.state_name );
                    $('#role').append($('<option>', {value: value.id, text: value.name}));
                    $('#role_ed').append($('<option>', {value: value.id, text: value.name}));

                });
            },
            error:function(e){
                console.log(e);
            }
        });

        $(".select2").select2({
            placeholder: 'Select Outlets',
            allowClear: true
        });

        // Assign Rider to merchant
        $( "#assignEmployeeOutlet" ).validate({
            submitHandler: function(form) {
                let formdata = new FormData($('#assignEmployeeOutlet')[0]);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    url: "{{ route('assign_employee_outlet')}}",
                    data: formdata,
                    success: function (r) {
                            console.log(r);
                            $("#getEmployeeOutletsModal").modal("hide");
                            Swal.fire(
                                'Success!',
                                'Employee Mapped Successfully.',
                                'success'
                            );
                            $('#item_table').DataTable().ajax.reload();
                    },
                    error:function(e){
                        Swal.fire(
                                'Error!',
                                'Something went wrong.',
                                'error'
                            );
                        console.log(e);
                    }
                });
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            rules:
            {},
            messages:
            {},
            ignore: []
        });
        
        // $(".dataTables_filter input").css({ "width": "250px" });

    });

    $('#addEmployeeModal').on('shown.bs.modal', function () {
        $('.first_name').focus();
    })

</script>

@endsection
