@extends('layouts.applist')

@section('content')
@include('layouts.header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">


            {{-- <div class="box ">
                
                <div class="box-body">
                    
                    <div class="row" >
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat "> Search</button>
                        <button class="btn btn-sm btn-info btn-flat "> Reset</button>
                        </div>
                    </div>
                </div>
            </div> --}}

            
            <!-- <div class="box">
                
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <input type="radio" name="r1x">
                            <label class="control-label">Mearchant</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="radio" name="r1x">
                            <label class="control-label">Vendor</label>
                        </div>
                    </div>
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Merchant Name</label>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>A</option>
                                <option>D</option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Vendor Name</label>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>A</option>
                                <option>D</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div> -->


            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">Add Employee</h3>
                </div>

                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">User ID</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Employee ID</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Last Name</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2">
                            <label class="control-label">Residential Address</label>
                        </div>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">State</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Country</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Pin Code</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Contact #</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Role</label>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option value="">Select from below User Group Profile</option>
                                <option value="manager">Manager</option>
                                <option value="accountant">Accountant</option>
                                <option value="rider">Rider</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-15">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <label class="control-label">Default PIN</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-15 text-center">
                        <div class="col-sm-12">
                            <button id="create_employee" class="btn btn-sm btn-success btn-flat mr-50">Create</button>
                            <button id="cancle_employee" class="btn btn-sm btn-danger btn-flat mr-50">Cancle</button>
                        </div>
                    </div>
                    
                </div>
            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">New message</h4>
        </div>
        <div class="modal-body">
          <form>  
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="price" class="control-label">Price</label>
                        <input type="text" class="form-control" id="price">
                    </div>
                    <div class="form-group">
                        <label for="quantity" class="control-label">Quantity</label>
                        <input type="text" class="form-control" id="quantity">
                    </div>
                    <div class="form-group">
                        <label for="gallery-photo-add" class="control-label">Images</label>
                        <input type="file" class="form-control" multiple="" id="gallery-photo-add">
                    </div>
                    
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="message-text" class="control-label">code</label>
                        <input type="text" class="form-control" id="message-text">
                    </div>           
                    <div class="form-group">
                        <label for="discount" class="control-label">Discount</label>
                        <input type="text" class="form-control" id="discount">
                    </div>
                    <div class="form-group">
                        <label  class="control-label">Type</label>
                        <select class="form-control">
                            <option>Select Type</option>
                            <option>VEG</option>
                            <option>NON-VEG</option>
                        </select>
                    </div>        
                </div>
                <div class="gallery" style="display: inline-block"></div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>

@endsection 


@section('js')

<script>

    $(document).ready(function(){

        $("#cancle_employee").on('click', function(){

            window.history.back();

        });

    });

</script>

@endsection


