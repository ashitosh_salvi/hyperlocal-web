@extends('layouts.applist')

<header class="header white-bg skin-green sidebar-mini">
    <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo" style="
        background-color: #008d4c;
        color: #fff;
        border-bottom: 0 solid transparent;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Employee</b>Dashboard</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Employee</b> Dashboard</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" style="background-color: #00a65a;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
            {{-- <li><a href="https://codecanyon.net/item/admin-lite-powerfull-php-admin-panel/21136710?ref=codeglamour" class="btn-danger"><i class="fa fa-download"></i>&nbsp; Buy Now</a></li> --}}
            {{-- <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <li><!-- start message -->
                        <a href="#">
                        <div class="pull-left">
                            <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                        </a>
                    </li>
                    <!-- end message -->
                    <li>
                        <a href="#">
                        <div class="pull-left">
                            <img src="http://52.66.16.21/blog/public/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <div class="pull-left">
                            <img src="http://52.66.16.21/blog/public/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <div class="pull-left">
                            <img src="http://52.66.16.21/blog/public/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <div class="pull-left">
                            <img src="http://52.66.16.21/blog/public/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                        </a>
                    </li>
                    </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
            </li> --}}
            <!-- Notifications: style can be found in dropdown.less -->
            {{-- <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <li>
                        <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                        page and may cause design problems
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                    </li>
                    </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
                </ul>
            </li> --}}
            <!-- Tasks: style can be found in dropdown.less -->
            {{-- <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-flag-o"></i>
                <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                    <li><!-- Task item -->
                        <a href="#">
                        <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">20% Complete</span>
                            </div>
                        </div>
                        </a>
                    </li>
                    <!-- end task item -->
                    <li><!-- Task item -->
                        <a href="#">
                        <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">40% Complete</span>
                            </div>
                        </div>
                        </a>
                    </li>
                    <!-- end task item -->
                    <li><!-- Task item -->
                        <a href="#">
                        <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">60% Complete</span>
                            </div>
                        </div>
                        </a>
                    </li>
                    <!-- end task item -->
                    <li><!-- Task item -->
                        <a href="#">
                        <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">80% Complete</span>
                            </div>
                        </div>
                        </a>
                    </li>
                    <!-- end task item -->
                    </ul>
                </li>
                <li class="footer">
                    <a href="#">View all tasks</a>
                </li>
                </ul>
            </li> --}}
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> --}}
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">username</span>
                </a>
                <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                    {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}
                    <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
  
                    <p>
                    CodeGlamour
                    </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                    <div class="row">
                    <div class="col-xs-4 text-center">
                        <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                        <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                        <a href="#">Friends</a>
                    </div>
                    </div>
                    <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-right">
                    <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                    <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">profile</a>
                    </div>
                </li>
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
                <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
            </li>
            </ul>
        </div>
        </nav>
    </header>
  </header>
  
  <aside class="main-sidebar skin-green sidebar-mini" style="background-color: #222d32;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          {{-- <img src="http://52.66.16.21/blog/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}
          <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>name</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li id="profile" class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Settle Bills</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="profile"><a href="profile"><i class="fa fa-circle-o"></i> profile</a></li>
          </ul>
        </li>
      </ul>
  
      <ul class="sidebar-menu">
        <li id="ui" class="treeview">
            <a href="#">
              <i class="fa fa-laptop"></i> <span>Viwe Pending Bills</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="general"><a href="ui/general"><i class="fa fa-circle-o"></i> General</a></li>
              <li id="widgets"><a href="ui/widgets"><i class="fa fa-circle-o"></i> Widgets</a></li>
              <li id="icons"><a href="ui/icons"><i class="fa fa-circle-o"></i> Icons</a></li>
              <li id="buttons"><a href="ui/buttons"><i class="fa fa-circle-o"></i> Buttons</a></li>
              <li id="sliders"><a href="ui/sliders"><i class="fa fa-circle-o"></i> Sliders</a></li>
              <li id="timeline"><a href="ui/timeline"><i class="fa fa-circle-o"></i> Timeline</a></li>
              <li id="modals"><a href="ui/modals"><i class="fa fa-circle-o"></i> Modals</a></li>
            </ul>
          </li>
      </ul> 
      
      <ul class="sidebar-menu">
        <li id="forms" class="treeview">
            <a href="#">
              <i class="fa fa-edit"></i> <span>Cash Drawer</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="gen"><a href=""><i class="fa fa-circle-o"></i> General</a></li>
              <li id="advanced"><a href=""><i class="fa fa-circle-o"></i> Advance</a></li>
              <li id="editors"><a href=""><i class="fa fa-circle-o"></i> Editors</a></li>
            </ul>
        </li>
      </ul> 
      
      <ul class="sidebar-menu">
        <li id="examples" class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Day close</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            {{-- <li id="invoice"><a href=""><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li id="prof"><a href="<?= base_url('example/profile'); ?>"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li id="login"><a target="_blank" href="<?= base_url('example/login'); ?>"><i class="fa fa-circle-o"></i> Login</a></li>
            <li id="register"><a target="_blank" href="<?= base_url('example/register'); ?>"><i class="fa fa-circle-o"></i> Register</a></li>
            <li id="lockscreen"><a target="_blank" href="<?= base_url('example/lockscreen'); ?>"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li id="404-error"><a href="<?= base_url('example/error404'); ?>"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li id="500-error"><a href="<?= base_url('example/errro500'); ?>"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li id="blank-page"><a href="<?= base_url('example/blank'); ?>"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li id="pace"><a href="<?= base_url('example/pace'); ?>"><i class="fa fa-circle-o"></i> Pace Page</a></li> --}}
          </ul>
        </li>
      </ul>
  
      {{-- <ul class="sidebar-menu">
        <li id="charts" class="treeview">
            <a href="#">
              <i class="fa fa-pie-chart"></i>
              <span>Items</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="chartjs"><a href="charts/chartjs"><i class="fa fa-circle-o"></i> ChartJS</a></li>
              <li id="morris"><a href="charts/morris"><i class="fa fa-circle-o"></i> Morris</a></li>
              <li id="flot"><a href=""><i class="fa fa-circle-o"></i> Flot</a></li>
              <li id="inline"><a href=""><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
      </ul> --}}
      
      <ul class="sidebar-menu">  
        <li id="calender">
          <a href="calendar">
            <i class="fa fa-calendar"></i> <span>Settings</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
      </ul>
        
      {{-- <ul class="sidebar-menu">
        <li id="mailbox" class="treeview">
          <a href="">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="inbox">
              <a href="mailbox/inbox">Inbox
                <span class="pull-right-container">
                  <span class="label label-primary pull-right">13</span>
                </span>
              </a>
            </li>
            <li id="compose"><a href="mailbox/compose">Compose</a></li>
            <li id="read"><a href="mailbox/read_mail">Read</a></li>
          </ul>
        </li>
      </ul> --}}
  
      {{-- <ul class="sidebar-menu">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
      </ul> --}}
  
    </section>
    <!-- /.sidebar -->
  </aside>

