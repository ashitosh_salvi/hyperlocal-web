@extends('layouts.applist')

@section('content')
@include('layouts.header')



<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">


            <div class="box ">
                
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Employee Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-sm-12">
                        <button class="btn btn-sm btn-info btn-flat "> Search</button>
                        <button class="btn btn-sm btn-info btn-flat "> Reset</button>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="box">
                <div class="box-header"><h3 class="box-title">Employee</h3></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Employee Name</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Emloyee Code</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Department</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Education details</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label">Designation</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="box ">                
                        <div class="box-header"><h3 class="box-title">Earnings : </h3></div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">Basic</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> HRA </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> DA</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">PF </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Other allowances </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Total Earnings</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">                
                        <div class="box-header"><h3 class="box-title">Deduction : </h3></div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">PF</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> Professional Tax </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label"> Any Fund</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Income Tax </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px">
                                <div class="col-sm-4">
                                    <label class="control-label">Total Deductions </label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                              
                        </div>
                    </div>
                </div>

            </div>

            <div class="box">
                <div class="box-body">
                    <div class="row" style="margin-top:15px">
                        <div class="col-sm-2">
                            <label class="control-label">Total salary</label>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control">
                        </div>
                        <button class="btn btn-info "> Update</button> 
                        <button class="btn btn-info "> Reset</button>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">New message</h4>
        </div>
        <div class="modal-body">
          <form>  
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="price" class="control-label">Price</label>
                        <input type="text" class="form-control" id="price">
                    </div>
                    <div class="form-group">
                        <label for="quantity" class="control-label">Quantity</label>
                        <input type="text" class="form-control" id="quantity">
                    </div>
                    <div class="form-group">
                        <label for="gallery-photo-add" class="control-label">Images</label>
                        <input type="file" class="form-control" multiple="" id="gallery-photo-add">
                    </div>
                    
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="message-text" class="control-label">code</label>
                        <input type="text" class="form-control" id="message-text">
                    </div>           
                    <div class="form-group">
                        <label for="discount" class="control-label">Discount</label>
                        <input type="text" class="form-control" id="discount">
                    </div>
                    <div class="form-group">
                        <label  class="control-label">Type</label>
                        <select class="form-control">
                            <option>Select Type</option>
                            <option>VEG</option>
                            <option>NON-VEG</option>
                        </select>
                    </div>        
                </div>
                <div class="gallery" style="display: inline-block"></div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>

@endsection 


@section('js')

<script>
    

</script>

@endsection


