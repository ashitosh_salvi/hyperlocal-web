@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->

    <section class="content">


        <div class="box ">
            <div class="box-header">
            </div>
            <div class="box-body">

                @if($flag === 1)
                <div class="table-responsive mt-15">
                        
                    <table id="item_table" class="table table-bordered table-striped">

                        <thead>
                            <tr>
                                
                                <th>Order No</th>
                                <th>Amount</th>
                                <th>Delivery </th>
                                <th>Discount</th>
                                <th>total Amt</th>
                                <th>Final Amt</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($orders as $order) 
                            <tr>
                                <td>{{ $order->order_id }}</td>
                                <td>{{ $order->amount }}</td>
                                <td>{{ $order->delivery_charges }}</td>
                                <td>{{ $order->discount }}</td>
                                <td>{{ $order->total_amount }}</td>
                                <td>{{ $order->final_amount }}</td>
                                <td> <a href="{{ route('transfer.view',['id' => $order->id ]) }}" class="btn btn-success btn-flat" style="font-size:10px "> Transfer </a> </td>
                            </tr>
                            @endforeach
                        </tbody> 
                        
                    </table>
                </div>
                <!-- /.table-responsive -->
                {{ $orders->links() }}
                @else
                    
                    <form method="POST" action="{{ route('transfer_amount') }}">
                    <div class="col-sm-3 box-body">
                        @csrf
                        <div class="">
                            <label class="control-label">Order Number</label>
                        <input type="text" class="form-control" name="" value="{{ $order->order_id }}"  readonly>
                            
                        </div>

                        <div class="mt-15">
                            <label class="control-label">Amount</label>
                            <input type="text" class="form-control" name="first_name" value="{{ $order->final_amount }}" readonly>
                            
                        </div>

                        <div class="mt-15">
                            <label class="control-label">Amount to be Transfer</label>
                            <input type="number" min="0" class="floatNumberField form-control"  name="amount" value="{{ old('amount') }}" required>
                            @error('amount')
                                <span class="error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mt-15">
                            <input type="submit">
                        </div>
                    </div>
                    
                    </form>


                @endif
            </div>

        </div>

    </section>
    
    </div>
</section>

@endsection 


@section('js')
<script>
    $(document).ready(function () {
        $(".floatNumberField").change(function() {
            $(this).val(parseFloat($(this).val()).toFixed(2));
        });

        @if( session()->has('success') )
            $.notify(" Amount Transfer Successfully ", "success");
        @endif
    });
</script>
    

@endsection
