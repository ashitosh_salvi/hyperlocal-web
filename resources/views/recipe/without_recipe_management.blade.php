@extends('layouts.applist')

@section('content')
@include('layouts.admin_header')

<section id="main-content">
    <div class="content-wrapper" style="min-height: 785px; padding: 15px;">
    <!-- page start-->
        <!-- Main content -->
        <section class="content">

          <div class="row" style="margin-top: 80px;">
            <div class="col-md-offset-2 col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Categories</span>
                  <span class="info-box-number">15</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Menu Items</span>
                  <span class="info-box-number">73</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          </div>
          <div class="row">
            <!-- /.col -->
            <div class="col-md-offset-2 col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Manage Menu Items</span>
                  <span class="info-box-number">3,648</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            
          </div>
        </section>     
    </div>
</section>


@endsection 


@section('js')

<script>
    $(function() {
        // Multiple images 
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    if(i==0){
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:100%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);   
                        }

                        reader.readAsDataURL(input.files[i]);
                    }else{
                        var reader = new FileReader();

                        reader.onload = function(event) {
                                $($.parseHTML('<img style="width:33%;padding:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);         
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            $('div.gallery').empty();
            imagesPreview(this, 'div.gallery');
        });
    });
    
  $(function () {
    $("#item_table").DataTable();
  });

  $(document).ready(function(){

        $("#add_outlet").click(function(e){

            // console.log('working');
            
            $(".outlet_details_box:first").clone().addClass("mt-15").appendTo("#outlet_details");

            e.preventDefault();

        });

        $("#edit_add_outlet").click(function(e){

            // console.log('working');
            
            $(".edit_outlet_details_box:first").clone().addClass("mt-15").appendTo("#edit_outlet_details");

            e.preventDefault();

        });

        $("#cancle_merchant").on('click', function(){

            window.history.back();

        });

    });

</script>

@endsection
