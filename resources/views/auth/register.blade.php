@extends('layouts.applist')

@section('content')

<div id="login-form" style="height: 100vh;
    background: url(https://ak.picdn.net/shutterstock/videos/20709541/thumb/1.jpg);
    background-size: cover;" >
    
    <div class="container">

        <div class="row">
            
            <div class="col-md-4 col-md-offset-4 text-center">
                <div class="login-title">
                    <h3><span>Merchant Register</span></h3>
                </div>

                <div class="form-box" style="margin: 0 auto;">
                    <div class="caption">
                        <h4>Sign in to start your session</h4>
                    </div>

                    <div class="input-group login-form">
                        <div class="login-div">
        
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">

                                    <input type="submit" class="form-control" name="submit" id="submit" value="{{ __('Register') }}">

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
