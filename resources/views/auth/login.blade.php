@extends('layouts.applist')



@section('content')


{{-- style="height: 100vh;background: url(https://ak.picdn.net/shutterstock/videos/20709541/thumb/1.jpg);background-size: cover;" --}}
<div id="login-form"  >
    
    <div class="container">

        <div class="row">
            
            <div class="col-md-4 col-md-offset-4 text-center">

                <div class="form-box" >
                    <div class="login-title">
                        <img src="{{ asset('dist/img/hyper-logo-v.png') }}" style="width: 100%;">
                    </div>
                    <div class="caption">
                        <h4>Sign in to start your session</h4>
                    </div>

                    <div class="input-group login-form">
                        <div class="login-div">
        
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Username">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <input type="submit" class="form-control" name="submit" id="submit" value="{{ __('Login') }}">

                            </form>

                        </div>
                        <div class="otp-div" style="display: none;">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter OTP" >
                            <input type="submit" class="form-control" value="Proceed">
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
	// $(document).ready(function() {
	// 	$('.login-btn').on('click',function(){
	// 		$('.login-div').hide();
	// 		$('.otp-div').show();
	// 	});
	// });
</script>

@endsection

